#
# Copyright (C) 2009 Gerber van der Graaf <gerber_graaf@users.sf.net>
#-------------------------------------------------------------------------------
# License
#   This file is part of Libgpiv.
#
#   Libgpiv is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2 of the License, or (at your
#   option) any later version.
#
#   Libgpiv is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Libgpiv; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
#-------------------------------------------------------------------------------

# - Find dc1394
#
# This module looks for dc1394 support and defines the following values
#  DC1394_FOUND        TRUE if dc1394 has been found
#  DC1394_INCLUDE_DIRS the include path for dc1394
#  DC1394_LIBRARIES    the libraries to link against

find_path(DC1394_INCLUDE_DIR libdc1394/dc1394.h)

find_library(DC1394_LIBRARY
  NAMES dc1394
  )

set(DC1394_INCLUDE_DIRS ${DC1394_INCLUDE_DIR})
set(DC1394_LIBRARIES ${DC1394_LIBRARY})

mark_as_advanced(
  DC1394_INCLUDE_DIR
  DC1394_LIBRARY
  )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Dc1394
  DEFAULT_MSG
  DC1394_INCLUDE_DIR
  DC1394_LIBRARY
  )

# ------------------------- vim: set sw=2 sts=2 et: --------------- end-of-file
