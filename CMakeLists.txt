cmake_minimum_required( VERSION 2.6 )
project( libgpiv )
set (LIBGPIV_MAJOR_VERSION 0)
set (LIBGPIV_MINOR_VERSION 6)
set (LIBGPIV_MICRO_VERSION 1)
set (LIBGPIV_SO_VERSION 4)

# set up custom cmake module path
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMake/Modules)
include(${CMAKE_SOURCE_DIR}/CMake/GpivUtilities.cmake)

set( CMAKE_VERBOSE_MAKEFILE on )

find_package( FFTW REQUIRED )
include_directories( ${FFTW_INCLUDE_DIRS} )
set( LIBS ${LIBS} ${FFTW_LIBRARIES} )

find_package( GSL )

find_package( HDF5 REQUIRED )
include_directories( ${HDF5_INCLUDE_DIRS} )
set( LIBS ${LIBS} ${HDF5_LIBRARIES} )

find_package( Glib REQUIRED )
include_directories( ${Glib_INCLUDE_DIRS} )
set( LIBS ${LIBS} ${Glib_LIBRARIES} )

find_package( PNG )
include_directories( ${PNG_INCLUDE_DIR} )
set( LIBS ${LIBS} ${PNG_LIBRARIES} )

# find_package( netpbm )

include_directories( ${MPI_INCLUDE_PATH} )
set( LIBS ${LIBS} ${MPI_LIBRARY} )
option( LGPIV_ENABLE_MPI "Parallel processing on distributed memory systems" OFF )
if(LGPIV_ENABLE_MPI)
  add_definitions(-DENABLE_MPI)
  find_package( MPI )
endif()
set(_PAR_ERROR
  "ENABLE_@PKG@ is true, but @PKG@ cannot be found.\n"
  "If you have @PKG@ installed on your system, edit the variables\n"
  "@PKG@_INCLUDE_DIR and @PKG@_LIBRARY in the cache, else disable the\n"
  "setting USE_@PKG@."
  )
if(LGPIV_ENABLE_MPI AND NOT MPI_FOUND)
  string(REPLACE "@PKG@" "MPI" MPI_ERROR "${_PAR_ERROR}")
  message(SEND_ERROR "${MPI_ERROR}")
endif()

option( LGPIV_ENABLE_OMP "Multi-core parallel processing on shared memory systems" ON )
if(LGPIV_ENABLE_OMP)
  add_definitions(-DENABLE_OMP)
  find_package( OpenMP REQUIRED )
  add_definitions(${OpenMP_C_FLAGS})
  set( LIBS ${LIBS} ${OpenMP_C_FLAGS} )
#  message( "link OMP with ${OpenMP_C_FLAGS}" )
endif()

option( LGPIV_ENABLE_CAM "IEEE 1394 (Firewire) camera for recording" OFF )
if(LGPIV_ENABLE_CAM)
  add_definitions(-DENABLE_CAM)
  find_package( Raw1394 REQUIRED )
  find_package( Dc1394 REQUIRED )
endif()

option( LGPIV_ENABLE_TRIG "Triggering of laser puls with camera. Needs RTAI-enabled kernel" OFF )
if(LGPIV_ENABLE_TRIG)
  add_definitions(-DENABLE_TRIG)
endif()

# check_include_files( fftw.h hdf5.h )

#file (GLOB SRC lib/*.c)
# or:
include( files.cmake )

# message ("src dir = ${PROJECT_SOURCE_DIR}" )
include_directories( ${PROJECT_SOURCE_DIR}/include )
add_library( gpiv SHARED ${SRC} )
target_link_libraries( gpiv ${LIBS} )
install_targets(/lib gpiv )

add_library( gpiv-static STATIC ${SRC} )
target_link_libraries( gpiv-static ${LIBS} )
install_targets(/lib gpiv-static )

set_target_properties(
  gpiv
  gpiv-static
  PROPERTIES
  VERSION ${LIBGPIV_MAJOR_VERSION}.${LIBGPIV_MINOR_VERSION}.${LIBGPIV_MICRO_VERSION}
  SOVERSION ${LIBGPIV_SO_VERSION}
  )

## file (GLOB MAIN_HDR include/*.h include/gpiv/*.h)
install (FILES include/gpiv.h DESTINATION include)
file (GLOB HDRS include/gpiv/*.h)
install (FILES ${HDRS} DESTINATION include/gpiv)


# API documentation
option(LGPIV_ENABLE_DOXYGEN_DOCS "Doxygen API documentation" OFF)
doxygen_format( HTML ON )
doxygen_format( XML OFF )
doxygen_format( LATEX OFF )
if ( LGPIV_ENABLE_DOXYGEN_DOCS )
  find_package( Doxygen )
  if ( DOXYGEN_FOUND )
    configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY )
    configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/doxygen_header.html.in ${CMAKE_CURRENT_BINARY_DIR}/doxygen_header.html @ONLY )
    configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/doxygen_footer.html.in ${CMAKE_CURRENT_BINARY_DIR}/doxygen_footer.html @ONLY )
    configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/doxygen_stylesheet.css.in ${CMAKE_CURRENT_BINARY_DIR}/doxygen_stylesheet.css @ONLY )
    add_custom_target( doc
      ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      COMMENT "Generating API documentation with Doxygen" VERBATIM
      )

  add_custom_target( apidoc ${DOXYGEN_CMD_STRING} )

  set( LGPIV_INSTALL_DOC_PATH share/doc/${PROJECT_NAME}/doc CACHE STRING "directory where docs will be installed")
  mark_as_advanced ( LGPIV_INSTALL_DOC_PATH )
#  message("install doc path: "${LGPIV_INSTALL_DOC_PATH})
#  message( "html = "${ENABLE_DOXYGEN_DOCS_HTML} )
  if ( ENABLE_DOXYGEN_DOCS_HTML )
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/html/ DESTINATION ${LGPIV_INSTALL_DOC_PATH}/html/ COMPONENT apidoc )
  endif( ENABLE_DOXYGEN_DOCS_HTML )

#  message( "xml = "${ENABLE_DOXYGEN_DOCS_XML} )
  if ( ENABLE_DOXYGEN_DOCS_XML )
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/xml/ DESTINATION ${LGPIV_INSTALL_DOC_PATH}/xml/ COMPONENT apidoc )
  endif( ENABLE_DOXYGEN_DOCS_XML )

#  message( "latex = "${ENABLE_DOXYGEN_DOCS_LATEX} )
  if ( ENABLE_DOXYGEN_DOCS_LATEX )
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/latex/ DESTINATION ${LGPIV_INSTALL_DOC_PATH}/latex COMPONENT apidoc )
  endif( ENABLE_DOXYGEN_DOCS_LATEX )

  endif(DOXYGEN_FOUND)
endif(LGPIV_ENABLE_DOXYGEN_DOCS)

# install configuration file gpiv.conf
option( LGPIV_INSTALL_SYSCONFIG "Install system wide configuration (needs admin privileges)" ON )
if( LGPIV_INSTALL_SYSCONFIG )
  set( SYSCONFIG_INSTALL_DIR /etc/ )
  install( FILES ${CMAKE_CURRENT_SOURCE_DIR}/gpiv.conf DESTINATION ${SYSCONFIG_INSTALL_DIR} )
endif( LGPIV_INSTALL_SYSCONFIG )

# uninstall target
configure_file(
  "${CMAKE_SOURCE_DIR}/CMake/Modules/cmake_uninstall.cmake.in"
  "${CMAKE_BINARY_DIR}/cmake_uninstall.cmake"
  @ONLY
  )

add_custom_target(uninstall
  "${CMAKE_COMMAND}" -P "${CMAKE_BINARY_DIR}/cmake_uninstall.cmake"
  )

option( LGPIV_ENABLE_DEBUG "Debug mode for verbose behaviour" OFF )
if(LGPIV_ENABLE_DEBUG)
  add_definitions(-DDEBUG)
endif()
