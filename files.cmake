set (SRC
lib/pivdata.c
lib/scalardata.c
lib/bindata.c
lib/image.c
lib/imagepar.c
lib/covariance.c
lib/fastfourier.c

lib/genpar.c
lib/imageprocpar.c
lib/pivpar.c
lib/validpar.c
lib/postpar.c

#lib/cam.c
#lib/cam_par.c

lib/imageproc.c
lib/imageproc_deform.c
lib/piv_proc.c
lib/valid_proc.c
lib/post_proc.c

lib/io.c
lib/io_hdf.c

#lib/trig.c
#lib/trig_par.c

lib/utils.c
lib/utils_alloc.c
lib/my_utils.c

)
