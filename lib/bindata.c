/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2011 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------*/

#include <gpiv.h>


static GpivBinData *
read_bindata_from_file                  (FILE                   *fp
					 );

static GpivBinData *
read_bindata_from_stdin                 (void
					 );

static int
count_bindata                           (FILE                   *fp
					 );


void
gpiv_null_bindata			(GpivBinData		*bin_data
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Sets all elements of bin_data structure to NULL
 *
 * INPUTS:
 *      bin_data:      Input Bin data structure
 *
 * OUTPUTS:
 *      bin_data:      Output Bin data structure
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    bin_data->count = NULL;
    bin_data->bound = NULL;
    bin_data->centre = NULL;
}



GpivBinData *
gpiv_alloc_bindata			(const guint		nbins
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for GpivScalarData
 *
 * INPUTS:
 *      bin_data:       nbins of GpivScalarData
 *
 * OUTPUTS:
 *      bin_data:       count, bound, centre of GpivScalarData
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{ 
    GpivBinData *bin_data = g_new0 (GpivBinData, 1);
    gint i;

    
    if (nbins == 0) {
        gpiv_warning ("GPIV_ALLOC_BINDATA: nbins = %d", nbins);
        return NULL;
    }
    bin_data->nbins = nbins;

    /* BUGFIX */
    /*     g_warning("gpiv_alloc_bindata:: 4/4 nbins = %d", bin_data->nbins); */
    bin_data->count = gpiv_ivector (bin_data->nbins);
    bin_data->bound = gpiv_vector (bin_data->nbins);
    bin_data->centre = gpiv_vector (bin_data->nbins);
        
    for (i = 0; i < bin_data->nbins; i++) {
        bin_data->count[i] = 0;
        bin_data->bound[i] = 0.0;
        bin_data->centre[i] = 0.0;
    }

    bin_data->min = 914.6e9;
    bin_data->max = -914.6e9;
    bin_data->comment = NULL;


    return bin_data;
}      



void
gpiv_free_bindata			(GpivBinData		*bin_data
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for GpivScalarData
 *
 * INPUTS:
 *      bin_data:       data of bins
 *
 * OUTPUTS:
 *      bin_data:       NULL pointer to count, bound, centre
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{

    g_return_if_fail (bin_data->count != NULL); /* gpiv_error ("bin_data->count not allocated"); */
    g_return_if_fail (bin_data->bound != NULL); /* gpiv_error ("bin_data->bound not allocated"); */
    g_return_if_fail (bin_data->centre != NULL); /* gpiv_error ("bin_data->centre not allocated"); */
    
    /*     g_warning ("gpiv_free_bindata:: 0 nbins = %d", bin_data->nbins); */
    gpiv_free_ivector (bin_data->count);

    gpiv_free_vector (bin_data->centre);
    gpiv_free_vector (bin_data->bound);
    
    bin_data->bound = NULL;
    bin_data->centre = NULL;
    bin_data->count = NULL;
}



GpivBinData *
gpiv_read_histo                         (FILE *fp
                                         )
/*-----------------------------------------------------------------------------
 * Wrapper for gpiv_read_bindata
 */
{
    GpivBinData *histo_data = NULL;
    histo_data = gpiv_read_bindata (fp);
    return histo_data;
}



gchar *
gpiv_print_histo			(FILE			*fp, 
					 GpivBinData		*bin_data, 
					 const gboolean		free
					 )
/*-----------------------------------------------------------------------------
 * Wrapper for gpiv_write_bindata
 */
{
  gchar *err_msg = NULL;


  if ((err_msg = gpiv_write_bindata (fp, bin_data, free)) != NULL) {
    return(err_msg);
  }


  return err_msg;
}



GpivBinData *
gpiv_read_bindata                       (FILE                   *fp
                                         )
/*-----------------------------------------------------------------------------
 * Reads scalar data from ascii data file
 */
{
    GpivBinData *bin_data = NULL;


/*
 * reads the data from stdin or file pointer
 */
    if (fp == stdin || fp == NULL) {
        /* bin_data =  read_histo_from_stdin (); */
    } else {
        bin_data =  read_bindata_from_file (fp);
    }


    return bin_data;
}



GpivBinData *
gpiv_fread_bindata                      (const gchar            *fname
                                         )
/*-----------------------------------------------------------------------------
 * Reads histogram data from file with name fname
 */
{
    FILE *fp = NULL;
    GpivBinData *bin_data = NULL;


    if ((fp = fopen (fname, "rb")) == NULL) {
        gpiv_warning ("gpiv_fread_bindata: failing fopen %s", fname);
        return NULL;
    }

    if ((bin_data = read_bindata_from_file (fp)) == NULL) {
        gpiv_warning ("gpiv_fread_bindata: failing gpiv_read_bindata");
        return NULL;
    }

    fclose (fp);
    return bin_data;
}



gchar *
gpiv_write_bindata			(FILE			*fp, 
					 GpivBinData		*bin_data, 
					 const gboolean		free
					 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writing bin data to fp
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, *count = bin_data->count, nbins = bin_data->nbins;
    gfloat *centre = bin_data->centre;


    g_return_val_if_fail (bin_data->count != NULL, 
                          "gpiv_print_histo: count != NULL");
    g_return_val_if_fail (bin_data->bound != NULL, 
                          "gpiv_print_histo: bound != NULL");
    g_return_val_if_fail (bin_data->centre != NULL, 
                          "gpiv_print_histo: bin_data != NULL");
  

    if (fp == stdout || fp == NULL) {
        if (bin_data->comment != NULL) {
            printf ("%s", bin_data->comment);
        }

        printf ("\n# bin  frequency\n");
        for (i = 0; i < nbins; i++) {
            printf("\n   %f          %d", centre[i], count[i]); 
        }

    } else {
        if (bin_data->comment != NULL) {
            fprintf (fp, "%s", bin_data->comment);
        }

        fprintf (fp, "\n# bin  frequency\n");
        for (i = 0; i < nbins; i++) {
            fprintf(fp, "\n   %f          %d", centre[i], count[i]); 
        }
        fflush (fp);
    }

    if (free) gpiv_free_bindata(bin_data);
    return err_msg;
}



gchar *
gpiv_fwrite_bindata                    (const gchar             *fname, 
                                        GpivBinData             *bin_data, 
                                        const gboolean          free
                                       )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writing bin data to fp
 *
 *---------------------------------------------------------------------------*/
{
     FILE *fp;
     gchar *err_msg = NULL;



       if ((fp = fopen (fname, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_bindata: failing fopen %s", fname);
        return NULL;
    }

    if ((err_msg = gpiv_write_bindata (fp, bin_data, free)) != NULL) {
        /* gpiv_warning ("gpiv_fwrite_bindata: failing gpiv_write_bindata"); */
        return err_msg;
    }



    if (free) {
        gpiv_free_bindata (bin_data);
    }
    fclose (fp);
    return err_msg;
}



gchar *
gpiv_print_cumhisto_eqdatbin		(FILE 			*fp,
					 GpivBinData 		*bin_data,
					 const gboolean		free
					 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writing cumulative histogram data with an equal number of date per bin 
 *     or klass to ouput file. Special output for validation; work around to
 *     print float data as y-values.
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, nbins = bin_data->nbins;
    gfloat *bound = bin_data->bound, *centre = bin_data->centre;


    g_return_val_if_fail (bin_data->bound != NULL, 
                          "gpiv_print_cumhisto_eqdatbin: bound != NULL");
    g_return_val_if_fail (bin_data->centre != NULL, 
                          "gpiv_print_cumhisto_eqdatbin: bound != NULL");
  

    if (bin_data->comment != NULL) {
        fprintf (fp, "%s", bin_data->comment);
    }

    fprintf (fp,"\n# bound      value\n");
    for (i = 0; i < nbins; i++) {
        fprintf (fp, "\n   %f          %f", bound[i], centre[i]); 
    }


    fflush (fp);
    if (free) gpiv_free_bindata (bin_data);
    return err_msg;
}



gchar *
gpiv_fread_hdf5_histo			(const gchar		*fname, 
                                         GpivBinData		*bin_data, 
                                         const gchar		*DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * Wrapper for gpiv_fread_hdf5_bindata
 */
{
  gchar *err_msg = NULL;

  
  if ((err_msg = gpiv_fread_hdf5_histo (fname, bin_data, DATA_KEY)) 
      != NULL) {
    return(err_msg);
  }


  return(err_msg);
}



gchar *
gpiv_fwrite_hdf5_histo			(const gchar            *fname, 
                                         const GpivBinData      *bin_data, 
                                         const gchar            *DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * Wrapper for gpiv_fwrite_hdf5_bindata
 */
{
  gchar *err_msg = NULL;

  
  if ((err_msg = gpiv_fwrite_hdf5_bindata (fname, bin_data, DATA_KEY)) 
      != NULL) {
    return(err_msg);
  }


  return(err_msg);
}



gchar *
gpiv_fread_hdf5_bindata			(const gchar		*fname, 
                                         GpivBinData		*bin_data, 
                                         const gchar		*DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads histogram data to ouput file in hdf version 5 format
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    gint i;

/*
 * HDF declarations
 */
    hid_t       file_id, group_data, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;
    
    g_return_val_if_fail (bin_data->count != NULL, "bin_data->count != NULL");
    g_return_val_if_fail (bin_data->bound != NULL, "bin_data->bound != NULL");
    g_return_val_if_fail (bin_data->centre != NULL, "bin_data->centre != NULL");
  

    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_fread_hdf5_histo: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_data = H5Gopen (file_id, "SCALARS", H5P_DEFAULT);
    group_id = H5Gopen (group_data, DATA_KEY, H5P_DEFAULT);
    H5Gget_comment (group_id, ".", GPIV_MAX_CHARS, RCSID);
    bin_data->comment = g_strdup (RCSID);

/*
 * centre
 */
    dataset_id = H5Dopen(group_id, "centre", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, bin_data->centre);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * count
 */
    dataset_id = H5Dopen(group_id, "count", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, bin_data->count);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

    H5Gclose (group_id);
    H5Gclose (group_data);
    status = H5Fclose(file_id);
    return err_msg;
 }



gchar *
gpiv_fwrite_hdf5_bindata		(const gchar            *fname, 
                                         const GpivBinData      *bin_data, 
                                         const gchar            *DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes histogram data to ouput file in hdf version 5 format
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    gint i;

/*
 * HDF declarations
 */
    hid_t       file_id, group_data, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[1];
    herr_t      status;
    
    g_return_val_if_fail (bin_data->count != NULL, "bin_data->count != NULL");
    g_return_val_if_fail (bin_data->bound != NULL, "bin_data->bound != NULL");
    g_return_val_if_fail (bin_data->centre != NULL, "bin_data->centre != NULL");
  

    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_fwrite_hdf5_histo: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_data = H5Gopen (file_id, "SCALARS", H5P_DEFAULT);
    group_id = H5Gopen (group_data, DATA_KEY, H5P_DEFAULT);
    H5Gset_comment (group_id, ".", bin_data->comment);

/*
 * Create the data space.
 */
   dims[0] = bin_data->nbins; 
/*
 * centre
 */
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "centre", H5T_NATIVE_FLOAT, 
                           dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, &bin_data->centre[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * count
 */
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "count", H5T_NATIVE_INT, 
                          dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, &bin_data->count[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

    H5Gclose (group_id);
    H5Gclose (group_data);
    status = H5Fclose(file_id);
    return err_msg;
}


/*
 * Local routines
 */

static GpivBinData *
read_bindata_from_file                  (FILE                   *fp
					 )
/*-----------------------------------------------------------------------------
 * Reads histogram data from file pointer
 */
{
    GpivBinData *bin_data = NULL;
    gint i = 0, nbins = 0;
    gchar line[GPIV_MAX_CHARS];


    nbins = count_bindata (fp);
    bin_data = gpiv_alloc_bindata (nbins);

    rewind (fp);
    i = 0;
    while ( !feof(fp) ) {         
        fgets (line, GPIV_MAX_CHARS, fp);
        if (line[0] != '#' && line[0] != '\n' && line[0] != '\t') {
            if (i < bin_data->nbins) {
                sscanf (line, "%f %d", 
                        &bin_data->centre[i], &bin_data->count[i]);
                i++;
           }
        }
    }


    return bin_data;
}



static GpivBinData *
read_bindata_from_stdin                 (void
					 )
/*-----------------------------------------------------------------------------
 * Reads histogram data from file pointer
 */
{
    GpivBinData *bin_data = NULL;
    gint i = 0, nbins = 0;
    gchar line[GPIV_MAX_LINES][GPIV_MAX_CHARS];


    i = 0;
    while (gets (line[i]) != NULL) {
       if (line[i][0] != '#' && line[i][0] != '\n' && line[i][0] != '\t') {
         i++;
    }
    }
    nbins = i - 1;
    bin_data = gpiv_alloc_bindata (nbins);

    i = 0;
    while (i < bin_data->nbins) {         
        if (line[i][0] != '#' && line[i][0] != '\n' && line[i][0] != '\t') {
            sscanf (line[i], "%f %d", 
                    &bin_data->centre[i], &bin_data->count[i]);
            i++;
        }
    }


    return bin_data;
}



static int
count_bindata                           (FILE                   *fp
					 )
/*-----------------------------------------------------------------------------
 * Counts number of data from file for GpivBinData
 */
{
    guint count = 0, nbins = 0;
    gchar line[GPIV_MAX_CHARS];


    if (fp == stdin || fp == NULL) {
        while (gets (line) != NULL) {
            if (line[0] != '#' && line[0] != '\n' && line[0] != '\t') {
                count++;
            }
        }

    } else {
        rewind (fp);
        while ( !feof(fp) ) {         
            fgets (line, GPIV_MAX_CHARS, fp);
            if (line[0] != '#' && line[0] != '\n' && line[0] != '\t') {
                count++;
           }
        }
        rewind (fp);
    }

    nbins = count;


    return nbins;
}



