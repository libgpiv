
/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>
   Julio Soria <julio.soria@eng.monash.edu.au>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------
FILENAME:                trig_par.c
LIBRARY:                 libgpiv
EXTERNAL FUNCTIONS:      
                         gpiv_trig_default_parameters
                         gpiv_trig_parameters_set
                         gpiv_trig_read_parameters
                         gpiv_trig_check_parameters_read
		         gpiv_trig_test_parameter
                         gpiv_trig_fprint_parameters
                         gpiv_trig_fread_hdf5_parameters

LAST MODIFICATION DATE:  $Id: trig_par.c,v 1.7 2008-05-05 14:38:42 gerber Exp $
--------------------------------------------------------------------------- */


#ifdef ENABLE_TRIG
#include <gpiv.h>



void
gpiv_trig_default_parameters (GpivTrigPar *trig_par_default,
                              const gboolean force
                              )
/*-----------------------------------------------------------------------------
 *     Sets default GpivTrigPar parameter values
 */
{
    if (!trig_par_default->ttime__cam_acq_period__set || force)
        trig_par_default->ttime.cam_acq_period = 
            (RTIME) (GPIV_TRIGPAR_DEFAULT__CAP * GPIV_MILI2NANO);
    if (!trig_par_default->ttime__laser_trig_pw__set || force)
        trig_par_default->ttime.laser_trig_pw = 
            (RTIME) (GPIV_TRIGPAR_DEFAULT__LPW * GPIV_MILI2NANO);
    if (!trig_par_default->ttime__time2laser__set || force)
        trig_par_default->ttime.time2laser = 
            (RTIME) (GPIV_TRIGPAR_DEFAULT__T2L * GPIV_MILI2NANO);
    if (!trig_par_default->ttime__dt__set || force)
        trig_par_default->ttime.dt = 
            (RTIME) (GPIV_TRIGPAR_DEFAULT__DT * GPIV_MILI2NANO);
    if (!trig_par_default->ttime__mode__set || force)
        trig_par_default->ttime.mode = GPIV_TRIGPAR_DEFAULT__MODE;
    if (!trig_par_default->ttime__cycles__set || force)
        trig_par_default->ttime.cycles = GPIV_TRIGPAR_DEFAULT__CYCLES;
    if (!trig_par_default->ttime__increment__set || force)
        trig_par_default->ttime.increment = 
            (RTIME) (GPIV_TRIGPAR_DEFAULT__INCR_DT * GPIV_MILI2NANO);
    
    
    gpiv_trig_parameters_set(trig_par_default, TRUE);
}



void 
gpiv_trig_read_parameters (FILE * fp_par, 
                           GpivTrigPar * trig_par, 
                           const gboolean verbose
                           )
/*-----------------------------------------------------------------------------
 *     Read all GpivTrigPar parameters
 */
{
    char line[GPIV_MAX_CHARS], par_name[GPIV_MAX_CHARS];
    gfloat val;

    while (fgets(line, GPIV_MAX_CHARS, fp_par) != NULL) {
        if (line[0] != '#' && line[0] != '\n' && line[0] != ' ' 
            && line[0] != '\t') {
            sscanf(line,"%s", par_name);

/*
 * Parameters used to acquire images
 */
            if (trig_par->ttime__cam_acq_period__set == FALSE) {
                trig_par->ttime__cam_acq_period__set =
                    gpiv_scan_fph (GPIV_TRIGPAR_KEY, 
                                    GPIV_TRIGPAR_KEY__CAP, TRUE,
                                    line, par_name, &val, 
                                    verbose, NULL);
                trig_par->ttime.cam_acq_period = (RTIME) GPIV_NANO2MILI * val;
            }

            if (trig_par->ttime__laser_trig_pw__set == FALSE) {
                trig_par->ttime__laser_trig_pw__set =
                    gpiv_scan_fph (GPIV_TRIGPAR_KEY, 
                                    GPIV_TRIGPAR_KEY__LPW, TRUE,
                                    line, par_name, &val, 
                                    verbose, NULL);
                trig_par->ttime.laser_trig_pw = (RTIME) GPIV_NANO2MILI * val;
            }

            if (trig_par->ttime__time2laser__set == FALSE) {
                trig_par->ttime__time2laser__set =
                    gpiv_scan_fph (GPIV_TRIGPAR_KEY, 
                                    GPIV_TRIGPAR_KEY__T2L, TRUE,
                                    line, par_name, &val, 
                                    verbose, NULL);
                trig_par->ttime.time2laser = (RTIME) GPIV_NANO2MILI * val;
            }
            
            if (trig_par->ttime__dt__set == FALSE) {
                trig_par->ttime__dt__set =
                    gpiv_scan_fph (GPIV_TRIGPAR_KEY, 
                                    GPIV_TRIGPAR_KEY__DT, TRUE,
                                    line, par_name, &val, 
                                    verbose, NULL);
                trig_par->ttime.dt = (RTIME) GPIV_NANO2MILI * val;
            }
            
            if (trig_par->ttime__mode__set == FALSE) {
                trig_par->ttime__mode__set =
                    gpiv_scan_iph (GPIV_TRIGPAR_KEY, 
                                    GPIV_TRIGPAR_KEY__MODE, TRUE,
                                    line, par_name, &trig_par->ttime.mode, 
                                    verbose, NULL);
            }
            
            if (trig_par->ttime__cycles__set == FALSE) {
                trig_par->ttime__cycles__set =
                    gpiv_scan_iph (GPIV_TRIGPAR_KEY, 
                                    GPIV_TRIGPAR_KEY__CYCLES, TRUE,
                                    line, par_name, &trig_par->ttime.cycles, 
                                    verbose, NULL);
            }
            
            if (trig_par->ttime__increment__set == FALSE) {
                trig_par->ttime__increment__set =
                    gpiv_scan_fph (GPIV_TRIGPAR_KEY, 
                                    GPIV_TRIGPAR_KEY__INCR_DT, TRUE,
                                    line, par_name, &val, 
                                    verbose, NULL);
                trig_par->ttime.increment = (RTIME) GPIV_NANO2MILI * val;
	    }
	    
    
	}
    }
    
}



gchar *
gpiv_trig_check_parameters_read (GpivTrigPar *trig_par,
                                 const GpivTrigPar *trig_par_default
                                 )
/*-----------------------------------------------------------------------------
 *     Check out if all GpivTrigPar parameters have been read
 */
{
    gchar *err_msg = NULL;

    if (trig_par->ttime__cam_acq_period__set == FALSE) {
        trig_par->ttime__cam_acq_period__set = TRUE;
        if (trig_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            trig_par->ttime.cam_acq_period = GPIV_TRIGPAR_DEFAULT__CAP;
        } else {
            err_msg = "Using default: ";
            trig_par->ttime.cam_acq_period = trig_par_default->ttime.cam_acq_period;
        }
        gpiv_warning("%s\n%s.%s %lld", err_msg, 
                     GPIV_TRIGPAR_KEY,
                     GPIV_TRIGPAR_KEY__CAP,
                     trig_par_default->ttime.cam_acq_period /* * GPIV_NANO2MILI */);
    }
 
    if (trig_par->ttime__laser_trig_pw__set == FALSE) {
        trig_par->ttime__laser_trig_pw__set = TRUE;
        if (trig_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            trig_par->ttime.laser_trig_pw = GPIV_TRIGPAR_DEFAULT__LPW;
        } else {
            err_msg = "Using default from libgpiv: ";
            trig_par->ttime.laser_trig_pw = trig_par_default->ttime.laser_trig_pw;
        }
        gpiv_warning("%s\n%.%s %lld", err_msg, 
                     GPIV_TRIGPAR_KEY,
                     GPIV_TRIGPAR_KEY__LPW,
                     trig_par_default->ttime.laser_trig_pw/*  * GPIV_NANO2MILI */);
    }

    if (trig_par->ttime__time2laser__set == FALSE) {
        trig_par->ttime__time2laser__set = TRUE;
        if (trig_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            trig_par->ttime.time2laser = GPIV_TRIGPAR_DEFAULT__T2L;
        } else {
            err_msg = "Using default: ";
            trig_par->ttime.time2laser = trig_par_default->ttime.time2laser;
        }
        gpiv_warning("%s\n%s.%s %lld", err_msg, 
                     GPIV_TRIGPAR_KEY,
                     GPIV_TRIGPAR_KEY__T2L,
                     trig_par_default->ttime.time2laser/*  * GPIV_NANO2MILI */);
    }

    if (trig_par->ttime__dt__set == FALSE) {
        trig_par->ttime__dt__set = TRUE;
        if (trig_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            trig_par->ttime.dt = trig_par_default->ttime.dt;
        } else {
            err_msg = "Using default: ";
            trig_par->ttime.dt = GPIV_TRIGPAR_DEFAULT__DT;
        }
        gpiv_warning("%s\n%s.%s %lld", err_msg, 
                     GPIV_TRIGPAR_KEY,
                     GPIV_TRIGPAR_KEY__DT,
                     trig_par_default->ttime.dt/*  * GPIV_NANO2MILI */);
    }

    if (trig_par->ttime__mode__set == FALSE) {
        trig_par->ttime__mode__set = TRUE;
        if (trig_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            trig_par->ttime.mode = GPIV_TRIGPAR_DEFAULT__MODE;
        } else {
            err_msg = "Using default: ";
            trig_par->ttime.mode = trig_par_default->ttime.mode;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_TRIGPAR_KEY,
                     GPIV_TRIGPAR_KEY__MODE,
                     trig_par_default->ttime.mode);
    }

    if (trig_par->ttime__cycles__set == FALSE) {
        trig_par->ttime__cycles__set = TRUE;
        if (trig_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            trig_par->ttime.cycles = GPIV_TRIGPAR_DEFAULT__CYCLES;
        } else {
            err_msg = "Using default: ";
            trig_par->ttime.cycles = trig_par_default->ttime.cycles;
        }
        gpiv_warning("%s\n%.%s %d", err_msg,
                     GPIV_TRIGPAR_KEY,
                     GPIV_TRIGPAR_KEY__CYCLES,
                     trig_par_default->ttime.cycles);
    }

    if (trig_par->ttime__increment__set == FALSE) {
        trig_par->ttime__increment__set = TRUE;
        if (trig_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            trig_par->ttime.increment = GPIV_TRIGPAR_DEFAULT__INCR_DT;
        } else {
            err_msg = "Using default: ";
            trig_par->ttime.increment = trig_par_default->ttime.increment;
        }
       gpiv_warning("%s\n%s.%s %lld", err_msg, 
                     GPIV_TRIGPAR_KEY,
                     GPIV_TRIGPAR_KEY__INCR_DT,
                     trig_par_default->ttime.increment/*  * GPIV_NANO2MILI */);
    }

    return (err_msg);
}



void
gpiv_trig_parameters_set (GpivTrigPar * trig_par,
                          const gboolean flag
                          )
/*-----------------------------------------------------------------------------
 *     Sets flags for __set variables of GpivTrigPar 
 */
{
    trig_par->ttime__cam_acq_period__set = flag;
    trig_par->ttime__laser_trig_pw__set = flag;
    trig_par->ttime__time2laser__set = flag;
    trig_par->ttime__dt__set = flag;
    trig_par->ttime__mode__set = flag;
    trig_par->ttime__cycles__set = flag;
    trig_par->ttime__increment__set = flag;
}



gchar *
gpiv_trig_test_parameter (GpivTrigPar * trig_par
                          )
/*-----------------------------------------------------------------------------
 *     Testing GpivTrigPar parameters on valid values
 */
{
    gchar * err_msg = NULL;


    if (trig_par->ttime.cam_acq_period < GPIV_TRIGPAR_CAP_MIN
        ||trig_par->ttime.cam_acq_period > GPIV_TRIGPAR_CAP_MAX) {
        err_msg = "gpiv_trig_test_parameter: ac_par->ttime.cam_acq_period out of range";
        return (err_msg);
    }

    if (trig_par->ttime.laser_trig_pw < GPIV_TRIGPAR_LPW_MIN
        ||trig_par->ttime.laser_trig_pw > GPIV_TRIGPAR_LPW_MAX) {
        err_msg = "gpiv_trig_test_parameter: ac_par->ttime.laser_trig_pw out of range";
        return (err_msg);
    }

    if (trig_par->ttime.time2laser < GPIV_TRIGPAR_T2L_MIN
        ||trig_par->ttime.time2laser > GPIV_TRIGPAR_T2L_MAX) {
        err_msg = "gpiv_trig_test_parameter: ac_par->ttime.time2laser out of range";
        return (err_msg);
    }

    if (trig_par->ttime.dt < GPIV_TRIGPAR_DT_MIN
        ||trig_par->ttime.dt > GPIV_TRIGPAR_DT_MAX) {
        err_msg = "gpiv_trig_test_parameter: ac_par->ttime.dt out of range";
        return (err_msg);
    }
    if (trig_par->ttime.mode < GPIV_TRIGPAR_MODE_MIN
        ||trig_par->ttime.mode > GPIV_TRIGPAR_MODE_MAX) {
        err_msg = "gpiv_trig_test_parameter: ac_par->ttime.mode out of range";
        return (err_msg);
    }

    if (trig_par->ttime.cycles < GPIV_TRIGPAR_CAP_MIN
        ||trig_par->ttime.cycles > GPIV_TRIGPAR_CAP_MAX) {
        err_msg = "gpiv_trig_test_parameter: ac_par->ttime.cycles out of range";
        return (err_msg);
    }

    if (trig_par->ttime.increment < GPIV_TRIGPAR_INCR_DT_MIN
        ||trig_par->ttime.increment > GPIV_TRIGPAR_INCR_DT_MAX) {
        err_msg = "gpiv_trig_test_parameter: ac_par->ttime.increment out of range";
        return (err_msg);
    }

/*
 * Adjusting parameter settings for selected timing mode
 */
/* BUGFIX: OLD
/*     if ((trig_par.ttime.mode != GPIV_TIMER_MODE__PERIODIC)  */
/*         && (trig_par.ttime.mode != GPIV_TIMER_MODE__DURATION)  */
/*         && (trig_par.ttime.mode != GPIV_TIMER_MODE__TRIGGER_IRQ)  */
/*         && (trig_par.ttime.mode != GPIV_TIMER_MODE__DOUBLE)) { */


/* BUGFIX: NEW */
    if ((trig_par->ttime.mode == GPIV_TIMER_MODE__ONE_SHOT_IRQ)
        || (trig_par->ttime.mode == GPIV_TIMER_MODE__INCREMENT)) {
        trig_par->ttime.cam_acq_period  = (trig_par->ttime.dt) * 2;
    }
        
/* BUGFIX: OLD
/*     if ((trig_par.ttime.mode != GPIV_TIMER_MODE__DURATION)  */
/*         && (trig_par.ttime.mode != GPIV_TIMER_MODE__ONE_SHOT_IRQ )  */
/*         && (trig_par.ttime.mode != GPIV_TIMER_MODE__TRIGGER_IRQ)  */
/*         && (trig_par.ttime.mode != GPIV_TIMER_MODE__INCREMENT)) { */

/* BUGFIX: NEW */
    if ((trig_par->ttime.mode == GPIV_TIMER_MODE__PERIODIC)
        || (trig_par->ttime.mode == GPIV_TIMER_MODE__DOUBLE)) {
        trig_par->ttime.cycles = 1;
    }
    
    if (trig_par->ttime.mode != GPIV_TIMER_MODE__INCREMENT) {
        trig_par->ttime.increment = 1;
    }


    return (err_msg);
}



void 
gpiv_trig_print_parameters (FILE * fp_par_out, 
                            GpivTrigPar trig_par
                            )
/*-----------------------------------------------------------------------------
 *      Prints GpivTrigPar parameters to fp_par_out
 */
{
    gfloat val;

    if (fp_par_out == NULL) fp_par_out = stdout;

    if (trig_par->ttime__cam_acq_period__set) {
        val = trig_par->ttime.cam_acq_period * GPIV_MILI2NANO;
	fprintf(fp_par_out, "%s.%s %f\n", 
                GPIV_TRIGPAR_KEY, 
                GPIV_TRIGPAR_KEY__CAP, 
		val);
    }

    if (trig_par->ttime__laser_trig_pw__set) {
        val = trig_par->ttime.laser_trig_pw * GPIV_MILI2NANO;
	fprintf(fp_par_out, "%s.%s %f\n", 
                GPIV_TRIGPAR_KEY, 
                GPIV_TRIGPAR_KEY__LPW, 
		val);
    }

    if (trig_par->ttime__time2laser__set) {
        val = trig_par->ttime.time2laser * GPIV_MILI2NANO;
	fprintf(fp_par_out, "%s.%s %f\n", 
                GPIV_TRIGPAR_KEY, 
                GPIV_TRIGPAR_KEY__T2L, 
		val);
    }

    if (trig_par->ttime__dt__set) {
        val = trig_par->ttime.dt * GPIV_MILI2NANO;
	fprintf(fp_par_out, "%s.%s %f\n", 
                GPIV_TRIGPAR_KEY, 
                GPIV_TRIGPAR_KEY__DT, 
		val);
    }

    if (trig_par->ttime__mode__set) {
	fprintf(fp_par_out, "%s.%s %d\n", 
                GPIV_TRIGPAR_KEY, 
                GPIV_TRIGPAR_KEY__MODE, 
		trig_par->ttime.mode);
    }

    if (trig_par->ttime__cycles__set) {
	fprintf(fp_par_out, "%s.%s %d\n", 
                GPIV_TRIGPAR_KEY, 
                GPIV_TRIGPAR_KEY__CYCLES, 
		trig_par->ttime.cycles);
    }

    if (trig_par->ttime__increment__set) {
        val = trig_par->ttime.increment * GPIV_MILI2NANO;
	fprintf(fp_par_out, "%s.%s %f\n", 
                GPIV_TRIGPAR_KEY, 
                GPIV_TRIGPAR_KEY__INCR_DT, 
		val);
    }

}



#endif /* ENABLE_TRIG */
