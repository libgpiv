/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                my_utils.c
LIBRARY:                 libgpiv
LAST MODIFICATION DATE:  $Id: my_utils.c,v 1.2 2008-10-09 14:02:03 gerber Exp $
 --------------------------------------------------------------------------- */

#include <string.h>
#include <gpiv.h>
#include "my_utils.h"


FILE *
my_utils_fopen_tmp (const gchar *tmpfile,
		    const gchar *mode)
/*-----------------------------------------------------------------------------
 * Attempts to open a filepointer for TMPDIR/USER/tmpfile, else in 
 * TMPDIR/tmpfile
 *
 * RETURNS:
 * filepointer or NULL
 */
{
    FILE *fp = NULL;
    gchar *tmp_dir = g_strdup (g_get_tmp_dir ());
    gchar *user_name = g_strdup (g_get_user_name ());
    gchar *tmp_user_dir = g_strdup_printf ("%s/%s", tmp_dir, user_name);
    gchar *filename =  g_strdup_printf ("%s/%s/%s", tmp_dir, user_name, 
                                        tmpfile);
    gint return_val;


    if ((fp = fopen(filename, mode)) != NULL) {
        if ((return_val = g_mkdir(tmp_user_dir, 700)) != 0) {
            g_free (filename);
            filename = g_strdup_printf ("%s/%s", tmp_dir, tmpfile);
        }
        if ((fp = fopen(filename, mode)) == NULL) {
            gpiv_warning("LIBGPIV internal error: my_utils_fopen_tmp: Failure opening %s for output", 
                         filename);
            return NULL;
        }

    }


    g_free (tmp_dir);
    g_free (user_name);
    g_free (tmp_user_dir);
    g_free (filename);
    return fp;
}


gchar *
my_utils_write_tmp_image (const GpivImage *image, 
			  const gchar *basename,
			  const gchar *message
			  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Stores deformed image to file system with pre defined name to TMPDIR
 *     and prints message to stdout
 *
 * INPUTS:
 *     img1:                   first image of PIV image pair
 *     img2:                   second image of PIV image pair
 *     image_par:              image parameters to be stored in header
 *     verbose:                prints the storing to stdout
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     char * to NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *tmp_dir = g_strdup (g_get_tmp_dir ());
    gchar *user_name = g_strdup (g_get_user_name ());
    gchar *filename = g_strdup_printf ("%s%s",  basename, GPIV_EXT_PNG_IMAGE);
    FILE *fp;


    if ((fp = my_utils_fopen_tmp (filename, "wb")) == NULL) {
        err_msg = "gpiv_piv_write_tmp_image: Failure opening for output";
        return err_msg;
    }

    g_message ("my_utils_write_tmp_image: %s %s",
               message, g_strdup_printf ("%s/%s/%s", tmp_dir, user_name, filename));

    if ((err_msg = 
         gpiv_write_png_image (fp, image, FALSE)) != NULL) {
        fclose (fp);
        return err_msg;
    }

    fclose(fp);
    g_free (tmp_dir);
    g_free (filename);
    return err_msg;
}



gboolean
my_utils_find_data_scaled (const gchar line[GPIV_MAX_CHARS]
                           )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Determines if the data have been scaled in order to read x-and y 
 *     positions as floating point varibles
 *
 * PROTOTYPE LOCATATION:
 *     io.h
 *
 * INPUTS:
 *      line:           character line containing program that generated data
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      scale:          TRUE if scaled, else FALSE
 *---------------------------------------------------------------------------*/
{
    gboolean scale = FALSE;
    

    if (strstr(line,"scale") != '\0') {
        scale = TRUE;
    }


    return scale;
}



gboolean
my_utils_count_asciidata (FILE *fp,
                 guint *nx,
                 guint *ny
                 )
/*---------------------------------------------------------------------------*/
/**
 *     Reads ASCII data from fp to find array length of x-and y data, 
 *     provided that both x and y-data are in 
 *     incremental or decremental order. Determines if scaled data have been used 
 *     in order to determine which format/header has to be used for writing the data
 *
 *     @param[out] nx          number of columns in piv data array
 *     @param[out] ny          number of columns in piv data array
 *     @param[in] fp           input file. If NULL, stdin is used.
 *     @return scale               TRUE or FALSE for scaled data
 */
/*---------------------------------------------------------------------------*/
{
    gboolean scale = FALSE;
    gchar line[GPIV_MAX_CHARS];
  
    gboolean increment = FALSE;
    gfloat x = 0, y = 0;
    gfloat dat_x = -10.0e5, dat_y = -10.0e5;
    gint line_nr = 0; 


    *nx = 0;
    *ny = 0;

    if (fp == stdin || fp == NULL) {
        while (gets (line) != NULL) {
            g_message ("count_asciidata:: line = %s", line);
            my_utils_obtain_nxny_fromline (line, &scale, nx, ny, 
                                  &increment, &dat_x, &dat_y, &line_nr);
        }

    } else {
        while (fgets (line, GPIV_MAX_CHARS, fp) != NULL) {
            my_utils_obtain_nxny_fromline (line, &scale, nx, ny, 
                                  &increment, &dat_x, &dat_y, &line_nr);


        }
    }

/*
 * Array size nx, ny
 */
    *nx = *nx + 1;
    *ny = *ny + 1;
#ifdef DEBUG
    g_message ("count_asciidata:: nx = %d ny = %d", *nx, *ny);
#endif


   return scale;
}



GpivImage *
my_utils_open_img (const gchar *fname
          )
/*-----------------------------------------------------------------------------
 * Opens an image from png, hdf or raw-data file
 */
{
    GpivImage *image = NULL;
    gchar *err_msg = NULL;
    gchar *ext = g_strdup(strrchr(fname, '.'));
    FILE *fp;

#ifdef DEBUG
    g_message ("open_img:: 0 fname=%s ext=%s", fname, ext);
#endif

    if (fname == NULL) {
        gpiv_warning ("LIBGPIV internal error: open_img: failing \"fname == NULL\"");
        return NULL;
    }

/*
 * Reads hdf format
 */
    if (strcmp (ext, GPIV_EXT_GPIV) == 0) {
#ifdef DEBUG
        g_message ("OPEN_IMG:: gpiv_fread_hdf5_image");
#endif
        if ((image = gpiv_fread_hdf5_image (fname)) == NULL) {
            g_message ("LIBGPIV internal error: open_img: gpiv_fread_hdf5_image failed");
            return NULL;
        }

/*
 * Reads Portable Network Graphics image format
 */
    } else if (strcmp (ext, GPIV_EXT_PNG_IMAGE) == 0) {
#ifdef DEBUG
        g_message ("OPEN_IMG:: calling gpiv_fread_png_image");
#endif
        if ((fp = fopen (fname, "rb")) == NULL) {
            gpiv_warning ("LIBGPIV internal error: open_img: failure opening %s", fname);
            return NULL;
        }

        if ((image = gpiv_read_png_image (fp)) == NULL) {
            g_message ("LIBGPIV internal error: open_img: gpiv_read_png_image failed");
            return NULL;
        }

        fclose (fp);

/*
 * Reads raw data format
 */
    } else if (strcmp (ext, GPIV_EXT_RAW_IMAGE) == 0) {
#ifdef DEBUG
        g_message ("OPEN_IMG:: gpiv_fread_raw_image");
#endif
       if ((image = gpiv_fread_raw_image (fname)) == NULL) {
            g_message ("LIBGPIV internal error: open_img: gpiv_read_raw_image failed");
            return NULL;
        }
        
/*
 * Reads LaVision's (tm) DaVis format (.img)
 */
    } else if (strcmp (ext, GPIV_EXT_DAVIS) == 0) {
#ifdef DEBUG
        g_message ("OPEN_IMG:: gpiv_fread_davis_image");
#endif
        if ((fp = fopen (fname, "rb")) == NULL) {
            gpiv_warning ("LIBGPIV internal error: open_img: failure opening %s", fname);
            return NULL;
        }

       if ((image = gpiv_read_davis_image (fp)) == NULL) {
            g_message ("LIBGPIV internal error: open_img: gpiv_read_davis_image failed");
            return NULL;
        }

        fclose (fp);


    } else {
        err_msg = "LIBGPIV internal error: open_img: should not arrive here";
            gpiv_warning ("%s", err_msg);
            return NULL;
    }


    g_free(ext);
    return image;
}



gchar *
my_utils_fcount_hdf5_data               (const gchar            *fname,
                                         gint                   *nx,
                                         gint                   *ny
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads array lengths of 2-dimensional data
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, dataset_id, dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_fcount_hdf5_pivdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "POSITIONS", H5P_DEFAULT);

/*
 * getting nx
 */
    dataset_id = H5Dopen(group_id, "point_x", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    rank_x = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_x != 1) {
        err_msg = "gpiv_fcount_hdf5_pivdata: rank_x != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    *nx = dims[0];
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
	
/*
 * getting ny
 */
    dataset_id = H5Dopen(group_id, "point_y", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    rank_y = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_y != 1) {
        err_msg = "gpiv_fcount_hdf5_pivdata: rank_y != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    *ny = dims[0];
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);


    H5Gclose (group_id);
    status = H5Fclose(file_id); 
    return err_msg;
}



gboolean
my_utils_obtain_nxny_fromline           (gchar                  line[], 
                                         gboolean               *scale,
                                         guint                  *nx, 
                                         guint                  *ny, 
                                         gboolean               *increment,
                                         gfloat                 *dat_x,
                                         gfloat                 *dat_y,
                                         guint                  *line_nr
                                         )
/*-----------------------------------------------------------------------------
 * Scans a line to get nx and ny and scale
 */
{
    gfloat x = 0, y = 0;


    if (line[0] == '#' 
        &&  *scale == FALSE) {
        *scale = my_utils_find_data_scaled (line);
    }

/*
 * Skip comment, blank lines and header line
 */
    if (line[0] != '#' 
        && line[0] != '\n' 
        && line[0] != '\t') {
        sscanf (line, "%f %f", &x, &y);
/*
 * The first line containig data has been found. 
 * When the second has been found it is 
 * determined whether the data have been arranged column-wise or row-wise
 */
        if (*line_nr == 0) {
/* get file position for first data line */
            *dat_x = x;
            *dat_y = y;
        }

        if (*line_nr == 1) {
            if (x > *dat_x || y > *dat_y) {
                *increment = TRUE;
            } else {
                *increment = FALSE;
            }
        }


        *line_nr = *line_nr + 1;
        if (*increment == TRUE) {
            if (x > *dat_x){
                *dat_x = x;
                *nx = *nx + 1;
            }
            if (y > *dat_y) {
                *dat_y = y;
                *ny = *ny + 1;
            }
        } else {
            if (x < *dat_x){
                *dat_x = x;
                *nx = *nx + 1;
            }
            if (y < *dat_y) {
                *dat_y = y;
                *ny = *ny + 1;
            }
        }

    }

}



