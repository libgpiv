/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2011
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------*/

#include <gpiv.h>
#include "my_utils.h"
#include <float.h>       /* for system limits of float numbers */

/*
 * Prototyping local functions
 */

static gchar *
fread_hdf5_piv_position                 (const gchar            *fname, 
                                         GpivPivData            *piv_data
                                         );

static gchar *
fwrite_hdf5_piv_position                (const gchar            *fname, 
                                         GpivPivData            *piv_data
                                         );

static gchar *
fread_hdf5_pivdata                      (const gchar            *fname, 
                                         GpivPivData            *piv_data,
                                         const gchar            *DATA_KEY
                                         );

static gchar *
fwrite_hdf5_pivdata                     (const gchar            *fname, 
                                         GpivPivData            *piv_data, 
                                         const gchar            *DATA_KEY
                                         );

static GpivPivData *
read_pivdata_from_file (FILE *fp,
                        gboolean fastx
                        );

static GpivPivData *
read_pivdata_from_stdin (gboolean fastx
                         );

static void
obtain_pivdata_fromline (gchar line[], 
                         GpivPivData *piv_data,
                         guint *i,
                         guint *j
                         );
static void
obtain_pivdata_fastx_fromline (gchar line[], 
                               GpivPivData *piv_data,
                               guint *i,
                               guint *j
                               );

/*
 * Public functions
 */
void
gpiv_null_pivdata                       (GpivPivData            *piv_data
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Sets all elements of piv_data structure to NULL
 *
 * INPUTS:
 *      piv_data:      Input PIV data structure
 *
 * OUTPUTS:
 *      piv_data:      Output PIV data structure
 *
 * RETURNS:
 *     void
 *---------------------------------------------------------------------------*/
{
    piv_data->point_x = NULL;
    piv_data->point_y = NULL;
    piv_data->dx = NULL;
    piv_data->dy = NULL;
    piv_data->dz = NULL;
    piv_data->snr = NULL;
    piv_data->peak_no = NULL;

    piv_data->comment = NULL;
    piv_data = NULL;
}



GpivPivData *
gpiv_alloc_pivdata                      (const guint            nx,
					const guint             ny
                                        )
/*-----------------------------------------------------------------------------
 */
{
    GpivPivData *piv_data = g_new0 (GpivPivData, 1);
    gint i, j;


    if (ny <= 0 || nx <= 0 ) {
        gpiv_warning ("gpiv_alloc_pivdata: nx = %d ny = %d",
                      nx, ny);
        return NULL;
    }
    piv_data->nx = nx;
    piv_data->ny = ny;

    piv_data->point_x = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->point_y = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->dx = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->dy = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->dz = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->snr = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->peak_no = gpiv_imatrix (piv_data->ny, piv_data->nx);
        
/*
 * Initializing values for structure members
 */
/* #pragma omp parallel for shared(piv_data) private(i, j) */
    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            piv_data->point_x[i][j] = 0.0;
            piv_data->point_y[i][j] = 0.0;
            piv_data->dx[i][j] = 0.0;
            piv_data->dy[i][j] = 0.0;
            piv_data->dz[i][j] = 0.0;
            piv_data->snr[i][j] = 0.0;
            piv_data->peak_no[i][j] = 0;
        }
    }


/*
 * Take system limits (for floats) as initial values for minima and maximuma
 */
    piv_data->mean_dx = 0.0;
    piv_data->sdev_dx = 0.0;
    piv_data->min_dx = FLT_MAX;
    piv_data->max_dx = FLT_MIN;

    piv_data->mean_dy = 0.0;
    piv_data->sdev_dy = 0.0;
    piv_data->min_dy = FLT_MAX;
    piv_data->max_dy = FLT_MIN;

    piv_data->mean_dz = 0.0;
    piv_data->sdev_dz = 0.0;
    piv_data->min_dz = FLT_MAX;
    piv_data->max_dz = FLT_MIN;

    piv_data->scale = FALSE;
    piv_data->scale__set = FALSE;
    piv_data->comment = NULL;


    return piv_data;
}



gchar *
gpiv_check_alloc_pivdata                (const GpivPivData      *piv_data
                                        )
/*-----------------------------------------------------------------------------
 * Check if piv_data have been allocated
 */
{
    gchar *err_msg = NULL;


    g_return_val_if_fail (piv_data->point_x != NULL, 
                          "gpiv_check_alloc_pivdata: point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, 
                          "gpiv_check_alloc_pivdata: point_y != NULL");
    g_return_val_if_fail (piv_data->dx != NULL, 
                          "gpiv_check_alloc_pivdata: dx != NULL");
    g_return_val_if_fail (piv_data->dy != NULL, 
                          "gpiv_check_alloc_pivdata: dy != NULL");
    g_return_val_if_fail (piv_data->dz != NULL, 
                          "gpiv_check_alloc_pivdata: dz != NULL");
    g_return_val_if_fail (piv_data->snr != NULL, 
                          "gpiv_check_alloc_pivdata: snr != NULL");
    g_return_val_if_fail (piv_data->peak_no != NULL, 
                          "gpiv_check_alloc_pivdata: peak_no != NULL");


    return err_msg;
}



void 
gpiv_free_pivdata                       (GpivPivData            *piv_data
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for GpivPivData
 *
 * INPUTS:
 *      piv_data:       PIV data structure
 *
 * OUTPUTS:
 *      piv_data:      NULL pointer to point_x, point_y, dx, dy, snr, peak_no
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (piv_data != NULL);
    g_return_if_fail (piv_data->point_x != NULL); /* gpiv_error ("piv_data->point_x not allocated"); */
    g_return_if_fail (piv_data->point_y != NULL); /* gpiv_error ("piv_data->point_y not allocated");   */
    g_return_if_fail (piv_data->dx != NULL); /* gpiv_error ("piv_data->dx not allocated"); */
    g_return_if_fail (piv_data->dy != NULL); /* gpiv_error ("piv_data->dy not allocated"); */
    g_return_if_fail (piv_data->dz != NULL); /* gpiv_error ("piv_data->dz not allocated"); */
    g_return_if_fail (piv_data->snr != NULL); /* gpiv_error ("piv_data->snr not allocated"); */
    g_return_if_fail (piv_data->peak_no != NULL); /* gpiv_error ("piv_data->peak_no not allocated"); */

    gpiv_free_matrix (piv_data->point_x);
    gpiv_free_matrix (piv_data->point_y);
    gpiv_free_matrix (piv_data->dx);
    gpiv_free_matrix (piv_data->dy);
    gpiv_free_matrix (piv_data->dz);
    gpiv_free_matrix (piv_data->snr);
    gpiv_free_imatrix (piv_data->peak_no);

    g_free (piv_data->comment);
    gpiv_null_pivdata (piv_data);
}



GpivPivData *
gpiv_cp_pivdata                         (const GpivPivData      *piv_data
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Copies data from piv_data_in to piv_data_out.
 *
 * INPUTS:
 *     piv_data_in:   Input PIV data structure
 *
 *
 * RETURNS:
 *     piv_data_out on success or NULL on failure
 *---------------------------------------------------------------------------*/
{
    GpivPivData *piv_data_out = NULL;
    gchar *err_msg = NULL;
    gint i, j;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return NULL;
    }

    if ((piv_data_out = gpiv_alloc_pivdata (piv_data->nx, piv_data->ny))
        == NULL) {
        gpiv_warning ("gpiv_cp_piv_data: failing gpiv_alloc_piv_data");
        return NULL;
    }

/* #pragma omp parallel for shared(piv_data_out, piv_data) private(i, j) */
    for (i = 0; i < piv_data_out->ny; i++) {
        for (j = 0; j < piv_data_out->nx; j++) {
            piv_data_out->point_x[i][j] = piv_data->point_x[i][j];
            piv_data_out->point_y[i][j] = piv_data->point_y[i][j];
            piv_data_out->dx[i][j] = piv_data->dx[i][j];
            piv_data_out->dy[i][j] = piv_data->dy[i][j];
            piv_data_out->dz[i][j] = piv_data->dz[i][j];
            piv_data_out->snr[i][j] = piv_data->snr[i][j];
            piv_data_out->peak_no[i][j] = piv_data->peak_no[i][j];
        }
    }



#ifdef DEBUG2
    gpiv_write_pivdata (NULL, piv_data_out, FALSE);
#endif
    return piv_data_out;
}



gchar *
gpiv_ovwrt_pivdata                      (const GpivPivData      *piv_data_in,
                                        const GpivPivData       *piv_data_out
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Copies data from piv_data_in to piv_data_out.
 *
 * INPUTS:
 *     piv_data_in:   Input PIV data structure
 *
 *
 * RETURNS:
 *     piv_data_out on success or NULL on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    if (piv_data_in->nx != piv_data_out->nx
        || piv_data_in->ny != piv_data_out->ny) {
        err_msg = "gpiv_ovwrt_pivdata: input and output data are of differnet sizes";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

/*
 * Checking input data if allocated
 */
    if ((err_msg = gpiv_check_alloc_pivdata (piv_data_in)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


/*
 * Checking output data if allocated
 */
    if ((err_msg = gpiv_check_alloc_pivdata (piv_data_out)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

/*
 * Overwriting
 */
/* #pragma omp parallel for  shared(piv_data_out, piv_data_in) private(i, j) */
    for (i = 0; i < piv_data_in->ny; i++) {
        for (j = 0; j < piv_data_in->nx; j++) {
            piv_data_out->point_x[i][j] = piv_data_in->point_x[i][j];
            piv_data_out->point_y[i][j] = piv_data_in->point_y[i][j];
            piv_data_out->dx[i][j] = piv_data_in->dx[i][j];
            piv_data_out->dy[i][j] = piv_data_in->dy[i][j];
            piv_data_out->dz[i][j] = piv_data_in->dz[i][j];
            piv_data_out->snr[i][j] = piv_data_in->snr[i][j];
            piv_data_out->peak_no[i][j] = piv_data_in->peak_no[i][j];
        }
    }

#ifdef DEBUG2
    gpiv_write_pivdata (NULL, piv_data_out, FALSE);
#endif
    return err_msg;
}



gchar *
gpiv_0_pivdata                          (const GpivPivData      *piv_data
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Sets estimators, snr and peak_nr of piv_data to 0 or 0.0. 
 *      The structure will have to be allocated before (with 
 *      gpiv_alloc_pivdata).
 *
 * INPUTS:
 *      piv_data:   PIV data structure
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     gchar * to NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    /*     check_if_null(piv_data, "GPIV_0_PIVDATA: "); */
    /*     gchar *check_if_null(GpivPivData * piv_data, gchar * string) */
    if (piv_data->point_x == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->point_x == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->point_y == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->point_y == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->dx == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->dx == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->dy == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->dy == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->dz == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->dz == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->snr == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->snr == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->peak_no == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->peak_no == NULL";
        gpiv_warning ("%s", err_msg);
    }


    if (err_msg != NULL) {
        return err_msg;
    }


/* #pragma omp parallel for  shared(piv_data) private(i, j) */
    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            piv_data->dx[i][j] = 0.0;
            piv_data->dy[i][j] = 0.0;
            piv_data->dz[i][j] = 0.0;
            piv_data->snr[i][j] = 0.0;
            piv_data->peak_no[i][j] = 0;
        }
    }
    

    return NULL;
}



gchar *
gpiv_add_dxdy_pivdata                   (const GpivPivData      *piv_data_in,
                                        GpivPivData             *piv_data_out
                                        )
/*-----------------------------------------------------------------------------
 *      Adds displacements (dx, dy), snr and peak_nr from piv_data_in to 
 *      piv_data_out. Both structures will have to be allocated before 
 *      (with gpiv_alloc_pivdata) and of same dimansions.
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    if (piv_data_in->nx != piv_data_out->nx
        || piv_data_in->ny != piv_data_out->ny) {
        err_msg = "gpiv_add_dxdy_pivdata: piv_data_in and piv_data_out are of different dimensions";
        return err_msg;
    }

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data_in)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data_out)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

/* #pragma omp parallel for  shared(piv_data_out, piv_data_in) private(i, j) */
    for (i = 0; i < piv_data_out->ny; i++) {
        for (j = 0; j < piv_data_out->nx; j++) {
            piv_data_out->point_x[i][j] = piv_data_in->point_x[i][j];
            piv_data_out->point_y[i][j] = piv_data_in->point_y[i][j];
            piv_data_out->dx[i][j] += piv_data_in->dx[i][j];
            piv_data_out->dy[i][j] += piv_data_in->dy[i][j];
            piv_data_out->snr[i][j] = piv_data_in->snr[i][j];
            piv_data_out->peak_no[i][j] = piv_data_in->peak_no[i][j];
        }
    }
    

    return NULL;
}



gchar * 
gpiv_sum_dxdy_pivdata                   (const GpivPivData      *piv_data,
					gfloat                  *sum
                                        )
/*-----------------------------------------------------------------------------
 *      Adds all displacements in order to calculate residuals
 *      The structure will have to be allocated before (with 
 *      gpiv_alloc_pivdata).
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            *sum += piv_data->dx[i][j];
            *sum += piv_data->dy[i][j];
        }
    }
    

    return NULL;
}



#ifdef ENABLE_MPI
void
gpiv_piv_mpi_scatter_pivdata            (GpivPivData            *pd, 
                                        GpivPivData             *pd_scat, 
                                        guint                   nprocs
                              		)
/*-----------------------------------------------------------------------------
  Scatters PivData structure for parallel processing with mpi
  (NOT: except of pd->nx and pd->ny, which should broadcasted in forward
  in order to allocate memory.)

  INPUT:
  pd 
  nprocs: number of processes

  OUTPUT:
  pd_scat: scattered piv data
*/
{
    gint root = 0;


    if (MPI_Scatter(*pd->point_x, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, 
                    *pd_scat->point_x, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for point_x");
    }

    if (MPI_Scatter(*pd->point_y, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, 
                    *pd_scat->point_y, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for point_y");
    }

    if (MPI_Scatter(*pd->dx, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                    *pd_scat->dx, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for dx");
    }

    if (MPI_Scatter(*pd->dy, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                    *pd_scat->dy, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for dy");
    }

    if (MPI_Scatter(*pd->snr, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                    *pd_scat->snr, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for snr");
    }

    if (MPI_Scatter(*pd->peak_no, (pd->nx * pd->ny) / nprocs, MPI_INT, 
                    *pd_scat->peak_no, (pd->nx * pd->ny)/nprocs, MPI_INT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for peak_no");
    }
}



void 
gpiv_piv_mpi_gather_pivdata             (GpivPivData           *pd_scat, 
                                        GpivPivData            *pd, 
                                        guint                  nprocs
                                        )
/*-----------------------------------------------------------------------------
 Gathers PivData structure for parallel processing with mpi.

 INPUT:
 pd_scat: scattered piv data

 OUTPUT:
 pd: 
*/
{
    gint root = 0;


    if (MPI_Gather(*pd_scat->point_x, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, 
                   *pd->point_x, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->point_y, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, 
                   *pd->point_y, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->dx, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                   *pd->dx, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->dy, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                   *pd->dy, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->snr, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                   *pd->snr, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->peak_no, (pd->nx * pd->ny) / nprocs, MPI_INT, 
                   *pd->peak_no, (pd->nx * pd->ny)/nprocs, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }
}



gint * 
gpiv_piv_mpi_compute_counts            (const guint             nx, 
                                        const guint             ny
                                        )
/*-----------------------------------------------------------------------------
 * Calculates counts and displc for scatterv rows of data
 */
{
    gint *counts = NULL;
    gint i, nprocs, rank;


    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_size(MPI_COMM_WORLD, &rank);

    counts = gpiv_ivector (nprocs);
    counts[0] = (ny  / nprocs + (0 < (ny)%nprocs ? 1 : 0)) * nx;
#ifdef DEBUG
    if (rank ==0) g_message ("mpi_compute_counts:: counts[0] = %d", 
                             counts[0]);
#endif

    for (i = 1; i < nprocs; i++) {
        counts[i] = (ny  / nprocs + (i < (ny)%nprocs ? 1 : 0)) * nx;
#ifdef DEBUG
        if (rank ==0) g_message ("mpi_compute_counts:: counts[%d] = %d", 
                                 i, counts[i]);
#endif
    }


    return counts;
}


gint * 
gpiv_piv_mpi_compute_displs            (gint                   *counts, 
                                        const guint             nx, 
                                        const guint             ny
					)
/*-----------------------------------------------------------------------------
 * Calculates displc for scatterv rows of data
 * As rows are scattered, nx is needed for correctly calculation of 
 * displacements
 */
{
    gint *displs = NULL;
    gint i, nprocs, rank;


    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_size(MPI_COMM_WORLD, &rank);

    displs = gpiv_ivector (nprocs);
    displs[0] = 0;
#ifdef DEBUG
    if (rank ==0) g_message ("mpi_compute_displs:: counts[0] = %d", 
                             displs[0]);
#endif

    for (i = 1; i < nprocs; i++) {
        displs[i] = displs[i-1] + counts[i-1];
#ifdef DEBUG
        if (rank ==0) g_message ("mpi_compute_displs:: displs[%d] = %d", 
                                 i, displs[i]);
#endif
    }


    return displs;
}



void
gpiv_piv_mpi_scatterv_pivdata          (GpivPivData             *pd, 
                                        GpivPivData             *pd_scat, 
                                        gint                    *counts,
                                        gint                    *displs
					)
/*-----------------------------------------------------------------------------
  Scatters PivData structure for parallel processing with mpi
  except of pd->nx and pd->ny, which should already be broadcasted for
  memory allocation.)

  INPUT:
  pd 
  nprocs: number of processes

  OUTPUT:
  pd_scat: scattered piv data
*/
{
    gint root = 0, rank;



    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#ifdef DEBUG
    g_message ("mpi_scatterv_pivdata:: rank=%d counts[%d]=%d nx=%d ny=%d", 
               rank, rank, counts[rank], pd_scat->nx, pd_scat->ny);
#endif

    if (MPI_Scatterv(*pd->point_x, counts, displs, MPI_FLOAT, 
                    *pd_scat->point_x, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for point_x");
    }

    if (MPI_Scatterv(*pd->point_y, counts, displs, MPI_FLOAT, 
                    *pd_scat->point_y, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for point_y");
    }

    if (MPI_Scatterv(*pd->dx, counts, displs, MPI_FLOAT, 
                    *pd_scat->dx, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for dx");
    }

    if (MPI_Scatterv(*pd->dy, counts, displs, MPI_FLOAT, 
                    *pd_scat->dy, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for dy");
    }

    if (MPI_Scatterv(*pd->snr, counts, displs, MPI_FLOAT, 
                    *pd_scat->snr, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for snr");
    }

    if (MPI_Scatterv(*pd->peak_no, counts, displs, MPI_INT, 
                    *pd_scat->peak_no, counts[rank], MPI_INT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for peak_no");
    }

}



void 
gpiv_piv_mpi_gatherv_pivdata           (GpivPivData             *pd_scat, 
                                        GpivPivData             *pd, 
                                        gint                    *counts,
                                        gint                    *displs
					)
/*-----------------------------------------------------------------------------
 Gathers PivData structure for parallel processing with mpi

 INPUT:
 pd_scat: scattered piv data
 comm: mpi common

 OUTPUT:
 pd: 
*/
{
    gint root = 0, rank;



    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#ifdef DEBUG
    g_message ("mpi_gatherv_pivdata:: 0 rank=%d counts[%d]=%d", 
               rank, rank, counts[rank]);
#endif

    if (MPI_Gatherv(*pd_scat->point_x, counts[rank], MPI_FLOAT, 
                   *pd->point_x, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->point_y, counts[rank], MPI_FLOAT, 
                   *pd->point_y, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->dx, counts[rank], MPI_FLOAT, 
                   *pd->dx, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->dy, counts[rank], MPI_FLOAT, 
                   *pd->dy, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->snr, counts[rank], MPI_FLOAT, 
                   *pd->snr, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->peak_no, counts[rank], MPI_INT, 
                   *pd->peak_no, counts, displs, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }
}



void 
gpiv_piv_mpi_bcast_pivdata             (GpivPivData             *pd
                                        )
/*-----------------------------------------------------------------------------
 Gathers PivData structure for parallel processing with mpi
 */
{
    gint root = 0;


    if (
        MPI_Bcast(*pd->point_x, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->point_y, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->dx, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->dy, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->snr, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->peak_no, pd->nx * pd->ny, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }
}
#endif /* ENABLE_MPI */

/*
 * Read / write functions
 */
 
enum GpivDataFormat
gpiv_find_pivdata_origin               (const gchar            line[GPIV_MAX_CHARS]
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Determines the name of the program that generated the data
 *
 * PROTOTYPE LOCATATION:
 *      io.h
 *
 * INPUTS:
 *      line:           character line containing program that generated data
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      pdf:            enumerator of data origin
 *---------------------------------------------------------------------------*/
{
    enum GpivDataFormat pdf;


    if (strstr(line, "rr") != '\0') {
        pdf = GPIV_RR;
    } else if (strstr(line, "DaVis") != '\0') { 
        pdf = GPIV_DAV;
    } else {
/* fprintf (stderr,"\n%s %s warning: No format defined: taking 'rr' format", */
/* 	     LIBNAME,function_name); */
        pdf = GPIV_RR;
    }


    return pdf;
}



GpivPivData *
gpiv_fread_pivdata                     (const gchar            *fname
                                        )
/*-----------------------------------------------------------------------------
 *      Reads PIV data from file fname
 */
{
    FILE *fp = NULL;
    GpivPivData *piv_data = NULL;


    if ((fp = fopen (fname, "rb")) == NULL) {
        gpiv_warning ("gpiv_fread_pivdata: failing fopen %s", fname);
        return NULL;
    }

    if ((piv_data = gpiv_read_pivdata (fp)) == NULL) {
        gpiv_warning ("gpiv_fread_pivdata: failing gpiv_fread_pivdata");
        return NULL;
    }


    fclose (fp);
    return piv_data;
}



gchar *
gpiv_fwrite_pivdata                    (const gchar            *fname,
                                        GpivPivData             *piv_data,
                                        const gboolean          free
                                        )
/*-----------------------------------------------------------------------------
 *      Writes PIV data to file fname
 */
{
    FILE *fp = NULL;
    gchar *err_msg = NULL;


    if ((fp = fopen (fname, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_pivdata: failing fopen %s", fname);
        return "gpiv_fwrite_pivdata: failing fopen";
    }

    if ((err_msg = gpiv_write_pivdata (fp, piv_data, free)) != NULL) {
/*         gpiv_warning ("gpiv_fwrite_pivdata: failing gpiv_write_pivdata"); */
        return err_msg;
    }


    fclose (fp);
    return err_msg;
}



GpivPivData *
gpiv_read_pivdata                      (FILE                   *fp
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Reads data from fp
 *
 *---------------------------------------------------------------------------*/
{
    GpivPivData *piv_data = NULL;

/*
 * reads the data from stdin or file pointer
 */
    if (fp == stdin || fp == NULL) {
        piv_data = read_pivdata_from_stdin (FALSE);
    } else {
        piv_data = read_pivdata_from_file (fp, FALSE);
    }


    return piv_data;
}



GpivPivData *
gpiv_read_pivdata_fastx                (FILE                   *fp
                                        )
/*  ---------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads data from ascii data file with fast running x-position variables 
 *     (1st column in data stream)
 *
 *-------------------------------------------------------------------------- */
{
    GpivPivData *piv_data = NULL;


/*
 * reads the data from stdin or file pointer
 */
    if (fp == stdin || fp == NULL) {
        piv_data = read_pivdata_from_stdin (TRUE);
    } else {
        piv_data = read_pivdata_from_file (fp, TRUE);
    }


    return piv_data;
}



gchar *
gpiv_write_pivdata                     (FILE                   *fp,
                                        GpivPivData             *piv_data,
                                        const gboolean          free
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes PIV data to fp
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j;

/*     g_message ("gpiv_write_pivdata:: 0"); */
    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }
    

    if (fp == stdout || fp == NULL) {
/*
 * Use stdout
 * Writing comment
 */
        if (piv_data->comment != NULL) {
            printf ("%s", piv_data->comment);
        }

/*
 * Writing data
 */
        if (piv_data->scale == TRUE) {
            printf ("\n# x(m)        y(m)         Vx(m/s)      Vy(m/s)      snr        peak#\n");
            for (i = 0; i < piv_data->ny; i++) { 
                for (j = 0; j < piv_data->nx; j++) {
                    printf (GPIV_PIV_S_FMT, 
                            piv_data->point_x[i][j], 
                            piv_data->point_y[i][j], 
                            piv_data->dx[i][j], 
                            piv_data->dy[i][j], 
                            piv_data->snr[i][j], 
                            piv_data->peak_no[i][j]);
                }
            }
            
        } else {
            printf ("\n# x(px) y(px)    dx(px)       dy(px)       snr  peak#\n");
            for (i = 0; i < piv_data->ny; i++) {
                for (j = 0; j < piv_data->nx; j++) {
                    printf (GPIV_PIV_FMT, 
                             piv_data->point_x[i][j], 
                             piv_data->point_y[i][j], 
                             piv_data->dx[i][j], 
                             piv_data->dy[i][j], 
                             piv_data->snr[i][j], 
                             piv_data->peak_no[i][j]
                             );      
                }
            }
        }


    } else {
/*
 * Use fp
 * Writing comment
 */
    if (piv_data->comment != NULL) {
        fprintf (fp, "%s", piv_data->comment);
    }

/*
 * Writing data
 */
        if (piv_data->scale == TRUE) {
            fprintf (fp, "\n# x(m)        y(m)         Vx(m/s)      Vy(m/s)      snr        peak#\n");
            for (i = 0; i < piv_data->ny; i++) { 
                for (j = 0; j < piv_data->nx; j++) {
                    fprintf (fp, GPIV_PIV_S_FMT, 
                             piv_data->point_x[i][j], 
                             piv_data->point_y[i][j], 
                             piv_data->dx[i][j], 
                             piv_data->dy[i][j], 
                             piv_data->snr[i][j], 
                             piv_data->peak_no[i][j]);
                }
            }
            
        } else {
            fprintf (fp, "\n# x(px) y(px)    dx(px)       dy(px)       snr  peak#\n");
            for (i = 0; i < piv_data->ny; i++) {
                for (j = 0; j < piv_data->nx; j++) {
                    fprintf (fp, GPIV_PIV_FMT, 
                             piv_data->point_x[i][j], 
                             piv_data->point_y[i][j], 
                             piv_data->dx[i][j], 
                             piv_data->dy[i][j], 
                             piv_data->snr[i][j], 
                             piv_data->peak_no[i][j]
                             );      
                }
            }
        }
        
        fflush (fp);
    }


    if (free) gpiv_free_pivdata (piv_data);
    return err_msg;
}



GpivPivData *
gpiv_fread_hdf5_pivdata                (const gchar            *fname,
                                        const gchar             *DATA_KEY
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads piv data from hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    GpivPivData *piv_data = NULL;
    gchar *err_msg = NULL;
    guint nx, ny;


    if ((err_msg = my_utils_fcount_hdf5_data (fname, &nx, &ny)) != NULL)  {
        g_message ("gpiv_fread_hdf5_pivdata: %s", err_msg);
        return NULL;
    }

    if ((piv_data = gpiv_alloc_pivdata (nx, ny)) == NULL) {
        g_message ("gpiv_fread_hdf5_pivdata: failing gpiv_alloc_pivdata");
        return NULL;
    }

    if ((err_msg = fread_hdf5_piv_position (fname, piv_data)) != NULL)  {
        g_message ("gpiv_fread_hdf5_pivdata: %s", err_msg);
        return NULL;
    }
    g_free (piv_data->comment);

    if ((err_msg = fread_hdf5_pivdata (fname, piv_data, DATA_KEY)) != NULL)  {
        g_message ("gpiv_fread_hdf5_pivdata: %s", err_msg);
        return NULL;
    }



    return piv_data;
}



gchar *
gpiv_fwrite_hdf5_pivdata               (const gchar             *fname, 
                                        GpivPivData             *piv_data, 
                                        const gchar             *DATA_KEY,
                                        const gboolean          free
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes PIV data to file in hdf version 5 format
 *
*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = fwrite_hdf5_pivdata (fname, piv_data, DATA_KEY)) != NULL) {
        gpiv_error ("gpiv_fwrite_hdf5_pivdata: %s", err_msg);
    }

    if ((err_msg = fwrite_hdf5_piv_position (fname, piv_data)) != NULL) {
        gpiv_error ("gpiv_fwrite_hdf5_piv_position: %s", err_msg);
    }

    if (free) gpiv_free_pivdata (piv_data);


    return err_msg;
}



/*
 * Local functions
 */

static gchar *
fread_hdf5_piv_position                 (const gchar            *fname, 
                                         GpivPivData            *piv_data
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads PIV position data from hdf5 data file into GpivPivData struct
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;


    g_return_val_if_fail (piv_data->point_x != NULL, "piv_data->point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, "piv_data->point_y != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_fread_hdf5_piv_position: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
/*
 * POSITION group with RCSID comment and scale attribute
 */
    group_id = H5Gopen (file_id, "POSITIONS", H5P_DEFAULT);
    H5Gget_comment (group_id, ".", GPIV_MAX_CHARS, RCSID);
    piv_data->comment = g_strdup (RCSID);
    attribute_id = H5Aopen_name(group_id, "scale");
    H5Aread(attribute_id, H5T_NATIVE_INT, &piv_data->scale); 
    H5Aclose(attribute_id); 

/*
 * point_x
 */
    dataset_id = H5Dopen(group_id, "point_x", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    rank_x = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_x != 1) {
        err_msg = "gpiv_fread_hdf5_piv_position: rank_x != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    piv_data->nx = dims[0];
    point_x_hdf = gpiv_vector(piv_data->nx) ;
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, point_x_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
	

/*
 * point_y
 */
    dataset_id = H5Dopen(group_id, "point_y", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    rank_y = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_y != 1) {
        err_msg = "gpiv_fread_hdf5_piv_position: rank_y != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    piv_data->ny = dims[0];
    point_y_hdf = gpiv_vector(piv_data->ny) ;
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, point_y_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * filling GpivPivData structure with point_x an point_y
 */
    for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {
                piv_data->point_x[i][j] = point_x_hdf[j];
                piv_data->point_y[i][j] = point_y_hdf[i];
            }
    }
    gpiv_free_vector(point_x_hdf);
    gpiv_free_vector(point_y_hdf);


    H5Gclose (group_id);
    status = H5Fclose(file_id);
    return err_msg;
}



static gchar *
fwrite_hdf5_piv_position                (const gchar            *fname, 
                                         GpivPivData            *piv_data
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writess PIV position data to hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;

/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, 
        dataspace_id, str_MC; 
    hsize_t     dims[2], dims_attrib[1];
    herr_t      status;


    g_return_val_if_fail (piv_data->point_x != NULL, "piv_data->point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, "piv_data->point_y != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_fwrite_hdf5_piv_position: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);

/*
 * POSITION group with RCSID comment and scale attribute
 */
    group_id = H5Gopen (file_id, "POSITIONS", H5P_DEFAULT);
    H5Gset_comment (group_id, ".", piv_data->comment);
    dims_attrib[0] = 1;
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "scale", H5T_NATIVE_INT, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_INT, &piv_data->scale); 
    H5Aclose(attribute_id); 

/*
 * data dimension
 */
    str_MC = H5Tcopy (H5T_C_S1);
    H5Tset_size (str_MC, GPIV_MAX_CHARS);
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "dimension", str_MC, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    if (piv_data->scale) {
        H5Awrite(attribute_id, str_MC, "Positions in m");
    } else {
        H5Awrite(attribute_id, str_MC, "Positions in pixels");
    }

    H5Aclose(attribute_id); 

/*
 * point_x
 */
    point_x_hdf = gpiv_vector(piv_data->nx);
    for (i = 0; i < piv_data->nx; i++) point_x_hdf[i] = piv_data->point_x[0][i];
    dims[0] = piv_data->nx; 
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "point_x", H5T_NATIVE_FLOAT, dataspace_id, 
                           H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                      H5P_DEFAULT, point_x_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    gpiv_free_vector(point_x_hdf);

/*
 * point_y
 */
    point_y_hdf = gpiv_vector(piv_data->ny);
    for (i = 0; i < piv_data->ny; i++) point_y_hdf[i] = piv_data->point_y[i][0];
    dims[0] = piv_data->ny; 
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "point_y", H5T_NATIVE_FLOAT, 
                           dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                      H5P_DEFAULT, point_y_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    gpiv_free_vector(point_y_hdf);

/* 
 *Closing group and file
 */
    status = H5Gclose (group_id);
    status = H5Fclose(file_id); 

    return err_msg;
}



static gchar *
fread_hdf5_pivdata                      (const gchar            *fname, 
                                         GpivPivData            *piv_data,
                                         const gchar            *DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads _only_ PIV displacements (no positions) from hdf5 data file
 *     into a GpivPivData struct
 *
 *---------------------------------------------------------------------------*/
{    
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    gint i, j, rank_x, rank_y, rank_vx, rank_vy;
    gfloat *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;


/*     g_return_val_if_fail (piv_data->nx == 0); */
/*     g_return_val_if_fail (piv_data->nx == 0); */
    g_return_val_if_fail (piv_data->point_x != NULL, "(piv_data->point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, "(piv_data->point_y != NULL");
    g_return_val_if_fail (piv_data->dx != NULL, "piv_data->dx != NULL");
    g_return_val_if_fail (piv_data->dy != NULL, "piv_data->dy != NULL");
    g_return_val_if_fail (piv_data->dz != NULL, "piv_data->dz != NULL");
    g_return_val_if_fail (piv_data->snr != NULL, "piv_data->snr != NULL");
    g_return_val_if_fail (piv_data->peak_no != NULL, "piv_data->peak_no != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_fread_hdf5_pivdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

/*
 * PIV group with RCSID comment and scale attribute
 */
    group_id = H5Gopen (file_id, DATA_KEY, H5P_DEFAULT);
    H5Gget_comment (group_id, ".", GPIV_MAX_CHARS, RCSID);
    piv_data->comment = g_strdup (RCSID);

    attribute_id = H5Aopen_name(group_id, "scale");
    H5Aread(attribute_id, H5T_NATIVE_INT, &piv_data->scale); 
    H5Aclose(attribute_id); 


/*
 * dx
 */
    dataset_id = H5Dopen(group_id, "dx", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->dx[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * dy
 */
    dataset_id = H5Dopen(group_id, "dy", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->dy[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * dz
 */
    dataset_id = H5Dopen(group_id, "dz", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->dz[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * snr
 */
    dataset_id = H5Dopen(group_id, "snr", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->snr[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * peak_no
 */
    dataset_id = H5Dopen(group_id, "peak_no", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->peak_no[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);



    status = H5Gclose (group_id);
    status = H5Fclose(file_id); 
    return err_msg;
}



static gchar *
fwrite_hdf5_pivdata                     (const gchar            *fname, 
                                         GpivPivData            *piv_data, 
                                         const gchar            *DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes _only_ PIV displacements (no positions) to file in hdf version 
 *     5 format
 *
*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j, k, line_nr = 0;
    gfloat *point_x_hdf = NULL,  *point_y_hdf = NULL;
    const  gchar *string = "RcsId";
    gchar *dir /*=  "/point_x" */;
    gint point = 5;

/*
 * HDF file, dataset,-space identifier
 */
    hid_t       file_id, dataset_id, dataspace_id, 
        group_id, attribute_id,
        str_MC; 
    hsize_t     dims[2], dims_attrib[1];
    herr_t      status;	      


    g_return_val_if_fail (piv_data->point_x != NULL, "piv_data->point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, "piv_data->point_y != NULL");
    g_return_val_if_fail (piv_data->dx != NULL, "piv_data->dx != NULL");
    g_return_val_if_fail (piv_data->dy != NULL, "piv_data->dy != NULL");
    g_return_val_if_fail (piv_data->dz != NULL, "piv_data->dz != NULL");
    g_return_val_if_fail (piv_data->snr != NULL, "piv_data->snr != NULL");
    g_return_val_if_fail (piv_data->peak_no != NULL, "piv_data->peak_no != NULL");

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fwrite_hdf5_pivdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
     file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);

/*
 * PIV group with RCSID comment and scale attribute
 */
    group_id = H5Gopen (file_id, DATA_KEY, H5P_DEFAULT);
    H5Gset_comment (group_id, ".", piv_data->comment);
    dims_attrib[0] = 1;
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "scale", H5T_NATIVE_INT, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_INT, &piv_data->scale); 
    H5Aclose(attribute_id); 
    H5Sclose(dataspace_id);

/*
 * data dimension
 */
    str_MC = H5Tcopy (H5T_C_S1);
    H5Tset_size (str_MC, GPIV_MAX_CHARS);
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "dimension", str_MC, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    if (piv_data->scale) {
        H5Awrite(attribute_id, str_MC, "PIV estimators in m/s");
    } else {
        H5Awrite(attribute_id, str_MC, "PIV estimators in pixels");
    }
    H5Aclose(attribute_id); 
    H5Sclose(dataspace_id);


    dims[0] = piv_data->ny; 
    dims[1] = piv_data->nx;

/* 
 * dx
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "dx", H5T_NATIVE_FLOAT, 
                          dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT, &piv_data->dx[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * dy
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "dy", H5T_NATIVE_FLOAT, 
                          dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT,&piv_data->dy[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * dz
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "dz", H5T_NATIVE_FLOAT, 
                          dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT,&piv_data->dz[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * snr
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "snr", H5T_NATIVE_FLOAT, 
			  dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT,&piv_data->snr[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * peak_no
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "peak_no", H5T_NATIVE_INT, dataspace_id, 
			  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
		     &piv_data->peak_no[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);


   status = H5Gclose (group_id);
   status = H5Fclose(file_id); 
   return err_msg;
}



static GpivPivData *
read_pivdata_from_file (FILE *fp,
                        gboolean fastx
                        )
/*-----------------------------------------------------------------------------
 * Reads PIV data from file pointer
 */
{
    GpivPivData *piv_data = NULL;
    gint i = 0, j = 0;
    gchar line[GPIV_MAX_CHARS];

    guint nx = 0, ny = 0;
    gboolean scale;


    scale = my_utils_count_asciidata (fp, &nx, &ny);
    piv_data = gpiv_alloc_pivdata (nx, ny);
    piv_data->scale = scale;

    fseek (fp, 0, SEEK_SET);
    while ((i < piv_data->ny - 1) || (j < piv_data->nx - 1)) {
        fgets (line, GPIV_MAX_CHARS, fp);
        if (fastx) {
            obtain_pivdata_fastx_fromline (line, piv_data, &i, &j);
        } else {
            obtain_pivdata_fromline (line, piv_data, &i, &j);
        }
    }
    fgets (line, GPIV_MAX_CHARS, fp);
    if (fastx) {
        obtain_pivdata_fastx_fromline (line, piv_data, &i, &j);
    } else {
        obtain_pivdata_fromline (line, piv_data, &i, &j);
    }    


    return piv_data;
}


static GpivPivData *
read_pivdata_from_stdin (gboolean fastx
                         )
/*-----------------------------------------------------------------------------
 * Reads PIV data from file pointer
 */
{
    GpivPivData *piv_data = NULL;
    gint i = 0, j = 0, k = 0;
    gchar line[GPIV_MAX_LINES][GPIV_MAX_CHARS];

    guint nx = 0, ny = 0;
    gboolean scale = FALSE;
    gulong bufsize = 2500;

    gboolean increment = FALSE;
    gfloat x = 0, y = 0;
    gfloat dat_x = -10.0e5, dat_y = -10.0e5;
    gint line_nr = 0; 

    gboolean bool = FALSE;


    /* 
     * Count nx, ny, similar to count_asciidata
     */


    k = 0;
    while (gets (line[k]) != NULL) {
        my_utils_obtain_nxny_fromline (line[k], &scale, &nx, &ny, 
                              &increment, &dat_x, &dat_y, &line_nr);
        k++;
    }
    nx = nx + 1;
    ny = ny + 1;
    piv_data = gpiv_alloc_pivdata (nx, ny);
    piv_data->scale = scale;


    k = 0;
    i = 0;
    j = 0;
    while ((i < piv_data->ny - 1) || (j < piv_data->nx - 1)) {
        if (fastx) {
            obtain_pivdata_fastx_fromline (line[k], piv_data, &i, &j);
        } else {
            obtain_pivdata_fromline (line[k], piv_data, &i, &j);
        }
        k++;
    }
    if (fastx) {
        obtain_pivdata_fastx_fromline (line[k], piv_data, &i, &j);
    } else {
        obtain_pivdata_fromline (line[k], piv_data, &i, &j);
    }


    return piv_data;
}



static void
obtain_pivdata_fromline (gchar line[], 
                         GpivPivData *piv_data,
                         guint *i,
                         guint *j
                         )
/*-----------------------------------------------------------------------------
 * Scans a line to get GpivPivData
 */
{

/*
 * Fill in comment, skip header
 */
    if (line[0] == '#'
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
        if (piv_data->comment == NULL) {
            piv_data->comment = g_strdup (line);
        } else {
            piv_data->comment = g_strconcat (piv_data->comment, line, NULL);
        }
        /*             printf ("\n Commentaar: %s\n\n", piv_data->comment); */
            

    } else if (line[0] != '\n'
               && line[0] != '\t'
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
/*
 * Fill in PIV data, skip empty lines and header
 */
        sscanf (line, "%f %f %f %f %f %d", 
                &piv_data->point_x[*i][*j], 
                &piv_data->point_y[*i][*j],
                &piv_data->dx[*i][*j], 
                &piv_data->dy[*i][*j], 
                &piv_data->snr[*i][*j], 
                &piv_data->peak_no[*i][*j]);
        if (*j < piv_data->nx - 1) {
            *j = *j + 1; 
        } else if (*i < piv_data->ny - 1) { 
            *j = 0; 
            *i = *i + 1; 
            /*             } else { */
            /*                 break;  */
        }
    }


    return;
}



static void
obtain_pivdata_fastx_fromline (gchar line[], 
                               GpivPivData *piv_data,
                               guint *i,
                               guint *j
                               )
/*-----------------------------------------------------------------------------
 * Scans a line to get GpivPivData, but with column (x-position) as inner loop 
 * variable
 */
{

/*
 * Fill in comment, skip header
 */
    if (line[0] == '#'
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
        if (piv_data->comment == NULL) {
            piv_data->comment = g_strdup (line);
        } else {
            piv_data->comment = g_strconcat (piv_data->comment, line, NULL);
        }
            
    } else if (line[0] != '\n'
               && line[0] != '\t'
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
        /*
         * Fill in data, skip empty lines and header
         */
        sscanf (line,"%f %f %f %f %f %d", 
                &piv_data->point_x[*i][*j], 
                &piv_data->point_y[*i][*j],
                &piv_data->dx[*i][*j], 
                &piv_data->dy[*i][*j], 
                &piv_data->snr[*i][*j], 
                &piv_data->peak_no[*i][*j]);
        /*
         * Increase first index (i) here!
         */
        if (*i < piv_data->nx - 1) {
            *i = *i + 1; 
        } else if (*j < piv_data->ny - 1) { 
            *i = 0; 
            *j = *j + 1; 
        }
    }


    return;
}



