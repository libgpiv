/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-----------------------------------------------------------------------
FILENAME:                valid_par.c
LIBRARY:                 libgpiv
EXTERNAL FUNCTIONS:      
                         gpiv_valid_default_parameters
                         gpiv_valid_get_parameters_from_resources
                         gpiv_valid_parameters_set
                         gpiv_valid_read_parameters
                         gpiv_valid_testonly_parameters
			 gpiv_valid_testadjust_parameters
			 gpiv_valid_fprint_parameters
                         gpiv_valid_cp_parameters
                         valid_ovwrt_parameters
                         gpiv_valid_fread_hdf5_parameters
                         gpiv_valid_fwrite_hdf5_parameters

LAST MODIFICATION DATE: $Id: valid_par.c,v 1.9 2008-09-25 13:19:54 gerber Exp $
--------------------------------------------------------------------- */
#include <gpiv.h>


/*
 * Default values and keys of GpivValidPar
 */
const gfloat VALIDPAR_DEFAULT__DATA_YIELD      = 0.95;    /**< Default parameter for data_yield of __GpivValidPar */
const gfloat VALIDPAR_DEFAULT__RESIDU_MAX      = GPIV_RESIDU_MAX_NORMMEDIAN;    /**< Default parameter for residu_max of __GpivValidPar */

enum {
  VALIDPAR_DEFAULT__RESIDU_TYPE     = 2,       /**< Default parameter for residu_type of __GpivValidPar */
  VALIDPAR_DEFAULT__SUBST_TYPE      = 2,       /**< Default parameter for subst_type of __GpivValidPar */
  VALIDPAR_DEFAULT__HISTO_TYPE      = 0,       /**< default parameter for histo_type of __GpivValidPar */
  VALIDPAR_DEFAULT__ARRAY_LENGTH    = 3,       /**< Default parameter for array_length of __GpivValidPar */
  VALIDPAR_DEFAULT__NEIGHBORS       = 3        /**< Default parameter for neighbors of __GpivValidPar */
};


const gchar *VALIDPAR_KEY__DATA_YIELD         = "Data_yield";   /**< Key for data_yield of __GpivValidPar */
const gchar *VALIDPAR_KEY__RESIDU_MAX         = "Residu_max";   /**< Key for residu_max of __GpivValidPar */
const gchar *VALIDPAR_KEY__RESIDU_TYPE        = "Residu_type";  /**< Key for residu_type of __GpivValidPar */
const gchar *VALIDPAR_KEY__SUBST_TYPE         = "Subst_type";   /**< Key for subst_type of __GpivValidPar */
const gchar *VALIDPAR_KEY__HISTO_TYPE         = "Histo_type";   /**< default parameter for histo_type of __GpivValidPar */
const gchar *VALIDPAR_KEY__ARRAY_LENGTH       = "";             /**< Key for array_length of __GpivValidPar */
const gchar *VALIDPAR_KEY__NEIGHBORS          = "Neighbors";    /**< Key for neighbors of __GpivValidPar */


/*
 * Prototypes local functions
 */

static void
valid_ovwrt_parameters                  (const GpivValidPar     *valid_par_src,
                                         GpivValidPar           *valid_par_dest,
                                         const gboolean         force
                                         );

static herr_t 
attr_info                               (hid_t                  loc_id, 
                                         const char             *name, 
                                         GpivValidPar           *valid_par
                                         );

static void
obtain_valdipar_fromline                (char                   line[], 
                                         GpivValidPar           *image_par, 
                                         gboolean               verbose
                                         );

/*
 * Public functions
 */

void
gpiv_valid_parameters_set               (GpivValidPar           *valid_par,
                                         const gboolean          flag
                                         )
/*-----------------------------------------------------------------------------
 * Set flag for valid_par __set */
{
    valid_par->data_yield__set = flag;
    valid_par->residu_max__set = flag;
    valid_par->neighbors__set = flag;
    valid_par->residu_type__set = flag;
    valid_par->subst_type__set = flag;
    valid_par->histo_type__set = flag;
}



void
gpiv_valid_default_parameters           (GpivValidPar           *valid_par_default,
                                         const gboolean         force
                                         )
/*-----------------------------------------------------------------------------
 * Default parameter values
 */
{
    if (!valid_par_default->data_yield__set || force)
        valid_par_default->data_yield = VALIDPAR_DEFAULT__DATA_YIELD;
    if (!valid_par_default->residu_max__set || force)
        valid_par_default->residu_max = VALIDPAR_DEFAULT__RESIDU_MAX;
    if (!valid_par_default->neighbors__set || force)
        valid_par_default->neighbors = VALIDPAR_DEFAULT__NEIGHBORS;
    if (!valid_par_default->residu_type__set || force)
        valid_par_default->residu_type = VALIDPAR_DEFAULT__RESIDU_TYPE;
    if (!valid_par_default->subst_type__set || force)
        valid_par_default->subst_type = VALIDPAR_DEFAULT__SUBST_TYPE;
    if (!valid_par_default->histo_type__set || force)
        valid_par_default->histo_type = VALIDPAR_DEFAULT__HISTO_TYPE;

    gpiv_valid_parameters_set(valid_par_default, TRUE);
}



GpivValidPar *
gpiv_valid_get_parameters_from_resources(const gchar            *localrc,
                                         const gboolean         verbose
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads validation parameters from system-wide gpiv.conf and $HOME/.gpivrc
 *---------------------------------------------------------------------------*/
{
    GpivValidPar *valid_par = g_new0 (GpivValidPar, 1);


    gpiv_valid_parameters_set (valid_par, FALSE);
    gpiv_scan_parameter (GPIV_VALIDPAR_KEY, localrc, valid_par, verbose);
    gpiv_scan_resourcefiles (GPIV_VALIDPAR_KEY, valid_par, verbose);


    return valid_par;
}



void 
gpiv_valid_read_parameters              (FILE                   *fp,
                                         GpivValidPar           *valid_par,
                                         const gboolean         verbose
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Read all parameters for PIV data validation 
 *     if not defined by command line keys
 *
 * PROTOTYPE LOCATATION:
 *     valid.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    
    char line[GPIV_MAX_CHARS], par_name[GPIV_MAX_CHARS];


    if (fp == stdin || fp == NULL) {
        while (gets (line) != NULL) {
            obtain_valdipar_fromline (line, valid_par, verbose);
        }
    } else {
        while (fgets (line, GPIV_MAX_CHARS, fp) != NULL) {
            obtain_valdipar_fromline (line, valid_par, verbose);
        }
    }


    return;
}



gchar *
gpiv_valid_check_parameters_read        (GpivValidPar           *valid_par,
                                         const GpivValidPar     *valid_par_default
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Check out if all parameters have been read
 *     Returns: NULL on success or *err_msg on failure
 *
 * PROTOTYPE LOCATATION:
 *     valid.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if (valid_par->data_yield__set == FALSE) {
        valid_par->data_yield__set = TRUE;
        if (valid_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            valid_par->data_yield = VALIDPAR_DEFAULT__DATA_YIELD;
        } else {
            err_msg = "Using default: ";
            valid_par->data_yield = valid_par_default->data_yield;
        }
        gpiv_warning("%s\n%s.%s %f", 
                     err_msg, 
                     GPIV_VALIDPAR_KEY,
                     VALIDPAR_KEY__DATA_YIELD,
                     valid_par->data_yield);
    }
    
    /*
     * Optional
     */
    if (valid_par->residu_max__set == FALSE) {
        valid_par->residu_max__set = TRUE;
        if (valid_par_default == NULL) {
            valid_par->residu_max = VALIDPAR_DEFAULT__RESIDU_MAX;
             err_msg = "Using default from libgpiv: ";
       } else {
            err_msg = "Using default: ";
            valid_par->residu_max = valid_par_default->residu_max;
        }
        gpiv_warning("%s\n%s.%s %f", 
                     err_msg, 
                     GPIV_VALIDPAR_KEY,
                     VALIDPAR_KEY__RESIDU_MAX,
                     valid_par->residu_max);
    }
    
    if (valid_par->neighbors__set == FALSE) {
        valid_par->neighbors__set = TRUE;
        if (valid_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            valid_par->neighbors = VALIDPAR_DEFAULT__NEIGHBORS;
        } else {
            err_msg = "Using default: ";
            valid_par->neighbors = valid_par_default->neighbors;
        }
       gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_VALIDPAR_KEY,
                     VALIDPAR_KEY__NEIGHBORS,
                     valid_par->neighbors);
    }
    
    /*
     * Optional:
     */
    if (valid_par->residu_type__set == FALSE) {
        valid_par->residu_type__set = TRUE;
        if (valid_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            valid_par->residu_type = VALIDPAR_DEFAULT__RESIDU_TYPE;
        } else {
            err_msg = "Using default: ";
            valid_par->residu_type = valid_par_default->residu_type;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_VALIDPAR_KEY,
                     VALIDPAR_KEY__RESIDU_TYPE,
                     valid_par->residu_type);
    }
    
    if (valid_par->subst_type__set == FALSE) {
        valid_par->subst_type__set = TRUE;
        if (valid_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            valid_par->subst_type = VALIDPAR_DEFAULT__SUBST_TYPE;
        } else {
            err_msg = "Using default: ";
            valid_par->subst_type = valid_par_default->subst_type;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_VALIDPAR_KEY,
                     VALIDPAR_KEY__SUBST_TYPE,
                     valid_par->subst_type);
    }
    
    if (valid_par->histo_type__set == FALSE) {
        valid_par->histo_type__set = TRUE;
        if (valid_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            valid_par->histo_type = VALIDPAR_DEFAULT__HISTO_TYPE;
        } else {
            err_msg = "Using default: ";
            valid_par->histo_type = valid_par_default->histo_type;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg,
                     GPIV_VALIDPAR_KEY,
                     VALIDPAR_KEY__HISTO_TYPE,
                     valid_par->histo_type);
    }


    return err_msg;
}



gchar *
gpiv_valid_testonly_parameters          (const GpivValidPar     *valid_par
                                         )
/*-----------------------------------------------------------------------------
 *     Testing parameters if have been set to correct values and 
 *     initializing derived variables
 */
{
    gchar *err_msg = NULL;



    if (valid_par->residu_type == GPIV_VALID_RESIDUTYPE__NORMMEDIAN
        && valid_par->residu_max != GPIV_RESIDU_MAX_NORMMEDIAN) {
        err_msg = "gpiv_valid_testonly_parameters: residu_type == GPIV_VALID_RESIDUTYPE__NORMMEDIAN, but valid_par->residu_max != GPIV_RESIDU_MAX_NORMMEDIAN";
        g_message ("gpiv_valid_testonly_parameters:: adapting residu_max = %f", valid_par->residu_max);
        return err_msg;
    }


    return err_msg;
}



gchar *
gpiv_valid_testadjust_parameters        (GpivValidPar           *valid_par
                                         )
/*-----------------------------------------------------------------------------
 *     Tests if all validation parameters have been read and have been defined 
 *     to valid values. Aplies missing parameters to defaults, as hard-coded
 *     in the library and ajusts parameters if necessary.
 */
{
    gchar *err_msg = NULL;


    if ((err_msg = gpiv_valid_check_parameters_read (valid_par, NULL))
        != NULL) {
        gpiv_warning ("gpiv_valid_testadjust_parameters: %s", err_msg);
        return err_msg;
    }

    if (valid_par->residu_type == GPIV_VALID_RESIDUTYPE__NORMMEDIAN) {
        valid_par->residu_max = GPIV_RESIDU_MAX_NORMMEDIAN;
            g_message ("gpiv_valid_testadjust_parameters:: adapting residu_max = %f", 
                       valid_par->residu_max);
    }


    return err_msg;
}



void 
gpiv_valid_print_parameters             (FILE                   *fp,
                                         const GpivValidPar     *valid_par
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Prints parameters to fp_par_out
 *
 * PROTOTYPE LOCATATION:
 *     valid.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    if (fp == stdout || fp == NULL) {
        /* Peaklocking */


        /* Outliers */
        if (valid_par->subst_type__set)
            printf ("%s.%s %d\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__SUBST_TYPE, 
                    valid_par->subst_type);

        if (valid_par->histo_type__set)
            printf ("%s.%s %d\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__HISTO_TYPE, 
                    valid_par->histo_type);

        if (valid_par->residu_type__set)
            printf ("%s.%s %d\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__RESIDU_TYPE, 
                    valid_par->residu_type);

        if (valid_par->residu_max__set)
            printf ("%s.%s %f\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__RESIDU_MAX, 
                    valid_par->residu_max);

        if (valid_par->neighbors__set)
            printf ("%s.%s %d\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__NEIGHBORS, 
                    valid_par->neighbors);

        if (valid_par->data_yield__set)
            printf ("%s.%s %f\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__DATA_YIELD, 
                    valid_par->data_yield);

    } else {
        /* Peaklocking */


        /* Outliers */
        if (valid_par->subst_type__set)
            fprintf (fp, "%s.%s %d\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__SUBST_TYPE, 
                    valid_par->subst_type);

        if (valid_par->histo_type__set)
            fprintf (fp, "%s.%s %d\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__HISTO_TYPE, 
                    valid_par->histo_type);

        if (valid_par->residu_type__set)
            fprintf (fp, "%s.%s %d\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__RESIDU_TYPE, 
                    valid_par->residu_type);

        if (valid_par->residu_max__set)
            fprintf (fp, "%s.%s %f\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__RESIDU_MAX, 
                    valid_par->residu_max);

        if (valid_par->neighbors__set)
            fprintf (fp, "%s.%s %d\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__NEIGHBORS, 
                    valid_par->neighbors);

        if (valid_par->data_yield__set)
            fprintf (fp, "%s.%s %f\n", GPIV_VALIDPAR_KEY,  
                    VALIDPAR_KEY__DATA_YIELD, 
                    valid_par->data_yield);
    }
}



GpivValidPar *
gpiv_valid_cp_parameters                (const GpivValidPar     *valid_par
                                         )
/*-----------------------------------------------------------------------------
 */
{
    GpivValidPar *valid_par_dest = g_new0 (GpivValidPar, 1);


    gpiv_valid_parameters_set (valid_par_dest, FALSE);
    valid_ovwrt_parameters (valid_par, valid_par_dest, TRUE);


    return valid_par_dest;
}



void
gpiv_valid_dupl_parameters              (const GpivValidPar     *valid_par_src, 
                                         GpivValidPar           *valid_par_dest
                                         )
/*-----------------------------------------------------------------------------
 */
{
    valid_ovwrt_parameters (valid_par_src, valid_par_dest, FALSE);
}



GpivValidPar *
gpiv_valid_fread_hdf5_parameters        (const gchar            *fname
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads validation parameters from hdf5 data file
 *
 * PROTOTYPE LOCATATION:
 *     valid.h
 *
 * INPUTS:
 *     fname:          complete file name
 *
 * OUTPUTS:
 *     valid_par:  parameter structure
 *
 * RETURNS: 
 *    NULL on success or *err_msg on failure
 *-------------------------------------------------------------------------- */
{ 
    GpivValidPar *valid_par = g_new0 (GpivValidPar, 1);
    gchar *err_msg = NULL;
    gint i;
/*
 * HDF declarations
 */
    hid_t       file_id, group_par, group_id, attribute_id; 
    herr_t      status;


    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_valid_fread_hdf5_parameters: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return NULL;
    }

    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "PIV", H5P_DEFAULT);
    H5Aiterate (group_id, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, 0,
                (H5A_operator_t)attr_info, valid_par);

    status = H5Gclose (group_id);
    status = H5Fclose(file_id); 


    return valid_par;
}



gchar *
gpiv_valid_fwrite_hdf5_parameters       (const gchar        *fname, 
                                         const GpivValidPar *valid_par
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes validation parameters to an existing hdf5 data file
 *
 * PROTOTYPE LOCATATION:
 *     valid.h
 *
 * INPUTS:
 *      fname:         complete file name
 *      valid_par: parameter structure
 *
 * OUTPUTS:
 *
 * RETURNS: 
 *     NULL on success or *err_msg on failure
 *-------------------------------------------------------------------------- */
{
    gchar *err_msg = NULL;
    int i;
/*
 * HDF declarations
 */
    hid_t       file_id, dataspace_id, group_par, group_id, attribute_id; 
    hsize_t     dims[1];
    herr_t      status;

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_valid_fwrite_hdf5_parameters: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
/*     group_par = H5Gopen (file_id, "PARAMETERS"); */
    group_id = H5Gopen (file_id, "PIV", H5P_DEFAULT);



    dims[0] = 1;
    dataspace_id = H5Screate_simple(1, dims, NULL);

/*
 * Outliers
 */
    if (valid_par->data_yield__set) {
        attribute_id = H5Acreate(group_id,  
                                 VALIDPAR_KEY__DATA_YIELD, 
                                 H5T_NATIVE_FLOAT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_FLOAT, 
                          &valid_par->data_yield); 
        status = H5Aclose(attribute_id); 
    }

    if (valid_par->residu_max__set) {
        attribute_id = H5Acreate(group_id,  
                                 VALIDPAR_KEY__RESIDU_MAX, 
                                 H5T_NATIVE_FLOAT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_FLOAT, 
                          &valid_par->residu_max); 
        status = H5Aclose(attribute_id); 
    }


    if (valid_par->neighbors__set) {
        attribute_id = H5Acreate(group_id,  
                                 VALIDPAR_KEY__NEIGHBORS, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &valid_par->neighbors); 
        status = H5Aclose(attribute_id); 
    }


    if (valid_par->residu_type__set) {
        attribute_id = H5Acreate(group_id,  
                                 VALIDPAR_KEY__RESIDU_TYPE, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &valid_par->residu_type); 
        status = H5Aclose(attribute_id); 
    }

    if (valid_par->subst_type__set) {
        attribute_id = H5Acreate(group_id,  
                                 VALIDPAR_KEY__SUBST_TYPE, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &valid_par->subst_type); 
        status = H5Aclose(attribute_id); 
    }

    if (valid_par->histo_type__set) {
        attribute_id = H5Acreate(group_id, 
                                 VALIDPAR_KEY__HISTO_TYPE, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &valid_par->histo_type); 
        status = H5Aclose(attribute_id); 
    }

    status = H5Sclose(dataspace_id);
    status = H5Gclose (group_id);
    /*     status = H5Gclose (group_par); */
    status = H5Fclose(file_id); 
    return err_msg;
}



#ifdef ENABLE_MPI
void
gpiv_valid_mpi_bcast_validpar           (GpivValidPar           *valid_par
                                         )
/*-----------------------------------------------------------------------------
 * Broadcasts valid_par
 */
{
    if (
        MPI_Bcast(&valid_par->data_yield, 1, MPI_FLOAT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&valid_par->data_yield__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&valid_par->residu_max, 1, MPI_FLOAT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&valid_par->residu_max__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&valid_par->neighbors, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&valid_par->neighbors__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&valid_par->residu_type, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&valid_par->residu_type__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&valid_par->subst_type, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&valid_par->subst_type__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&valid_par->histo_type, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&valid_par->histo_type__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        ) {
        gpiv_error("ALL SHIPS AND PIRATES! mpi_bcast_validpar: An error ocurred in MPI_Bcast");
    }
}

#endif /* ENABLE_MPI */

/*
 * Local functions
 */

static void
valid_ovwrt_parameters                  (const GpivValidPar     *valid_par_src,
                                         GpivValidPar           *valid_par_dest,
                                         const gboolean         force
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Copies validation parameters from valid_par_src
 *
 * INPUTS:
 *      valid_par_src:      source validation parameters
 *      force:                  flag to force the copying, even if destination
 *                              already exists
 *
 *
 * RETURNS:
 *   GpivValidPar *
 *---------------------------------------------------------------------------*/
{
    if ((force && valid_par_src->subst_type__set)
        || (valid_par_src->subst_type__set
        && !valid_par_dest->subst_type__set)) {
        valid_par_dest->subst_type = valid_par_src->subst_type;
        valid_par_dest->subst_type__set = TRUE;
    }

    if ((force && valid_par_src->histo_type__set)
        || (valid_par_src->histo_type__set
        && !valid_par_dest->histo_type__set)) {
        valid_par_dest->histo_type = valid_par_src->histo_type;
        valid_par_dest->histo_type__set = TRUE;
    }

    if ((force && valid_par_src->residu_type__set)
        || (valid_par_src->residu_type__set
        && !valid_par_dest->residu_type__set)) {
        valid_par_dest->residu_type = valid_par_src->residu_type;
        valid_par_dest->residu_type__set = TRUE;
    }

    if ((force && valid_par_src->residu_max__set)
        || (valid_par_src->residu_max__set
        && !valid_par_dest->residu_max__set)) {
        valid_par_dest->residu_max = valid_par_src->residu_max;
        valid_par_dest->residu_max__set = TRUE;
    }

    if ((force && valid_par_src->neighbors__set)
        || (valid_par_src->neighbors__set
        && !valid_par_dest->neighbors__set)) {
        valid_par_dest->neighbors = valid_par_src->neighbors;
        valid_par_dest->neighbors__set = TRUE;
    }

    if ((force && valid_par_src->data_yield__set)
        || (valid_par_src->data_yield__set
        && !valid_par_dest->data_yield__set)) {
        valid_par_dest->data_yield = valid_par_src->data_yield;
        valid_par_dest->data_yield__set = TRUE;
    }
}



static herr_t 
attr_info                               (hid_t                  loc_id, 
                                         const char             *name, 
                                         GpivValidPar           *valid_par
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Operator function.
 *
 * PROTOTYPE LOCATATION:
 *     valid.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    hid_t attribute_id, atype;
    herr_t status;



/*
 * Outliers
 */
    if (strcmp(name,  VALIDPAR_KEY__DATA_YIELD) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_FLOAT, 
                         &valid_par->data_yield); 
        status = H5Aclose(attribute_id); 
        valid_par->data_yield__set = TRUE;
    }
    

    if (strcmp(name,  VALIDPAR_KEY__RESIDU_MAX) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_FLOAT, 
                         &valid_par->residu_max); 
        status = H5Aclose(attribute_id); 
        valid_par->residu_max__set = TRUE;
    }
    

    if (strcmp(name,  VALIDPAR_KEY__NEIGHBORS) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &valid_par->neighbors); 
        status = H5Aclose(attribute_id); 
        valid_par->neighbors__set = TRUE;
    }
    

    if (strcmp(name,  VALIDPAR_KEY__RESIDU_TYPE) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &valid_par->residu_type); 
        status = H5Aclose(attribute_id); 
        valid_par->residu_type__set = TRUE;
    }
    

    if (strcmp(name,  VALIDPAR_KEY__SUBST_TYPE) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &valid_par->subst_type); 
        status = H5Aclose(attribute_id); 
        valid_par->subst_type__set = TRUE;
    }
    

    if (strcmp(name,  VALIDPAR_KEY__HISTO_TYPE) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &valid_par->histo_type); 
        status = H5Aclose(attribute_id); 
        valid_par->histo_type__set = TRUE;
    }


    return 0;
}



static void
obtain_valdipar_fromline                (char                   line[], 
                                         GpivValidPar           *valid_par, 
                                         gboolean               verbose
                                         )
/*-----------------------------------------------------------------------------
 * Scans a line to get a member of GpivValidPar
 */
{
    char par_name[GPIV_MAX_CHARS];


    if (line[0] != '#' && line[0] != '\n' && line[0] != ' ' 
        && line[0] != '\t') {
        sscanf(line,"%s", par_name);

        if (valid_par->data_yield__set == FALSE) {
            valid_par->data_yield__set =  
                gpiv_scan_fph (GPIV_VALIDPAR_KEY, 
                                VALIDPAR_KEY__DATA_YIELD, TRUE,
                                line, par_name, &valid_par->data_yield, 
                                verbose, NULL);
        }

        /*
         * Optional:
         */
        if (valid_par->residu_max__set == FALSE) {
            valid_par->residu_max__set =  
                gpiv_scan_fph (GPIV_VALIDPAR_KEY, 
                                VALIDPAR_KEY__RESIDU_MAX, TRUE,
                                line, par_name, &valid_par->residu_max, 
                                verbose, NULL);
        }

        if (valid_par->neighbors__set == FALSE) {
            valid_par->neighbors__set =  
                gpiv_scan_iph (GPIV_VALIDPAR_KEY, 
                                VALIDPAR_KEY__NEIGHBORS, TRUE,
                                line, par_name, &valid_par->neighbors, 
                                verbose, NULL);
        }

        /*
         * Optional:
         */
        if (valid_par->residu_type__set == FALSE) {
            valid_par->residu_type__set = 
                gpiv_scan_iph (GPIV_VALIDPAR_KEY, 
                                VALIDPAR_KEY__RESIDU_TYPE, TRUE,
                                line, par_name, (int *) &valid_par->residu_type, 
                                verbose, NULL);
        }
	      	      

        if (valid_par->subst_type__set == FALSE) {
            valid_par->subst_type__set = 
                gpiv_scan_iph (GPIV_VALIDPAR_KEY, 
                                VALIDPAR_KEY__SUBST_TYPE, TRUE,
                                line, par_name, (int *) &valid_par->subst_type, 
                                verbose, NULL);
        }

        if (valid_par->histo_type__set == FALSE) {
            valid_par->histo_type__set = 
                gpiv_scan_iph (GPIV_VALIDPAR_KEY, 
                                VALIDPAR_KEY__HISTO_TYPE, TRUE,
                                line, par_name, &valid_par->histo_type, 
                                verbose, NULL);
        }
    }
}
