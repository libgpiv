/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                l_utils.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:



LAST MODIFICATION DATE:  $Id: utils_alloc.c,v 1.3 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------------------- */

#include <gpiv.h>

#define GPIV_END 1                 /*  Used by vector */




gfloat **
gpiv_matrix (long nr,
             long nc
             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for 2-dimensional matrix of gfloat data
 *
 *---------------------------------------------------------------------------*/
{
    long i;
    gfloat **m;


    m = (gfloat **) g_malloc0 (nr * sizeof (gfloat *));
    m[0] = (gfloat *) g_malloc0 (nr * nc * sizeof (gfloat));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



void 
gpiv_free_matrix (gfloat **m
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for 2-dimensional array of float data
 *
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[0]);
    g_free (m);
    m = NULL;
}



gfloat **
gpiv_matrix_index (const long nrl, 
                   const long nrh, 
                   const long ncl, 
                   const long nch
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for 2-dimensional matrix of float data 
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      long nrl:	lowest row index number
 *      nrh:		highest row index number
 *      ncl:		lowest column index number
 *      nch	        highest column index number
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      2-dimensional array
 *---------------------------------------------------------------------------*/
{
    long i, nrows = nrh - nrl + 1, ncolumns = nch - ncl + 1;
    gfloat **m;


    m = (gfloat **) g_malloc0 ((size_t) ((nrows + GPIV_END) * 
                                    sizeof (gfloat *)));
    if (!m)  gpiv_error ("gpiv_matrix_index: allocation failure");
    m += GPIV_END;
    m -= nrl;
    
    m[nrl] =(gfloat *) g_malloc0 ((size_t) ((nrows * ncolumns + GPIV_END) *
                                       sizeof (gfloat)));
    if (!m[nrl]) gpiv_error ("gpiv_matrix_index: allocation failure");
    m[nrl] += GPIV_END;
    m[nrl] -= ncl;
    
    for (i=nrl+1; i <= nrh; i++) {
        m[i] = m[i-1] + ncolumns;
    }


    return m;
}



void 
gpiv_free_matrix_index (gfloat **m, 
                        const long nrl, 
                        const long nrh, 
                        const long ncl, 
                        const long nch
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for 2-dimensional array of float data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      m:	        matrix
 *      nrl:	        lowest row index number
 *      long nrh:	highest row index number
 *	long ncl:	lowest column index number
 *	long nch:	highest column index number
 *
 * OUTPUTS:
 *      m:             NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[nrl]+ncl-GPIV_END);
    g_free (m+nrl-GPIV_END);
    m = NULL;
}



guchar **
gpiv_ucmatrix (const long nr, 
               const long nc
               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for 2-dimensional matrix of guchar data
 *
 * INPUTS:
 *      nr:	row index
 *      nc:	column index
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     matrix
 *---------------------------------------------------------------------------*/
{
    long i;
    guchar **m;


    m = (guchar **) g_malloc0 (nr * sizeof (guchar*));
    m[0] = (guchar *) g_malloc0 (nr * nc * sizeof (guchar));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



void 
gpiv_free_ucmatrix (guchar **m 
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for 2-dimensional array of guchar data
 *
 * INPUTS:
 *      m:	        matrix
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      m:              NULL pointer
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[0]);
    g_free (m);
    m = NULL;
}



guint8 **
gpiv_matrix_guint8 (const long nr, 
                    const long nc
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      allocate a guint8 matrix with subscript range m[0..nr][0..nc]
 *
 * INPUTS:
 *      nr:             number of rows
 *      nc:             number of columns
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      matrix
 *---------------------------------------------------------------------------*/
{
    long i;
    guint8 **m;


    m = (guint8 **) g_malloc0 (nr * sizeof (guint8*));
    m[0] = (guint8 *) g_malloc0 (nr * nc * sizeof (guint8));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



void 
gpiv_free_matrix_guint8 (guint8 **m
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      free a guint8 matrix allocated by gpiv_matrix_guint8
 *
 * INPUTS:
 *      m:	        matrix 
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[0]);
    g_free (m);
    m = NULL;
}



guint16 **
gpiv_matrix_guint16 (const long nr, 
                     const long nc
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      allocate a guint16 matrix with subscript range m[0..nr][0..nc]
 *
 * INPUTS:
 *      nr:             number of rows
 *      nc:             number of columns
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      matrix
 *---------------------------------------------------------------------------*/
{
    long i;
    guint16 **m;


    m = (guint16 **) g_malloc0 (nr * sizeof (guint16*));
    m[0] = (guint16 *) g_malloc0 (nr * nc * sizeof (guint16));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



void 
gpiv_free_matrix_guint16 (guint16 **m
                          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      free a guint16 matrix allocated by gpiv_matrix_guint16
 *
 * INPUTS:
 *      m:	        matrix 
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[0]);
    g_free (m);
    m = NULL;
}



gint **
gpiv_imatrix_index (const long nrl, 
                    const long nrh, 
                    const long ncl, 
                    const long nch
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for 2-dimensional matrix of integer data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      m:	        matrix 
 *	nrl:	        lowest row index number
 *	nrh:	        highest row index number
 *	ncl:	        lowest column index number
 *	nch:	        highest column index number
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     matrix
 *---------------------------------------------------------------------------*/
{
    long i, nrows = nrh - nrl + 1, ncolumns = nch - ncl + 1;
    gint **m;


    m = (gint **) g_malloc0 ((size_t) ((nrows + GPIV_END)*
                                     sizeof (gint*)));
    m += GPIV_END;
    m -= nrl;
    m[nrl] = (gint *) g_malloc0 ((size_t) ((nrows * ncolumns + GPIV_END)*
                                         sizeof (gint)));
    m[nrl] += GPIV_END;
    m[nrl] -= ncl;
    
    for (i = nrl + 1; i <= nrh; i++) {
        m[i] = m[i-1] + ncolumns;
    }
    

    return m;
}



void 
gpiv_free_imatrix_index (gint **m, 
                         const long nrl, 
                         const long nrh, 
                         const long ncl, 
                         const long nch
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for 2-dimensional array of integer data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      m:	        matrix 
 *	nrl:	        lowest row index number
 *	nrh:	        highest row index number
 *	ncl:	        lowest column index number
 *	nch:	        highest column index number
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[nrl]+ncl - GPIV_END);
    g_free (m+nrl - GPIV_END);
    m = NULL;
}



gint **
gpiv_imatrix (const long nr, 
              const long nc)

/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for 2-dimensional matrix of integer data
 *      unappreciated, use gpiv_imatrix_index instead
 *
 * INPUTS:
 *	nr:	        lowest row index number
 *	nc:	        lowest column index number
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     matrix
 *---------------------------------------------------------------------------*/
{
    long i;
    gint **m;


    m = (gint **) g_malloc0 (nr * sizeof (gint*));
    m[0] = (gint *) g_malloc0 (nr * nc * sizeof (gint));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



void 
gpiv_free_imatrix (gint **m
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for 2-dimensional array of integer data
 *      unappreciated, use gpiv_free_imatrix_index instead
 *
 * INPUTS:
 *      m:	        matrix 
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[0]);
    g_free (m);
    m = NULL;
}



double **
gpiv_double_matrix (const glong nr, 
                    const glong nc 
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      allocate a contiguous 2-dimensional double matrix 
 *      of nr x nc
 *
 * INPUTS:
 *      nr:             number of rows
 *      nc:             number of columns
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     matrix
 *---------------------------------------------------------------------------*/
{
    double **m;
    gint i;


    m = (double **) g_malloc0 (nr * sizeof (double *));
    m[0] = (double *) g_malloc0 (nr * nc * sizeof (double));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



void 
gpiv_free_double_matrix (double **m
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      free a double matrix allocated by gpiv_double_matrix()
 *
 * INPUTS:
 *      m:	        matrix 
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[0]);
    g_free (m);
    m = NULL;
}


#ifndef USE_FFTW3
fftw_real **
gpiv_fftw_real_matrix (glong nr, 
                       glong nc 
                       )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      allocate a contiguous 2-dimensional fftw_real matrix 
 *      of nr x nc
 *
 * INPUTS:
 *      nr:             number of rows
 *      nc:             number of columns
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     matrix
 *---------------------------------------------------------------------------*/
{
    fftw_real **m;
    gint i;


    m = (fftw_real **) g_malloc0 (nr * sizeof (fftw_real *));
    m[0] = (fftw_real *) g_malloc0 (nr * nc * sizeof (fftw_real));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



void 
gpiv_free_fftw_real_matrix (fftw_real **m
                            )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      free a gdouble matrix allocated by gpiv_fftw_real_matrix()
 *
 * INPUTS:
 *      m:	        matrix 
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    g_free (m[0]);
    g_free (m);
    m = NULL;
}
#endif /* USE_FFTW3 */



fftw_complex **
gpiv_fftw_complex_matrix (const long nr, 
                          const long nc 
                          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      allocate a contiguous 2-dimensional fftw_complex matrix 
 *      of nr x nc
 *
 * INPUTS:
 *      nr:             number of rows
 *      nc:             number of columns
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     matrix
 *---------------------------------------------------------------------------*/
{
    fftw_complex **m;
    gint i;


    /*
     * allocate pointers to rows
     */
    m = (fftw_complex **) fftw_malloc (nr * sizeof (fftw_complex *));
    m[0] = (fftw_complex *) fftw_malloc (nr * nc * sizeof (fftw_complex));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



void 
gpiv_free_fftw_complex_matrix (fftw_complex **m
                               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      free a fftw_complex matrix allocated by gpiv_fftw_complex_matrix()
 *
 * INPUTS:
 *      m:	        matrix 
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL); /* gpiv_error ("m not allocated"); */
    fftw_free (m[0]);
    fftw_free (m);
    m = NULL;
}



float *
gpiv_vector (const long nl
             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of float data
 *
 * INPUTS:
 *      nl:		vector length
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    float *v;


    v = (float *)g_malloc0 ((size_t) (nl * sizeof (float)));
/*     v = (float *)g_malloc0 (gsize * nl * sizeof (float))); */


    return v;
}



void 
gpiv_free_vector (float *v
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of float data
 *
 * INPUTS:
 *      vector:             vector of 1-dimensional float data
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v);
    v = NULL;
}



float *
gpiv_vector_index (const long nl, 
                   const long nh
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of float data
 *      with subscript range v[nl..nh]
 *
 * INPUTS:
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    float *v;
    
    v = (float *)g_malloc0 ((size_t) ((nh + 1 - nl + GPIV_END) 
                                      * sizeof (float)));


    return v - nl;
}



void 
gpiv_free_vector_index (float *v,
                        const long nl,
                        const long nh
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of float data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      vector:        vector of 1-dimensional float data
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v + nl);
    v = NULL;
}



gdouble *
gpiv_dvector (const glong nl 
              )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of double data
 *
 * INPUTS:
 *      nl:		vector length
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    gdouble *v;


    v = (gdouble *)g_malloc0 ((size_t) (nl * sizeof (gdouble)));


    return v;
}



void
gpiv_free_dvector (gdouble *v
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of double data
 *
 * INPUTS:
 *      vector:             vector of 1-dimensional float data
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v);
    v = NULL;
}



gdouble *
gpiv_dvector_index (const long nl, 
                    const long nh
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of double data
 *      with subscript range v[nl..nh]
 *
 * INPUTS:
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    gdouble *v;
    

    v = (gdouble *)g_malloc0 ((size_t) ((nh + 1 -nl + GPIV_END) * 
                                      sizeof (gdouble)));


    return v - nl;
}



void
gpiv_free_dvector_index (gdouble *v, 
                         const long nl, 
                         const long nh
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of double data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      vector:        vector of 1-dimensional float data
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v + nl);
    v = NULL;
}



long *
gpiv_nulvector (const long nl
                )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of long data
 *
 * INPUTS:
 *      nl:		vector length
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    long *v;
    

    v = (long *)g_malloc0 ((size_t) (nl * sizeof (long)));


    return v;
}



void 
gpiv_free_nulvector (long *v
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of long data
 *
 * INPUTS:
 *      vector:             vector of 1-dimensional float data
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v);
    v = NULL;
}



long *
gpiv_nulvector_index (const long nl, 
                      const long nh
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of long data
 *      with subscript range v[nl..nh]
 *
 * INPUTS:
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    long *v;
    

    v = (long *)g_malloc0 ((size_t) ((nh + 1 - nl + GPIV_END) * 
                                    sizeof (long)));


    return v - nl;
}



void 
gpiv_free_nulvector_index (long *v, 
                           const long nl, 
                           const long nh)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of long data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      vector:        vector of 1-dimensional float data
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v + nl);
    v = NULL;
}



unsigned long *
gpiv_ulvector (const long nl
               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of unsigned long data
 *
 * INPUTS:
 *      nl:		vector length
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    unsigned long *v;
    

    v = (unsigned long *)g_malloc0 ((size_t) (nl * sizeof (long)));
    if (!v); gpiv_error ("gpiv_ulvector: allocation failure");


    return v;
}



void 
gpiv_free_ulvector (unsigned long *v
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of unsigned long data
 *
 * INPUTS:
 *      vector:             vector of 1-dimensional float data
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v);
    v = NULL;
}



unsigned long *
gpiv_ulvector_index (const long nl, 
                     const long nh
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of unsigned long data
 *      with subscript range v[nl..nh]
 *
 * INPUTS:
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    unsigned long *v;

    
    v = (unsigned long *)g_malloc0 ((size_t) ((nh + 1 - nl + GPIV_END) *
                                             sizeof (long)));
    if (!v); gpiv_error ("gpiv_ulvector_index: allocation failure");


    return v - nl;
}



void 
gpiv_free_ulvector_index (unsigned long *v, 
                          const long nl, 
                          const long nh
                          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of unsigned long data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      vector:        vector of 1-dimensional float data
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v + nl);
    v = NULL;
}



gint *
gpiv_ivector (const long nl
              )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of integer data
 *
 * INPUTS:
 *      nl:		vector length
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    gint *v;


    v = (gint *) g_malloc0 ((size_t) (nl * sizeof (gint)));
    if (!v)  gpiv_error ("gpiv_ivector: allocation failure");


    return v;
}



void 
gpiv_free_ivector (gint *v
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of integer data
 *
 * INPUTS:
 *      vector:             vector of 1-dimensional float data
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v);
    v = NULL;
}



gint *
gpiv_ivector_index (const long nl, 
                    const long nh
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of integer data
 *      with subscript range v[nl..nh]
 *
 * INPUTS:
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    gint *v;


    v = (gint *)g_malloc0 ((size_t) ((nh + 1 - nl + GPIV_END)*sizeof (gint)));
    if (!v) gpiv_error ("gpiv_ivector_index: allocation failure");


    return v - nl;
}



void 
gpiv_free_ivector_index (gint *v, 
                         const long nl, 
                         const long nh
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of integer data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 * INPUTS:
 *      vector:        vector of 1-dimensional float data
 *      nl:	       lowest index number
 *      nh:	       highest index number
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v + nl);
    v = NULL;
}



gboolean *
gpiv_gbolvector (const glong nl
                 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for a 1-dimensional vector of gboolean data
 *
 * INPUTS:
 *      nl:		vector length
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      vector
 *---------------------------------------------------------------------------*/
{
    gboolean *v;

    
    v = (gboolean *)g_malloc0 ((size_t) (nl * sizeof (gboolean)));


    return v;
}



void 
gpiv_free_gbolvector (gboolean *v
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      frees memory for a 1-dimensional vector of gboolean data
 *
 * INPUTS:
 *      vector:             vector of 1-dimensional boolean data
 *
 * OUTPUTS:
 *      vector:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (v != NULL); /* gpiv_error ("v not allocated"); */
    g_free (v);
    v = NULL;
}



#undef GPIV_END
