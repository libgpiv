/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------
FILENAME:                piv_par.c
LIBRARY:                 libgpiv
EXTERNAL FUNCTIONS:      
                         gpiv_piv_parameters_set
                         gpiv_piv_default_parameters
                         gpiv_get_pivparameter_from_resources

                         gpiv_piv_read_parameters
                         gpiv_piv_check_parameters_read
		         gpiv_piv_testonly_parameters
		         gpiv_piv_testadjust_parameters
                         gpiv_piv_print_parameters
                         gpiv_piv_cp_parameters
                         gpiv_piv_cp_undef_parameters
                         gpiv_piv_fread_hdf5_parameters
                         gpiv_piv_fwrite_hdf5_parameters

LAST MODIFICATION DATE:  $Id: piv_par.c,v 1.6 2008-09-25 13:19:53 gerber Exp $
--------------------------------------------------------------------- */

#include <gpiv.h>

/*
 * Default values and keys of GpivPivPar
 */
enum {
  PIVPAR_DEFAULT__INT_GEO                    = 0,       /**< Default parameter for int_geo of __GpivPivPar */
  PIVPAR_DEFAULT__COL_START                  = 0,       /**< Default parameter for col_start of __GpivPivPar */
  PIVPAR_DEFAULT__COL_END                    = 255,     /**< Default parameter for col_end of __GpivPivPar */
  PIVPAR_DEFAULT__ROW_START                  = 0,       /**< Default parameter for row_start of __GpivPivPar */
  PIVPAR_DEFAULT__ROW_END                    = 255,     /**< Default parameter for row_end of __GpivPivPar */
  PIVPAR_DEFAULT__INT_LINE_ROW               = 64,      /**< Default parameter for int_line_row of __GpivPivPar */
  PIVPAR_DEFAULT__INT_LINE_ROW_START         = 0,       /**< Default parameter for int_line_row_start of __GpivPivPar */
  PIVPAR_DEFAULT__INT_LINE_ROW_END           = 127,     /**< Default parameter for int_line_row_end of __GpivPivPar */
  PIVPAR_DEFAULT__INT_LINE_COL               = 64,      /**< Default parameter for int_line_col of __GpivPivPar */
  PIVPAR_DEFAULT__INT_LINE_COL_START         = 0,       /**< Default parameter for int_line_col_start of __GpivPivPar */
  PIVPAR_DEFAULT__INT_LINE_COL_END           = 127,     /**< Default parameter for int_line_col_end of __GpivPivPar */
  PIVPAR_DEFAULT__INT_POINT_COL              = 64,      /**< Default parameter for int_line_col_end of __GpivPivPar */
  PIVPAR_DEFAULT__INT_POINT_ROW              = 64,      /**< Default parameter for int_point_row of __GpivPivPar */
  PIVPAR_DEFAULT__INT_SIZE_F                 = 32,      /**< Default parameter for final interrogation size of __GpivPivPar */
  PIVPAR_DEFAULT__INT_SIZE_I                 = 64,      /**< Default parameter for initial int_size */
  PIVPAR_DEFAULT__INT_SHIFT                  = 10,      /**< Default parameter for int_shift of __GpivPivPar */
  PIVPAR_DEFAULT__PRE_SHIFT_COL              = 0,       /**< Default parameter for pre_shift_col of __GpivPivPar */
  PIVPAR_DEFAULT__PRE_SHIFT_ROW              = 0,       /**< Default parameter for pre_shift_row of __GpivPivPar */
  PIVPAR_DEFAULT__IFIT                       = 1,       /**< Default parameter for ifit of __GpivPivPar */
  PIVPAR_DEFAULT__PEAK                       = 1,       /**< Default parameter for peak of __GpivPivPar */
  PIVPAR_DEFAULT__INT_SCHEME                 = 4,       /**< Default parameter for int_scheme of __GpivPivPar */
  PIVPAR_DEFAULT__GAUSS_WEIGHT_IA            = 1,       /**< Default parameter for gauss_weight_ia of __GpivPivPar */
  PIVPAR_DEFAULT__SPOF_FILTER                = 0        /**< Default parameter for spof_filter of __GpivPivPar */
};


const gchar *PIVPAR_KEY__INT_GEO             = "Int_geo";       /**< Key for int_geo of __GpivPivPar */
const gchar *PIVPAR_KEY__COL_START           = "Col_start";     /**< Key for col_start of __GpivPivPar */
const gchar *PIVPAR_KEY__COL_END             = "Col_end";       /**< Key for col_end of __GpivPivPar */
const gchar *PIVPAR_KEY__ROW_START           = "Row_start";     /**< Key for row_start of __GpivPivPar */
const gchar *PIVPAR_KEY__ROW_END             = "Row_end";       /**< Key for row_end of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_LINE_ROW        = "Int_line_row";  /**< Key for int_line_row of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_LINE_ROW_START  = "Int_line_row_start";    /**< Key for int_line_row_start of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_LINE_ROW_END    = "Int_line_row_end";      /**< Key for int_line_row_end of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_LINE_COL        = "Int_line_col";  /**< Key for int_line_col of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_LINE_COL_START  = "Int_line_col_start";    /**< Key for int_line_col_start of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_LINE_COL_END    = "Int_line_col_end";      /**< Key for int_line_col_end of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_POINT_COL       = "Int_point_col"; /**< Key for int_line_col_end of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_POINT_ROW       = "Int_point_row"; /**< Key for int_point_row of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_SIZE_F          = "Int_size_f";    /**< Key for int_size_f of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_SIZE_I          = "Int_size_i";    /**< Key for int_size_i */
const gchar *PIVPAR_KEY__INT_SHIFT           = "Int_shift";     /**< Key for int_shift of __GpivPivPar */
const gchar *PIVPAR_KEY__PRE_SHIFT_COL       = "Pre_shift_col"; /**< Key for pre_shift_col of __GpivPivPar */
const gchar *PIVPAR_KEY__PRE_SHIFT_ROW       = "Pre_shift_row"; /**< Key for pre_shift_row of __GpivPivPar */
const gchar *PIVPAR_KEY__IFIT                = "Ifit";          /**< Key for ifit of __GpivPivPar */
const gchar *PIVPAR_KEY__PEAK                = "Peak";          /**< Key for peak of __GpivPivPar */
const gchar *PIVPAR_KEY__INT_SCHEME          = "Int_scheme";    /**< Key for int_scheme of __GpivPivPar */
const gchar *PIVPAR_KEY__GAUSS_WEIGHT_IA     = "Gauss_weight_ia";       /**< Key for gauss_weight_ia of __GpivPivPar */
const gchar *PIVPAR_KEY__SPOF_FILTER         = "Spof_filter";   /**< Key for spof_filter of __GpivPivPar */



static void
piv_ovwrt_parameters                    (const GpivPivPar       *piv_par_src, 
                                        GpivPivPar              *piv_par_dest,
                                        const gboolean          force
                                        );

static herr_t 
attr_info                               (hid_t                  loc_id, 
                                        const gchar             *name, 
                                        GpivPivPar              *piv_par
                                        );

static void 
obtain_pivpar_fromline                  (char                   line[], 
                                        GpivPivPar              *piv_par, 
                                        gboolean                verbose
                                        );

/*
 * Public functions
 */

void
gpiv_piv_default_parameters             (GpivPivPar             *piv_par_default,
					const gboolean          force
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Sets default parameter values
 *
 * INPUTS:
 *     force:                 flag to enforce parameters set to defaults
 *
 * OUTPUTS:
 *     piv_par_default:   structure of piv evaluation parameters
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    if (!piv_par_default->int_geo__set || force)
        piv_par_default->int_geo = PIVPAR_DEFAULT__INT_GEO;
    if (!piv_par_default->col_start__set || force)
        piv_par_default->col_start = PIVPAR_DEFAULT__COL_START;
    if (!piv_par_default->col_end__set || force)
        piv_par_default->col_end = PIVPAR_DEFAULT__COL_END;
    if (!piv_par_default->row_start__set || force)
        piv_par_default->row_start = PIVPAR_DEFAULT__ROW_START;
    if (!piv_par_default->row_end__set || force)
        piv_par_default->row_end = PIVPAR_DEFAULT__ROW_END;
    if (!piv_par_default->int_line_row_start__set || force)
        piv_par_default->int_line_row_start = PIVPAR_DEFAULT__INT_LINE_ROW_START;
    if (!piv_par_default->int_line_row_end__set || force)
        piv_par_default->int_line_row_end = PIVPAR_DEFAULT__INT_LINE_ROW_END;
    if (!piv_par_default->int_line_col_start__set || force)
        piv_par_default->int_line_col_start = PIVPAR_DEFAULT__INT_LINE_COL_START;
    if (!piv_par_default->int_line_col_end__set || force)
        piv_par_default->int_line_col_end = PIVPAR_DEFAULT__INT_LINE_COL_END;
    if (!piv_par_default->int_point_col__set || force)
        piv_par_default->int_point_col = PIVPAR_DEFAULT__INT_POINT_COL;
    if (!piv_par_default->int_point_row__set || force)
        piv_par_default->int_point_row = PIVPAR_DEFAULT__INT_POINT_ROW;
    if (!piv_par_default->int_size_f__set || force)
        piv_par_default->int_size_f = PIVPAR_DEFAULT__INT_SIZE_F;
    if (!piv_par_default->int_size_i__set || force)
        piv_par_default->int_size_i = PIVPAR_DEFAULT__INT_SIZE_I;
    if (!piv_par_default->int_shift__set || force)
        piv_par_default->int_shift = PIVPAR_DEFAULT__INT_SHIFT;
    if (!piv_par_default->pre_shift_col__set || force)
        piv_par_default->pre_shift_col = PIVPAR_DEFAULT__PRE_SHIFT_COL;
    if (!piv_par_default->pre_shift_row__set || force)
        piv_par_default->pre_shift_row = PIVPAR_DEFAULT__PRE_SHIFT_ROW;
    if (!piv_par_default->ifit__set || force)
        piv_par_default->ifit = PIVPAR_DEFAULT__IFIT;
    if (!piv_par_default->peak__set || force)
        piv_par_default->peak = PIVPAR_DEFAULT__PEAK;
    if (!piv_par_default->int_scheme__set || force)
        piv_par_default->int_scheme = PIVPAR_DEFAULT__INT_SCHEME;
    if (!piv_par_default->gauss_weight_ia__set || force)
        piv_par_default->gauss_weight_ia = PIVPAR_DEFAULT__GAUSS_WEIGHT_IA;
    if (!piv_par_default->spof_filter__set || force)
        piv_par_default->spof_filter = PIVPAR_DEFAULT__SPOF_FILTER;


    gpiv_piv_parameters_set(piv_par_default, TRUE);
}



GpivPivPar *
gpiv_piv_get_parameters_from_resources  (const gchar            *localrc,
                                        const gboolean          verbose
                                        )

/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads piv parameters from system-wide gpiv.conf and $HOME/.gpivrc
 *---------------------------------------------------------------------------*/
{
    GpivPivPar *piv_par = g_new0 (GpivPivPar, 1);


    gpiv_piv_parameters_set (piv_par, FALSE);
    gpiv_scan_parameter (GPIV_PIVPAR_KEY, localrc, piv_par, verbose);
    gpiv_scan_resourcefiles (GPIV_PIVPAR_KEY, piv_par, verbose);


    return piv_par;
}



void
gpiv_piv_parameters_set                 (GpivPivPar             *piv_par,
                                        const gboolean          flag
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Set flags for all piv_par__set
 *
 * INPUTS:
 *      flag:           true (1) or false (0)
 *
 * OUTPUTS:
 *      piv_par:   PIV evaluation parameters
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    piv_par->col_start__set = flag;
    piv_par->col_end__set = flag;
    piv_par->int_geo__set = flag;
    piv_par->ifit__set = flag;
    piv_par->int_line_row__set = flag;
    piv_par->int_line_row_start__set = flag;
    piv_par->int_line_row_end__set = flag;
    piv_par->int_line_col__set = flag;
    piv_par->int_line_col_start__set = flag;
    piv_par->int_line_col_end__set = flag;
    piv_par->int_point_col__set = flag;
    piv_par->int_point_row__set = flag;
    piv_par->int_size_f__set = flag;
    piv_par->int_size_i__set = flag;
    piv_par->int_shift__set = flag;
    piv_par->pre_shift_col__set = flag;
    piv_par->pre_shift_row__set = flag; 
    piv_par->peak__set = flag;
    piv_par->print_par__set = flag;
    piv_par->print_piv__set = flag;
    piv_par->row_start__set = flag;
    piv_par->row_end__set = flag;
    piv_par->int_scheme__set = flag;
    piv_par->gauss_weight_ia__set = flag;
    piv_par->spof_filter__set = flag;

}



void 
gpiv_piv_read_parameters                (FILE                   *fp, 
                                        GpivPivPar              *piv_par,
                                        const gboolean          verbose
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Read all parameters for PIV evaluation
 *
 *---------------------------------------------------------------------------*/
{
    char line[GPIV_MAX_CHARS], par_name[GPIV_MAX_CHARS];


    if (fp == stdin || fp == NULL) {
        while (gets (line) != NULL) {
            obtain_pivpar_fromline (line, piv_par, verbose);
        }
    } else {
        while (fgets(line, GPIV_MAX_CHARS, fp) != NULL) {
            obtain_pivpar_fromline (line, piv_par, verbose);
        }
    }


    return;
}



gchar *
gpiv_piv_check_parameters_read          (GpivPivPar             *piv_par,
                                         const GpivPivPar       *piv_par_default
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Check out if all parameters have been read
 *
 * INPUTS:
 *     piv_par_default:
 *
 * OUTPUTS:
 *     piv_par:
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if (piv_par->int_geo__set == FALSE) {
        piv_par->int_geo__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_geo = PIVPAR_DEFAULT__INT_GEO;
        } else {
            err_msg = "Using default: ";
            piv_par->int_geo = piv_par_default->int_geo;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_GEO,
                     piv_par->int_geo);
    }

    if (piv_par->int_geo == GPIV_AOI && piv_par->col_start__set == FALSE) {
        piv_par->col_start__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->col_start = PIVPAR_DEFAULT__COL_START;
        } else {
            err_msg = "Using default: ";
            piv_par->col_start = piv_par_default->col_start;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__COL_START,
                     piv_par->col_start);
    }

    if (piv_par->int_geo == GPIV_AOI && piv_par->col_end__set == FALSE) {
        piv_par->col_end__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->col_end = PIVPAR_DEFAULT__COL_END;
        } else {
            err_msg = "Using default: ";
            piv_par->col_end = piv_par_default->col_end;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__COL_END,
                     piv_par->col_end);
    }

    if (piv_par->int_geo == GPIV_AOI && piv_par->row_start__set == FALSE) {
        piv_par->row_start__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->row_start = PIVPAR_DEFAULT__ROW_START;
        } else {
            err_msg = "Using default: ";
            piv_par->row_start = piv_par_default->row_start;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__ROW_START,
                     piv_par->row_start);
    }

    if (piv_par->int_geo == GPIV_AOI && piv_par->row_end__set == FALSE) {
        piv_par->row_end__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->row_end = PIVPAR_DEFAULT__ROW_END;
        } else {
            err_msg = "Using default: ";
            piv_par->row_end = piv_par_default->row_end;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__ROW_END,
                     piv_par->row_end);
    }

    if (piv_par->int_geo == GPIV_LINE_R 
        && piv_par->int_line_row__set == FALSE) {
        piv_par->int_line_row__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_line_row = PIVPAR_DEFAULT__INT_LINE_ROW;
        } else {
            err_msg = "Using default: ";
            piv_par->int_line_row = piv_par_default->int_line_row;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_LINE_ROW,
                     piv_par->int_line_row);
    }

    if (piv_par->int_geo == GPIV_LINE_C 
        && piv_par->int_line_row_start__set == FALSE) {
        piv_par->int_line_row_start__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_line_row_start = 
                PIVPAR_DEFAULT__INT_LINE_ROW_START;
        } else {
            err_msg = "Using default: ";
            piv_par->int_line_row_start = piv_par_default->int_line_row_start;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_LINE_ROW_START,
                     piv_par->int_line_row_start);
    }

    if (piv_par->int_geo == GPIV_LINE_C 
        && piv_par->int_line_row_end__set == FALSE) {
        piv_par->int_line_row_end__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_line_row_end = 
                PIVPAR_DEFAULT__INT_LINE_ROW_END;
        } else {
            err_msg = "Using default: ";
            piv_par->int_line_row_end = piv_par_default->int_line_row_end;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_LINE_ROW_END,
                     piv_par->int_line_row_end);
    }

    if (piv_par->int_geo == GPIV_LINE_C 
        && piv_par->int_line_col__set == FALSE) {
        piv_par->int_line_col__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_line_col = PIVPAR_DEFAULT__INT_LINE_COL;
        } else {
            err_msg = "Using default: ";
            piv_par->int_line_col = piv_par_default->int_line_col;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_LINE_COL,
                     piv_par->int_line_col);
    }

    if (piv_par->int_geo == GPIV_LINE_R 
        && piv_par->int_line_col_start__set == FALSE) {
        piv_par->int_line_col_start__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_line_col_start = 
                PIVPAR_DEFAULT__INT_LINE_COL_START;
        } else {
            err_msg = "Using default: ";
            piv_par->int_line_col_start = 
                piv_par_default->int_line_col_start;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_LINE_COL_START,
                     piv_par->int_line_col_start);
    }

    if (piv_par->int_geo == GPIV_LINE_R 
        && piv_par->int_line_col_end__set == FALSE) {
        piv_par->int_line_col_end__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_line_col_end = 
                PIVPAR_DEFAULT__INT_LINE_COL_END;
        } else {
            err_msg = "Using default: ";
            piv_par->int_line_col_end = piv_par_default->int_line_col_end;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_LINE_COL_END,
                     piv_par->int_line_col_end);
    }

    if (piv_par->int_geo == GPIV_POINT 
        && piv_par->int_point_col__set == FALSE) {
        piv_par->int_point_col__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_point_col = PIVPAR_DEFAULT__INT_POINT_COL;
        } else {
            err_msg = "Using default: ";
            piv_par->int_point_col = piv_par_default->int_point_col;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_POINT_COL,
                     piv_par->int_point_col);
    }

    if (piv_par->int_geo == GPIV_POINT 
        && piv_par->int_point_row__set == FALSE) {
        piv_par->int_point_row__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_point_row = PIVPAR_DEFAULT__INT_POINT_ROW;
        } else {
            piv_par->int_point_row = piv_par_default->int_point_row;
            err_msg = "Using default: ";
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_POINT_ROW,
                     piv_par->int_point_row);
    }



    if (piv_par->int_size_f__set == FALSE) {
        piv_par->int_size_f__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_size_f = PIVPAR_DEFAULT__INT_SIZE_F;
        } else {
            err_msg = "Using default: ";
            piv_par->int_size_f = piv_par_default->int_size_f;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_SIZE_F,
                     piv_par->int_size_f);
    }

    if (piv_par->int_size_i__set == FALSE) {
        piv_par->int_size_i__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_size_i = PIVPAR_DEFAULT__INT_SIZE_I;
        } else {
            err_msg = "Using default: ";
            piv_par->int_size_i = piv_par_default->int_size_i;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_SIZE_I,
                     piv_par->int_size_i);
    }

    if (piv_par->int_shift__set == FALSE) {
        piv_par->int_shift__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_shift = PIVPAR_DEFAULT__INT_SHIFT;
        } else {
            err_msg = "Using default: ";
            piv_par->int_shift = piv_par_default->int_shift;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_SHIFT,
                     piv_par->int_shift);
    }

    if (piv_par->pre_shift_col__set == FALSE) {
        piv_par->pre_shift_col__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->pre_shift_col = PIVPAR_DEFAULT__PRE_SHIFT_COL;
        } else {
            err_msg = "Using default: ";
            piv_par->pre_shift_col = piv_par_default->pre_shift_col;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__PRE_SHIFT_COL,
                     piv_par->pre_shift_col);
    }

    if (piv_par->pre_shift_row__set == FALSE) {
        piv_par->pre_shift_row__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->pre_shift_row = PIVPAR_DEFAULT__PRE_SHIFT_ROW;
        } else {
            err_msg = "Using default: ";
            piv_par->pre_shift_row = piv_par_default->pre_shift_row;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__PRE_SHIFT_ROW,
                     piv_par->pre_shift_row);
    }

    if (piv_par->peak__set == FALSE) {
        piv_par->peak__set = TRUE;
        if (piv_par_default == NULL) {
            piv_par->peak = PIVPAR_DEFAULT__PEAK;
            err_msg = "Using default from libgpiv: ";
        } else {
            err_msg = "Using default: ";
            piv_par->peak = piv_par_default->peak;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__PEAK,
                     piv_par->peak);
    }

    if (piv_par->int_scheme__set == FALSE) {
        piv_par->int_scheme__set = TRUE;
       if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->int_scheme = PIVPAR_DEFAULT__INT_SCHEME;
        } else {
            piv_par->int_scheme = piv_par_default->int_scheme;
            err_msg = "Using default: ";
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__INT_SCHEME,
                     piv_par->int_scheme);
    }

    if (piv_par->spof_filter__set == FALSE) {
        piv_par->spof_filter__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->spof_filter = PIVPAR_DEFAULT__SPOF_FILTER;
        } else {
            err_msg = "Using default: ";
            piv_par->spof_filter = piv_par_default->spof_filter;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__SPOF_FILTER,
                     piv_par->spof_filter);
    }

    if (piv_par->gauss_weight_ia__set == FALSE) {
        piv_par->gauss_weight_ia__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->gauss_weight_ia = 
                PIVPAR_DEFAULT__GAUSS_WEIGHT_IA;
        } else {
            err_msg = "Using default: ";
            piv_par->gauss_weight_ia = piv_par_default->gauss_weight_ia;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__GAUSS_WEIGHT_IA,
                     piv_par->gauss_weight_ia);
    }

    if (piv_par->ifit__set == FALSE) {
        piv_par->ifit__set = TRUE;
        if (piv_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            piv_par->ifit = PIVPAR_DEFAULT__IFIT;
        } else {
            err_msg = "Using default: ";
            piv_par->ifit = piv_par_default->ifit;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg, 
                     GPIV_PIVPAR_KEY,
                     PIVPAR_KEY__IFIT,
                     piv_par->ifit);
    }


   return err_msg;
}



gchar *
gpiv_piv_testonly_parameters            (const GpivImagePar     *image_par, 
                                        const GpivPivPar        *piv_par
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Testing parameters on valid values
 */
{
    gchar *err_msg = NULL;


/*
 * Check if col_end and row_end are within the image size
 */
    if (piv_par->col_end__set == TRUE
        && piv_par->col_end > image_par->ncolumns - piv_par->pre_shift_col - 1) {
	err_msg = g_strdup_printf ("gpiv_piv_testonly_parameters: last column to be analysed is out of \
image region;\n%s.%s > Ncolumns-%s.%s-1",
                                   GPIV_PIVPAR_KEY, 
                                   PIVPAR_KEY__COL_END,
                                   GPIV_PIVPAR_KEY, 
                                   PIVPAR_KEY__PRE_SHIFT_COL);
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

    if (piv_par->row_end > image_par->nrows - piv_par->pre_shift_row - 1) {
	err_msg = g_strdup_printf ("gpiv_piv_testonly_parameters: last row to be analysed is out of image "
                                   "region;\n%s.%s > Nrows-%s.%s-1",
                                   GPIV_PIVPAR_KEY, 
                                   PIVPAR_KEY__ROW_END,
                                   GPIV_PIVPAR_KEY, 
                                   PIVPAR_KEY__PRE_SHIFT_ROW);
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

/*
 * Check if size of Interrogation Area's is allowed
 */
    if (piv_par->int_size_f > GPIV_MAX_INTERR_SIZE || 
        piv_par->int_size_f > image_par->ncolumns ||
	piv_par->int_size_f > image_par->nrows) {
	err_msg = g_strdup_printf ("gpiv_piv_testonly_parameters: %s = %d larger than allowed maximum, ncolumns or nrows",
                                   PIVPAR_KEY__INT_SIZE_F, 
                                   piv_par->int_size_f);
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

    if (piv_par->int_size_i > GPIV_MAX_INTERR_SIZE || 
        piv_par->int_size_i > image_par->ncolumns ||
	piv_par->int_size_i > image_par->nrows) {
	err_msg = g_strdup_printf ("gpiv_piv_testonly_parameters: %s = %d larger than allowed maximum, ncolumns or nrows",
                                   PIVPAR_KEY__INT_SIZE_I,
                                   piv_par->int_size_i);
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

    if (piv_par->int_size_i < piv_par->int_size_f) {
	err_msg = g_strdup_printf ("gpiv_piv_testonly_parameters: %s smaller than %s. \n\
Use %s larger than %s for adaptive interrogation sizes.",
                                   PIVPAR_KEY__INT_SIZE_I,
                                   PIVPAR_KEY__INT_SIZE_F,
                                   PIVPAR_KEY__INT_SIZE_I,
                                   PIVPAR_KEY__INT_SIZE_F
                                   );
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

/*
 * Check ifit
 */
    if (piv_par->ifit != 0 
        && piv_par->ifit != 1 
        && piv_par->ifit != 2 
        && piv_par->ifit != 3) {
	err_msg = g_strdup_printf("gpiv_piv_testonly_parameters: invalid value of %s.%s (0, 1, 2 or 3)",
                                  GPIV_PIVPAR_KEY,
                                  PIVPAR_KEY__IFIT);
        gpiv_warning("%s", err_msg);
        return err_msg;
    }


    return err_msg;
}



gchar *
gpiv_piv_testadjust_parameters          (const GpivImagePar     *image_par, 
                                        GpivPivPar              *piv_par
                                        )
/*-----------------------------------------------------------------------------
 *     Tests if all piv parameters have been read and have been defined 
 *     to valid values. Aplies missing parameters to defaults, as hard-coded
 *     in the library and adjusts parameters if necessary.
 */
{
    gchar *err_msg = NULL;


    if ((err_msg = gpiv_piv_check_parameters_read (piv_par, NULL)) != NULL) {
        gpiv_warning("gpiv_piv_testadjust_parameters: %s", err_msg);
        return (err_msg);
    }

/*
 * Check if col_end and row_end are within the image size
 */
    if (piv_par->col_end__set == TRUE
        && piv_par->col_end > image_par->ncolumns - piv_par->pre_shift_col - 1) {
        piv_par->col_end = image_par->ncolumns - piv_par->pre_shift_col - 1;
        gpiv_warning("gpiv_piv_testadjust_parameters: last column set to %f",
                                   piv_par->col_end);
    }

    if (piv_par->col_start__set == TRUE
        && piv_par->col_start  > piv_par->col_end) {
        piv_par->col_start = PIVPAR_DEFAULT__COL_START;
        gpiv_warning("gpiv_piv_testadjust_parameters: first column set to %f",
                                   piv_par->col_start);
    }

    if (piv_par->row_end > image_par->nrows - piv_par->pre_shift_row - 1) {
        piv_par->row_end = image_par->nrows - piv_par->pre_shift_row - 1;
        gpiv_warning("gpiv_piv_testadjust_parameters: last row set to %f",
                                   piv_par->row_end);
    }

    if (piv_par->row_start > piv_par->row_start) {
        piv_par->row_start = PIVPAR_DEFAULT__ROW_START;
        gpiv_warning("gpiv_piv_testadjust_parameters: first row set to %f",
                                   piv_par->row_start);
    }

/*
 * Check if size of Interrogation Area's is allowed
 */
    if (piv_par->int_size_f > GPIV_MAX_INTERR_SIZE
        || piv_par->int_size_f > image_par->ncolumns
        || piv_par->int_size_f > image_par->nrows) {
        piv_par->int_size_f = GPIV_MAX_INTERR_SIZE;
        gpiv_warning("gpiv_piv_testadjust_parameters: %s = %d larger than allowed maximum, ncolumns or nrows",
                     PIVPAR_KEY__INT_SIZE_F, 
                     piv_par->int_size_f);
    }

    if (piv_par->int_size_i > GPIV_MAX_INTERR_SIZE 
        || piv_par->int_size_i > image_par->ncolumns
        || piv_par->int_size_i > image_par->nrows) {
        piv_par->int_size_i = GPIV_MAX_INTERR_SIZE;
        gpiv_warning("gpiv_piv_testadjust_parameters: %s = %d larger than allowed maximum, ncolumns or nrows",
                     PIVPAR_KEY__INT_SIZE_I,
                     piv_par->int_size_i);
    }

    if (piv_par->int_size_i < piv_par->int_size_f) {
        piv_par->int_size_i = piv_par->int_size_f;
        gpiv_warning("Gpiv_piv_testadjust_parameters: %s set equal to %s.",
                                   PIVPAR_KEY__INT_SIZE_I,
                                   PIVPAR_KEY__INT_SIZE_F);
    }

/*
 * Check ifit
 */
    if (piv_par->ifit != 0 
        && piv_par->ifit != 1 
        && piv_par->ifit != 2 
        && piv_par->ifit != 3) {
        piv_par->ifit = 1; 
        gpiv_warning("gpiv_piv_testadjust_parameters: ifit set to 1 (Gauss)",
                                  GPIV_PIVPAR_KEY);
    }


    return err_msg;
}



void 
gpiv_piv_print_parameters               (FILE                   *fp,
                                        const GpivPivPar        *piv_par
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Prints parameters to fp
 *
 *---------------------------------------------------------------------------*/
{
    if (fp == stdout || fp == NULL) {
/*
 * prints to stdout
 */
        if (piv_par->int_geo__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_GEO, 
                    piv_par->int_geo);

        if (piv_par->col_start__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__COL_START, 
                    piv_par->col_start);

        if (piv_par->col_end__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__COL_END, 
                    piv_par->col_end);

        if (piv_par->row_start__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__ROW_START, 
                    piv_par->row_start);

        if (piv_par->row_end__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__ROW_END, 
                    piv_par->row_end);



        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_R 
            && piv_par->int_line_row__set) 
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_ROW, 
                    piv_par->int_line_row);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_C 
            && piv_par->int_line_row_start__set) 
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_ROW_START, 
                    piv_par->int_line_row_start);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_C 
            && piv_par->int_line_row_end__set) 
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_ROW_END, 
                    piv_par->int_line_row_end);
    


        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_C 
            && piv_par->int_line_col__set) 
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_COL, 
                    piv_par->int_line_col);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_R 
            && piv_par->int_line_col_start__set) 
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_COL_START, 
                    piv_par->int_line_col_start);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_R 
            && piv_par->int_line_col_end__set) 
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_COL_END, 
                    piv_par->int_line_col_end);
    


        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_POINT 
            && piv_par->int_point_row__set)
            printf ("%s.%s %d\n",
                                  
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_POINT_ROW, 
                    piv_par->int_point_row);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_POINT 
            && piv_par->int_point_col__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_POINT_COL,
                    piv_par->int_point_col);



        if (piv_par->int_size_f__set)
            printf ("%s.%s %d\n",
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_SIZE_F, 
                    piv_par->int_size_f);

        if (piv_par->int_size_i__set)
            printf ("%s.%s %d\n",
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_SIZE_I, 
                    piv_par->int_size_i);

        if (piv_par->int_shift__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_SHIFT, 
                    piv_par->int_shift);

        if (piv_par->int_scheme__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_SCHEME, 
                    piv_par->int_scheme);

        if (piv_par->spof_filter__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__SPOF_FILTER, 
                    piv_par->spof_filter);

        if (piv_par->gauss_weight_ia__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__GAUSS_WEIGHT_IA, 
                    piv_par->gauss_weight_ia);

        if (piv_par->pre_shift_col__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__PRE_SHIFT_COL, 
                    piv_par->pre_shift_col);

        if (piv_par->pre_shift_row__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__PRE_SHIFT_ROW, 
                    piv_par->pre_shift_row);

        if (piv_par->ifit__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__IFIT, 
                    piv_par->ifit);



        if (piv_par->peak__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__PEAK, 
                    piv_par->peak);

        if (piv_par->print_piv__set)
            printf ("%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, "Print_piv", 
                    piv_par->print_piv);

    } else {
/*
 * prints to fp
 */
        if (piv_par->int_geo__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_GEO, 
                    piv_par->int_geo);

        if (piv_par->col_start__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__COL_START, 
                    piv_par->col_start);

        if (piv_par->col_end__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__COL_END, 
                    piv_par->col_end);

        if (piv_par->row_start__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__ROW_START, 
                    piv_par->row_start);

        if (piv_par->row_end__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__ROW_END, 
                    piv_par->row_end);



        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_R 
            && piv_par->int_line_row__set) 
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_ROW, 
                    piv_par->int_line_row);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_C 
            && piv_par->int_line_row_start__set) 
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_ROW_START, 
                    piv_par->int_line_row_start);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_C 
            && piv_par->int_line_row_end__set) 
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_ROW_END, 
                    piv_par->int_line_row_end);
    


        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_C 
            && piv_par->int_line_col__set) 
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_COL, 
                    piv_par->int_line_col);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_R 
            && piv_par->int_line_col_start__set) 
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_COL_START, 
                    piv_par->int_line_col_start);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_LINE_R 
            && piv_par->int_line_col_end__set) 
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_COL_END, 
                    piv_par->int_line_col_end);
    


        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_POINT 
            && piv_par->int_point_row__set)
            fprintf (fp, "%s.%s %d\n",
                                  
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_POINT_ROW, 
                    piv_par->int_point_row);

        if (piv_par->int_geo__set
            && piv_par->int_geo == GPIV_POINT 
            && piv_par->int_point_col__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_POINT_COL,
                    piv_par->int_point_col);



        if (piv_par->int_size_f__set)
            fprintf (fp, "%s.%s %d\n",
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_SIZE_F, 
                    piv_par->int_size_f);

        if (piv_par->int_size_i__set)
            fprintf (fp, "%s.%s %d\n",
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_SIZE_I, 
                    piv_par->int_size_i);

        if (piv_par->int_shift__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_SHIFT, 
                    piv_par->int_shift);

        if (piv_par->int_scheme__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_SCHEME, 
                    piv_par->int_scheme);

        if (piv_par->spof_filter__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__SPOF_FILTER, 
                    piv_par->spof_filter);

        if (piv_par->gauss_weight_ia__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__GAUSS_WEIGHT_IA, 
                    piv_par->gauss_weight_ia);

        if (piv_par->pre_shift_col__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__PRE_SHIFT_COL, 
                    piv_par->pre_shift_col);

        if (piv_par->pre_shift_row__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__PRE_SHIFT_ROW, 
                    piv_par->pre_shift_row);

        if (piv_par->ifit__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__IFIT, 
                    piv_par->ifit);


        if (piv_par->peak__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, PIVPAR_KEY__PEAK, 
                    piv_par->peak);

        if (piv_par->print_piv__set)
            fprintf (fp, "%s.%s %d\n", 
                    GPIV_PIVPAR_KEY, "Print_piv", 
                    piv_par->print_piv);
    }
}



GpivPivPar *
gpiv_piv_cp_parameters                  (const GpivPivPar       *piv_par
                                        )
/*-----------------------------------------------------------------------------
 */
{
    GpivPivPar *piv_par_dest = g_new0 (GpivPivPar, 1);


    gpiv_piv_parameters_set (piv_par_dest, FALSE);
    piv_ovwrt_parameters (piv_par, piv_par_dest, TRUE);


    return piv_par_dest;
}



void
gpiv_piv_cp_undef_parameters            (const GpivPivPar       *piv_par_src, 
                                        GpivPivPar              *piv_par_dest
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Duplicates piv parameters from _src to _dest if _dest has not been set
 */
{
    piv_ovwrt_parameters (piv_par_src, piv_par_dest, FALSE);
}



GpivPivPar *
gpiv_piv_fread_hdf5_parameters          (const gchar            *fname
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *   Reads piv parameters from hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    GpivPivPar *piv_par = g_new0 (GpivPivPar, 1);
    gchar *err_msg = NULL;
    gint i;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id; 
    herr_t      status;

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_piv_fread_hdf5_parameters: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return NULL;
    }

    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "PIV", H5P_DEFAULT);
    H5Aiterate (group_id, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, 0,
                (H5A_operator_t)attr_info, piv_par);


   status = H5Gclose (group_id);
   status = H5Fclose(file_id); 
   return piv_par;
}



gchar *
gpiv_piv_fwrite_hdf5_parameters         (const gchar            *fname,
                                        const GpivPivPar       *piv_par 
                                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *   Writes piv parameters to an existing hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i;
    
    
/*
 * HDF declarations
 */
    hid_t       file_id, dataspace_id, group_id, attribute_id; 
    hsize_t     dims[1];
    herr_t      status;

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_piv_fwrite_hdf5_parameters: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "PIV", H5P_DEFAULT);



    dims[0] = 1;
    dataspace_id = H5Screate_simple(1, dims, NULL);

    /* 
     * Image piv parametes
     */
    if (piv_par->int_geo__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_GEO, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_geo); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_AOI 
        && piv_par->col_start__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__COL_START, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->col_start); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_AOI 
        && piv_par->col_end__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__COL_END, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->col_end); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_AOI 
        && piv_par->row_start__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__ROW_START, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->row_start); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_AOI 
        && piv_par->row_end__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__ROW_END, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->row_end); 
        status = H5Aclose(attribute_id); 
    }

    /* 
     * line
     */
    if (piv_par->int_geo == GPIV_LINE_R 
        && piv_par->int_line_row__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_LINE_ROW, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_line_row); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_LINE_C 
        && piv_par->int_line_row_start__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_LINE_ROW_START, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, &piv_par->int_line_row_start); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_LINE_C && piv_par->int_line_row_end__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_LINE_ROW_END, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_line_row_end); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_LINE_C 
        && piv_par->int_line_col__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_LINE_COL, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_line_col); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_LINE_R 
        && piv_par->int_line_col_start__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_LINE_COL_START, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_line_col_start); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_LINE_R 
        && piv_par->int_line_col_end__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_LINE_COL_END, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_line_col_end); 
        status = H5Aclose(attribute_id); 
    }

    /*
     * point
     */


    if (piv_par->int_geo == GPIV_POINT 
        && piv_par->int_point_col__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_POINT_COL, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_point_col); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_geo == GPIV_POINT && piv_par->int_point_row__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_POINT_ROW, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_point_row); 
        status = H5Aclose(attribute_id); 
    }

    /*
     * Interrogation
     */
    if (piv_par->int_size_f__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_SIZE_F, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_size_f); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_size_i__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_SIZE_I, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_size_i); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_shift__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_SHIFT, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_shift); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->pre_shift_col__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__PRE_SHIFT_COL, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->pre_shift_col); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->pre_shift_row__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__PRE_SHIFT_ROW, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->pre_shift_row); 
        status = H5Aclose(attribute_id); 
    }

    /*
     * Sub-pixel interrogation
     */
    if (piv_par->ifit__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__IFIT, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, &piv_par->ifit); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->peak__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__PEAK, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, &piv_par->peak); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->int_scheme__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__INT_SCHEME, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->int_scheme); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->spof_filter__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__SPOF_FILTER, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->spof_filter); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->gauss_weight_ia__set) {
        attribute_id = H5Acreate(group_id, PIVPAR_KEY__GAUSS_WEIGHT_IA, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->gauss_weight_ia); 
        status = H5Aclose(attribute_id); 
    }

    if (piv_par->print_piv__set) {
        attribute_id = H5Acreate(group_id, "print_piv", 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &piv_par->print_piv); 
        status = H5Aclose(attribute_id); 
    }


    status = H5Sclose(dataspace_id);
    status = H5Gclose (group_id);
    status = H5Fclose(file_id); 
    return err_msg;
}



#ifdef ENABLE_MPI
gchar *
gpiv_piv_mpi_bcast_pivpar               (GpivPivPar             *piv_par)
/*-----------------------------------------------------------------------------
 * Broadcasts piv_par, usring root as 0 and MPI_COMM_WORLD
 */
{
    gchar *err_msg = NULL;
    gint root = 0;

    if (
        MPI_Bcast(&piv_par->int_geo, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_geo__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->col_start, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->col_start__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->col_end, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->col_end__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->row_start, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->row_start__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->row_end, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->row_end__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_line_col, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_line_col__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
 
        || MPI_Bcast(&piv_par->int_line_col_start, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_line_col_start__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_line_col_end, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_line_col_end__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_line_row, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_line_row__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_line_row_start, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_line_row_start__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_line_row_end, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_line_row_end__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_point_col, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_point_col__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
 
        || MPI_Bcast(&piv_par->int_point_row, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_point_row__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_size_f, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_size_f__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_size_i, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_size_i__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_shift, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_shift__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_deform, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_deform__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->spline_degree, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->spline_degree__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->pre_shift_col, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->pre_shift_col__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->pre_shift_row, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->pre_shift_row__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->ifit, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->ifit__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->peak, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->peak__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->int_scheme__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->int_scheme, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->gauss_weight_ia, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->gauss_weight_ia__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS

        || MPI_Bcast(&piv_par->spof_filter, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        || MPI_Bcast(&piv_par->spof_filter__set, 1, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_warning("THOUSAND BOMBS AND SHELLS!");
        err_msg = "gpiv_piv_mpi_bcast_pivpar: An error ocurred in MPI_Bcast";
    }

    return err_msg;
}
#endif /* ENABLE_MPI */


/*
 * Local functions
 */

static void
piv_ovwrt_parameters                    (const GpivPivPar *piv_par_src, 
                                         GpivPivPar *piv_par_dest,
                                         const gboolean force
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Copies piv parameters
 *
 * INPUTS:
 *      piv_par_src:       source piv parameters
 *      force:                  flag to force the copying, even if destination
 *                              already exists
 *
 *
 * RETURNS:
 *      *piv_par_dest:     destination piv parameters
 *
 *---------------------------------------------------------------------------*/
{
    if ((force && piv_par_src->col_start__set)
        || ( piv_par_src->col_start__set 
             && !piv_par_dest->col_start__set)) {
        piv_par_dest->col_start = piv_par_src->col_start;
        piv_par_dest->col_start__set = TRUE;
    }

    if ((force && piv_par_src->col_end__set)
        || ( piv_par_src->col_end__set 
             && !piv_par_dest->col_end__set)) {
        piv_par_dest->col_end = piv_par_src->col_end;
        piv_par_dest->col_end__set = TRUE;
    }

    if ((force && piv_par_src->row_start__set)
        || ( piv_par_src->row_start__set 
             && !piv_par_dest->row_start__set)) {
        piv_par_dest->row_start = piv_par_src->row_start;
        piv_par_dest->row_start__set = TRUE;
    }

    if ((force && piv_par_src->row_end__set)
        || ( piv_par_src->row_end__set 
             && !piv_par_dest->row_end__set)) {
        piv_par_dest->row_end = piv_par_src->row_end;
        piv_par_dest->row_end__set = TRUE;
    }

    
    if ((force && piv_par_src->int_geo__set)
        || ( piv_par_src->int_geo__set 
             && !piv_par_dest->int_geo__set)) {
        piv_par_dest->int_geo = piv_par_src->int_geo;
        piv_par_dest->int_geo__set = TRUE;
    }

    if ((force && piv_par_src->int_line_col__set)
        || ( piv_par_src->int_line_col__set 
             && !piv_par_dest->int_line_col__set)) {
        piv_par_dest->int_line_col = piv_par_src->int_line_col;
        piv_par_dest->int_line_col__set = TRUE;
    }

    if ((force && piv_par_src->int_line_col_start__set)
        || ( piv_par_src->int_line_col_start__set 
             && !piv_par_dest->int_line_col_start__set)) {
        piv_par_dest->int_line_col_start = 
            piv_par_src->int_line_col_start;
        piv_par_dest->int_line_col_start__set = TRUE;
    }

    if ((force && piv_par_src->int_line_col_end__set)
        || ( piv_par_src->int_line_col_end__set 
             && !piv_par_dest->int_line_col_end__set)) {
        piv_par_dest->int_line_col_end = 
            piv_par_src->int_line_col_end;
        piv_par_dest->int_line_col_end__set = TRUE;
    }

    if ((force && piv_par_src->int_line_row__set)
        || ( piv_par_src->int_line_row__set 
             && !piv_par_dest->int_line_row__set)) {
        piv_par_dest->int_line_row = piv_par_src->int_line_row;
        piv_par_dest->int_line_row__set = TRUE;
    }

    if ((force && piv_par_src->int_line_row_start__set)
        || ( piv_par_src->int_line_row_start__set 
             && !piv_par_dest->int_line_row_start__set)) {
        piv_par_dest->int_line_row_start = 
            piv_par_src->int_line_row_start;
        piv_par_dest->int_line_row_start__set = TRUE;
    }

    if ((force && piv_par_src->int_line_row_end__set)
        || ( piv_par_src->int_line_row_end__set 
             && !piv_par_dest->int_line_row_end__set)) {
        piv_par_dest->int_line_row_end = 
            piv_par_src->int_line_row_end;
        piv_par_dest->int_line_row_end__set = TRUE;
    }

    if ((force && piv_par_src->int_point_col__set)
        || ( piv_par_src->int_point_col__set 
             && !piv_par_dest->int_point_col__set)) {
        piv_par_dest->int_point_col = piv_par_src->int_point_col;
        piv_par_dest->int_point_col__set = TRUE;
    }

    if ((force && piv_par_src->int_point_row__set)
        || ( piv_par_src->int_point_row__set 
             && !piv_par_dest->int_point_row__set)) {
        piv_par_dest->int_point_row = piv_par_src->int_point_row;
        piv_par_dest->int_point_row__set = TRUE;
    }

    if ((force && piv_par_src->int_size_f__set)
        || ( piv_par_src->int_size_f__set 
             && !piv_par_dest->int_size_f__set)) {
        piv_par_dest->int_size_f = piv_par_src->int_size_f;
        piv_par_dest->int_size_f__set = TRUE;
    }

    if ((force && piv_par_src->int_size_i__set)
        || ( piv_par_src->int_size_i__set 
             && !piv_par_dest->int_size_i__set)) {
        piv_par_dest->int_size_i = piv_par_src->int_size_i;
        piv_par_dest->int_size_i__set = TRUE;
    }

    if ((force && piv_par_src->int_shift__set)
        || ( piv_par_src->int_shift__set 
             && !piv_par_dest->int_shift__set)) {
        piv_par_dest->int_shift = 
            piv_par_src->int_shift;
        piv_par_dest->int_shift__set = TRUE;
    }

    if ((force && piv_par_src->pre_shift_col__set)
        || ( piv_par_src->pre_shift_col__set 
             && !piv_par_dest->pre_shift_col__set)) {
        piv_par_dest->pre_shift_col = piv_par_src->pre_shift_col;
        piv_par_dest->pre_shift_col__set = TRUE;
    }

    if ((force && piv_par_src->pre_shift_row__set)
        || ( piv_par_src->pre_shift_row__set 
             && !piv_par_dest->pre_shift_row__set)) {
        piv_par_dest->pre_shift_row = piv_par_src->pre_shift_row;
        piv_par_dest->pre_shift_row__set = TRUE;
    }

    if ((force && piv_par_src->peak__set)
        || ( piv_par_src->peak__set 
             && !piv_par_dest->peak__set)) {
        piv_par_dest->peak = piv_par_src->peak;
        piv_par_dest->peak__set = TRUE;
    }

    if ((force && piv_par_src->print_par__set)
        || ( piv_par_src->print_par__set 
             && !piv_par_dest->print_par__set)) {
        piv_par_dest->print_par = piv_par_src->print_par;
        piv_par_dest->print_par__set = TRUE;
    }

    if ((force && piv_par_src->print_piv__set)
        || ( piv_par_src->print_piv__set 
             && !piv_par_dest->print_piv__set)) {
        piv_par_dest->print_piv = piv_par_src->print_piv;
        piv_par_dest->print_piv__set = TRUE;
    }

    if ((force && piv_par_src->int_scheme__set)
        || ( piv_par_src->int_scheme__set 
             && !piv_par_dest->int_scheme__set)) {
        piv_par_dest->int_scheme = piv_par_src->int_scheme;
        piv_par_dest->int_scheme__set = TRUE;
    }

    if ((force && piv_par_src->spof_filter__set)
        || ( piv_par_src->spof_filter__set 
             && !piv_par_dest->spof_filter__set)) {
        piv_par_dest->spof_filter = piv_par_src->spof_filter;
        piv_par_dest->spof_filter__set = TRUE;
    }

    if ((force && piv_par_src->gauss_weight_ia__set)
        || ( piv_par_src->gauss_weight_ia__set 
             && !piv_par_dest->gauss_weight_ia__set)) {
        piv_par_dest->gauss_weight_ia = piv_par_src->gauss_weight_ia;
        piv_par_dest->gauss_weight_ia__set = TRUE;
    }

    if ((force && piv_par_src->ifit__set)
        || ( piv_par_src->ifit__set 
             && !piv_par_dest->ifit__set)) {
        piv_par_dest->ifit = piv_par_src->ifit;
        piv_par_dest->ifit__set = TRUE;
    }

}



static herr_t 
attr_info                               (hid_t loc_id, 
                                         const gchar *name, 
                                         GpivPivPar * piv_par
                                         )
/*-----------------------------------------------------------------------------
 * Operator function.
 */
{
    hid_t attribute_id, atype;
    herr_t status;



/*
 * Image piv parametes
 */
    if (strcmp(name, PIVPAR_KEY__INT_GEO) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_geo); 
        status = H5Aclose(attribute_id); 
        piv_par->int_geo__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__COL_START) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->col_start); 
        status = H5Aclose(attribute_id); 
        piv_par->col_start__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__COL_END) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->col_end); 
        status = H5Aclose(attribute_id); 
        piv_par->col_end__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__ROW_START) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->row_start); 
        status = H5Aclose(attribute_id); 
        piv_par->row_start__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__ROW_END) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->row_end); 
        status = H5Aclose(attribute_id); 
        piv_par->row_end__set = TRUE;
    }

/*
 * line
 */
    if (strcmp(name, PIVPAR_KEY__INT_LINE_ROW) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_line_row); 
        status = H5Aclose(attribute_id); 
        piv_par->int_line_row__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__INT_LINE_ROW_START) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_line_row_start); 
        status = H5Aclose(attribute_id); 
        piv_par->int_line_row_start__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__INT_LINE_ROW_END) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_line_row_end); 
        status = H5Aclose(attribute_id); 
        piv_par->int_line_row_end__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__INT_LINE_COL) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_line_col); 
        status = H5Aclose(attribute_id); 
        piv_par->int_line_col__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__INT_LINE_COL_START) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_line_col_start); 
        status = H5Aclose(attribute_id); 
        piv_par->int_line_col_start__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__INT_LINE_COL_END) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_line_col_end); 
        status = H5Aclose(attribute_id); 
        piv_par->int_line_col_end__set = TRUE;
    }


/*
 * Interrogation
 */
    if (strcmp(name, PIVPAR_KEY__INT_SIZE_F) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_size_f); 
        status = H5Aclose(attribute_id); 
        piv_par->int_size_f__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__INT_SIZE_I) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_size_i); 
        status = H5Aclose(attribute_id); 
        piv_par->int_size_i__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__INT_SHIFT) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_shift); 
        status = H5Aclose(attribute_id); 
        piv_par->int_shift__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__PRE_SHIFT_COL) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->pre_shift_col); 
        status = H5Aclose(attribute_id); 
        piv_par->pre_shift_col__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__PRE_SHIFT_ROW) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->pre_shift_row); 
        status = H5Aclose(attribute_id); 
        piv_par->pre_shift_row__set = TRUE;
    }

/*
 * Sub-pixel interrogation
 */
    if (strcmp(name, PIVPAR_KEY__IFIT) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->ifit); 
        status = H5Aclose(attribute_id); 
        piv_par->ifit__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__PEAK) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->peak); 
        status = H5Aclose(attribute_id); 
        piv_par->peak__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__INT_SCHEME) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->int_scheme); 
        status = H5Aclose(attribute_id); 
        piv_par->int_scheme__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__SPOF_FILTER) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->spof_filter); 
        status = H5Aclose(attribute_id); 
        piv_par->spof_filter__set = TRUE;
    }


    if (strcmp(name, PIVPAR_KEY__GAUSS_WEIGHT_IA) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->gauss_weight_ia); 
        status = H5Aclose(attribute_id); 
        piv_par->gauss_weight_ia__set = TRUE;
    }


    if (strcmp(name, "print_piv") == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &piv_par->print_piv); 
        status = H5Aclose(attribute_id); 
        piv_par->print_piv__set = TRUE;
    }


    return 0;
}



static void 
obtain_pivpar_fromline                  (char line[], 
                                         GpivPivPar *piv_par, 
                                         gboolean verbose
                                         )
/*-----------------------------------------------------------------------------
 * Scans a line to get a member of GpivPivPar
 */
{
    char par_name[GPIV_MAX_CHARS];


    if (line[0] != '#' && line[0] != '\n' && line[0] != ' ' 
        && line[0] != '\t') {
        sscanf (line,"%s", par_name);

        if (piv_par->int_geo__set == FALSE) {
            piv_par->int_geo__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_GEO, TRUE,
                               line, par_name, &piv_par->int_geo, 
                               verbose, NULL);
        }

        if (piv_par->col_start__set == FALSE) {
            piv_par->col_start__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, PIVPAR_KEY__COL_START, TRUE,
                               line, par_name, &piv_par->col_start, 
                               verbose, NULL);
        }

        if (piv_par->col_end__set == FALSE) {
            piv_par->col_end__set =  
                gpiv_scan_iph (GPIV_PIVPAR_KEY, PIVPAR_KEY__COL_END, TRUE,
                               line, par_name, &piv_par->col_end, 
                               verbose, NULL);
        }

        if (piv_par->row_start__set == FALSE) {
            piv_par->row_start__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, PIVPAR_KEY__ROW_START, TRUE,
                               line, par_name, &piv_par->row_start, 
                               verbose, NULL);
        }

        if (piv_par->row_end__set == FALSE) {
            piv_par->row_end__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, PIVPAR_KEY__ROW_END, TRUE,
                               line, par_name, &piv_par->row_end, 
                               verbose, NULL);
        }

        if (piv_par->int_line_row__set == FALSE) {
            piv_par->int_line_row__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_ROW, TRUE,
                               line, par_name, &piv_par->int_line_row, 
                               verbose, NULL);
        }

        if (piv_par->int_line_row_start__set == FALSE) {
            piv_par->int_line_row_start__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_LINE_ROW_START, TRUE,
                               line, par_name, &piv_par->int_line_row_start, 
                               verbose, NULL);
        }

        if (piv_par->int_line_row_end__set == FALSE) {
            piv_par->int_line_row_end__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY,  
                               PIVPAR_KEY__INT_LINE_ROW_END, TRUE,
                               line, par_name, &piv_par->int_line_row_end, 
                               verbose, NULL);
        }

        if (piv_par->int_line_col__set == FALSE) {
            piv_par->int_line_col__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, PIVPAR_KEY__INT_LINE_COL, TRUE,
                               line, par_name, &piv_par->int_line_col, 
                               verbose, NULL);
        }

        if (piv_par->int_line_col_start__set == FALSE) {
            piv_par->int_line_col_start__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_LINE_COL_START, TRUE,
                               line, par_name, &piv_par->int_line_col_start, 
                               verbose, NULL);
        }

        if (piv_par->int_line_col_end__set == FALSE) {
            piv_par->int_line_col_end__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_LINE_COL_END, TRUE,
                               line, par_name, &piv_par->int_line_col_end, 
                               verbose, NULL);
        }

        if (piv_par->int_point_col__set == FALSE) {
            piv_par->int_point_col__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_POINT_COL, TRUE,
                               line, par_name, &piv_par->int_point_col, 
                               verbose, NULL);
        }

        if (piv_par->int_point_row__set == FALSE) {
            piv_par->int_point_row__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_POINT_ROW, TRUE,
                               line, par_name, &piv_par->int_point_row, 
                               verbose, NULL);
        }



        if (piv_par->int_size_f__set == FALSE) {
            piv_par->int_size_f__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_SIZE_F, TRUE,
                               line, par_name, &piv_par->int_size_f, 
                               verbose, NULL);
        }

        if (piv_par->int_size_i__set == FALSE) {
            piv_par->int_size_i__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_SIZE_I,  TRUE,
                               line, par_name, &piv_par->int_size_i, 
                               verbose, NULL);
        }

        if (piv_par->int_shift__set == FALSE) {
            piv_par->int_shift__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_SHIFT,  TRUE,
                               line, par_name, &piv_par->int_shift, 
                               verbose, NULL);
        }

        if (piv_par->pre_shift_col__set == FALSE) {
            piv_par->pre_shift_col__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__PRE_SHIFT_COL,  TRUE,
                               line, par_name, &piv_par->pre_shift_col, 
                               verbose, NULL);
        }

        if (piv_par->pre_shift_row__set == FALSE) {
            piv_par->pre_shift_row__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__PRE_SHIFT_ROW, TRUE,
                               line, par_name, &piv_par->pre_shift_row, 
                               verbose, NULL);
        }

        if (piv_par->int_scheme__set == FALSE) {
            piv_par->int_scheme__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__INT_SCHEME,  TRUE,
                               line, par_name, (int *) &piv_par->int_scheme, 
                               verbose, NULL);
        }

        if (piv_par->spof_filter__set == FALSE) {
            piv_par->spof_filter__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__SPOF_FILTER,  TRUE,
                               line, par_name, (int *) &piv_par->spof_filter, 
                               verbose, NULL);
        }

        if (piv_par->gauss_weight_ia__set == FALSE) {
            piv_par->gauss_weight_ia__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__GAUSS_WEIGHT_IA, TRUE,
                               line, par_name, (int *) &piv_par->gauss_weight_ia, 
                               verbose, NULL);
        }

        if (piv_par->peak__set == FALSE) {
            piv_par->peak__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__PEAK, TRUE,
                               line, par_name, &piv_par->peak, 
                               verbose, NULL);
        }
            
        if (piv_par->ifit__set == FALSE) {
            piv_par->ifit__set = 
                gpiv_scan_iph (GPIV_PIVPAR_KEY, 
                               PIVPAR_KEY__IFIT, TRUE,
                               line, par_name, &piv_par->ifit, 
                               verbose, NULL);
        }

    }


    return;
}
