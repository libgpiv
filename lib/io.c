/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



---------------------------------------------------------------
FILENAME:                io.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:      gpiv_io_make_fname

			 gpiv_read_image
			 gpiv_fread_image

                         gpiv_find_pivdata_origin
                         gpiv_read_pivdata 
                         gpiv_read_pivdata_fastx 
                         gpiv_write_pivdata

                         gpiv_read_scdata 
                         gpiv_write_scdata

			 gpiv_fwrite_griddata
			 gpiv_print_histo
			 gpiv_fprint_histo


LOCAL FUNCTIONS:

LAST MODIFICATION DATE:  $Id: io.c,v 1.19 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------- */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <hdf5.h>

#include <gpiv.h>


static void
obtain_scdata_fromline (gchar line[], 
                        GpivScalarData *piv_data,
                        guint *i,
                        guint *j
                        );

/*
 * Public functions
 */

void 
gpiv_io_make_fname (const gchar *fname_base, 
		    const gchar *EXT, 
                    gchar *fname_out
		   ) 
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *  Constructs (output) filename from base name and exetsion 
 *
 * INPUTS:
 *      fname_base:     file base name
 *      EXT:            file extension name
 * OUTPUTS:
 *      fname_out:      completed filename
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_snprintf(fname_out, GPIV_MAX_CHARS, "%s%s", fname_base, EXT);
}




/*
 * Local functions
 */

