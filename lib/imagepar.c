/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2011 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <gpiv.h>


/*
 * Prototypes
 */

static void
ovwrt_parameters                        (const GpivImagePar     *image_par_src, 
					 GpivImagePar           *image_par_dest,
					 gboolean                force
					 );

static GpivImagePar *
scan_image_parameters                   (FILE                   *fp_h, 
					 FILE                   *fp_par_out,
					 gboolean               verbose,
					 gboolean               par_key
					 );

static herr_t 
attr_info                               (hid_t                  loc_id, 
					 const gchar            *name, 
					 GpivImagePar           *image_par
					 );

static void
img_print_parameters                    (FILE                   *fp, 
					 const GpivImagePar     *image_par,
					 const gboolean         key
					 );

static void
obtain_imagepar_fromline                (char                   line[], 
					 GpivImagePar           *image_par, 
					 FILE                   *fp_verbose, 
					 gboolean               verbose, 
					 gboolean               include_key
					 );

/*
 * Public functions
 */

void
gpiv_img_parameters_set                 (GpivImagePar           *image_par,
                                         const gboolean         flag
                                         )
/*-----------------------------------------------------------------------------
 * Set flag for image_par __set */
{
    image_par->ncolumns__set = flag;
    image_par->nrows__set = flag;
    image_par->depth__set = flag;
    image_par->x_corr__set = flag;
    image_par->s_scale__set = flag;
    image_par->t_scale__set = flag;
    image_par->z_off_x__set = flag;
    image_par->z_off_y__set = flag;
    
    image_par->title__set = flag;
    image_par->creation_date__set = flag;
    image_par->location__set = flag;
    image_par->author__set = flag;
    image_par->software__set = flag;
    image_par->source__set = flag;
    image_par->usertext__set = flag;
    image_par->warning__set = flag;
    image_par->disclaimer__set = flag;
    image_par->comment__set = flag;
    image_par->copyright__set = flag;
    image_par->email__set = flag;
    image_par->url__set = flag;

}



GpivImagePar *
gpiv_img_get_parameters_from_resources  (const gchar		*localrc,
                                         const gboolean		verbose
                                         )
/*-----------------------------------------------------------------------------
 *      Reads image parameters from localrc, $HOME/.gpivrc and 
 *      system-wide gpiv.conf 
 */
{
    GpivImagePar *image_par = g_new0 (GpivImagePar, 1);


    gpiv_img_parameters_set (image_par, FALSE);
    gpiv_scan_parameter (GPIV_IMGPAR_KEY, localrc, image_par, verbose);
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, verbose);


    return image_par;
}



void
gpiv_img_default_parameters		(GpivImagePar		*image_par,
					 const gboolean		force
					 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Sets default parameter values
 *
 * INPUTS:
 *
 * OUTPUTS:
 *     piv_img_par_default:   structure of image parameters
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    if (!image_par->depth__set || force)
        image_par->depth = GPIV_IMGPAR_DEFAULT__DEPTH;
    if (!image_par->ncolumns__set || force)
        image_par->ncolumns = GPIV_IMGPAR_DEFAULT__NCOLUMNS;
    if (!image_par->nrows__set || force)
        image_par->nrows = GPIV_IMGPAR_DEFAULT__NROWS;
    if (!image_par->x_corr__set || force)
        image_par->x_corr = GPIV_IMGPAR_DEFAULT__X_CORR;
    if (!image_par->s_scale__set || force)
        image_par->s_scale = GPIV_IMGPAR_DEFAULT__S_SCALE;
    if (!image_par-> t_scale__set || force)
        image_par-> t_scale = GPIV_IMGPAR_DEFAULT__T_SCALE;
    if (!image_par->z_off_x__set || force)
        image_par->z_off_x = GPIV_IMGPAR_DEFAULT__Z_OFF_X;
    if (!image_par->z_off_y__set || force)
        image_par->z_off_y = GPIV_IMGPAR_DEFAULT__Z_OFF_Y;

    if (!image_par->title__set || force)
        snprintf (image_par->title, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__TITLE);
    if (!image_par->creation_date__set || force)
        snprintf (image_par->creation_date, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__CREATION_DATE);
    if (!image_par->location__set || force)
        snprintf (image_par->location, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__LOCATION);
    if (!image_par->author__set || force)
        snprintf (image_par->author, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__AUTHOR);
    if (!image_par->software__set || force)
        snprintf (image_par->software, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__SOFTWARE);
    if (!image_par->source__set || force)
        snprintf (image_par->source, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__SOURCE);
    if (!image_par->usertext__set || force)
        snprintf (image_par->usertext, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__USERTEXT);
    if (!image_par->warning__set || force)
        snprintf (image_par->warning, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__WARNING);
    if (!image_par->disclaimer__set || force)
        snprintf (image_par->disclaimer, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__DISCLAIMER);
    if (!image_par->comment__set || force)
        snprintf (image_par->comment, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__COMMENT);
    if (!image_par->copyright__set || force)
        snprintf (image_par->copyright, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__COPYRIGHT);
    if (!image_par->email__set || force)
        snprintf (image_par->email, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__EMAIL);
    if (!image_par->url__set || force)
        snprintf (image_par->url, GPIV_MAX_CHARS, "%s", 
                  GPIV_IMGPAR_DEFAULT__URL);

    gpiv_img_parameters_set (image_par, TRUE);
}



void
gpiv_img_read_header			(FILE			*fp,
					GpivImagePar		*image_par,
					const gboolean		include_key,
					const gboolean		verbose,
					FILE			*fp_verbose
					)
/*---------------------------------------------------------------------------*/
/**
 *     Reads each line of file fp and looks for image header parameters 
 *     without IMAGE_PAR_KEY
 *
 *     @param[in] fp              file pointer of which parameters will be read from.
 *                                If NULL, stdin will be used.
 *     @param[in] fp_verbose      file pointer of which parameter will have to be printed
 *                                or NULL for stdout
 *     @param[in] verbose         flag to print parameters to fp_verbose (TRUE) or not (FALSE)
 *     @param[in] include_key  flag if module key is included
 *     @return                    pointer to GpivImagePar
 */
/*---------------------------------------------------------------------------*/
{
    char line[GPIV_MAX_CHARS];


    if (fp_verbose == NULL) fp_verbose = stdout;

    if (fp == stdin || fp == NULL) {
        while (gets (line) != NULL) {
            obtain_imagepar_fromline (line, image_par, fp_verbose, verbose, include_key);
        }
    } else {
        while (fgets (line, GPIV_MAX_CHARS, fp) != NULL) {
            obtain_imagepar_fromline (line, image_par, fp_verbose, verbose, include_key);
        }
    }


    return;
}



gchar *
gpiv_img_check_header_read		(const GpivImagePar     *image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *   Check out if all parameters have been read
 *
 * INPUTS:
 *     image_par:      image parameters
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;

    if ((err_msg =
         gpiv_img_check_header_required_read (image_par))
        != NULL) return (err_msg);

    if ((err_msg =
         gpiv_img_check_header_scale_read (image_par))
        != NULL) return (err_msg);

    return (err_msg);
}


gchar *
gpiv_img_check_header_required_read     (const GpivImagePar     *image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Check out if the required image header info for reading/storing
 *      has been read
 *
 * INPUTS:
 *     image_par:      image parameters
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if (image_par->ncolumns__set == FALSE) {
	err_msg = g_strdup_printf ("gpiv_img_check_header_required_read: %s not set",
                                   GPIV_IMGPAR_KEY__NCOLUMNS);
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_par->nrows__set == FALSE) {
	err_msg = g_strdup_printf ("gpiv_img_check_header_required_read: %s not set",
                                   GPIV_IMGPAR_KEY__NROWS);
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_par->x_corr__set == FALSE) {
	err_msg = g_strdup_printf ("gpiv_img_check_header_required_read: %s not set",
                                   GPIV_IMGPAR_KEY__X_CORR);
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_par->depth__set == FALSE) {
	err_msg = g_strdup_printf ("gpiv_img_check_header_required_read: %s not set",
            GPIV_IMGPAR_KEY__DEPTH);
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


    return (err_msg);
}



gchar *
gpiv_img_check_header_scale_read        (const GpivImagePar     *image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Check out if parameters for time/spatial scaling have been read
 *
 * INPUTS:
 *     image_par:      image parameters
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if (image_par->s_scale__set == FALSE) {
        err_msg = g_strdup_printf ("gpiv_img_check_header_read: %s not set\n",
                                   GPIV_IMGPAR_KEY__S_SCALE);
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_par->t_scale__set == FALSE) {
	err_msg = g_strdup_printf ("gpiv_img_check_header_read: %s not set\n",
                                   GPIV_IMGPAR_KEY__T_SCALE);
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_par->z_off_x__set == FALSE) {
	err_msg = g_strdup_printf ("gpiv_img_check_header_read: %s not set\n",
                                   GPIV_IMGPAR_KEY__Z_OFF_X);
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_par->z_off_y__set == FALSE) {
	err_msg = g_strdup_printf ("gpiv_img_check_header_read: %s not set\n",
                                   GPIV_IMGPAR_KEY__Z_OFF_Y);
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


    return err_msg;
}



gchar *
gpiv_img_test_header			(const GpivImagePar	*image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Test header information on validity
 *
 * INPUTS:
 *     image_par:      image parameters
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
 

    if (image_par->nrows < 0 || image_par->nrows > GPIV_MAX_IMG_SIZE) {
        err_msg = "gpiv_img_test_header: numbers of rows larger than allowed maximum";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_par->ncolumns < 0 || image_par->ncolumns > GPIV_MAX_IMG_SIZE) {
        err_msg = "gpiv_img_test_header: numbers of columns larger than allowed maximum";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_par->depth < 1 || image_par->depth > GPIV_MAX_IMG_DEPTH) {
        err_msg = "gpiv_img_test_header: depth larger than allowed maximum";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


    return err_msg;
}



void 
gpiv_img_print_header			(FILE			*fp,
					const GpivImagePar	*image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      prints image header parameters to fp with or without key
 *---------------------------------------------------------------------------*/
{
    img_print_parameters (fp, image_par, FALSE);
}



void 
gpiv_img_fprint_header			(const gchar		*fname,
					 const GpivImagePar	*image_par
					 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     prints image header parameters to file
 *     without GPIV_IMGPAR_KEY
 *
 * PROTOTYPE LOCATATION:
 *     img.h
 *
 * INPUTS:
 *     fp_par_out:     file pointer to output file
 *     image_par:      image parameter structure
 *
 * OUTPUTS:
 *---------------------------------------------------------------------------*/
{
    FILE *fp;
    gboolean key = FALSE;


    if ((fp = fopen (fname, "w")) == NULL) {
        gpiv_warning ("gpiv_img_fprint_header: failing fopen");
        return;
    }

    img_print_parameters (fp, image_par, FALSE);
    fclose (fp);
}



void
gpiv_img_print_parameters		(FILE                   *fp, 
					const GpivImagePar	*image_par
					)
/*-----------------------------------------------------------------------------
 *     prints image header parameters to file fp
 *---------------------------------------------------------------------------*/
{
    img_print_parameters (fp, image_par, TRUE);
}


void 
gpiv_img_fprint_parameters		(const gchar            *fname,
					const GpivImagePar	*image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     prints image header parameters to file
 *---------------------------------------------------------------------------*/
{
    FILE *fp;


    if ((fp = fopen (fname, "w")) == NULL) {
        gpiv_warning ("gpiv_img_fprint_parameters: failing fopen");
        return;
    }

    img_print_parameters (fp, image_par, TRUE);
    fclose (fp);
}



GpivImagePar *
gpiv_img_cp_parameters			(const GpivImagePar	*image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Copies image parameters from src to dest
 */
{
    GpivImagePar *image_par_dest = g_new0 (GpivImagePar, 1);


    gpiv_img_parameters_set (image_par_dest, FALSE);
    ovwrt_parameters (image_par, image_par_dest, FALSE);


    return image_par_dest;
}



void
gpiv_img_ovwrt_parameters		(const GpivImagePar	*image_par_src,
					GpivImagePar		*image_par_dest
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Duplicates image parameters from src to dest if _dest has not been set
 */
{
    ovwrt_parameters (image_par_src, image_par_dest, TRUE);
}



GpivImagePar *
gpiv_img_fread_hdf5_parameters		(const gchar		*fname
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads image parameters from hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    GpivImagePar *image_par = g_new0 (GpivImagePar, 1);
    gchar *err_msg = NULL;
    int i;
    /*
     * HDF declarations
     */
    hid_t       file_id, group_id, attribute_id, atype; 
    herr_t      status;

    if ((i = H5Fis_hdf5 (fname)) == 0) {
        err_msg = "gpiv_img_fread_hdf5_parameters: not an hdf5 file";
        gpiv_warning ("%s", err_msg);
        return NULL;
    }

    file_id = H5Fopen (fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "IMAGE", H5P_DEFAULT);
    H5Aiterate (group_id, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, 0,
                (H5A_operator_t)attr_info, image_par);
    status = H5Gclose (group_id);
    status = H5Fclose (file_id); 
    return image_par;
}



gchar *
gpiv_img_fwrite_hdf5_parameters		(const gchar            *fname,
                                         const GpivImagePar	*image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes image parameters to an existing hdf5 data file
 *
 * PROTOTYPE LOCATATION:
 *     img.h
 *
 * INPUTS:
 *     fname:          complete file name
 *     piv_valid_par:  parameter structure
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i;

    /*
     * HDF declarations
     */
    hid_t       file_id, dataspace_id, group_id, attribute_id,
        atype; 
    hsize_t     dims[1];
    herr_t      status;


    if ((i = H5Fis_hdf5 (fname)) == 0) {
        err_msg = "gpiv_img_fwrite_hdf5_parameters: not an hdf5 file";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    file_id = H5Fopen (fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "IMAGE", H5P_DEFAULT);



    dims[0] = 1;
    dataspace_id = H5Screate_simple (1, dims, NULL);



    /*
     * General image parameters
     */
    if (image_par->ncolumns__set) {
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__NCOLUMNS, 
                                  H5T_NATIVE_INT, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &image_par->ncolumns); 
        status = H5Aclose (attribute_id); 
    }

    if (image_par->nrows__set) {
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__NROWS, 
                                  H5T_NATIVE_INT, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, H5T_NATIVE_INT, 
                           &image_par->nrows); 
        status = H5Aclose (attribute_id); 
    }

    if (image_par->x_corr__set) {
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__X_CORR, 
                                  H5T_NATIVE_INT, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, H5T_NATIVE_INT, 
                           &image_par->x_corr); 
        status = H5Aclose (attribute_id); 
    }

    if (image_par->depth__set) {
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__DEPTH, 
                                  H5T_NATIVE_INT, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, H5T_NATIVE_INT, 
                           &image_par->depth); 
        status = H5Aclose (attribute_id); 
    }

    /*
     * Variables and parameters used by gpiv_post_scale
     */
    if (image_par->s_scale__set) {
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__S_SCALE, 
                                  H5T_NATIVE_FLOAT, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, H5T_NATIVE_FLOAT, 
                           &image_par->s_scale); 
        status = H5Aclose (attribute_id); 
    }

    if (image_par->t_scale__set) {
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__T_SCALE, 
                                  H5T_NATIVE_FLOAT, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, H5T_NATIVE_FLOAT, 
                           &image_par->t_scale); 
        status = H5Aclose (attribute_id); 
    }

    if (image_par->z_off_x__set) {
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__Z_OFF_X, 
                                  H5T_NATIVE_FLOAT, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, H5T_NATIVE_FLOAT, 
                           &image_par->z_off_x); 
        status = H5Aclose (attribute_id); 
    }

    if (image_par->z_off_y__set) {
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__Z_OFF_Y, 
                                  H5T_NATIVE_FLOAT, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, H5T_NATIVE_FLOAT, 
                           &image_par->z_off_y); 
        status = H5Aclose (attribute_id); 
    }

    status = H5Sclose (dataspace_id);


    /*
     * Optional parameters
     */
    if (image_par->title__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__TITLE, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->title); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->creation_date__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__CREATION_DATE, 
                                  atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->creation_date); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->location__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__LOCATION, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->location); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->author__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__AUTHOR, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->author); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->software__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__SOFTWARE, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->software); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->source__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__SOURCE, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->source); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->usertext__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__USERTEXT, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->usertext); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->warning__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__WARNING, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->warning); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->disclaimer__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__DISCLAIMER, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->disclaimer); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->comment__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__COMMENT, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->comment); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }



    if (image_par->copyright__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__COPYRIGHT, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->copyright); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }


    if (image_par->email__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__EMAIL, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->email); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }

    if (image_par->url__set) {
        dataspace_id = H5Screate (H5S_SCALAR);
	atype = H5Tcopy (H5T_C_S1);
	H5Tset_size (atype, GPIV_MAX_CHARS);
        attribute_id = H5Acreate (group_id, GPIV_IMGPAR_KEY__URL, atype, 
                                  dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite (attribute_id, atype, 
                           &image_par->url); 
        status = H5Aclose (attribute_id); 
	status = H5Sclose (dataspace_id);
    }



    status = H5Gclose (group_id);
    status = H5Fclose (file_id); 
    return err_msg;
}



gchar *
gpiv_img_fread_davis_parameters		(const gchar		*fname,
					GpivImagePar		*image_par
					)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Reads image specifications from Davis formatted image, with ext .IMG, 
 *      from file
 *
 * PROTOTYPE LOCATATION:
 *     img.h
 *
 * INPUTS:
 *     fname:          pointer to complete filename to be read
 *
 * RETURNS:
 *     image_par:      structure containing image dimensions and other image 
 *                     specifications
 *---------------------------------------------------------------------------*/
{
    FILE *fp;
    gchar *err_msg = NULL;

    image_par->nrows = 0;
    image_par->ncolumns = 0;
    image_par->depth = GPIV_DAVIS_IMG_DEPTH; 
    image_par->x_corr = 1;
    image_par->depth__set = TRUE; 
    image_par->x_corr__set = TRUE;

    if ((fp = fopen (fname, "rb")) == NULL) {
	err_msg = "gpiv_img_fread_davis_image: Failure opening for input";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


    fseek (fp, 10, SEEK_SET);
    fread (&image_par->nrows, sizeof (guint16), 1, fp);
    fread (&image_par->ncolumns, sizeof (guint16), 1, fp);
    image_par->nrows__set = TRUE;
    image_par->ncolumns__set = TRUE;

    fclose (fp);
    return err_msg;
}



#ifdef ENABLE_MPI
void
gpiv_img_mpi_bcast_imgpar		(GpivImagePar		*image_par
					)
/*-----------------------------------------------------------------------------
 * Broadcasts ONLY required image header/parameters
 */
{
    if (
        MPI_Bcast(&image_par->ncolumns, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, gpiv_img_mpi_bcast_img_par: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(&image_par->ncolumns__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, gpiv_img_mpi_bcast_img_par: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(&image_par->nrows, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, gpiv_img_mpi_bcast_img_par: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(&image_par->nrows__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, gpiv_img_mpi_bcast_img_par: An error ocurred in MPI_Bcast");
        }

    if (
        MPI_Bcast(&image_par->depth, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, gpiv_img_mpi_bcast_img_par: An error ocurred in MPI_Bcast");
        }

    if (
        MPI_Bcast(&image_par->depth__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, gpiv_img_mpi_bcast_img_par: An error ocurred in MPI_Bcast");
        }

    if (
        MPI_Bcast(&image_par->x_corr, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, gpiv_img_mpi_bcast_img_par: An error ocurred in MPI_Bcast");
        }

    if (
        MPI_Bcast(&image_par->x_corr__set, 1, MPI_INT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, gpiv_img_mpi_bcast_img_par: An error ocurred in MPI_Bcast");
    }
}

#endif /* ENABLE_MPI */


/*
 * Local functions
 */

static void
ovwrt_parameters (const GpivImagePar *image_par_src, 
                  GpivImagePar *image_par_dest,
                  gboolean force
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Overwrites image parameters from src to dest
 *
 *---------------------------------------------------------------------------*/
{
    if ((force && image_par_src->nrows__set) 
        || (image_par_src->nrows__set && !image_par_dest->nrows__set)) {
        image_par_dest->nrows = image_par_src->nrows;
        image_par_dest->nrows__set = TRUE;
    }

    if ((force && image_par_src->ncolumns__set)
        || (image_par_src->ncolumns__set && !image_par_dest->ncolumns__set)) {
        image_par_dest->ncolumns = image_par_src->ncolumns;
        image_par_dest->ncolumns__set = TRUE;
    }

    if ((force && image_par_src->depth__set)
       || (image_par_src->depth__set && !image_par_dest->depth__set)) {
        image_par_dest->depth = image_par_src->depth;
        image_par_dest->depth__set = TRUE;
    }

    if ((force && image_par_src->x_corr__set)
       || (image_par_src->x_corr__set && !image_par_dest->x_corr__set)) {
        image_par_dest->x_corr = image_par_src->x_corr;
        image_par_dest->x_corr__set = TRUE;
    }

    if ((force && image_par_src->s_scale__set)
        || (image_par_src->s_scale__set && !image_par_dest->s_scale__set)) {
        image_par_dest->s_scale = image_par_src->s_scale;
        image_par_dest->s_scale__set = TRUE;
    }

    if ((force && image_par_src->t_scale__set)
       || (image_par_src->t_scale__set && !image_par_dest->t_scale__set)) {
        image_par_dest->t_scale = image_par_src->t_scale;
        image_par_dest->t_scale__set = TRUE;
    }

    if ((force && image_par_src->z_off_x__set)
        || (image_par_src->z_off_x__set && !image_par_dest->z_off_x__set)) {
        image_par_dest->z_off_x = image_par_src->z_off_x;
        image_par_dest->z_off_x__set = TRUE;
    }

    if ((force && image_par_src->z_off_y__set)
        || (image_par_src->z_off_y__set && !image_par_dest->z_off_y__set)) {
        image_par_dest->z_off_y = image_par_src->z_off_y;
        image_par_dest->z_off_y__set = TRUE;
    }

    if ((force && image_par_src->title__set)
        || (image_par_src->title__set && !image_par_dest->title__set)) {
        image_par_dest->title__set = TRUE;
        snprintf(image_par_dest->title, GPIV_MAX_CHARS, "%s", 
                 image_par_src->title);
    }
    
    
    if ((force && image_par_src->creation_date__set)
        || (image_par_src->creation_date__set 
            && !image_par_dest->creation_date__set)) {
        image_par_dest->creation_date__set = TRUE;
        snprintf (image_par_dest->creation_date, GPIV_MAX_CHARS, "%s", 
                  image_par_src->creation_date);
    }
    
    if ((force && image_par_src->creation_date__set)
        || (image_par_src->creation_date__set 
            && !image_par_dest->creation_date__set)) {
        snprintf (image_par_dest->creation_date, GPIV_MAX_CHARS, "%s", 
                  image_par_src->creation_date);
        image_par_dest->creation_date__set = TRUE;

    } else if (force && !image_par_src->creation_date__set) {
        /*
         * Create if not exists
         */
        char time_string[GPIV_MAX_CHARS];
        struct tm time_struct;
        time_t itime;

        time (&itime);
        time_struct = *gmtime (&itime);
        strftime (time_string, GPIV_MAX_CHARS, "%a %b %d %Y %T", &time_struct);
        snprintf (image_par_dest->creation_date, GPIV_MAX_CHARS, "%s", 
                  time_string);
        image_par_dest->creation_date__set = TRUE;
    }
    
    
    if ((force && image_par_src->location__set)
        || (image_par_src->location__set && !image_par_dest->location__set)) {
        image_par_dest->location__set = TRUE;
        snprintf (image_par_dest->location, GPIV_MAX_CHARS, "%s", 
                  image_par_src->location);
    }
    
    
    if ((force && image_par_src->author__set)
        || (image_par_src->author__set && !image_par_dest->author__set)) {
        image_par_dest->author__set = TRUE;
        snprintf (image_par_dest->author, GPIV_MAX_CHARS, "%s", 
                  image_par_src->author);
    }

    if ((force && image_par_src->software__set)
        || (image_par_src->software__set && !image_par_dest->software__set)) {
        image_par_dest->software__set = TRUE;
        snprintf (image_par_dest->software, GPIV_MAX_CHARS, "%s", 
                  image_par_src->software);
    }

    if ((force && image_par_src->source__set)
        || (image_par_src->source__set && !image_par_dest->source__set)) {
        image_par_dest->source__set = TRUE;
        snprintf (image_par_dest->source, GPIV_MAX_CHARS, "%s", 
                  image_par_src->source);
    }

    if ((force && image_par_src->usertext__set)
        || (image_par_src->usertext__set && !image_par_dest->usertext__set)) {
        image_par_dest->usertext__set = TRUE;
        snprintf (image_par_dest->usertext, GPIV_MAX_CHARS, "%s", 
                  image_par_src->usertext);
    }

    if ((force && image_par_src->warning__set)
        || (image_par_src->warning__set && !image_par_dest->warning__set)) {
        image_par_dest->warning__set = TRUE;
        snprintf (image_par_dest->warning, GPIV_MAX_CHARS, "%s", 
                  image_par_src->warning);
    }

    if ((force && image_par_src->disclaimer__set)
        || (image_par_src->disclaimer__set && !image_par_dest->disclaimer__set)) {
        image_par_dest->disclaimer__set = TRUE;
        snprintf (image_par_dest->disclaimer, GPIV_MAX_CHARS, "%s", 
                  image_par_src->disclaimer);
    }

    if ((force && image_par_src->comment__set)
        || (image_par_src->comment__set && !image_par_dest->comment__set)) {
        image_par_dest->comment__set = TRUE;
        snprintf (image_par_dest->comment, GPIV_MAX_CHARS, "%s", 
                  image_par_src->comment);
    }

    if ((force && image_par_src->copyright__set)
        || (image_par_src->copyright__set && !image_par_dest->copyright__set)) {
        image_par_dest->copyright__set = TRUE;
        snprintf (image_par_dest->copyright, GPIV_MAX_CHARS, "%s", 
                  image_par_src->copyright);
    }

    if ((force && image_par_src->email__set)
        || (image_par_src->email__set && !image_par_dest->email__set)) {
        image_par_dest->email__set = TRUE;
        snprintf (image_par_dest->email, GPIV_MAX_CHARS, "%s", 
                  image_par_src->email);
    }

    if ((force && image_par_src->url__set)
        || (image_par_src->url__set && !image_par_dest->url__set)) {
        image_par_dest->url__set = TRUE;
        snprintf (image_par_dest->url, GPIV_MAX_CHARS, "%s", 
                  image_par_src->url);
    }
}


/*
 * Local functions
 */

static herr_t 
attr_info (hid_t loc_id, 
           const gchar *name, 
           GpivImagePar *image_par
           )
/*-----------------------------------------------------------------------------
 * Operator function that actually reads the parameters.
 */
{
    hid_t attribute_id, atype;
    herr_t status;



    /*
     * General image parameters
     */
    if (strcmp (name, GPIV_IMGPAR_KEY__NCOLUMNS) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        status = H5Aread (attribute_id, H5T_NATIVE_INT, 
                          &image_par->ncolumns); 
	status = H5Aclose (attribute_id); 
	image_par->ncolumns__set = TRUE;
    }    


    if (strcmp (name, GPIV_IMGPAR_KEY__NROWS) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        status = H5Aread (attribute_id, H5T_NATIVE_INT, 
                          &image_par->nrows); 
	status = H5Aclose (attribute_id); 
	image_par->nrows__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__X_CORR) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        status = H5Aread (attribute_id, H5T_NATIVE_INT, 
                          &image_par->x_corr); 
        status = H5Aclose (attribute_id); 
        image_par->x_corr__set = TRUE;
    }    


    if (strcmp (name, GPIV_IMGPAR_KEY__DEPTH) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        status = H5Aread (attribute_id, H5T_NATIVE_INT, 
                          &image_par->depth); 
        status = H5Aclose (attribute_id); 
        image_par->depth__set = TRUE;
    }


    /*
     * Variables and parameters of scale
     */
    if (strcmp (name, GPIV_IMGPAR_KEY__S_SCALE) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        status = H5Aread (attribute_id, H5T_NATIVE_FLOAT, 
                          &image_par->s_scale); 
        status = H5Aclose (attribute_id); 
        image_par->s_scale__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__T_SCALE) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        status = H5Aread (attribute_id, H5T_NATIVE_FLOAT, 
                          &image_par->t_scale); 
        status = H5Aclose (attribute_id); 
        image_par->t_scale__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__Z_OFF_X) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        status = H5Aread (attribute_id, H5T_NATIVE_FLOAT, 
                          &image_par->z_off_x); 
        status = H5Aclose (attribute_id); 
        image_par->z_off_x__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__Z_OFF_Y) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        status = H5Aread (attribute_id, H5T_NATIVE_FLOAT, 
                          &image_par->z_off_y); 
        status = H5Aclose (attribute_id); 
        image_par->z_off_y__set = TRUE;
    }


    /*
     * Optional parameters
     */

    if (strcmp (name, GPIV_IMGPAR_KEY__TITLE) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->title); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->title__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__CREATION_DATE) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->creation_date); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->creation_date__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__LOCATION) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->location); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->location__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__AUTHOR) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->author); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->author__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__SOFTWARE) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->software); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->software__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__SOURCE) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->source); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->source__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__USERTEXT) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->usertext); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->usertext__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__WARNING) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->warning); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->warning__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__DISCLAIMER) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->disclaimer); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->disclaimer__set = TRUE;
    }


    if (strcmp (name, GPIV_IMGPAR_KEY__COMMENT) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->comment); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->comment__set = TRUE;
    }
    

    if (strcmp (name, GPIV_IMGPAR_KEY__COPYRIGHT) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->copyright); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->copyright__set = TRUE;
    }
    

    if (strcmp (name, GPIV_IMGPAR_KEY__EMAIL) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->email); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->email__set = TRUE;
    }
    

    if (strcmp (name, GPIV_IMGPAR_KEY__URL) == 0) {
        attribute_id = H5Aopen_name (loc_id, name);
        atype = H5Tcopy (H5T_C_S1);
        H5Tset_size (atype, GPIV_MAX_CHARS);
        status = H5Aread (attribute_id, atype, 
                          &image_par->url); 
        status = H5Tclose (atype);
        status = H5Aclose (attribute_id); 
        image_par->url__set = TRUE;
    }
    

    return 0;
}



static void
img_print_parameters (FILE *fp, 
                      const GpivImagePar *image_par,
                      const gboolean key
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     prints image header parameters to file fp
 *---------------------------------------------------------------------------*/
{
    if (fp == stdout || fp == NULL) {
        /*
         * General image parameters
         */
        if (image_par->ncolumns__set) {
            if (key) {
                printf ("%s.%s %d\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__NCOLUMNS, 
                        image_par->ncolumns);
            } else {
                printf ("%s %d\n", GPIV_IMGPAR_KEY__NCOLUMNS, 
                        image_par->ncolumns);
            }
        }

        if (image_par->nrows__set) {
            if (key) {
                printf ("%s.%s %d\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__NROWS, 
                        image_par->nrows);
            } else {
                printf ("%s %d\n", GPIV_IMGPAR_KEY__NROWS, 
                        image_par->nrows);
            }
        }

        if (image_par->x_corr__set) {
            if (key) {
                printf ("%s.%s %d\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__X_CORR, 
                        image_par->x_corr);
            } else {
                printf ("%s %d\n", GPIV_IMGPAR_KEY__X_CORR, image_par->x_corr);
            }
        }

        if (image_par->depth__set) {
            if (key) {
                printf ("%s.%s %d\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__DEPTH, 
                        image_par->depth);
            } else {
                printf ("%s %d\n", GPIV_IMGPAR_KEY__DEPTH, 
                        image_par->depth);
            }
        }

        /*
         * Parameters for scale
         */
        if (image_par->s_scale__set) {
            if (key) {
                printf ("%s.%s %f\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__S_SCALE, 
                        image_par->s_scale);
            } else {
                printf ("%s %f\n", GPIV_IMGPAR_KEY__S_SCALE, 
                        image_par->s_scale);
            }
        }

        if (image_par->t_scale__set) {
            if (key) {
                printf ("%s.%s %f\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__T_SCALE, 
                        image_par->t_scale);
            } else {
                printf ("%s %f\n", GPIV_IMGPAR_KEY__T_SCALE, 
                        image_par->t_scale);
            }
        }

        if (image_par->z_off_x__set) {
            if (key) {
                printf ("%s.%s %f\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__Z_OFF_X, 
                        image_par->z_off_x);
            } else {
                printf ("%s %f\n", GPIV_IMGPAR_KEY__Z_OFF_X, 
                        image_par->z_off_x);
            }
        }

        if (image_par->z_off_y__set) {
            if (key) {
                printf ("%s.%s %f\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__Z_OFF_Y, 
                        image_par->z_off_y);
            } else {
                printf ("%s %f\n", GPIV_IMGPAR_KEY__Z_OFF_Y, 
                        image_par->z_off_y);
            }
        }

        /*
         * Optional parameters
         */
        if (image_par->title__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__TITLE, 
                        image_par->title);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__TITLE, 
                        image_par->title);
            }
        }

        if (image_par->creation_date__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__CREATION_DATE, 
                        image_par->creation_date);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__CREATION_DATE, 
                        image_par->creation_date);
            }
        }

        if (image_par->location__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__LOCATION, 
                        image_par->location);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__LOCATION, 
                        image_par->location);
            }
        }

        if (image_par->author__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__AUTHOR, 
                        image_par->author);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__AUTHOR, 
                        image_par->author);
            }
        }

        if (image_par->software__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__SOFTWARE, image_par->software);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__SOFTWARE, 
                        image_par->software);
            }
        }

        if (image_par->source__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__SOURCE, 
                        image_par->source);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__SOURCE, 
                        image_par->source);
            }
        }

        if (image_par->usertext__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__USERTEXT, image_par->usertext);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__USERTEXT, 
                        image_par->usertext);
            }
        }

        if (image_par->warning__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__WARNING, image_par->warning);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__WARNING, 
                        image_par->warning);
            }
        }

        if (image_par->disclaimer__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__DISCLAIMER, 
                        image_par->disclaimer);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__DISCLAIMER, 
                        image_par->disclaimer);
            }
        }

        if (image_par->comment__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__COMMENT, 
                        image_par->comment);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__COMMENT, 
                        image_par->comment);
            }
        }

        if (image_par->copyright__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__COPYRIGHT, 
                        image_par->copyright);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__COPYRIGHT, 
                        image_par->copyright);
            }
        }

        if (image_par->email__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__EMAIL, 
                        image_par->email);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__EMAIL, 
                        image_par->email);
            }
        }

        if (image_par->url__set) {
            if (key) {
                printf ("%s.%s %s\n", GPIV_IMGPAR_KEY, 
                        GPIV_IMGPAR_KEY__URL, 
                        image_par->url);
            } else {
                printf ("%s %s\n", GPIV_IMGPAR_KEY__URL, 
                        image_par->url);
            }
        }


    } else {
        /*
         * Prints to fp
         * General image parameters
         */
        if (image_par->ncolumns__set) {
            if (key) {
                fprintf (fp, "%s.%s %d\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__NCOLUMNS, 
                         image_par->ncolumns);
            } else {
                fprintf (fp, "%s %d\n", GPIV_IMGPAR_KEY__NCOLUMNS, 
                         image_par->ncolumns);
            }
        }

        if (image_par->nrows__set) {
            if (key) {
                fprintf (fp, "%s.%s %d\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__NROWS, 
                         image_par->nrows);
            } else {
                fprintf (fp, "%s %d\n", GPIV_IMGPAR_KEY__NROWS, 
                         image_par->nrows);
            }
        }

        if (image_par->x_corr__set) {
            if (key) {
                fprintf (fp, "%s.%s %d\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__X_CORR, 
                         image_par->x_corr);
            } else {
                fprintf (fp, "%s %d\n", GPIV_IMGPAR_KEY__X_CORR, image_par->x_corr);
            }
        }

        if (image_par->depth__set) {
            if (key) {
                fprintf (fp, "%s.%s %d\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__DEPTH, 
                         image_par->depth);
            } else {
                fprintf (fp, "%s %d\n", GPIV_IMGPAR_KEY__DEPTH, 
                         image_par->depth);
            }
        }

        /*
         * Parameters for scale
         */
        if (image_par->s_scale__set) {
            if (key) {
                fprintf (fp, "%s.%s %f\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__S_SCALE, 
                         image_par->s_scale);
            } else {
                fprintf (fp, "%s %f\n", GPIV_IMGPAR_KEY__S_SCALE, 
                         image_par->s_scale);
            }
        }

        if (image_par->t_scale__set) {
            if (key) {
                fprintf (fp, "%s.%s %f\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__T_SCALE, 
                         image_par->t_scale);
            } else {
                fprintf (fp, "%s %f\n", GPIV_IMGPAR_KEY__T_SCALE, 
                         image_par->t_scale);
            }
        }

        if (image_par->z_off_x__set) {
            if (key) {
                fprintf (fp, "%s.%s %f\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__Z_OFF_X, 
                         image_par->z_off_x);
            } else {
                fprintf (fp, "%s %f\n", GPIV_IMGPAR_KEY__Z_OFF_X, 
                         image_par->z_off_x);
            }
        }

        if (image_par->z_off_y__set) {
            if (key) {
                fprintf (fp, "%s.%s %f\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__Z_OFF_Y, 
                         image_par->z_off_y);
            } else {
                fprintf (fp, "%s %f\n", GPIV_IMGPAR_KEY__Z_OFF_Y, 
                         image_par->z_off_y);
            }
        }

        /*
         * Optional parameters
         */
        if (image_par->title__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__TITLE, 
                         image_par->title);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__TITLE, 
                         image_par->title);
            }
        }

        if (image_par->creation_date__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__CREATION_DATE, 
                         image_par->creation_date);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__CREATION_DATE, 
                         image_par->creation_date);
            }
        }

        if (image_par->location__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__LOCATION, 
                         image_par->location);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__LOCATION, 
                         image_par->location);
            }
        }

        if (image_par->author__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__AUTHOR, 
                         image_par->author);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__AUTHOR, 
                         image_par->author);
            }
        }

        if (image_par->software__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__SOFTWARE, image_par->software);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__SOFTWARE, 
                         image_par->software);
            }
        }

        if (image_par->source__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__SOURCE, 
                         image_par->source);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__SOURCE, 
                         image_par->source);
            }
        }

        if (image_par->usertext__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__USERTEXT, image_par->usertext);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__USERTEXT, 
                         image_par->usertext);
            }
        }

        if (image_par->warning__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__WARNING, image_par->warning);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__WARNING, 
                         image_par->warning);
            }
        }

        if (image_par->disclaimer__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__DISCLAIMER, 
                         image_par->disclaimer);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__DISCLAIMER, 
                         image_par->disclaimer);
            }
        }

        if (image_par->comment__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__COMMENT, 
                         image_par->comment);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__COMMENT, 
                         image_par->comment);
            }
        }

        if (image_par->copyright__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__COPYRIGHT, 
                         image_par->copyright);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__COPYRIGHT, 
                         image_par->copyright);
            }
        }

        if (image_par->email__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__EMAIL, 
                         image_par->email);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__EMAIL, 
                         image_par->email);
            }
        }

        if (image_par->url__set) {
            if (key) {
                fprintf (fp, "%s.%s %s\n", GPIV_IMGPAR_KEY, 
                         GPIV_IMGPAR_KEY__URL, 
                         image_par->url);
            } else {
                fprintf (fp, "%s %s\n", GPIV_IMGPAR_KEY__URL, 
                         image_par->url);
            }
        }


    }
        
}




static void
obtain_imagepar_fromline (char line[], 
                          GpivImagePar *image_par, 
                          FILE *fp_verbose, 
                          gboolean verbose, 
                          gboolean include_key)
/*-----------------------------------------------------------------------------
 */
{
    char par_name[GPIV_MAX_CHARS];


    if (line[0] != '#' && line[0] != '\n' && line[0] != ' '
        && line[0] != '\t') {
        sscanf (line, "%s", par_name);

        /*
         * Required image parameters
         */
        /*                g_message ("gpiv_img_read_header:: line = %s par_name = %s",line, par_name); */
        if (image_par->ncolumns__set == FALSE) {
            image_par->ncolumns__set =
                gpiv_scan_iph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__NCOLUMNS, include_key,
                               line, par_name, &image_par->ncolumns, 
                               verbose, fp_verbose);
        }
            
        if (image_par->nrows__set == FALSE) {
            image_par->nrows__set =
                gpiv_scan_iph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__NROWS, include_key,
                               line, par_name, &image_par->nrows, 
                               verbose, fp_verbose);
        }
            
        if (image_par->x_corr__set == FALSE) {
            image_par->x_corr__set =
                gpiv_scan_iph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__X_CORR, include_key,
                               line, par_name, &image_par->x_corr, 
                               verbose, fp_verbose);
        }
            
        if (image_par->depth__set == FALSE) {
            image_par->depth__set =
                gpiv_scan_iph (GPIV_IMGPAR_KEY,
                               GPIV_IMGPAR_KEY__DEPTH, include_key,
                               line, par_name, &image_par->depth, 
                               verbose, fp_verbose);
        }
            
        /*
         * Parameters used by gpiv_post_scale
         */
        if (image_par->s_scale__set == FALSE) {
            image_par->s_scale__set = 
                gpiv_scan_fph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__S_SCALE, include_key,
                               line, par_name, &image_par->s_scale, 
                               verbose, fp_verbose);
        }
            
        if (image_par->t_scale__set == FALSE) {
            image_par->t_scale__set = 
                gpiv_scan_fph (GPIV_IMGPAR_KEY,  
                               GPIV_IMGPAR_KEY__T_SCALE, include_key,
                               line, par_name, &image_par->t_scale, 
                               verbose, fp_verbose);
        }
            
        if (image_par->z_off_x__set == FALSE) {
            image_par->z_off_x__set = 
                gpiv_scan_fph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__Z_OFF_X, include_key,
                               line, par_name, &image_par->z_off_x, 
                               verbose, fp_verbose);
        }
            
        if (image_par->z_off_y__set == FALSE) {
            image_par->z_off_y__set = 
                gpiv_scan_fph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__Z_OFF_Y, include_key,
                               line, par_name, &image_par->z_off_y, 
                               verbose, fp_verbose);
        }

        /*
         * Optional text parameters
         */
        if (image_par->title__set == FALSE) {
            /* BUGFIX: TODO */
            /* 		if ((image_par->title = */
            /*                      gpiv_scan_sph (include_key, GPIV_IMGPAR_KEY,  */
            /*                                      line, par_name,  */
            /*                                      GPIV_IMGPAR_KEY__TITLE, */
            /*                                      verbose, fp_verbose))  */
            /*                     != GPIV_FAIL_INT) { */
            /*                     if (image_par->title[0] == '\0') { */
            /*                         image_par->title__set = FALSE; */
            /*                     } else { */
            /*                         image_par->title__set = TRUE; */
            /*                     } */
            /*                 } */

            image_par->title__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__TITLE, include_key,
                               line, par_name, image_par->title, 
                               verbose, fp_verbose);
            if (image_par->title[0] == '\0') {
                image_par->title__set = FALSE;
            }
        }

        if (image_par->creation_date__set == FALSE) {
            image_par->creation_date__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__CREATION_DATE, include_key,
                               line, par_name, image_par->creation_date, 
                               verbose, fp_verbose);
            if (image_par->creation_date[0] == '\0') {
                image_par->creation_date__set = FALSE;
            }
        }

        if (image_par->location__set == FALSE) {
            image_par->location__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__LOCATION, include_key,
                               line, par_name, image_par->location, 
                               verbose, fp_verbose);
            if (image_par->location[0] == '\0') {
                image_par->location__set = FALSE;
            }
        }

        if (image_par->author__set == FALSE) {
            image_par->author__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__AUTHOR, include_key,
                               line, par_name, image_par->author, 
                               verbose, fp_verbose);
            if (image_par->author[0] == '\0') {
                image_par->author__set = FALSE;
            }
        }

        if (image_par->software__set == FALSE) {
            image_par->software__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__SOFTWARE, include_key,
                               line, par_name, image_par->software, 
                               verbose, fp_verbose);
            if (image_par->software[0] == '\0') {
                image_par->software__set = FALSE;
            }
        }

        if (image_par->source__set == FALSE) {
            image_par->source__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY,
                               GPIV_IMGPAR_KEY__SOURCE, include_key,
                               line, par_name, image_par->source, 
                               verbose, fp_verbose);
            if (image_par->source[0] == '\0') {
                image_par->source__set = FALSE;
            }
        }

        if (image_par->usertext__set == FALSE) {
            image_par->usertext__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__USERTEXT, include_key,
                               line, par_name, image_par->usertext, 
                               verbose, fp_verbose);
            if (image_par->usertext[0] == '\0') {
                image_par->usertext__set = FALSE;
            }
        }

        if (image_par->warning__set == FALSE) {
            image_par->warning__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__WARNING, include_key,
                               line, par_name, image_par->warning, 
                               verbose, fp_verbose);
            if (image_par->warning[0] == '\0') {
                image_par->warning__set = FALSE;
            }
        }

        if (image_par->disclaimer__set == FALSE) {
            image_par->disclaimer__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__DISCLAIMER, include_key,
                               line, par_name, image_par->disclaimer, 
                               verbose, fp_verbose);
            if (image_par->disclaimer[0] == '\0') {
                image_par->disclaimer__set = FALSE;
            }
        }

        if (image_par->comment__set == FALSE) {
            image_par->comment__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__COMMENT, include_key,
                               line, par_name, image_par->comment, 
                               verbose, fp_verbose);
            if (image_par->comment[0] == '\0') {
                image_par->comment__set = FALSE;
            }
        }

        if (image_par->copyright__set == FALSE) {
            image_par->copyright__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__COPYRIGHT, include_key,
                               line, par_name, image_par->copyright, 
                               verbose, fp_verbose);
            if (image_par->copyright[0] == '\0') {
                image_par->copyright__set = FALSE;
            }
        }

        if (image_par->email__set == FALSE) {
            image_par->email__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__EMAIL, include_key,
                               line, par_name, image_par->email, 
                               verbose, fp_verbose);
            if (image_par->email[0] == '\0') {
                image_par->email__set = FALSE;
            }
        }

        if (image_par->url__set == FALSE) {
            image_par->url__set =
                gpiv_scan_sph (GPIV_IMGPAR_KEY, 
                               GPIV_IMGPAR_KEY__URL, include_key,
                               line, par_name, image_par->url, 
                               verbose, fp_verbose);
            if (image_par->url[0] == '\0') {
                image_par->url__set = FALSE;
            }
        }
    }


    return;
}

