/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



---------------------------------------------------------------
FILENAME:                io_hdf.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_fcreate_hdf5
                         gpiv_fread_hdf5_image,
                         gpiv_fwrite_hdf5_image,

                         gpiv_open_hdf5_image,
                         gpiv_fread_hdf5_parameters,
                         gpiv_fwrite_hdf5_parameters,

			 gpiv_fread_hdf5_pivdata, 
			 gpiv_fwrite_hdf5_pivdata, 

                         gpiv_fread_hdf5_scdata, 
			 gpiv_fwrite_hdf5_scdata, 


LOCAL FUNCTIONS:

LAST MODIFICATION DATE:  $Id: io_hdf.c,v 1.4 2007-12-19 08:46:46 gerber Exp $
 --------------------------------------------------------------- */
#include <gpiv.h>


/*
 * Prototyping local functions
 */

/*
 * Public functions
 */

gchar *
gpiv_fcreate_hdf5 (const gchar *fname
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Creates an hdf5 data file with DATA and PARAMETERS groups
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id; 
    herr_t      status;

    if ((file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT))
        < 0) {
        err_msg = "gpiv_fcreate_hdf5: H5Fcreate unable to create file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

    /*
     * hid_t H5Gcreate2( hid_t loc_id, const char *name, hid_t lcpl_id, 
     * hid_t gcpl_id, hid_t gapl_id ) 
     */
    group_id = H5Gcreate (file_id, "IMAGE", H5P_DEFAULT /* lcpl_id */, H5P_DEFAULT /* gcpl_id */, H5P_DEFAULT /*gapl_id*/);
    status = H5Gclose (group_id);
    group_id = H5Gcreate (file_id, "POSITIONS", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Gclose (group_id);
    group_id = H5Gcreate (file_id, "PIV", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Gclose (group_id);
/*     group_id = H5Gcreate (file_id, "VECTORS", 0); */
/*     status = H5Gclose (group_id); */
    group_id = H5Gcreate (file_id, "SCALARS", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Gclose (group_id);

    status = H5Fclose(file_id); 
    return err_msg;
}



gchar *
gpiv_fread_hdf5_parameters (const gchar *fname,
                            const gchar *par_key,
                            void *pstruct
                            )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Reads parameters from hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;    

    if (strcmp(par_key, GPIV_PIVPAR_KEY) == 0) {
        pstruct = gpiv_piv_fread_hdf5_parameters (fname);
    } else {
        err_msg = "gpiv_scan_parameters: called with unexisting key";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

  return err_msg;
}



gchar *
gpiv_fwrite_hdf5_parameters (const gchar *fname,
                             const gchar *par_key,
                             void *pstruct
                             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes parameters to hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{    
    gchar *err_msg = NULL;
    if (strcmp(par_key, GPIV_PIVPAR_KEY) == 0) {
        gpiv_piv_fwrite_hdf5_parameters (fname, pstruct);
    } else {
        err_msg = "gpiv_fwrite_hdf5_parameters: called with unexisting key";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

    return err_msg;
}



/*
 * Local functions
 */
