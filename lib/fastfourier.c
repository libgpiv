/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2012 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------*/

#include <gpiv.h>


GpivFt *
gpiv_alloc_ft				(const guint            size
					)
/*---------------------------------------------------------------------------*/
/**
 *      Allocates memory for GpivFt
 *      Creates plans for fft and inverse fft
 *
 *      @param[in] size          size of zero-padded interrogation area
 *      @return                  covariance on success or NULL on failure
 */
/*---------------------------------------------------------------------------*/
{
    GpivFt *ft = g_new0 (GpivFt, 1);


    ft->size = size;

    ft->intreg1 = gpiv_matrix (size, size);
    ft->intreg2 = gpiv_matrix (size, size);
    ft->cov = gpiv_alloc_cov (size, TRUE);
    ft->re = gpiv_double_matrix (size, size+2);
    ft->co = gpiv_fftw_complex_matrix (size, size/2+1);
    ft->A = gpiv_fftw_complex_matrix (size, size/2+1);
    ft->B = gpiv_fftw_complex_matrix (size, size/2+1);

    ft->plan_forwardFFT = 
        fftw_plan_dft_r2c_2d(ft->size, ft->size, (double *) ft->re[0], 
                             (fftw_complex *) ft->co[0], FFTW_MEASURE);
    ft->plan_reverseFFT = 
        fftw_plan_dft_c2r_2d(ft->size, ft->size,
                             (fftw_complex *) ft->co[0], 
                             (double *) ft->re[0], FFTW_MEASURE);


    return ft;
}



gchar *
gpiv_check_alloc_ft                   (const GpivFt		*ft
                                        )
/*-----------------------------------------------------------------------------
 * Check if ft arrays have been allocated
 */
{
    gchar *err_msg = NULL;


    g_return_val_if_fail (ft->intreg1[0] != NULL, 
                          "gpiv_check_alloc_ft: intreg1 != NULL");
    g_return_val_if_fail (ft->intreg2[0] != NULL, 
                          "gpiv_check_alloc_ft: intreg2 != NULL");
    g_return_val_if_fail (ft->cov != NULL, 
                          "gpiv_check_alloc_ft: cov != NULL");
    g_return_val_if_fail (ft->re[0] != NULL, 
                          "gpiv_check_alloc_ft: re_ar != NULL");
    g_return_val_if_fail (ft->co[0] != NULL, 
                          "gpiv_check_alloc_ft: co_ar != NULL");
    g_return_val_if_fail (ft->A[0] != NULL, 
                          "gpiv_check_alloc_ft: A != NULL");
    g_return_val_if_fail (ft->B[0] != NULL, 
                          "gpiv_check_alloc_ft: B != NULL");
    g_return_val_if_fail (ft->plan_forwardFFT != NULL, 
                          "gpiv_check_alloc_ft: plan_forwardFFT != NULL");
    g_return_val_if_fail (ft->plan_reverseFFT != NULL, 
                          "gpiv_check_alloc_ft: plan_reverseFFT != NULL");


    return err_msg;
}



void 
gpiv_free_ft				(GpivFt		*ft
                                         )
/*-----------------------------------------------------------------------------
 */
{
    g_return_if_fail (ft != NULL);
    g_return_if_fail (ft->intreg1 != NULL);
    g_return_if_fail (ft->intreg2 != NULL);
    g_return_if_fail (ft->cov != NULL);
    g_return_if_fail (ft->re != NULL);
    g_return_if_fail (ft->co != NULL);
    g_return_if_fail (ft->A != NULL);
    g_return_if_fail (ft->B != NULL);
    g_return_if_fail (ft->plan_forwardFFT != NULL);
    g_return_if_fail (ft->plan_reverseFFT != NULL);


    fftw_destroy_plan( ft->plan_forwardFFT);
    fftw_destroy_plan( ft->plan_reverseFFT);

    gpiv_free_matrix (ft->intreg1);
    ft->intreg1 = NULL;
    gpiv_free_matrix (ft->intreg2);
    ft->intreg2 = NULL;
    gpiv_free_cov (ft->cov);
    ft->cov = NULL;
    gpiv_free_double_matrix (ft->re);
    ft->re = NULL;
    gpiv_free_fftw_complex_matrix (ft->co);
    ft->co = NULL;
    gpiv_free_fftw_complex_matrix (ft->A);
    ft->A = NULL;
    gpiv_free_fftw_complex_matrix (ft->B);
    ft->B = NULL;

    ft = NULL;
}
