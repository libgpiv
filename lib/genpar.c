/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                genpar.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                        gpiv_genpar_default_parameters
                        gpiv_genpar_read_parameters
                        gpiv_genpar_check_parameters_read
                        gpiv_genpar_print_parameters
                        gpiv_genpar_fprint_parameters


LAST MODIFICATION DATE:  $Id: genpar.c,v 1.2 2008-05-05 14:38:41 gerber Exp $
 --------------------------------------------------------------------------- */

#include <gpiv.h>


enum {
    GENPAR_DEFAULT__FIRST_DIR    = 0,         /**< Default parameter for first_dir of __GpivGenPar */
    GENPAR_DEFAULT__LAST_DIR     = 0,         /**< Default parameter for last_dir of __GpivGenPar */
    GENPAR_DEFAULT__DIR_PREFIX   = 0,         /**< Default parameter for dir_prefix of __GpivGenPar */
    GENPAR_DEFAULT__FIRST_FILE   = 0,         /**< Default parameter for first_file of __GpivGenPar */
    GENPAR_DEFAULT__LAST_FILE    = 0,         /**< Default parameter for last_file of __GpivGenPar */
    GENPAR_DEFAULT__FILE_PREFIX  = 0,         /**< Default parameter for file_prefix of __GpivGenPar */
};

const gchar *GENPAR_KEY__FIRST_DIR  = "First_dir";     /**< Key for first_dir of __GpivGenPar */
const gchar *GENPAR_KEY__LAST_DIR   = "Last_dir";      /**< Key for last_dir of __GpivGenPar */
const gchar *GENPAR_KEY__DIR_PREFIX = "Dir_prefix";    /**< Key for dir_prefix of __GpivGenPar */
const gchar *GENPAR_KEY__FIRST_FILE = "First_file";    /**< Key for first_file of __GpivGenPar */
const gchar *GENPAR_KEY__LAST_FILE  = "Last_file";    /**< Key for last_file of __GpivGenPar */
const gchar *GENPAR_KEY__FILE_PREFIX = "File_prefix";  /**< Key for file_prefix of __GpivGenPar */



static void
print_parameters (FILE *fp,
                  const GpivGenPar *gen_par
                  );

gchar *
gpiv_genpar_test_parameters             (const GpivGenPar       *gen_par
                                        )
{}



void
gpiv_genpar_parameters_set (GpivGenPar                          *gen_par,
			    const gboolean                      flag
			    )
/*-----------------------------------------------------------------------------
 */
{
    gen_par->first_dir__set = flag;
    gen_par->last_dir__set = flag;
    gen_par->dir_prefix__set = flag;     
    gen_par->first_file__set = flag;
    gen_par->last_file__set = flag;
    gen_par->file_prefix__set = flag;

}



void
gpiv_genpar_default_parameters		(GpivGenPar             *gen_par_default,
					const gboolean          force
					)
/*-----------------------------------------------------------------------------
 */
{
    if (!gen_par_default->dir_prefix__set || force)
        gen_par_default->dir_prefix = GENPAR_DEFAULT__DIR_PREFIX;

    if (!gen_par_default->first_dir__set || force)
        gen_par_default->first_dir = GENPAR_DEFAULT__FIRST_DIR;

    if (!gen_par_default->last_dir__set || force)
        gen_par_default->last_dir = GENPAR_DEFAULT__LAST_DIR;

    if (!gen_par_default->first_file__set || force)
        gen_par_default->first_file = GENPAR_DEFAULT__FIRST_FILE;

    if (!gen_par_default->last_file__set || force)
        gen_par_default->last_file = GENPAR_DEFAULT__LAST_FILE;

    if (!gen_par_default->file_prefix__set || force)
        gen_par_default->file_prefix = GENPAR_DEFAULT__FILE_PREFIX;
}



void
gpiv_genpar_read_parameters	        (FILE                   *fp_par, 
                                         GpivGenPar	        *gen_par, 
                                         const gboolean	        print_par
                                         )
/*----------------------------------------------------------------------------
 */
{
    char line[GPIV_MAX_CHARS], par_name[GPIV_MAX_CHARS];


    while (fgets(line, GPIV_MAX_CHARS, fp_par) != NULL) {
        if (line[0] != '#' && line[0] != '\n' && line[0] != ' '
            && line[0] != '\t') {
            sscanf(line, "%s", par_name);

            if (gen_par->first_dir__set == FALSE) {
                gen_par->first_dir__set =
                    gpiv_scan_iph (GPIV_GENPAR_KEY, 
                                   GENPAR_KEY__FIRST_DIR, TRUE,
                                   line, par_name, &gen_par->first_dir, 
                                   print_par, NULL);
            }

            if (gen_par->last_dir__set == FALSE) {
                gen_par->last_dir__set =
                    gpiv_scan_iph (GPIV_GENPAR_KEY, 
                                   GENPAR_KEY__LAST_DIR, TRUE,
                                   line, par_name, &gen_par->last_dir, 
                                   print_par, NULL);
            }

            if (gen_par->dir_prefix__set == FALSE) {
                gen_par->dir_prefix__set =
                    gpiv_scan_iph (GPIV_GENPAR_KEY, 
                                   GENPAR_KEY__DIR_PREFIX, TRUE,
                                   line, par_name, &gen_par->dir_prefix, 
                                   print_par, NULL);
                /*                     gpiv_scan_sph (GPIV_GENPAR_KEY, line, par_name,  */
                /*                                    "DirPrefix:", */
                /*                                    &gen_par->dir_prefix, print_par, 0); */
                /*                 if (gen_par->dir_prefix[0] == '\0') { */
                /*                     gen_par->dir_prefix__set = FALSE; */
                /*                 } */
            }


            if (gen_par->first_file__set == FALSE) {
                gen_par->first_file__set =
                    gpiv_scan_iph (GPIV_GENPAR_KEY, 
                                   GENPAR_KEY__FIRST_FILE, TRUE,
                                   line, par_name, &gen_par->first_file, 
                                   print_par, NULL);
            }

            if (gen_par->last_file__set == FALSE) {
                gen_par->last_file__set =
                    gpiv_scan_iph (GPIV_GENPAR_KEY, 
                                   GENPAR_KEY__LAST_FILE, TRUE,
                                   line, par_name, &gen_par->last_file, 
                                   print_par, NULL);
            }

            if (gen_par->file_prefix__set == FALSE) {
                gen_par->file_prefix__set =
                    gpiv_scan_iph (GPIV_GENPAR_KEY, 
                                   GENPAR_KEY__FILE_PREFIX, TRUE,
                                   line, par_name, &gen_par->file_prefix, 
                                   print_par, NULL);
            }
        }
    }

}



gchar *
gpiv_genpar_check_parameters_read	(GpivGenPar	        *gen_par,
                                         const GpivGenPar       *gen_par_default
                                         )
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;


    if (gen_par->first_dir__set == FALSE) {
        gen_par->first_dir__set = TRUE;
        if (gen_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            gen_par->first_dir = GENPAR_DEFAULT__FIRST_DIR;
        } else {
            err_msg = "Using default: ";
            gen_par->first_dir = gen_par_default->first_dir;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_GENPAR_KEY,
                     GENPAR_KEY__FIRST_DIR,
                     gen_par->first_dir);
    }

    if (gen_par->last_dir__set == FALSE) {
        gen_par->last_dir__set = TRUE;
        if (gen_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            gen_par->last_dir = GENPAR_DEFAULT__LAST_DIR;
        } else {
            err_msg = "Using default: ";
            gen_par->last_dir = gen_par_default->last_dir;
        }
        gpiv_warning("s\n%s.%s %d", err_msg,
                     GPIV_GENPAR_KEY,
                     GENPAR_KEY__LAST_DIR,
                     gen_par->last_dir);
    }

    if (gen_par->dir_prefix__set == FALSE) {
        gen_par->dir_prefix__set = TRUE;
        if (gen_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            gen_par->dir_prefix = GENPAR_DEFAULT__DIR_PREFIX;
       } else {
            err_msg = "Using default: ";
            gen_par->dir_prefix = gen_par_default->dir_prefix;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_GENPAR_KEY,
                     GENPAR_KEY__DIR_PREFIX,
                     gen_par->dir_prefix);
    }

    if (gen_par->first_file__set == FALSE) {
        gen_par->first_file__set = TRUE;
        if (gen_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            gen_par->first_file =  GENPAR_DEFAULT__FIRST_FILE;
        } else {
            err_msg = "Using default : ";
            gen_par->first_file = gen_par_default->first_file;
        }
        gpiv_warning("s\n%s.%s %d", err_msg,
                     GPIV_GENPAR_KEY,
                     GENPAR_KEY__FIRST_FILE,
                     gen_par->first_file);
    }

    if (gen_par->last_file__set == FALSE) {
        gen_par->last_file__set = TRUE;
        if (gen_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            gen_par->last_file = GENPAR_DEFAULT__LAST_FILE;
        } else {
            err_msg = "Using default: ";
            gen_par->last_file = gen_par_default->last_file;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_GENPAR_KEY,
                     GENPAR_KEY__LAST_FILE,
                     gen_par->last_file);
    }

    if (gen_par->file_prefix__set == FALSE) {
        gen_par->file_prefix__set = TRUE;
        if (gen_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            gen_par->file_prefix = GENPAR_DEFAULT__FILE_PREFIX;
        } else {
            err_msg = "Using default: ";
            gen_par->file_prefix = gen_par_default->file_prefix;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_GENPAR_KEY,
                     GENPAR_KEY__FILE_PREFIX,
                     gen_par->file_prefix);
    }


    return err_msg;
}



void
gpiv_genpar_print_parameters		(FILE                   *fp, 
                                         const GpivGenPar	*gen_par
                                         )
{
    print_parameters (fp, gen_par);
}



GpivGenPar *
gpiv_genpar_cp_parameters		(const GpivGenPar       *gen_par
                                         )
{
    GpivGenPar *par = g_new0 (GpivGenPar, 1);


    par->first_dir = gen_par->first_dir;
    par->first_dir__set = TRUE;

    par->last_dir = gen_par->last_dir;
    par->last_dir__set = TRUE;

    par->dir_prefix = gen_par->dir_prefix;
    par->dir_prefix__set = TRUE;

    par->first_file = gen_par->first_file;
    par->first_file__set = TRUE;

    par->last_file = gen_par->last_file;
    par->last_file__set = TRUE;

    par->file_prefix = gen_par->file_prefix;
    par->file_prefix__set = TRUE;


    return par;
}


/*
 * Local functions
 */

static void
print_parameters (FILE *fp,
		  const GpivGenPar      *gen_par
		  )
/*  ---------------------------------------------------------------------------
 * prints general parameters to fp_par_out
 */
{
    if (fp == stdout || fp == NULL) {
        if (gen_par->first_dir__set)
            printf ("%s.%s %d\n", 
                    GPIV_GENPAR_KEY, 
                    GENPAR_KEY__FIRST_DIR, 
                    gen_par->first_dir);

        if (gen_par->last_dir__set)
            printf ("%s.%s %d\n", 
                    GPIV_GENPAR_KEY, 
                    GENPAR_KEY__LAST_DIR, 
                    gen_par->last_dir);

        if (gen_par->first_file__set)
            printf ("%s.%s %d\n", 
                    GPIV_GENPAR_KEY, 
                    GENPAR_KEY__FIRST_FILE, 
                    gen_par->first_file);

        if (gen_par->last_file__set)
            printf ("%s.%s %d\n", 
                    GPIV_GENPAR_KEY, 
                    GENPAR_KEY__LAST_FILE, 
                    gen_par->last_file);

    } else {
        if (gen_par->first_dir__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_GENPAR_KEY, 
                     GENPAR_KEY__FIRST_DIR, 
                     gen_par->first_dir);

        if (gen_par->last_dir__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_GENPAR_KEY, 
                     GENPAR_KEY__LAST_DIR, 
                     gen_par->last_dir);

        if (gen_par->first_file__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_GENPAR_KEY, 
                     GENPAR_KEY__FIRST_FILE, 
                     gen_par->first_file);

        if (gen_par->last_file__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_GENPAR_KEY, 
                     GENPAR_KEY__LAST_FILE, 
                     gen_par->last_file);

    }
}
