/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2011 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------*/

#include <gpiv.h>


static void
bounds_cov                              (GpivCov                *cov,
					 const guint            int_size_0,
					 const gboolean         x_corr
					 );


GpivCov *
gpiv_alloc_cov				(const guint            int_size0,
					const gboolean		x_corr
					)
/*---------------------------------------------------------------------------*/
/**
 *      Allocates memory for GpivCov
 *
 *      @param[in] int_size0     size of zere-padded interrogation area
 *      @param[in] x_corr        two frame image / cross correlation
 *      @return                  covariance on success or NULL on failure
 */
/*---------------------------------------------------------------------------*/
{
    GpivCov *cov = g_new0 (GpivCov, 1);

    bounds_cov (cov, int_size0, x_corr);
    cov->z = gpiv_matrix_index (cov->z_rl, cov->z_rh, cov->z_cl, cov->z_ch);

    return cov;
}



void 
gpiv_free_cov				(GpivCov		*cov
					)
/*-----------------------------------------------------------------------------
 */
{
    gpiv_free_matrix_index (cov->z, cov->z_rl, cov->z_rh, cov->z_cl, cov->z_ch);
    cov = NULL;
}


/*
 * Local functions
 */
static void
bounds_cov                              (GpivCov                *cov,
					 const guint            int_size_0,
					 const gboolean         x_corr
					 )
/* ----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Defines boundaries of array to use for pos. and neg. displacements
 *     in covariance[][] and puts the data in z[][].
 *     Uses area of -2 until int_size/2 for auto-corr
 *
 * INPUTS:
 *     int_size_0:     size of zere-padded interrogation area
 *     image_par:      image parameters
 *
 * OUTPUTS:
 *     cov:            structure containing covariance boundaries
 *
 * SOME MNENOSYNTACTICS ON ARRAY BOUNDARIES:
 *    z:               name of array
 *    r:               row
 *    c:               column
 *    p:               positive displacements; from 0 to int_size_0/4 of array
 *                     covariance
 *    n:               negative displacements; from 3*int_size_0/4 to
 *                     int_size_0 of array covariance
 *    l:               lowest index
 *    h:               highest index without n or p indicates general array
 *                     boundaries (both auto and cross correlation)
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (cov != NULL);

    if (x_corr == TRUE) {
	cov->z_rnl = 3.0 * (int_size_0) / 4 + 1;
	cov->z_rnh = int_size_0;
	cov->z_rpl = 0;
	cov->z_rph = int_size_0 / 4;
	cov->z_cnl = 3.0 * (int_size_0) / 4.0 + 1;
	cov->z_cnh = int_size_0;
	cov->z_cpl = 0;
	cov->z_cph = int_size_0 / 4;
    } else {
	cov->z_rnl = int_size_0 - 2;
	cov->z_rnh = int_size_0;
	cov->z_rpl = 0;
	cov->z_rph = int_size_0 / 4;
	cov->z_cnl = int_size_0 - 2;
	cov->z_cnh = int_size_0;
	cov->z_cpl = 0;
	cov->z_cph = int_size_0 / 4;
    }

    cov->z_rl = cov->z_rnl - int_size_0;
    cov->z_rh = cov->z_rph;
    cov->z_cl = cov->z_cnl - int_size_0;
    cov->z_ch = cov->z_cph;
}


