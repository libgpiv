/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-----------------------------------------------------------------------
FILENAME:               postpar.c
LIBRARY:                libgpiv


EXTERNAL FUNCTIONS:     
                        gpiv_post_read_parameters
                        gpiv_post_default_parameters
                        gpiv_post_get_parameters_from_resources

                        gpiv_post_parameters_set
                        gpiv_post_check_parameters_read
			gpiv_post_fprint_parameters
                        gpiv_post_fread_hdf_parameters
                        gpiv_post_fwrite_hdf_parameters

LAST MODIFICATION DATE: $Id: post_par.c,v 1.8 2008-05-05 14:38:42 gerber Exp $
 ------------------------------------------------------------------- */

#include <gpiv.h>


/*
 * Default values and keys of GpivPostPar
 */
const gfloat POSTPAR_DEFAULT__SET_DX           = 0.0;      /**< Default parameter for set_dx of __GpivPostPar */
const gfloat POSTPAR_DEFAULT__SET_DY           = 0.0;      /**< Default parameter for set_dx of __GpivPostPar */
const gfloat POSTPAR_DEFAULT__Z_OFF_DX         = 0.0;      /**< Default parameter for z_off_dx of __GpivPostPar */
const gfloat POSTPAR_DEFAULT__Z_OFF_DY         = 0.0;      /**< Default parameter for z_off_dy of __GpivPostPar */

enum {
  POSTPAR_DEFAULT__OPERATOR_MANIPIV = 0,        /**< Default parameter for operator_manipiv of __GpivPostPar */
  POSTPAR_DEFAULT__SET              = 1,        /**< Default parameter for set of __GpivPostPar */
  POSTPAR_DEFAULT__SUBTRACT         = 0,        /**< Default parameter for subtract of __GpivPostPar */
  POSTPAR_DEFAULT__DIFF_TYPE        = 2,        /**< Default parameter for diff_type of __GpivPostPar */
  POSTPAR_DEFAULT__OPERATOR_VORSTRA = 0,        /**< Default parameter for operator_vorstra of __GpivPostPar */
  POSTPAR_DEFAULT__SCALE_TYPE       = 0         /**< Default parameter for scale_type of __GpivPostPar */
};


const gchar *POSTPAR_KEY__OPERATOR_MANIPIV     = "Operator_manipiv";      /**< Key for operator_manipiv of __GpivPostPar */
const gchar *POSTPAR_KEY__SET                  = "Set";           /**< Key for set of __GpivPostPar */
const gchar *POSTPAR_KEY__SET_DX               = "Set_dx";        /**< Key for set_dx of __GpivPostPar */
const gchar *POSTPAR_KEY__SET_DY               = "Set_dy";        /**< Key for set_dx of __GpivPostPar */
const gchar *POSTPAR_KEY__SUBTRACT             = "Subtract";      /**< Key for subtract of __GpivPostPar */
const gchar *POSTPAR_KEY__Z_OFF_DX             = "Zoff_dx";       /**< Key for z_off_dx of __GpivPostPar */
const gchar *POSTPAR_KEY__Z_OFF_DY             = "Zoff_dy";       /**< Key for z_off_dy of __GpivPostPar */
const gchar *POSTPAR_KEY__DIFF_TYPE            = "Differential_type";     /**< Key for diff_type of __GpivPostPar */
const gchar *POSTPAR_KEY__OPERATOR_VORSTRA     = "Operator_vorstra";      /**< Key for operator_vorstra of __GpivPostPar */
const gchar *POSTPAR_KEY__SCALE_TYPE           = "Scale_type";   /**< Key for scale_type of __GpivPostPar */


/* 
 * Prototypes local functions
 */

static void
obtain_postpar_fromline                 (gchar                  line[], 
                                         GpivPostPar            *post_par, 
                                         const gboolean         verbose
                                         );

static herr_t 
attr_info                               (hid_t                  loc_id, 
                                         const gchar            *name, 
                                         GpivPostPar            *post_par
                                         );

/*
 * Public functions
 */

void
gpiv_post_parameters_set                (GpivPostPar            *post_par,
                                         const gboolean         flag
                                         )
/*-----------------------------------------------------------------------------
 * Set flag for post_par __set */
{
    post_par->operator_manipiv__set = flag;
    post_par->set__set = flag;
    post_par->set_dx__set = flag;
    post_par->set_dy__set = flag;
    post_par->subtract__set = flag;
    post_par->z_off_dx__set = flag;
    post_par->z_off_dy__set = flag;
    post_par->diff_type__set = flag;
    post_par->operator_vorstra__set = flag;
    post_par->scale_type__set = flag;
}



void
gpiv_post_default_parameters            (GpivPostPar            *post_par_default,
                                         const gboolean         force
                                         )
/*-----------------------------------------------------------------------------
 * Default parameter values
 */
{
    if (!post_par_default->operator_manipiv__set || force)
        post_par_default->operator_manipiv = 
            POSTPAR_DEFAULT__OPERATOR_MANIPIV;
    if (!post_par_default->set__set || force)
        post_par_default->set = POSTPAR_DEFAULT__SET;
    if (!post_par_default->set_dx__set || force)
        post_par_default->set_dx = POSTPAR_DEFAULT__SET_DX;
    if (!post_par_default->set_dy__set || force)
        post_par_default->set_dy = POSTPAR_DEFAULT__SET_DY;
    if (!post_par_default->subtract__set || force)
        post_par_default->subtract = POSTPAR_DEFAULT__SUBTRACT;
    if (!post_par_default->z_off_dx__set || force) 
        post_par_default->z_off_dx = POSTPAR_DEFAULT__Z_OFF_DX;
    if (!post_par_default->z_off_dy__set || force) 
        post_par_default->z_off_dy = POSTPAR_DEFAULT__Z_OFF_DY;
    if (!post_par_default->diff_type__set || force)
        post_par_default->diff_type = POSTPAR_DEFAULT__DIFF_TYPE;
    if (!post_par_default->operator_vorstra__set || force)
        post_par_default->operator_vorstra = 
            POSTPAR_DEFAULT__OPERATOR_VORSTRA;
    if (!post_par_default->scale_type__set || force)
        post_par_default->scale_type = POSTPAR_DEFAULT__SCALE_TYPE;
    
    gpiv_post_parameters_set(post_par_default, TRUE);
}



GpivPostPar *
gpiv_post_get_parameters_from_resources (const gchar            *localrc,
                                         const gboolean         verbose
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads pos-processing parameters from system-wide gpiv.conf and $HOME/.gpivrc
 *---------------------------------------------------------------------------*/
{
    GpivPostPar *post_par = g_new0 (GpivPostPar, 1);


    gpiv_post_parameters_set (post_par, FALSE);
    gpiv_scan_parameter (GPIV_POSTPAR_KEY, localrc, post_par, verbose);
    gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, post_par, verbose);


    return post_par;
}



void 
gpiv_post_read_parameters               (FILE                   *fp, 
                                         GpivPostPar            *post_par,
                                         const gboolean         verbose
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Read all parameters for PIV post processing 
 *     if not defined by command line keys
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar line[GPIV_MAX_CHARS], par_name[GPIV_MAX_CHARS];


    if (fp == stdin || fp == NULL) {
        while (gets (line) != NULL) {
            obtain_postpar_fromline (line, post_par, verbose);
        }
    } else {
        while (fgets(line, GPIV_MAX_CHARS, fp) != NULL) {
            obtain_postpar_fromline (line, post_par, verbose);
        }
    }


    return;
}



gchar *
gpiv_post_check_parameters_read         (GpivPostPar            *post_par,
                                         const GpivPostPar      *post_par_default
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Check out if all parameters have been read
 *     Returns: NULL on success or *err_msg on failure
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar * err_msg = NULL;


    /*
     * Parameters for manipiv
     */
    if (post_par->operator_manipiv__set == FALSE) {
        post_par->operator_manipiv__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->operator_manipiv = POSTPAR_DEFAULT__OPERATOR_MANIPIV;
            } else {
            err_msg = "Using default: ";
            post_par->operator_manipiv = post_par_default->operator_manipiv;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__OPERATOR_MANIPIV,
                     post_par->operator_manipiv);
    }


    if (post_par->set__set == FALSE) {
        post_par->set__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->set = POSTPAR_DEFAULT__SET;
        } else {
            err_msg = "Using default: ";
            post_par->set = post_par_default->set;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__SET,
                     post_par->set);
    }


    if (post_par->set_dx__set == FALSE) {
        post_par->set_dx__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->set_dx = POSTPAR_DEFAULT__SET_DX;
        } else {
            err_msg = "Using default: ";
            post_par->set_dx = post_par_default->set_dx;
        }
        gpiv_warning("%s\n%s.%s %f", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__SET_DX,
                     post_par->set_dx);
    }


    if (post_par->set_dy__set == FALSE) {
        post_par->set_dy__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->set_dy = POSTPAR_DEFAULT__SET_DY;
        } else {
            err_msg = "Using default: ";
            post_par->set_dy = post_par_default->set_dy;
        }
        gpiv_warning("%s\n%s.%s %f", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__SET_DY,
                     post_par->set_dy);
    }


    /*
     * Parameters of s-avg
     */
    if (post_par->subtract__set == FALSE) {
        post_par->subtract__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->subtract = POSTPAR_DEFAULT__SUBTRACT;
        } else {
            err_msg = "Using default: ";
            post_par->subtract = post_par_default->subtract;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__SUBTRACT,
                     post_par->subtract);
    }

    if (post_par->z_off_dx__set == FALSE) {
        post_par->z_off_dx__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->z_off_dx = POSTPAR_DEFAULT__Z_OFF_DX;
        } else {
            err_msg = "Using default: ";
            post_par->z_off_dx = post_par_default->z_off_dx;
        }
        gpiv_warning("%s\n%s.%s %f", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__Z_OFF_DX,
                     post_par->z_off_dx);
    }

    if (post_par->z_off_dy__set == FALSE) {
        post_par->z_off_dy__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->z_off_dy = POSTPAR_DEFAULT__Z_OFF_DY;
        } else {
            err_msg = "Using default: ";
            post_par->z_off_dy = post_par_default->z_off_dy;
        }
        gpiv_warning("%s\n%s.%s %f", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__Z_OFF_DY,
                     post_par->z_off_dy);
    }


    /*
     * Parameters for vorstra
     */
    if (post_par->diff_type__set == FALSE) {
        post_par->diff_type__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->diff_type = POSTPAR_DEFAULT__DIFF_TYPE;
        } else {
            err_msg = "Using default: ";
            post_par->diff_type = post_par_default->diff_type;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__DIFF_TYPE,
                     post_par->diff_type);
    }

    if (post_par->operator_vorstra__set == FALSE) {
        post_par->operator_vorstra__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->operator_vorstra = 
                POSTPAR_DEFAULT__OPERATOR_VORSTRA;
        } else {
            err_msg = "Using default: ";
            post_par->operator_vorstra = post_par_default->operator_vorstra;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__OPERATOR_VORSTRA,
                     post_par->operator_vorstra);
    }

    /*
     * Parameters for scale
     */
    if (post_par->scale_type__set == FALSE) {
        post_par->scale_type__set = TRUE;
        if (post_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            post_par->scale_type = POSTPAR_DEFAULT__SCALE_TYPE;
        } else {
            err_msg = "Using default: ";
            post_par->scale_type = post_par_default->scale_type;
        }
        gpiv_warning("%s\n%s.%s %d", 
                     err_msg, 
                     GPIV_POSTPAR_KEY,
                     POSTPAR_KEY__SCALE_TYPE,
                     post_par->scale_type);
    }


    return err_msg;
}



void 
gpiv_post_print_parameters              (FILE                   *fp,
                                         const GpivPostPar      *post_par
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Prints parameters to fp
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    if (fp == stdout || fp == NULL) fp = stdout;
    /*
     * Parameters for manipiv
     */
    if (post_par->operator_manipiv__set)
        fprintf(fp,"%s.%s  %d\n", 
                GPIV_POSTPAR_KEY, 
                POSTPAR_KEY__OPERATOR_MANIPIV, 
                post_par->operator_manipiv);
    
    if (post_par->set__set)
        fprintf(fp,"%s.%s  %d\n", 
                GPIV_POSTPAR_KEY, 
                POSTPAR_KEY__SET, 
                post_par->set);
    
    if (post_par->set_dx__set)
        fprintf(fp,"%s.%s  %f\n", 
                GPIV_POSTPAR_KEY, 
                POSTPAR_KEY__SET_DX, 
                post_par->set_dx);
    
    if (post_par->set_dy__set)
        fprintf(fp,"%s.%s  %f\n", 
                GPIV_POSTPAR_KEY, 
                POSTPAR_KEY__SET_DY, 
                post_par->set_dy);
    
    /*
     * Parameters of s-avg
     */
    if (post_par->subtract__set) 
        fprintf(fp,"%s.%s %d\n",
                GPIV_POSTPAR_KEY, 
                POSTPAR_KEY__SUBTRACT, 
                post_par->subtract);
    
    if (post_par->z_off_dx__set) 
        fprintf(fp,"%s.%s %f\n",
                GPIV_POSTPAR_KEY, 
                POSTPAR_KEY__Z_OFF_DX, 
                post_par->z_off_dx);
    
    if (post_par->z_off_dy__set) 
        fprintf(fp,"%s.%s %f\n",
                GPIV_POSTPAR_KEY, 
                POSTPAR_KEY__Z_OFF_DY, 
                post_par->z_off_dy);

    /*
     * Parameters for vorstra
     */
    if (post_par->operator_vorstra__set)
	fprintf(fp, "%s.%s %d\n",
                GPIV_POSTPAR_KEY,
                POSTPAR_KEY__OPERATOR_VORSTRA, 
		post_par->operator_vorstra);
    
    if (post_par->diff_type__set)
	fprintf(fp, "%s.%s %d\n",
                GPIV_POSTPAR_KEY,
                POSTPAR_KEY__DIFF_TYPE,
		post_par->diff_type);

    /*
     * Parameter for scale
     */
    if (post_par->scale_type__set)
	fprintf(fp, "%s.%s %d\n",
                GPIV_POSTPAR_KEY,
                POSTPAR_KEY__SCALE_TYPE, 
                post_par->scale_type);
    
    
    fprintf(fp, "\n");
    
}



GpivPostPar *
gpiv_post_fread_hdf5_parameters         (const gchar        *fname
                                         )
/* --------------------------------------------------------------------------- 
 * DESCRIPTION:
 *     Reads post parameters from hdf5 data file
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *     group_id:       hdf group identity
 *
 * OUTPUTS:
 *     piv_post_par:   parameter structure
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *-------------------------------------------------------------------------- */
{
    GpivPostPar *post_par = g_new0 (GpivPostPar, 1);
    gchar *err_msg = NULL;
    int i;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id; 
    herr_t      status;


    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_post_fread_hdf5_parameters: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return NULL;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

    group_id = H5Gopen (file_id, "PIV", H5P_DEFAULT);
    /* H5Aiterate (group_id, NULL, (H5A_operator_t)attr_info, post_par); */
    H5Aiterate (group_id, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, 0,
                (H5A_operator_t)attr_info, post_par);
    status = H5Gclose (group_id);

    group_id = H5Gopen (file_id, "SCALARS", H5P_DEFAULT);
    /* H5Aiterate(group_id, NULL, (H5A_operator_t)attr_info, post_par); */
    H5Aiterate (group_id, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, 0,
                (H5A_operator_t)attr_info, post_par);
    status = H5Gclose (group_id);

    status = H5Fclose(file_id); 


    return post_par;
}



gchar *
gpiv_post_fwrite_hdf5_parameters        (const gchar            *fname, 
                                         const GpivPostPar      *post_par 
                                         )
/*---------------------------------------------------------------------------- 
 * DESCRIPTION:
 *     Writes post parameters to an existing hdf5 data file
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *     group_id:       hdf group identity
 *     post_par:       parameter structure
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *-------------------------------------------------------------------------- */
{
    gchar *err_msg = NULL;
    int i;
    /*
     * HDF declarations
     */
    hid_t       file_id, dataspace_id, group_id, attribute_id; 
    hsize_t     dims[1];
    herr_t      status;

    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_post_fwrite_hdf5_parameters: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "PIV", H5P_DEFAULT);



    dims[0] = 1;
    dataspace_id = H5Screate_simple(1, dims, NULL);

    /*
     * Parameters of manipiv
     */
    if (post_par->operator_manipiv__set) {
        attribute_id = H5Acreate(group_id, 
                                 POSTPAR_KEY__OPERATOR_MANIPIV, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &post_par->operator_manipiv); 
        status = H5Aclose(attribute_id); 
    }

    /*
     * parameters of s-avg
     */
    if (post_par->subtract__set) {
        attribute_id = H5Acreate(group_id, 
                                 POSTPAR_KEY__SUBTRACT, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &post_par->subtract); 
        status = H5Aclose(attribute_id); 
    }


    if (post_par->z_off_dx__set) {
        attribute_id = H5Acreate(group_id, 
                                 POSTPAR_KEY__Z_OFF_DX, 
                                 H5T_NATIVE_FLOAT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_FLOAT, 
                          &post_par->z_off_dx); 
        status = H5Aclose(attribute_id); 
    }

    if (post_par->z_off_dy__set) {
        attribute_id = H5Acreate(group_id, 
                                 POSTPAR_KEY__Z_OFF_DY, 
                                 H5T_NATIVE_FLOAT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_FLOAT, 
                          &post_par->z_off_dy); 
        status = H5Aclose(attribute_id); 
    }

    status = H5Gclose (group_id);

    /*
     * Parameters of vorstra
     */
    group_id = H5Gopen (file_id, "SCALARS", H5P_DEFAULT);

    if (post_par->diff_type__set) {
        attribute_id = H5Acreate(group_id, 
                                 POSTPAR_KEY__DIFF_TYPE, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &post_par->diff_type); 
        status = H5Aclose(attribute_id); 
    }

    if (post_par->operator_vorstra__set) {
        attribute_id = H5Acreate(group_id, 
                                 POSTPAR_KEY__OPERATOR_VORSTRA, 
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &post_par->operator_vorstra); 
        status = H5Aclose(attribute_id); 
    }


    /*
     * Parameters of scale
     */
    if (post_par->scale_type__set) {
        attribute_id = H5Acreate(group_id,
                                 POSTPAR_KEY__SCALE_TYPE,
                                 H5T_NATIVE_INT, 
                                 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Awrite(attribute_id, H5T_NATIVE_INT, 
                          &post_par->scale_type); 
        status = H5Aclose(attribute_id); 
    }

    status = H5Sclose(dataspace_id);
    status = H5Gclose (group_id);
    status = H5Fclose(file_id); 
    return err_msg;
}


/*
 * Local functions
 */

static void
obtain_postpar_fromline                 (gchar                  line[], 
                                         GpivPostPar            *post_par, 
                                         const gboolean         verbose
                                         )
/*-----------------------------------------------------------------------------
 * Scans a line to get a member of GpivPostPar
 */
{
    char par_name[GPIV_MAX_CHARS];


    if (line[0] != '#' && line[0] != '\n' && line[0] != ' ' 
        && line[0] != '\t') {
        sscanf(line,"%s",par_name);

        /*
         * Parameters for manipulating PIV data
         */
        if (post_par->operator_manipiv__set == FALSE) {
            post_par->operator_manipiv__set = 
                gpiv_scan_iph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__OPERATOR_MANIPIV, TRUE,
                                line, par_name, (int *) &post_par->operator_manipiv, 
                                verbose, NULL);
        }
            
        if (post_par->set__set == FALSE) {
            post_par->set__set = 
                gpiv_scan_iph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__SET, TRUE,
                                line, par_name, &post_par->set, 
                                verbose, NULL);
        }
            
        if (post_par->set_dx__set == FALSE) {
            post_par->set_dx__set = 
                gpiv_scan_fph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__SET_DX, TRUE,
                                line, par_name, &post_par->set_dx, 
                                verbose, NULL);
        }
            
        if (post_par->set_dy__set == FALSE) {
            post_par->set_dy__set = 
                gpiv_scan_fph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__SET_DY, TRUE,
                                line, par_name, &post_par->set_dy, 
                                verbose, NULL);
        }

        /*
         * Parameters for s-avg
         */
        if (post_par->subtract__set == FALSE) {
            post_par->subtract__set = 
                gpiv_scan_iph (GPIV_POSTPAR_KEY,
                                POSTPAR_KEY__SUBTRACT, TRUE,
                                line,par_name, &post_par->subtract, 
                                verbose, NULL);
        }
            
        if (post_par->z_off_dx__set == FALSE) {
            post_par->z_off_dx__set = 
                gpiv_scan_fph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__Z_OFF_DX, TRUE,
                                line, par_name, &post_par->z_off_dx, 
                                verbose, NULL);
        }
            
        if (post_par->z_off_dy__set == FALSE) {
            post_par->z_off_dy__set = 
                gpiv_scan_fph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__Z_OFF_DY, TRUE,
                                line, par_name, &post_par->z_off_dy, 
                                verbose, NULL);
        }

        /*
         * Parameters for vorticity and strain
         */
        if (post_par->diff_type__set == FALSE) {
            post_par->diff_type__set = 
                gpiv_scan_iph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__DIFF_TYPE, TRUE,
                                line, par_name, (int *) &post_par->diff_type, 
                                verbose, NULL);
        }
            
        if (post_par->operator_vorstra__set == FALSE) {
            post_par->operator_vorstra__set = 
                gpiv_scan_iph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__OPERATOR_VORSTRA, TRUE,
                                line, par_name, (int *) &post_par->operator_vorstra, 
                                verbose, NULL);
        }
            
        /*
         * Parameters for scaling PIV data.
         */
        if (post_par->scale_type__set == FALSE) {
            post_par->scale_type__set = 
                gpiv_scan_iph (GPIV_POSTPAR_KEY, 
                                POSTPAR_KEY__SCALE_TYPE, TRUE,
                                line, par_name, (int *) &post_par->scale_type, 
                                verbose, NULL);
        }
            
    }


    return;
}



static herr_t 
attr_info                               (hid_t                  loc_id, 
                                         const gchar            *name, 
                                         GpivPostPar            *post_par
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Operator function.
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    hid_t attribute_id, atype;
    herr_t status;



/*
 * Parameters of manipiv
 */
    if (strcmp(name, POSTPAR_KEY__OPERATOR_MANIPIV) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &post_par->operator_manipiv); 
        status = H5Aclose(attribute_id); 
        post_par->operator_manipiv__set = TRUE;
    }


/*
 * Global variables and parameters of s-avg
 */
    if (strcmp(name, POSTPAR_KEY__SUBTRACT) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &post_par->subtract); 
        status = H5Aclose(attribute_id); 
        post_par->subtract__set = TRUE;
    }

    if (strcmp(name, POSTPAR_KEY__Z_OFF_DX) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_FLOAT, 
                         &post_par->z_off_dx); 
        status = H5Aclose(attribute_id); 
        post_par->z_off_dx__set = TRUE;
    }


    if (strcmp(name, POSTPAR_KEY__Z_OFF_DY) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_FLOAT, 
                         &post_par->z_off_dy); 
        status = H5Aclose(attribute_id); 
        post_par->z_off_dy__set = TRUE;
    }


/*
 * Parameters of vorstra
 */
    if (strcmp(name, POSTPAR_KEY__DIFF_TYPE) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &post_par->diff_type); 
        status = H5Aclose(attribute_id); 
        post_par->diff_type__set = TRUE;
    }


    if (strcmp(name, POSTPAR_KEY__OPERATOR_VORSTRA) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &post_par->operator_vorstra); 
        status = H5Aclose(attribute_id); 
        post_par->operator_vorstra__set = TRUE;
    }

/*
 * Parameters of scale
 */
    if (strcmp(name, POSTPAR_KEY__SCALE_TYPE) == 0) {
        attribute_id = H5Aopen_name(loc_id, name);
        status = H5Aread(attribute_id, H5T_NATIVE_INT, 
                         &post_par->scale_type); 
        status = H5Aclose(attribute_id); 
        post_par->scale_type__set = TRUE;
    }

    return 0;
}



