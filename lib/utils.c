/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                utils.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_scan_parameter
                         gpiv_scan_resourcefiles
                         gpiv_add_datetime_to_comment
                         gpiv_sort_3,
                         gpiv_max, 
                         gpiv_min, 
                         gpiv_warning, 
                         gpiv_error, 
                         gpiv_fscan_iph_nl
                         gpiv_scan_iph, 
                         gpiv_scan_cph, 
                         gpiv_scan_sph, 


LOCAL FUNCTIONS:         compare_float
                         local_scan_parameter

LAST MODIFICATION DATE:  $Id: utils.c,v 1.27 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------------------- */

#include <string.h>
#include <gpiv.h>

#define NSTACK 50
#define M 7
#define SWAP(a,b) temp=(a); (a)=(b); (b)=temp;

/*
 * Prototyping
 */
static gchar *
local_scan_parameter (FILE *fp,
                      const gchar *fname, 
                      const gchar *PAR_KEY, 
                      void *pstruct, 
                      gboolean verbose);


/* gint */
/* compare_float(const void *a, */
/*               const void *b */
/*               ); */

/*
 * Function definitions
 */
/* void *gpiv_fread_parameter (const gchar *PAR_KEY,  */
/*                       const gchar *PARFILE, */
/*                       gboolean verbose */
/*                       ) */
/* { */
/*     void *pstruct; */
/*     gpiv_scan_parameter (PAR_KEY, PARFILE, pstruct, verbose); */
/*     gpiv_scan_resourcefiles (PAR_KEY, pstruct, verbose); */
/*     return pstruct; */
/* } */

void
gpiv_scan_parameter (const gchar *PAR_KEY,
                     const gchar *PARFILE,
                     void *pstruct,
                     gboolean verbose
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Read parameters from local parameter file PARFILE
 *
 * PROTOTYPE LOCATATION:
 *     utils.h
 *
 * INPUTS:
 *      PAR_KEY:        Parameter key, specific for each process
 *      PARFILE:        Parameter file, specific for each process
 *      verbose:      flag for printing parameters
 *
 * OUTPUTS:
 *      pstruct:        pointer to parameter structure
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    FILE *fp_par;
    gchar system_rsc[GPIV_MAX_CHARS];

    snprintf(system_rsc, GPIV_MAX_CHARS, "%s/%s", SYSTEM_RSC_DIR, 
             GPIV_SYSTEM_RSC_FILE);

    if ((fp_par = fopen (PARFILE, "rb")) == NULL) {
        if (verbose) {
            gpiv_warning ("gpiv_scan_parameter: failing fopen(%s)", PARFILE);
            gpiv_warning ("You may copy this file from %s", system_rsc);
        }
        return;
    }

    if ((err_msg = 
         local_scan_parameter (fp_par, PARFILE, PAR_KEY, pstruct, verbose))
        != NULL) {
        gpiv_warning ("gpiv_scan_parameter: %s", err_msg);
        return;
    }
    fclose(fp_par);
    return;
}



gchar *
gpiv_scan_resourcefiles (const gchar *PAR_KEY, 
                         void *pstruct,
                         gint verbose
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Opens resource files ~/GPIV_HOME_RSC_FILE (hidden) and 
 *     /SYSTEM_RSC_DIR/GPIV_SYSTEM_RSC_FILE
 *     Reads parameters from it
 *
 * PROTOTYPE LOCATATION:
 *     utils.h
 *
 * INPUTS:
 *      PAR_KEY:        Parameter key, specific for each process
 *      verbose:      parameter to print to stdout
 *
 * OUTPUTS:
 *      pstruct:        pointer to parameter structure
 *
 * RETURNS:
 *      NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    FILE *fp_par;
    const gchar *home_dir = NULL;
    gchar home_rsc[GPIV_MAX_CHARS]; 
    gchar system_rsc[GPIV_MAX_CHARS];
    gboolean home_rsc__exist = TRUE;   

    snprintf(system_rsc, GPIV_MAX_CHARS, "%s/%s", SYSTEM_RSC_DIR, 
             GPIV_SYSTEM_RSC_FILE);

    home_dir = g_get_home_dir ();
    snprintf(home_rsc, GPIV_MAX_CHARS, "%s/%s", home_dir, GPIV_HOME_RSC_FILE);

/*
 * Reads from home_rsc
 */
    if ((fp_par=fopen (home_rsc, "rb")) == NULL) {
        if (verbose) {
            g_message ("gpiv_scan_resourcefiles: failing fopen(%s)", home_rsc);
            g_message ("You may copy this file from %s\n", system_rsc);
        }
        home_rsc__exist = FALSE;
    } else {
        home_rsc__exist = TRUE;
    }

    if (home_rsc__exist) {
        if ((err_msg = 
             local_scan_parameter (fp_par, home_rsc, PAR_KEY, pstruct, verbose))
            != NULL) {
            ("gpiv_scan_resourcefiles: %s", err_msg);
            return err_msg;
        }
        fclose(fp_par);
    }

/*
 * Reads from system_rsc
 */
    if ((fp_par = fopen (system_rsc, "rb")) == NULL) {
        if (verbose) g_message ("gpiv_scan_resourcefiles: failing fopen(%s)", system_rsc);
        return ("gpiv_scan_resourcefiles: failing fopen");
    }

    if ((err_msg = 
         local_scan_parameter (fp_par, system_rsc, PAR_KEY, pstruct, verbose))
        != NULL) {
        gpiv_warning ("gpiv_scan_resourcefiles: %s", err_msg);
        return err_msg;
    }
    fclose(fp_par);    

    return err_msg;
}


gchar *
gpiv_add_datetime_to_comment (gchar *comment
                              )
/*----------------------------------------------------------------------------
 */
{
    gchar *ctime = NULL, *cdate = NULL;
    GDate *gdate;
    time_t ttime;
    struct tm *local_time;


    gdate = g_date_new ();
    g_date_set_time_t(gdate, time (NULL));
    cdate = g_strdup_printf("# Creation date: %d/%d/%d", 
                            g_date_day (gdate), 
                            g_date_month (gdate), 
                            g_date_year (gdate));

    time (&ttime);
    local_time = localtime (&ttime);

    ctime = g_strdup_printf(", time: %d:%d:%d\n", 
                            local_time->tm_hour, 
                            local_time->tm_min, 
                            local_time->tm_sec);

    if (comment == NULL) {
        comment = g_strdup_printf ("%s%s", cdate, ctime, NULL);
    } else {
        comment = g_strconcat (comment, cdate, ctime, NULL);
    }

    g_free (cdate);
    g_free (ctime);
    return comment;
}



gchar *
gpiv_sort_3 (const unsigned long n, 
             float arr[], 
             float arr_2[], 
             float arr_3[]
             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Orders array arr AND its belonging arrays arr_2 and arr_3
 *      Also swaps accompanying arrays
 *
 * INPUTS:
 *      n:	        length of array
 *      arr[]:	        array to be sorted
 *      arr_3[]:	second belonging array to arr
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar * err_msg = NULL;
    unsigned long i, ir = n, j, k, l = 1, *istack;
    gint jstack = 0;
    float a, b, c, temp;
    

    istack = gpiv_ulvector_index (1, NSTACK);

    for (;;) {
        if (ir-l < M) {
            for (j = l + 1; j <= ir; j++) {
                a = arr[j];
                b = arr_2[j];
                c = arr_3[j];
                for (i = j-1; i >= l; i--) {
                    if (arr[i] <= a) break;
                    arr[i+1] = arr[i];
                    arr_2[i+1] = arr_2[i];
                    arr_3[i+1] = arr_3[i];
                }
                arr[i+1] = a;
                arr_2[i+1] = b;
                arr_3[i+1] = c;
            }
            if (jstack == 0) break;
            ir = istack[jstack--];
            l = istack[jstack--];
        } else {
            k = (l+ir) >> 1;
            SWAP (arr[k], arr[l+1]);
            SWAP (arr_2[k], arr_2[l+1]);
            SWAP (arr_3[k], arr_3[l+1]);
            if (arr[l] > arr[ir]) {
                SWAP (arr[l], arr[ir]);
                SWAP (arr_2[l], arr_2[ir]);
                SWAP (arr_3[l], arr_3[ir]);
            }
            if (arr[l+1] > arr[ir]) {
                SWAP (arr[l+1], arr[ir]);
                SWAP (arr_2[l+1], arr_2[ir]);
                SWAP (arr_3[l+1], arr_3[ir]);
            }
            if (arr[l] > arr[l+1]) {
                SWAP (arr[l], arr[l+1]);
                SWAP (arr_2[l], arr_2[l+1]);
                SWAP (arr_3[l], arr_3[l+1]);
            }
            i = l+1;
            j = ir;
            a = arr[l+1];
            b = arr_2[l+1];
            c = arr_3[l+1];
            for (;;) {
                do i++; while (arr[i] < a);
                do j--; while (arr[j] > a);
                if (j < i) break;
                SWAP (arr[i], arr[j]);
                SWAP (arr_2[i], arr_2[j]);
                SWAP (arr_3[i], arr_3[j]);
            }
            arr[l+1] = arr[j];
            arr_2[l+1] = arr_2[j];
            arr_3[l+1] = arr_3[j];
            arr[j] = a;
            arr_2[j] = b;
            arr_3[j] = c;
            jstack += 2;
            if (jstack > NSTACK) { 
                err_msg = "gpiv_sort_3: NSTACK too small in sort.";
                gpiv_warning("%s", err_msg);
                return err_msg;
            }
            if (ir-i+1 >= j-l) {
                istack[jstack] = ir;
                istack[jstack-1] = i;
                ir = j-1;
            } else {
                istack[jstack] = j-1;
                istack[jstack-1] = l;
                l = i;
            }
        }
    }


    gpiv_free_ulvector_index(istack, 1, NSTACK);
    return err_msg;
}



long
gpiv_lmax (long a, 
           long b
           )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Calculates maximum of longs a and b
 *
 * INPUTS:
 *      a:              first variable to be tested
 *      b:              second variable to be tested
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      result of maximum
 *---------------------------------------------------------------------------*/
{
    long c;


    if (a >= b) {
        c=a;
    } else {
        c=b;
    }


    return c;
}



long
gpiv_lmin (long a, 
           long b
           )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Calculates minimum of longs a and b
 *
 * INPUTS:
 *      a:              first variable to be tested
 *      b:              second variable to be tested
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      result of minimum
 *---------------------------------------------------------------------------*/
{
    long c;


    if (a <= b) {
        c=a;
    } else {
        c=b;
    }


    return c;
}



gint 
gpiv_max (gint a, 
          gint b
          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Calculates maximum of integers a and b
 *
 * INPUTS:
 *      a:              first variable to be tested
 *      b:              second variable to be tested
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      result of maximum
 *---------------------------------------------------------------------------*/
{
    gint c;


    if (a >= b) {
        c=a;
    } else {
        c=b;
    }


    return c;
}



gint 
gpiv_min (gint a, 
          gint b
          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Calculates minimum of integers a and b
 *
 * INPUTS:
 *      a:              first variable to be tested
 *      b:              second variable to be tested
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      result of minimum
 *---------------------------------------------------------------------------*/
{
    gint c;


    if (a <= b) {
        c=a;
    } else {
        c=b;
    }


    return c;
}



void 
gpiv_warning (gchar *message, ...
              )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Warning message with variable argumanent list
 *
 * INPUTS:
 *      message:       warning message
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    va_list args;
    

    va_start (args, message);
    fprintf (stderr,"\n# *** message: ");
    vfprintf (stderr, message, args);
    va_end (args);
    
}



void 
gpiv_error (gchar *message, ...
            )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Prints error handling with variable argumanent list to stdout 
 *      and exits program
 *
 * INPUTS:
 *      message:       warning message
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    va_list args;
    

    va_start (args, message);
    fprintf (stderr,"\nerror: ");
    vfprintf (stderr, message, args);
    fprintf (stderr,"\n");
    va_end (args);


#ifdef ENABLE_MPI
    MPI_Finalize();
/*     MPI_Abort(MPI_COMM_WORLD, rc); */
#endif
    exit (1);
}


static gchar *
local_scan_parameter (FILE *fp,
                      const gchar *FNAME, 
                      const gchar *PAR_KEY, 
                      void *pstruct, 
                      gboolean verbose)
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *   Scans parameters in fp
 */
{
    gchar *err_msg = NULL;

    if (strcmp(PAR_KEY, GPIV_GENPAR_KEY) == 0) {
        if (verbose) printf("\n# Default %s parameters from %s: \n", 
                            PAR_KEY, FNAME);
        gpiv_imgproc_read_parameters(fp, pstruct, verbose);
        
    } else if (strcmp (PAR_KEY, GPIV_IMGPAR_KEY) == 0) {
        if (verbose) printf("\n# Default %s parameters from %s: \n", 
                            PAR_KEY, FNAME);
        /*             gpiv_img_read_parameters(fp, pstruct, verbose); */
        gpiv_img_read_header (fp, pstruct, TRUE, verbose, NULL);

#ifdef ENABLE_CAM
    } else if (strcmp(PAR_KEY, GPIV_CAMPAR_KEY) == 0) {
        if (verbose) printf("\n# Default %s parameters from %s: \n", 
                            PAR_KEY, FNAME);
        gpiv_cam_read_parameters(fp, pstruct, verbose);
#endif /* ENABLE_CAM */

#ifdef ENABLE_TRIG
    } else if (strcmp(PAR_KEY, GPIV_TRIGPAR_KEY) == 0) {
        if (verbose) printf("\n# Default %s parameters from %s: \n", 
                            PAR_KEY, FNAME);
        gpiv_trig_read_parameters(fp, pstruct, verbose);
#endif /* ENABLE_TRIG */

    } else if (strcmp(PAR_KEY, GPIV_IMGPROCPAR_KEY) == 0) {
        if (verbose) printf("\n# Default %s parameters from %s: \n", 
                            PAR_KEY, FNAME);
        gpiv_imgproc_read_parameters(fp, pstruct, verbose);

    } else if (strcmp(PAR_KEY, GPIV_PIVPAR_KEY) == 0) {
        if (verbose) printf("\n# Default %s parameters from %s: \n", 
                            PAR_KEY, FNAME);
        gpiv_piv_read_parameters (fp, pstruct, verbose);

    } else if (strcmp(PAR_KEY, GPIV_VALIDPAR_KEY) == 0 ) {
        if (verbose) printf("\n# Default %s parameters from %s: \n", 
                            PAR_KEY, FNAME);
        gpiv_valid_read_parameters(fp, pstruct, verbose);

    } else if (strcmp(PAR_KEY, GPIV_POSTPAR_KEY) == 0) {
        if (verbose) printf("\n# Default %s parameters from %s: \n", 
                            PAR_KEY, FNAME);
        gpiv_post_read_parameters(fp, pstruct, verbose);

    } else {
        err_msg = "LIBGPIV internal error: local_scan_parameter: called with unexisting key: ";
        gpiv_warning("%s", err_msg, PAR_KEY);
        return err_msg;
    }


    return err_msg;
}


gboolean
gpiv_fscan_iph_nl (FILE *fp_h, 
                   const gchar *MOD_KEY, 
                   const gchar *PAR_KEY, 
                   const gboolean use_mod_key,
                   gchar *line, 
                   const gchar *par_name, 
                   gint *parameter, 
                   const gboolean verbose,
                   FILE *fp
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Function to scan line string on **integer** parameter key
 *      and value or image header value (without program key). The
 *      value to be read is on the next line of the key.
 *
 */
{
    gchar *tmp_string;
    gboolean flag_par_set = FALSE;


    if (use_mod_key) {
        tmp_string = g_strdup_printf("%s.%s", MOD_KEY, PAR_KEY);
    } else {
        tmp_string = g_strdup_printf("%s", PAR_KEY);
    }
    
    if (strcmp (par_name, tmp_string) == 0) {
        flag_par_set = TRUE;
        if (fgets (line, GPIV_MAX_CHARS, fp_h) != NULL) {
            sscanf (line, "%d", parameter);
            if (verbose) printf ("%s %d\n", par_name, *parameter);
        } else {
            gpiv_error ("gpiv_scan_iph_nl: line = NULL");
        }
    }
    

    g_free (tmp_string);
    return flag_par_set;
}



gboolean
gpiv_scan_iph (const gchar *MOD_KEY,
               const gchar *PAR_KEY,
               const gboolean use_mod_key,
               const gchar *line,
               const gchar *par_name,
               gint *parameter,
               const gboolean verbose,
               FILE *fp
               )
/*-----------------------------------------------------------------------------
 *      Scans line string on **integer** parameter key
 *      and value or image header value (without program key). Prints result
 *      to file.
 */
{
    gchar *tmp_string;
    gboolean flag_par_set = FALSE;
    

    if (fp == NULL) fp = stdout;
    if (use_mod_key) {
        tmp_string = g_strdup_printf("%s.%s", MOD_KEY, PAR_KEY);
    } else {
        tmp_string = g_strdup_printf("%s", PAR_KEY);
    }
    
    if (strcmp (par_name, tmp_string) == 0) {
        sscanf (line, "%s %d", par_name, parameter);
        flag_par_set = TRUE;
        if (verbose) fprintf (fp, "%s %d\n", par_name, *parameter);
    }
    

    g_free (tmp_string);
    return flag_par_set;
}



gboolean
gpiv_scan_cph (const gchar *MOD_KEY,
               const gchar *PAR_KEY,
               const gboolean use_mod_key,
               const gchar *line,
               const gchar *par_name,
               gchar *parameter,
               const gboolean verbose,
               FILE *fp
               )
/*---------------------------------------------------------------------------- 
 *      Scans line string on **char** parameter key
 *      and value or image header value (without program key). Prints result
 *      to file.
 */
{
    gchar *tmp_string;
    gboolean flag_par_set = FALSE;


    if (fp == NULL) fp = stdout;
    if (use_mod_key) {
        tmp_string = g_strdup_printf("%s.%s", MOD_KEY, PAR_KEY);
    } else {
        tmp_string = g_strdup_printf("%s", PAR_KEY);
    }
    
    if (strcmp (par_name, tmp_string) == 0) {
        sscanf (line, "%s %c", par_name, parameter);
        flag_par_set = TRUE;
        if (verbose) fprintf (fp, "%s %c\n", par_name, *parameter);
    }
    

    g_free (tmp_string);
    return flag_par_set;
}



gboolean
gpiv_scan_fph (const gchar *MOD_KEY,
               const gchar *PAR_KEY,
               const gboolean use_mod_key,
               const gchar *line,
               const gchar *par_name,
               gfloat *parameter,
               const gboolean verbose,
               FILE *fp
               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Function to scan line string on **float** parameter/header
 *     key and value or image header value (without program key). Prints result
 *      to file.
*/
{
    gchar *tmp_string;
    gboolean flag_par_set = FALSE;
    

    if (fp == NULL) fp = stdout;
    if (use_mod_key) {
        tmp_string = g_strdup_printf("%s.%s", MOD_KEY, PAR_KEY);
    } else {
        tmp_string = g_strdup_printf("%s", PAR_KEY);
    }
    
    if (strcmp (par_name, tmp_string) == 0) {
        sscanf (line, "%s %f", par_name, parameter);
        flag_par_set = TRUE;
        if (verbose) fprintf (fp, "%s %f\n", par_name, *parameter);
    }
    

    g_free (tmp_string);
    return flag_par_set;
}



gboolean
gpiv_scan_sph (const gchar *MOD_KEY,
               const gchar *PAR_KEY,
               const gboolean use_mod_key,
               const gchar *line,
               const gchar *par_name,
               gchar *parameter,
               const gboolean verbose,
               FILE *fp
               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Function to scan line string on **string** parameter/header
 *     key and value or image header value (without program key). Prints result
 *     to file.
 */
{
    gchar *tmp_string;
    gboolean flag_par_set = FALSE;
    gint key_length, i = 0;
    

    if (fp == NULL) fp = stdout;
    if (use_mod_key) {
        tmp_string = g_strdup_printf("%s.%s", MOD_KEY, PAR_KEY);
    } else {
        tmp_string = g_strdup_printf("%s", PAR_KEY);
    }
    
    key_length = strlen (tmp_string) + 1;
    
    if (strcmp (par_name, tmp_string) == 0) {
/*         parameter = g_strdup_printf("%s:", line[key_length + i]); */

        while (line[key_length + i] != '\0' 
               && i < GPIV_MAX_CHARS - key_length) {
            if (line[key_length + i] != '\n') {
                parameter[i] = line[key_length + i];
                i++;
            } else {
                parameter[i] = '\0';
                break;
            }
        }
        
        flag_par_set = TRUE;
        if (verbose) fprintf (fp, "%s %s\n", par_name, parameter);
    }


    g_free (tmp_string);
    return flag_par_set;
}


#undef SWAP
#undef NSTACK
#undef M
