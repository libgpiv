/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2011 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------*/

#include <gpiv.h>
#include "my_utils.h"


#define PNM_GET1(x) PPM_GETB(x)
#define PPM_GETB(x) ((x) & 0x3ff)
#define PNG_BYTES_TO_CHECK 4
#define PNG_KEY__PIV_XCORR "Piv: Xcorr (TRUE/FALSE)"
#define PNG_KEY__PIV_DT    "Piv: Delta_t (milliseconds)"


static png_uint_16 
get_png_val (png_byte **pp, 
	     int bit_depth
	     );

static png_byte **
matrix_png_byte (long nr, 
                 long nc
                 );

static void 
free_matrix_png_byte (png_byte **m
                      );

static void
get_imgpar_from_textchunks (GpivImagePar *image_par, 
                            png_structp png_ptr,
                            png_infop info_ptr
                            );
static void
set_textchunks_from_imgpar (GpivImagePar *image_par, 
                            png_structp png_ptr, 
                            png_infop info_ptr
                            );


static void
print_img_parameters_set (GpivImagePar *gpiv_image_par);

static void 
read_raw_image_frames (FILE *fp, 
                       GpivImage *image                        
                       );

static void
write_raw_image_frames (FILE *fp, 
                        GpivImage *image
                        );


/*
 * Public functions
 */
GpivImage *
gpiv_read_image (FILE *fp
                 )
/*---------------------------------------------------------------------------*/
/**
 *     Reads image from fp, png formatted.
 */
{
    GpivImage *image = NULL;


    if (fp == NULL) fp = stdin;

    if ((image = gpiv_read_png_image (fp)) == NULL) {
        g_message ("gpiv_read_image: gpiv_read_png_image failed");
        return NULL;
    }


    return image;
}


GpivImage *
gpiv_fread_image (const gchar *fname
                  )
/* ----------------------------------------------------------------------------
 * checks filename extension on valid image / data name,
 * loads image header and data
 */
{
    GpivImage *image = NULL;

    char *err_msg = NULL;
    gchar *ext, *ext_ORG = NULL,
        *dirname, 
        *fname_base, 
        *fname_ext,
        *fname_org = g_strdup(fname);
    gchar command[2 * GPIV_MAX_CHARS];

    FILE *fp = NULL;
    gchar tmp_textfile[GPIV_MAX_CHARS];
/* BUGFIX: for the moment use the variable: cleanup_tmp_image */
    gboolean cleanup_tmp_image = TRUE;


    if (fname == NULL) {
        gpiv_warning ("gpiv_fread_image: failing \"fname == NULL\"");
        return NULL;
    }

    if (g_file_test (fname, G_FILE_TEST_EXISTS) == FALSE) {
        gpiv_warning ("gpiv_fread_image: file does not exist");
        return NULL;
    }
    
/*
 * Stripping file name to examine suffix
 *
 */
    ext = g_strdup(strrchr(fname, '.'));
    dirname = g_strdup(g_path_get_dirname(fname));
    fname_base = g_strdup(g_path_get_basename(fname));
    strtok(fname_base, ".");
    fname_ext = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S,
                                      fname_base, NULL));
#ifdef DEBUG
    g_message("gpiv_fread_image:: dirname = %s\n fname_base = %s\n fname_ext = %s", 
              dirname, fname_base, fname_ext);    
#endif /* DEBUG */

/*
 * Converting image from pgm, gif, tif, bmp, ppm format to png
 * Creating tempory text file to include as required comment in PNG image
 * for use in gpiv software
 */
    if (g_str_has_suffix (fname, GPIV_EXT_PGM_IMAGE)
        || g_str_has_suffix (fname, GPIV_EXT_PGM_IMAGE_UPCASE)) {
/*    if (strcmp(ext, GPIV_EXT_PGM_IMAGE) == 0) { */
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "pnmtopng < %s > %s%s", 
                    fname, fname_ext, GPIV_EXT_PNG_IMAGE);

        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }

        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);
        

    } else if (g_str_has_suffix (fname, ".gif")
               || g_str_has_suffix (fname, ".GIF")) {
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "giftopnm < %s  | pnmtopng > %s%s", 
                    fname, fname_ext, GPIV_EXT_PNG_IMAGE);
        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }
        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);
        

    } else if (g_str_has_suffix (fname, ".tif")
               || g_str_has_suffix (fname, ".TIF")) {
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "tifftopnm < %s  | pnmtopng > %s%s", 
                    fname, fname_ext, GPIV_EXT_PNG_IMAGE);

        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }

        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);
        

    } else if (g_str_has_suffix (fname, ".bmp")
               || g_str_has_suffix (fname, ".BMP")) {
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "bmptoppm < %s  | pnmtopng > %s%s", 
                   fname, fname_ext, GPIV_EXT_PNG_IMAGE);

        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }

        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);

    } else if (g_str_has_suffix (fname, ".ppm")
               || g_str_has_suffix (fname, ".PPM")) {
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "pnmtopng < %s > %s%s", 
                   fname, fname_ext, GPIV_EXT_PNG_IMAGE);

        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }

        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);
    }

/*
 * reading image data from raw, PNG, hdf or Davis(tm) format
 */
    if (strcmp (ext, GPIV_EXT_RAW_IMAGE) == 0 
        || strcmp (ext, GPIV_EXT_PNG_IMAGE) == 0
        || strcmp (ext, GPIV_EXT_PNG_IMAGE_UPCASE) == 0
        || strcmp (ext, GPIV_EXT_GPIV) == 0
        || strcmp (ext, GPIV_EXT_GPIV_UPCASE) == 0
        || strcmp (ext, GPIV_EXT_DAVIS) == 0
        || strcmp (ext, GPIV_EXT_DAVIS_UPCASE) == 0) {
        
#ifdef DEBUG
        g_message ("gpiv_fread_image::  my_utils_open_img");
#endif
        if ((image = my_utils_open_img (fname)) == NULL) {
            gpiv_warning ("gpiv_fread_image: failure open_img");
            return NULL;
        }

    } else {
        gpiv_warning ("gpiv_fread_image: image format not recognised");
        return NULL;
    }

/*
 * Remove original image if it has temporarly been converted to png
 */
    if (ext_ORG != NULL
        && (strcmp(ext_ORG, GPIV_EXT_PGM_IMAGE) == 0
            || strcmp(ext_ORG, ".pgm") == 0
            || strcmp(ext_ORG, ".PGM") == 0
            || strcmp(ext_ORG, ".gif") == 0
            || strcmp(ext_ORG, ".GIF") == 0
            || strcmp(ext_ORG, ".tif") == 0
            || strcmp(ext_ORG, ".TIF") == 0
            || strcmp(ext_ORG, ".bmp") == 0
            || strcmp(ext_ORG, ".BMP") == 0
            || strcmp(ext_ORG, ".ppm") == 0
            || strcmp(ext_ORG, ".PPM") == 0
            )) {

        if (cleanup_tmp_image) {
            g_snprintf (command, 2 * GPIV_MAX_CHARS, "rm %s%s", 
                        fname_ext, GPIV_EXT_PNG_IMAGE);
        } else {
            g_snprintf (command, 2 * GPIV_MAX_CHARS, "rm %s ", fname_org);
        }
        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }
    }


    g_free (ext_ORG);
    g_free (ext);
    g_free (dirname); 
    g_free (fname_base); 
    g_free (fname_ext);
    g_free (fname_org);

    return image;
}



GpivImage *
gpiv_read_png_image (FILE *fp
                     )
/*---------------------------------------------------------------------------*/
/**
 *     Reads png formatted image.
 *
 *     @param[in] fp           input file
 *     @return                 GpivImage
 */
/*---------------------------------------------------------------------------*/
{
    GpivImage *gpiv_image = NULL;
    GpivImagePar *gpiv_image_par = g_new0 (GpivImagePar, 1);

    char buf[PNG_BYTES_TO_CHECK];
    
    png_structp png_ptr;
    png_infop info_ptr, end_info_ptr;
    unsigned int sig_read = 0;
    png_uint_32 width, height;
    gint bit_depth, color_type, interlace_type, compression_type, 
        unit_type_offs, filter_method;

    gint row, column;
    png_int_32  x_offset = 0, y_offset = 0;
    double res_x = 0;
    gint linesize;
    png_byte **png_image = NULL;
    png_byte *png_rowpointer;

    
/*     @param[in] allocate     boolean whether to allocate memory for image arrays
 *     @param[in] x_corr__set  boolean if x_corr has been defined
 *     @param[in] x_corr       boolean whether image contains two frames for cross-correlation.
 *                             Will only be used if absent in header of input image and if 
 *                              x_corr__set = TRUE
 */

#ifdef PNG_tIME_SUPPORTED
    png_timep mod_time;
#endif /* PNG_tIME_SUPPORTED */

    gpiv_img_parameters_set (gpiv_image_par, FALSE);
#ifdef DEBUG
    g_message ("gpiv_read_png_image:: 00");
#endif

    /* Read in some of the signature bytes */
    if (fread (buf, 1, PNG_BYTES_TO_CHECK, fp) != PNG_BYTES_TO_CHECK) {
        g_warning ("gpiv_read_png_image: failing fread (buf, 1, PNG_BYTES_TO_CHECK, fp)");
        return NULL;
    }

    /* Testing the signature bytes */
    if (png_sig_cmp ((png_bytep)buf, (png_size_t)0, PNG_BYTES_TO_CHECK) != 0) {
        g_warning ("gpiv_read_png_image: failing signature bytes test");
        return NULL;
    } else {
        sig_read = 1;
    }
    
    png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING,
                                      png_voidp_NULL, png_error_ptr_NULL, 
                                      png_error_ptr_NULL);
    if (png_ptr == NULL) {
        g_warning ("gpiv_read_png_image: failing png_create_read_struct()");
        return NULL;
    }
   
    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL) {
        fclose (fp);
        png_destroy_read_struct (&png_ptr, png_infopp_NULL, png_infopp_NULL);
        g_warning ("gpiv_read_png_image: failing png_create_info_struct (png_ptr)");
        return NULL;
    }
    
    end_info_ptr = png_create_info_struct (png_ptr);
    
    if (setjmp (png_jmpbuf (png_ptr))) {
        /* Free all of the memory associated with the png_ptr and info_ptr */
        /* If we get here, we had a problem reading the file */
        png_destroy_read_struct (&png_ptr, &info_ptr, 
                                 /* NULL */ &end_info_ptr );
        g_warning ("gpiv_read_png_image: failing setjmp (png_jmpbuf (png_ptr)");
        return NULL;
    }

    png_init_io (png_ptr, fp);

    /* If we have already read some of the signature */
    if (sig_read == 1) {
        png_set_sig_bytes (png_ptr, PNG_BYTES_TO_CHECK);
    }
    
#ifdef HILEVEL
    png_read_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, png_voidp_NULL);
#else
    png_read_info (png_ptr, info_ptr);
#endif /* HILEVEL */


    /* Quering image info: */
    png_get_IHDR (png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
                  &interlace_type, &compression_type, &filter_method);

    /* Other metadata chunks: */
#ifdef PNG_oFFs_SUPPORTED
    png_get_oFFs (png_ptr, info_ptr, &x_offset, &y_offset, &unit_type_offs);
#endif /* PNG_oFFs_SUPPORTED */

#ifdef PNG_pCAL_SUPPORTED
    res_x = png_get_x_pixels_per_meter (png_ptr, info_ptr);
#endif /* PNG_pCAL_SUPPORTED */

#ifdef PNG_tIME_SUPPORTED
    png_get_tIME (png_ptr, info_ptr, &mod_time);
#endif /* PNG_tIME_SUPPORTED */


#ifdef PNG_TEXT_SUPPORTED
    get_imgpar_from_textchunks (gpiv_image_par, png_ptr, info_ptr);
#endif /* PNG_TEXT_SUPPORTED */

/*     if (gpiv_image_par.x_corr__set == FALSE) { */
/*         g_warning ("GPIV_READ_PNG_IMAGE: couldn't read or x_corr"); */
/*         return NULL; */
/*     } */

#ifdef DEBUG
    printf ("\n");
    printf ("Image info:\nwidth = %d height = %d depth = %d \n",
            (int)width,
            (int)height,
            (int)bit_depth
            );

    if (color_type == PNG_COLOR_TYPE_GRAY) {
        printf ("color_type = PNG_COLOR_TYPE_GRAY\n");
    } else if (color_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
        printf ("color_type = PNG_COLOR_TYPE_GRAY_ALPHA\n");
    } else if (color_type == PNG_COLOR_TYPE_PALETTE) {
        printf ("color_type = PNG_COLOR_TYPE_PALETTE\n");
    } else if (color_type == PNG_COLOR_TYPE_RGB) {
        printf ("color_type = PNG_COLOR_TYPE_RGB\n");
    } else if (color_type == PNG_COLOR_TYPE_RGB_ALPHA) {
        printf ("color_type = PNG_COLOR_TYPE_RGB_ALPHA\n");
    } else if (color_type == PNG_COLOR_MASK_PALETTE) {
        printf ("color_type = PNG_COLOR_MASK_PALETTE\n");
    } else if (color_type == PNG_COLOR_MASK_COLOR) {
        printf ("color_type = PNG_COLOR_MASK_COLOR\n");
    } else if (color_type == PNG_COLOR_MASK_ALPHA) {
        printf ("color_type = PNG_COLOR_MASK_ALPHA\n");
    } else {
        g_warning ("gpiv_read_png_image: no existing color type");
        return NULL;
    }

    if (compression_type == PNG_COMPRESSION_TYPE_DEFAULT) {
        printf ("compression_type = PNG_COMPRESSION_TYPE_DEFAULT\n");
    } else {
        g_warning ("gpiv_read_png_image: no existing compression type");
        return NULL;
   }
        
    if (interlace_type == PNG_INTERLACE_NONE) {
        printf ("interlace_type = PNG_INTERLACE_NONE\n");
    } else if (interlace_type == PNG_INTERLACE_ADAM7) {
        printf ("interlace_type = PNG_INTERLACE_ADAM7\n");
    } else {
        g_warning ("gpiv_read_png_image: no existing interlace type");
        return NULL;
    }
    

#ifdef PNG_oFFs_SUPPORTED
    printf ("x_offset = %d y_offset = %d\n", 
            (int)x_offset, (int)y_offset);
#endif /* PNG_oFFs_SUPPORTED */

#ifdef PNG_pCAL_SUPPORTED
    printf ("resolution (pixels/meter) res_x = %d \n", 
            (int)res_x);
#endif

#ifdef PNG_tIME_SUPPORTED
    printf ("Modification GMT time: (Y/M/D: h/m/s) %d/%d/%d: %d/%d/%d \n", 
            mod_time->year, mod_time->month, mod_time->day,
            mod_time->hour, mod_time->minute, mod_time->second
            );
#endif /* PNG_tIME_SUPPORTED */

    printf ("\n");
#endif /* DEBUG */


/*
 * If the image is of correct format GRAY and has no INTERLACE, apply header 
 * info to gpiv_image structure
 */
    gpiv_image_par->depth = bit_depth;
    gpiv_image_par->depth__set = TRUE;

    if (color_type != PNG_COLOR_TYPE_GRAY) {
        g_warning ("gpiv_read_png_image: image is not PNG_COLOR_TYPE_GRAY");
        return NULL;
    }

    if (interlace_type != PNG_INTERLACE_NONE) {
        g_warning ("gpiv_read_png_image: image is not PNG_INTERLACE_NONE");
        return NULL;
    }

    if (info_ptr->bit_depth == 16) {
        linesize = 2 * width;
    } else {
        linesize = width;
    }
    gpiv_image_par->ncolumns = width;
    gpiv_image_par->ncolumns__set = TRUE;


#ifdef GPIV_IMG_PARAM_RESOURCES
#ifdef DEBUG
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, gpiv_image_par, TRUE);
#else
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, gpiv_image_par, FALSE);
#endif /* DEBUG */
#endif /* GPIV_IMG_PARAM_RESOURCES */

    if (gpiv_image_par->x_corr__set == FALSE) {
        g_warning ("gpiv_read_png_image: couldn't read or x_corr");
        return NULL;
    }
    
    if (gpiv_image_par->x_corr) {
        gpiv_image_par->nrows = height / 2;
    } else {
        gpiv_image_par->nrows = height;
    }
    gpiv_image_par->nrows__set = TRUE;
    
    
    if (res_x != 0.0) {
        gpiv_image_par->s_scale = res_x;
        gpiv_image_par->s_scale__set = TRUE;
    } else {
        gpiv_image_par->s_scale__set = FALSE;
    }


    /*
     * Allocating (png) image
     */
    gpiv_image = gpiv_alloc_img (gpiv_image_par);
    
    if ((png_image = matrix_png_byte (height, linesize)) == NULL) {
        png_destroy_read_struct (&png_ptr, &info_ptr, &end_info_ptr);
        g_warning ("gpiv_read_png_image: couldn't allocate space for image");
        return NULL;
    }


    /*
     * Obtaining image frame data
     */
#ifdef HILEVEL
    png_image = png_get_rows (png_ptr, info_ptr);
#else
    png_read_image (png_ptr, png_image);
    png_read_end (png_ptr, info_ptr);
#endif /* HILEVEL */


    /*
     * Copy png image data to img1, img2
     */
    for (row = 0; row < gpiv_image->header->nrows; row++) {
#ifdef DEBUG2
        printf ("\ngpiv_read_png_image: IMG1  row = %d img_png: \n", row);
#endif /* DEBUG2 */
        png_rowpointer = png_image[row];
        for (column = 0; column < gpiv_image->header->ncolumns; column++) {
            gpiv_image->frame1[row][column] = 
                get_png_val (&png_rowpointer, bit_depth);
#ifdef DEBUG2
            printf ("%d ", (int) gpiv_image->frame1[row][column]);
#endif /* DEBUG2 */
        }
    }

    if (gpiv_image->header->x_corr) {
        for (row = 0; row < gpiv_image->header->nrows; row++) {
#ifdef DEBUG2
            printf ("\ngpiv_read_png_image: IMG2  row = %d img_png: \n", 
                    row);
#endif /* DEBUG2 */
            png_rowpointer = png_image[row + gpiv_image->header->nrows];
            for (column = 0; column < gpiv_image->header->ncolumns; 
                 column++) {
                gpiv_image->frame2[row][column] = 
                    get_png_val (&png_rowpointer, bit_depth);
#ifdef DEBUG2
                printf ("%d ", (int) gpiv_image->frame2[row][column]);
#endif /* DEBUG2 */
            }
        }
    }

    /*
     * clean up after the read, and free any memory allocated
     */
    free_matrix_png_byte (png_image);


    png_destroy_read_struct (&png_ptr, &info_ptr, &end_info_ptr);
    return gpiv_image;
}



gchar *
gpiv_fwrite_png_image (const gchar *fname,
                       GpivImage *gpiv_image,
                       const gboolean free
                       )
/*------------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes image data and header to png formatted image. 
 *     Frees allocated image data.
 *
 *--------------------------------------------------------------------------- */
{
    FILE *fp = NULL;
    gchar *err_msg = NULL;


    if (fname == NULL) {
        gpiv_warning ("gpiv_fwrite_png_image: failing \"fname == NULL\"");
        return "gpiv_fwrite_png_image: failing \"fname == NULL\"";
    }

    if ((fp = fopen (fname, "wb")) == NULL) {
        gpiv_warning ("LIBGPIV internal error: gpiv_fwrite_png_image: failure \
opening %s", fname);
        return "gpiv_fwrite_png_image: failing fopen";
    }

    if ((err_msg = gpiv_write_png_image (fp, gpiv_image, free)) != NULL) {
        fclose (fp);
        return err_msg;
    }


    fclose (fp);
    return NULL;
}



gchar *
gpiv_write_png_image (FILE *fp,
                      GpivImage *gpiv_image,
                      const gboolean free
                      )
/*------------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes image data and header to png formatted image. 
 *     Frees allocated image data.
 *
 *--------------------------------------------------------------------------- */
{
    gchar *err_msg = NULL;
/*     guint16 **img1, **img2; */

    png_structp png_ptr;
    png_infop info_ptr;
    png_uint_32 width = gpiv_image->header->ncolumns, height;
    gint bit_depth = gpiv_image->header->depth;
    gint  color_type = PNG_COLOR_TYPE_GRAY;
    gint interlace_type = PNG_INTERLACE_NONE;
    gint compression_type  = PNG_COMPRESSION_TYPE_BASE;
    gint filter_method = PNG_FILTER_TYPE_BASE;
    gint linesize = 2 * width;

#ifdef PNG_tIME_SUPPORTED
    time_t ltime_t;
    png_time mod_time;
#endif /* PNG_tIME_SUPPORTED */
    gint i = 0, j = 0;


    if (gpiv_image->frame1 == NULL) {
        err_msg = "gpiv_write_png_image: gpiv_image->frame1 == NULL";
        return (err_msg);
    }
    if (gpiv_image->header->x_corr
        && gpiv_image->frame2 == NULL) {
        err_msg = "gpiv_write_png_image: gpiv_image->frame2 == NULL";
        return (err_msg);
    }

    /*
     * Allocate png_ structures
     */
    png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING,
                                       png_voidp_NULL, png_error_ptr_NULL, 
                                       png_error_ptr_NULL);
    if (png_ptr == NULL) {
        err_msg = "gpiv_write_png_image: failing png_create_write_struct()";
        return err_msg;
    }
   
    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL) {
        png_destroy_write_struct (&png_ptr, &info_ptr);
        err_msg = "gpiv_write_png_image: failing png_create_info_struct (png_ptr)";
        return err_msg;
    }

    if (setjmp (png_jmpbuf (png_ptr))) {
        png_destroy_write_struct (&png_ptr, &info_ptr);
        err_msg = "gpiv_write_png_image: failing at setjmp(png_jmpbuf (png_ptr)";
        return err_msg;
    }


    png_init_io (png_ptr, fp);

    /*
     * Apply gpiv_image->header to png header
     */
    if (gpiv_image->header->x_corr) {
        height = 2 * gpiv_image->header->nrows;
    } else {
        height = gpiv_image->header->nrows;
    }

    png_set_compression_level (png_ptr, Z_BEST_COMPRESSION);
    png_set_IHDR (png_ptr, info_ptr, width, height, bit_depth, color_type,
                  interlace_type, compression_type, filter_method);


#ifdef PNG_pCAL_SUPPORTED
    if (gpiv_image->header->s_scale__set) {
        png_set_sCAL (png_ptr, info_ptr, PNG_SCALE_METER,
                      (png_int_32) gpiv_image->header->s_scale * width, 
                      (png_int_32) gpiv_image->header->s_scale * height);
    }
#endif /* PNG_pCAL_SUPPORTED */

#ifdef PNG_oFFs_SUPPORTED
    if (gpiv_image->header->z_off_x__set 
        && gpiv_image->header->z_off_y__set) {
        /* / gpiv_image->header.sscale * 1.0e3 */ /* x_offset*/ 
        /* / gpiv_image->header.sscale * 1.0e3 */ /* y_offset*/ 
        png_set_oFFs (png_ptr, info_ptr, 
                      (png_uint_32) gpiv_image->header->z_off_x,
                      (png_uint_32) gpiv_image->header->z_off_y, 
                      PNG_OFFSET_PIXEL);
    }
#endif /* PNG_oFFs_SUPPORTED */


#ifdef PNG_tIME_SUPPORTED
    ltime_t = time (&ltime_t);
    png_convert_from_time_t (&mod_time, ltime_t);
    png_set_tIME (png_ptr, info_ptr, &mod_time);
#endif /* PNG_tIME_SUPPORTED */


#ifdef PNG_TEXT_SUPPORTED
    set_textchunks_from_imgpar (gpiv_image->header, png_ptr, info_ptr);
#endif /* PNG_TEXT_SUPPORTED */

    png_write_info (png_ptr, info_ptr);

    /* 
     * Write image to png struct
     */
    if (info_ptr->color_type == PNG_COLOR_TYPE_GRAY) {
#ifdef WRITE_IMAGEDATA_LINE
        png_byte *line;
#else
        png_byte **png_image;
#endif /* WRITE_IMAGEDATA_LINE */
        unsigned long p_png; /* (pi)xel */
        png_byte *pp;

#ifdef WRITE_IMAGEDATA_LINE
        if ((line = (png_byte *) g_malloc (linesize)) == NULL) {
            png_destroy_write_struct (&png_ptr, &info_ptr);
            err_msg = "gpiv_write_png_image: out of memory allocating PNG row buffer";
            return (err_msg);
        }
#else
        if ((png_image = matrix_png_byte (gpiv_image->header->nrows, linesize))
            == NULL) {
            png_destroy_write_struct (&png_ptr, &info_ptr);
            err_msg = "gpiv_write_png_image: out of memory allocating PNG row buffer";
            return (err_msg);
        }
#endif /* WRITE_IMAGEDATA_LINE */

 
        for (i = 0; i < gpiv_image->header->nrows; i++) {
#ifdef WRITE_IMAGEDATA_LINE
            pp = line;
#else
            pp = png_image[i];
#endif /* WRITE_IMAGEDATA_LINE */
            for (j = 0; j < width; j++) {
                p_png = gpiv_image->frame1[i][j];
                if (bit_depth == 16) *pp++ = PNM_GET1 (p_png) >> 8;
                *pp++ = PNM_GET1 (p_png) & 0xff;
            }
#ifdef WRITE_IMAGEDATA_LINE
            png_write_row (png_ptr, line);
#endif /* WRITE_IMAGEDATA_LINE */
        }
#ifndef WRITE_IMAGEDATA_LINE
        png_write_rows (png_ptr, png_image, gpiv_image->header->nrows);
#endif /* WRITE_IMAGEDATA_LINE */


        if (gpiv_image->header->x_corr) {
            for (i = 0; i < gpiv_image->header->nrows; i++) {
#ifdef WRITE_IMAGEDATA_LINE
                pp = line;
#else
                pp = png_image[i];
#endif /* WRITE_IMAGEDATA_LINE */
                for (j = 0; j < width; j++) {
                    p_png = gpiv_image->frame2[i][j];
                    if (bit_depth == 16) *pp++ = PNM_GET1 (p_png) >> 8;
                    *pp++ = PNM_GET1 (p_png) & 0xff;
                }
#ifdef WRITE_IMAGEDATA_LINE
                png_write_row (png_ptr, line);
#endif /* WRITE_IMAGEDATA_LINE */
            }
#ifndef WRITE_IMAGEDATA_LINE
            png_write_rows (png_ptr, png_image, gpiv_image->header->nrows);
#endif /* WRITE_IMAGEDATA_LINE */
        }


#ifdef WRITE_IMAGEDATA_LINE
        g_free (line);
#else
        free_matrix_png_byte (png_image);
#endif /* WRITE_IMAGEDATA_LINE */

    } else {
        png_destroy_write_struct (&png_ptr, &info_ptr);
        err_msg = "gpiv_write_png_image: color_type is not PNG_COLOR_TYPE_GRAY";
        return (err_msg);
    }

    png_write_end (png_ptr, info_ptr);
    fflush (fp);

    /*
     * Freeing image memory
     */
    if (free == TRUE) {
        gpiv_free_img (gpiv_image);
    }


    png_destroy_write_struct (&png_ptr, &info_ptr);
    return err_msg;
}

/*
 * Local functions
 */
static png_uint_16 
get_png_val (png_byte **pp, 
	     int bit_depth
	     )
/*-----------------------------------------------------------------------------
 */
{
    png_uint_16 c = 0;


    if (bit_depth == 16) {
        c = (*((*pp)++)) << 8;
    }
    c |= (*((*pp)++));

    /*   if (maxval > maxmaxval) */
    /*     c /= ((maxval + 1) / (maxmaxval + 1)); */


    return c;
}



static png_byte **
matrix_png_byte (long nr, 
                 long nc
                 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      allocate a png_byte matrix with subscript range m[0..nr][0..nc]
 *
 * INPUTS:
 *      nr:             number of rows
 *      nc:             number of columns
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      matrix
 *---------------------------------------------------------------------------*/
{
    long i;
    png_byte **m;


    m = (png_byte **) g_malloc0 (nr * sizeof (png_byte*));
    m[0] = (png_byte *) g_malloc0 (nr * nc * sizeof (png_byte));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



static void 
free_matrix_png_byte (png_byte **m
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      free a png_byte matrix allocated by matrix_png_byte
 *
 * INPUTS:
 *      m:	        matrix 
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL);
    g_free (m[0]);
    g_free (m);
    m = NULL;
}



static void
get_imgpar_from_textchunks (GpivImagePar *image_par, 
                            png_structp png_ptr,
                            png_infop info_ptr
                            )
/*------------------------------------------------------------------------------
 * DESCRIPTION:
 *      Obtains image paramameters from text chunks in png image
 *
 * INPUTS:
 *      png_ptr:	        png structure
 *     info_ptr:                png info structure
 *
 * OUTPUTS:
 *    image_par:               image parameter structure
 *
 * RETURNS:
 *--------------------------------------------------------------------------- */
{
    png_textp text;
    gint num_text = 0, i = 0;
    png_uint_32 num_comments = 0;


    num_comments = png_get_text (png_ptr, info_ptr, &text, &num_text);

    for (i = 0; i < num_comments; i++) {
        if (strcmp (text[i].key, PNG_KEY__PIV_XCORR) == 0) {
            sscanf (text[i].text, "%d", &image_par->x_corr);
            image_par->x_corr__set = TRUE;
        }
        
        if (strcmp (text[i].key, PNG_KEY__PIV_DT) == 0) {
            sscanf (text[i].text, "%f", &image_par->t_scale);
            image_par->t_scale__set = TRUE;
        }


        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__TITLE) == 0) {
            g_snprintf (image_par->title, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->title__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__AUTHOR) == 0) {
            g_snprintf (image_par->author, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->author__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__USERTEXT) == 0) {
            g_snprintf (image_par->usertext, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->usertext__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__SOFTWARE) == 0) {
            g_snprintf (image_par->software, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->software__set = TRUE;
        }
        
        if (strcmp (text[i].key,GPIV_IMGPAR_KEY__DISCLAIMER ) == 0) {
            g_snprintf (image_par->disclaimer, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->disclaimer__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__WARNING) == 0) {
            g_snprintf (image_par->warning, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->warning__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__SOURCE) == 0) {
            g_snprintf (image_par->source, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->source__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__COMMENT) == 0) {
            g_snprintf (image_par->comment, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->comment__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__CREATION_DATE) == 0) {
            g_snprintf (image_par->creation_date, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->creation_date__set = TRUE;
        }
        
        if (strcmp (text[i].key,  GPIV_IMGPAR_KEY__COPYRIGHT) == 0) {
            g_snprintf (image_par->copyright, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->copyright__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__EMAIL) == 0) {
            g_snprintf (image_par->email, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->email__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__URL) == 0) {
            g_snprintf (image_par->url, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->url__set = TRUE;
        }

    }
    
#ifdef DEBUG
    printf ("num_comments = %d\n", (int) num_comments);
    for (i = 0; i < num_comments; i++) {
        printf ("key[%d] = %s: text[%d] = %s\n", 
                i, text[i].key, i, text[i].text);
    }
#endif /* DEBUG */

}




static void
set_textchunks_from_imgpar (GpivImagePar *image_par, 
                            png_structp png_ptr, 
                            png_infop info_ptr
                            )
/*------------------------------------------------------------------------------
 * DESCRIPTION:
 *      Set text chunks for png image from image paramameters
 *
 * INPUTS:
 *    image_par:               image parameter structure
 *
 * OUTPUTS:
 *      png_ptr:	        png structure
 *     info_ptr:                png info structure
 *
 * RETURNS:
 *--------------------------------------------------------------------------- */
{
    gint num_text = 0;
    png_text text[14];


    /*
     * Additional header text, needed for (G)PIV
     */
    if (image_par->x_corr__set == TRUE) {
        text[num_text].key = PNG_KEY__PIV_XCORR;
        text[num_text].text = g_strdup_printf ("%d", image_par->x_corr);
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[0].lang = NULL;
#endif

   if (image_par->t_scale__set == TRUE) {
        text[num_text].key = PNG_KEY__PIV_DT;
        text[num_text].text = g_strdup_printf ("%f", image_par->t_scale);
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[1].lang = NULL;
#endif

    /*
     * non required header info
     * PNG recognized header text
     */
    if (image_par->title__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__TITLE;
        text[num_text].text = image_par->title;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[2].lang = NULL;
#endif

    if (image_par->author__set == TRUE) {
        text[num_text].key =  GPIV_IMGPAR_KEY__AUTHOR;
        text[num_text].text = image_par->author; 
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[3].lang = NULL;
#endif

    if (image_par->usertext__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__USERTEXT;
        text[num_text].text = image_par->usertext;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[4].lang = NULL;
#endif

    if (image_par->software__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__SOFTWARE;
        text[num_text].text = image_par->software;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[5].lang = NULL;
#endif

    if (image_par->disclaimer__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__DISCLAIMER;
        text[num_text].text = image_par->disclaimer;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[6].lang = NULL;
#endif

    if (image_par->warning__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__WARNING;
        text[num_text].text = image_par->warning;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
#ifdef PNG_iTXt_SUPPORTED
    text[7].lang = NULL;
#endif
    }

    if (image_par->source__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__SOURCE;
        text[num_text].text = image_par->source;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[8].lang = NULL;
#endif

    if (image_par->comment__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__COMMENT;
        text[num_text].text = image_par->comment;
        text[num_text].compression =  PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[9].lang = NULL;
#endif

    if (image_par->creation_date__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__CREATION_DATE;
        text[num_text].text = image_par->creation_date;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[10].lang = NULL;
#endif


    if (image_par->copyright__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__COPYRIGHT;
        text[num_text].text = image_par->copyright;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[11].lang = NULL;
#endif

    if (image_par->email__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__EMAIL;
        text[num_text].text = image_par->email;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[12].lang = NULL;
#endif
    
    if (image_par->url__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__URL;
        text[num_text].text = image_par->url;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[13].lang = NULL;
#endif


    png_set_text (png_ptr, info_ptr,  text, num_text);
}

#ifdef DEBUG
#undef DEBUG
#endif

#ifdef DEBUG2
#undef DEBUG2
#endif

GpivImage *
gpiv_fread_hdf5_image (const gchar *fname
                       )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Opens an image from hdf file
 *     Expects the arrray(s) have not been allocated yet
 *
 *--------------------------------------------------------------------------- */
{
    GpivImage *gpiv_image = NULL;
    GpivImagePar *gpiv_image_par = g_new0 (GpivImagePar, 1);
    gchar *err_msg = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;

/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;



    if (fname == NULL) {
        gpiv_warning ("gpiv_fread_hdf5_image: failing \"fname == NULL\"");
        return NULL;
    }

    if ((i = H5Fis_hdf5 (fname)) == 0)  {
        gpiv_warning ("gpiv_fread_hdf5_image: not an hdf5 file");
        return NULL;
    }


    gpiv_img_parameters_set (gpiv_image_par, FALSE);
    if ((gpiv_image_par = gpiv_img_fread_hdf5_parameters (fname))
        == NULL) {
        gpiv_warning ("gpiv_fread_hdf5_image: failing gpiv_img_fread_hdf5_parameters"); 
        return NULL;
    }

#ifdef GPIV_IMG_PARAM_RESOURCES
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, &gpiv_image_par, TRUE);
#endif
    if ((err_msg = 
         gpiv_img_check_header_required_read (gpiv_image_par)) 
        != NULL) {
        gpiv_warning (err_msg); 
        return NULL;
    }
    
    gpiv_image = gpiv_alloc_img (gpiv_image_par);
    
    
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "IMAGE", H5P_DEFAULT);

/*
 * image #1
 */

    dataset_id = H5Dopen (group_id, "#1", H5P_DEFAULT);
    dataspace_id = H5Dget_space (dataset_id);
#ifdef HD5_IMAGE_INT
    if ((status = H5Dread (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                          H5P_DEFAULT, gpiv_image->frame1[0])) < 0) {
        gpiv_warning ("gpiv_fread_hdf5_image: failing H5Dread");
        return NULL;
    }
#else
    if ((status = H5Dread(dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL, 
                          H5P_DEFAULT, gpiv_image->frame1[0])) < 0) {
        gpiv_warning ("gpiv_fread_hdf5_image: failing H5Dread");
        return NULL;
    }
#endif
/*     msg_error = "H5Dread: no image data";
 *     GpivIo
 *     gpiv_io
 */
    status = H5Sclose(dataspace_id);
    status = H5Dclose(dataset_id);

/*
 * image #2
 */
    if (gpiv_image->header->x_corr) {
        dataset_id = H5Dopen(group_id, "#2", H5P_DEFAULT);
        dataspace_id = H5Dget_space(dataset_id);
#ifdef HD5_IMAGE_INT
        status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                         H5P_DEFAULT, gpiv_image->frame2[0]);
#else
        status = H5Dread(dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL, 
                         H5P_DEFAULT, gpiv_image->frame2[0]);
#endif
        status = H5Sclose(dataspace_id);
        status = H5Dclose(dataset_id);
    } else {
	gpiv_image->frame2 = gpiv_image->frame1;
    }



    H5Gclose (group_id);
    status = H5Fclose(file_id);

    return gpiv_image;
}


gchar *
gpiv_fwrite_hdf5_image (const gchar *fname, 
                        GpivImage *image,
                        gboolean free
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Writes IMAGE data to file in hdf version 5 format
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j, k, line_nr=0, image_white_is_zero = 0;
    gfloat *point_x_hdf = NULL;
/*
 * HDF file, dataset,-space identifier
 */
    hid_t       file_id, group_id, dataset_id, dataspace_id, 
        attribute_id, atype; 
    hsize_t     dims[2];
    herr_t      status;


    g_return_val_if_fail (image->frame1[0] != NULL, "frame1[0] != NULL");
    if (image->header->x_corr) g_return_val_if_fail (image->frame2[0] != NULL, "frame2[0] != NULL");



    if ((err_msg = gpiv_img_fwrite_hdf5_parameters (fname, image->header))
        != NULL) {
        return err_msg;
    }


    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_fwrite_hdf5_image: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "IMAGE", H5P_DEFAULT);
/*     H5Gset_comment (group_id, ".", RCSID); */

/*
 * Hdf required image specifications
 */
    dims[0] = 1;

    dataspace_id = H5Screate(H5S_SCALAR);
    atype = H5Tcopy(H5T_C_S1);
    H5Tset_size(atype, 5);
    attribute_id = H5Acreate(group_id, "CLASS", atype, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Awrite(attribute_id, atype, 
                      "IMAGE"); 
    status = H5Aclose(attribute_id); 
    status = H5Sclose(dataspace_id);


    dataspace_id = H5Screate(H5S_SCALAR);
    atype = H5Tcopy(H5T_C_S1);
    H5Tset_size(atype, 15);
    attribute_id = H5Acreate(group_id, "SUBCLASS", atype, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Awrite(attribute_id, atype, 
                      "IMAGE_GRAYSCALE"); 
    status = H5Aclose(attribute_id); 
    status = H5Sclose(dataspace_id);


    dataspace_id = H5Screate_simple(1, dims, NULL);
    attribute_id = H5Acreate(group_id, "IMAGE_WHITE_IS_ZERO", 
                             H5T_NATIVE_USHORT, dataspace_id, 
                             H5P_DEFAULT, H5P_DEFAULT);
    status = H5Awrite(attribute_id, H5T_NATIVE_USHORT, &image_white_is_zero); 
    status = H5Aclose(attribute_id); 
    status = H5Sclose(dataspace_id);


    dataspace_id = H5Screate(H5S_SCALAR);
    atype = H5Tcopy(H5T_C_S1);
    H5Tset_size(atype, 3);
    attribute_id = H5Acreate(group_id, "IMAGE_VERSION", atype, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Awrite(attribute_id, atype, 
                      "1.2"); 
    status = H5Aclose(attribute_id); 
    status = H5Sclose(dataspace_id);


 /*
 * Create the data space.
 */
   dims[0] = image->header->ncolumns; 
   dims[1] = image->header->nrows;
   dataspace_id = H5Screate_simple(2, dims, NULL);


/* 
 * image #1
 */
#ifdef HD5_IMAGE_INT
   dataset_id = H5Dcreate (group_id, "#1", H5T_NATIVE_INT, 
			  dataspace_id, H5P_DEFAULT);
   status = H5Dwrite (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT, image->frame1[0][0]);
#else
/*    dataset_id = H5Dcreate (group_id, "#1", H5T_NATIVE_USHORT,  */
/* 			  dataspace_id, H5P_DEFAULT); */
/*    status = H5Dwrite (dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL,  */
/* 		     H5P_DEFAULT, image->frame1[0][0]); */
#endif
   status = H5Dclose (dataset_id);


/* 
 * image #2
 */
   if (image->header->x_corr) {
#ifdef HD5_IMAGE_INT
       dataset_id = H5Dcreate(group_id, "#2", H5T_NATIVE_INT, 
                              dataspace_id, H5P_DEFAULT);
       status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                         H5P_DEFAULT, &image->frame2[0][0]);
#else
/*        dataset_id = H5Dcreate(group_id, "#2", H5T_NATIVE_USHORT,  */
/*                               dataspace_id, H5P_DEFAULT); */
/*        status = H5Dwrite(dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL,  */
/*                          H5P_DEFAULT, &image->frame2[0][0]); */
#endif
       status = H5Dclose(dataset_id);
   }


   status = H5Sclose(dataspace_id);
   status = H5Gclose (group_id);
   status = H5Fclose(file_id);
   if (free == TRUE) gpiv_free_img (image);
   return err_msg;
}



GpivImage *
gpiv_read_davis_image (FILE *fp
                       ) 
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Reads Davis formatted image, with ext .IMG from file
 *
 * PROTOTYPE LOCATATION:
 *     io.h
 *
 * INPUTS:
 *     fp:             pointer to input file
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    GpivImage *gpiv_image = NULL;
    GpivImagePar *gpiv_image_par = g_new0 (GpivImagePar, 1);
    gchar *err_msg = NULL;
    

/*
 * Obtaining image dimensions
 */
    fseek (fp, 10, SEEK_SET);
    fread (&gpiv_image_par->nrows, sizeof (guint16), 1, fp);
    fread (&gpiv_image_par->ncolumns, sizeof (guint16), 1, fp);

/*
 * Obtaining other requitered parameters, like depth and x_corr.
 * These will definitely no be present in the image header.
 * So searching unconditionally.
 */
#ifdef DEBUG
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, &gpiv_image_par, TRUE);
#else
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, &gpiv_image_par, FALSE);
#endif /* DEBUG */

    if ((err_msg = gpiv_img_check_header_required_read (gpiv_image_par)) 
        != NULL) {
        gpiv_warning ("gpiv_read_davis_image: %s", err_msg);
        return NULL;
    }

/*
 * Reading image data
 */
    gpiv_image = gpiv_alloc_img (gpiv_image_par);
    fseek (fp, 256, SEEK_SET);
    fread (gpiv_image->frame1[0], sizeof (guint16) 
           * gpiv_image->header->ncolumns 
           * gpiv_image->header->nrows, 1, fp);
    fread (gpiv_image->frame2[0], sizeof (guint16)
           * gpiv_image->header->ncolumns 
           * gpiv_image->header->nrows, 1, fp);

    
    return gpiv_image;
}


GpivImage * 
gpiv_read_raw_image (FILE *fp 
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Read raw image data from fp. 
 *      Frames memory will be allocated here.
 *
 * INPUTS:
 *      fname:          pointer to complete filename to be written
 *
 * RETURNS:
 *     GpivImage on success or NULL on failure
 *---------------------------------------------------------------------------*/
{
    GpivImage *image = NULL;
    GpivImagePar *image_par = g_new0 (GpivImagePar, 1);
    gint i, j;
    gchar *err_msg = NULL;

/*
 * Read header info. If required parameters are absent, scan from resource 
 * files, check again if present, else bail out.
 */
    gpiv_img_parameters_set (image_par, FALSE);
    gpiv_img_read_header (fp, image_par, FALSE, FALSE, NULL);

#ifdef GPIV_IMG_PARAM_RESOURCES
    if ((err_msg = gpiv_img_check_header_required_read (image_par))
        != NULL) {
#ifdef DEBUG
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, TRUE);
#else
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, FALSE);
#endif /* DEBUG */
    }
#endif /* GPIV_IMG_PARAM_RESOURCES */


    if ((err_msg = gpiv_img_check_header_required_read (image_par)) 
        != NULL) {
        gpiv_warning ("%s", err_msg);
        return NULL;
    }

/*
 * From header info memory allocation of image arrays can be done
 */
    image = gpiv_alloc_img (image_par);
    read_raw_image_frames (fp, image);


    return image;
}


GpivImage *
gpiv_fread_raw_image (const gchar *fname
                      ) 
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads an image from raw binary file.
 *     Expects the arrray(s) have not been allocated yet
 *     Reads image header data from file.h
 *
 * INPUTS:
 *     fname:          file name
 *
 * OUTPUTS:
 *     image1_p:       pointer to first 2-dimensional image array
 *     image1_p:       pointer to second 2-dimensional image array
 *     image_par:      pointer to image parameter structure
 *
 * RETURNS:
 *     GpivImage on success or NULL on failure
 *--------------------------------------------------------------------------- */
{
    GpivImage *image = NULL;
    GpivImagePar *image_par = g_new0 (GpivImagePar, 1);
    gchar *err_msg = NULL;
    gchar fname_header[GPIV_MAX_CHARS];
    gchar *dirname, *fname_base, *fname_nosuffix;
    FILE *fp;


    if (fname == NULL) {
        gpiv_warning ("gpiv_fread_raw_image: failing \"fname == NULL\"");
        return NULL;
    }

/*     image->header = image_par; */
    gpiv_img_parameters_set (image_par, FALSE);

/*
 * Stripping fname and create header filename
 */
    dirname = g_strdup (g_path_get_dirname (fname));
    fname_base = g_strdup (g_path_get_basename (fname));
    strtok (fname_base, ".");
    fname_nosuffix = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S,
                                            fname_base, NULL));
#ifdef DEBUG
    g_message ("gpiv_fread_raw_image: dirname = %s\n fname_base = %s\n fname_nosuffix = %s", 
              dirname, fname_base, fname_nosuffix);    
#endif /* DEBUG */

    g_snprintf(fname_header, GPIV_MAX_CHARS, "%s%s", 
               fname_nosuffix, GPIV_EXT_HEADER);

#ifdef DEBUG
    g_message ("gpiv_fread_raw_image: image header is: %s", fname_header);
#endif /* DEBUG */

/*
 * Read header info. If required parameters are absent, scan from resource files,
 * check again if present, else bail out.
 */
    if ((fp = fopen (fname_header, "rb")) == NULL) {
        gpiv_warning ("gpiv_fread_raw_image: failure opening %s for input", fname_header);
        return NULL;
    }
    gpiv_img_read_header (fp, image_par, FALSE, FALSE, NULL);
    fclose (fp);


#ifdef GPIV_IMG_PARAM_RESOURCES
    if ((err_msg = gpiv_img_check_header_required_read (image_par))
        != NULL) {
#ifdef DEBUG
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, TRUE);
#else
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, FALSE);
#endif /* DEBUG */
    }
#endif /* GPIV_IMG_PARAM_RESOURCES */


    if ((err_msg = gpiv_img_check_header_required_read (image_par)) 
        != NULL) {
        gpiv_warning ("%s", err_msg);
        return NULL;
    }


/*
 * reads image data from file in binary format
 */
    if ((fp = fopen (fname, "rb")) == NULL) {
	err_msg = "gpiv_fread_raw_image: failure opening for input";
        gpiv_warning ("%s", err_msg);
        return NULL;
    }

    image = gpiv_alloc_img (image_par);
    read_raw_image_frames (fp, image);
    fclose (fp);


    g_free(dirname);
    g_free(fname_base);
    g_free(fname_nosuffix);
    return image;
}


gchar *
gpiv_write_raw_image (FILE *fp, 
                      GpivImage *image
                      )
/*---------------------------------------------------------------------------
 *     Writes raw binary image to fp
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    

    gpiv_img_print_header (fp, image->header);
    write_raw_image_frames (fp, image);
    
    
    return err_msg;
}


gchar *
gpiv_fwrite_raw_image (const gchar *fname,
                       GpivImage *image
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writess binary image to file fname.r and header to fname.h
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar fname_header[GPIV_MAX_CHARS];
    gchar *dirname, *fname_base, *fname_nosuffix;
    FILE *fp;


    if (fname == NULL) {
        gpiv_warning ("gpiv_fwrite_raw_image: failing \"fname == NULL\"");
        return NULL;
    }

    g_return_if_fail (image->frame1[0] != NULL);
    g_return_if_fail (image->frame2[0] != NULL);



/*
 * Stripping file name, create header filename and print header info
 */
    dirname = g_strdup (g_path_get_dirname (fname));
    fname_base = g_strdup (g_path_get_basename (fname));
    strtok (fname_base, ".");
    fname_nosuffix = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S,
                                            fname_base, NULL));
    g_snprintf(fname_header, GPIV_MAX_CHARS, "%s%s", 
               fname_nosuffix, GPIV_EXT_HEADER);

    if ((fp = fopen (fname_header, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_raw_image: failure opening %s for output", 
                      fname_header);
        return "PIV_FWRITE_RAW_IMAGE: failure opening header for output";
    }

    gpiv_img_print_header (fp, image->header);
    fclose (fp);

/*
 * Write image frames
 */
    if ((fp = fopen (fname, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_raw_image: failure opening %s for output", 
                      fname);
        return "gpiv_fwrite_raw_image: failure opening image for output";
    }

    write_raw_image_frames (fp, image);
    fclose (fp);


    return err_msg;
}



/*
 * Local functions
 */

static void
print_img_parameters_set (GpivImagePar *gpiv_image_par)
{
    printf ("ncolumns__set = %d \n", gpiv_image_par->ncolumns__set);
    printf ("nrows__set = %d \n", gpiv_image_par->nrows__set);
    printf ("x_corr__set = %d \n", gpiv_image_par->x_corr__set);
    printf ("depth__set = %d \n", gpiv_image_par->depth__set);
    printf ("s_scale__set = %d \n", gpiv_image_par->s_scale__set);
    printf ("t_scale__set = %d \n", gpiv_image_par->t_scale__set);
    printf ("z_off_x__set = %d \n", gpiv_image_par->z_off_x__set);
    printf ("z_off_y__set = %d \n", gpiv_image_par->z_off_y__set);
    printf ("title__set = %d \n", gpiv_image_par->title__set);
    printf ("creation_date__set = %d \n", gpiv_image_par->creation_date__set);
    printf ("location__set = %d \n", gpiv_image_par->location__set);
    printf ("author__set = %d \n", gpiv_image_par->author__set);
    printf ("software__set = %d \n", gpiv_image_par->software__set);
    printf ("source__set = %d \n", gpiv_image_par->source__set);
    printf ("usertext__set = %d \n", gpiv_image_par->usertext__set);
    printf ("warning__set = %d \n", gpiv_image_par->warning__set);
    printf ("disclaimer__set = %d \n", gpiv_image_par->disclaimer__set);
    printf ("comment__set = %d \n", gpiv_image_par->comment__set);
    printf ("copyright__set = %d \n", gpiv_image_par->copyright__set);
    printf ("email__set = %d \n", gpiv_image_par->email__set);
    printf ("url__set = %d \n", gpiv_image_par->url__set);
}


static void
read_raw_image_frames (FILE *fp, 
                       GpivImage *image
                       )
/*-----------------------------------------------------------------------------
 *      Read raw image data frames from fp. 
 */
{
    guint8 **lo_img1, **lo_img2;
    gint i, j;


    if (fp == stdin || fp == NULL) {
        if (image->header->depth <= 8) {
            lo_img1 = gpiv_matrix_guint8 (image->header->nrows, 
                                          image->header->ncolumns);
            read (0, lo_img1[0], image->header->ncolumns 
                   * image->header->nrows * (sizeof(guint8)));
                  
            if (image->header->x_corr) {
                lo_img2 = gpiv_matrix_guint8(image->header->nrows, 
                                             image->header->ncolumns);
                read (0, lo_img2[0], image->header->ncolumns * 
                       image->header->nrows * (sizeof(guint8)));
            } else {
                lo_img2 = lo_img1;
            }
            
            
            for (i = 0; i < image->header->nrows; i++) {
                for (j = 0; j < image->header->ncolumns; j++) {
                    image->frame1[i][j] = lo_img1[i][j];
                    image->frame2[i][j] = lo_img2[i][j];
                }
            }
            
            gpiv_free_matrix_guint8 (lo_img1);
            if (image->header->x_corr) {
                gpiv_free_matrix_guint8 (lo_img2);
            }
            
        } else {
            read (0, image->frame1[0], image->header->ncolumns 
                   * image->header->nrows 
                   * (sizeof(guint16)));
            
            if (image->header->x_corr) {
                read (0, image->frame2[0], image->header->ncolumns 
                       * image->header->nrows 
                       * (sizeof(guint16)));
            } else {
                image->frame2 = image->frame1;
            }
        
        }


    } else {
        if (image->header->depth <= 8) {
            lo_img1 = gpiv_matrix_guint8 (image->header->nrows, 
                                          image->header->ncolumns);
            fread (lo_img1[0], image->header->ncolumns 
                   * image->header->nrows * (sizeof(guint8)), 1, fp);
            
            if (image->header->x_corr) {
                lo_img2 = gpiv_matrix_guint8(image->header->nrows, 
                                             image->header->ncolumns);
                fread (lo_img2[0], image->header->ncolumns * 
                       image->header->nrows * (sizeof(guint8)), 1, fp);
            } else {
                lo_img2 = lo_img1;
            }
            
            
            for (i = 0; i < image->header->nrows; i++) {
                for (j = 0; j < image->header->ncolumns; j++) {
                    image->frame1[i][j] = lo_img1[i][j];
                    image->frame2[i][j] = lo_img2[i][j];
                }
            }
            
            gpiv_free_matrix_guint8 (lo_img1);
            if (image->header->x_corr) {
                gpiv_free_matrix_guint8 (lo_img2);
            }
            
            
        } else {
            fread (image->frame1[0], image->header->ncolumns 
                   * image->header->nrows 
                   * (sizeof(guint16)), 1, fp);
            
            if (image->header->x_corr) {
                fread (image->frame2[0], image->header->ncolumns 
                       * image->header->nrows 
                       * (sizeof(guint16)), 1, fp);
            } else {
                image->frame2 = image->frame1;
            }
        }
    }
}


static void
write_raw_image_frames (FILE *fp, 
                        GpivImage *image
                        )
/*---------------------------------------------------------------------------
 *     Writes raw binary image frames to fp
 */
{
    guint ncolumns = image->header->ncolumns;
    guint nrows = image->header->nrows;


    if (fp == stdout || fp == NULL) {
        if (image->header->depth <= 8) {
            write (1, image->frame1[0], ncolumns * nrows * (sizeof (guint8)));
 
            if (image->header->x_corr) {
                write (1, image->frame2[0], ncolumns * nrows * (sizeof (guint8)));
            }


        } else {
            write (1, image->frame1[0], ncolumns * nrows * (sizeof (guint16)));

            if (image->header->x_corr) {
                write (1, image->frame2[0], ncolumns * nrows * (sizeof (guint16)));
            }
        }

    } else {
        if (image->header->depth <= 8) {
            fwrite (image->frame1[0], ncolumns * nrows * (sizeof (guint8)), 1, fp);
 
            if (image->header->x_corr) {
                fwrite (image->frame2[0], ncolumns * nrows * (sizeof (guint8)), 1, fp);
            }


        } else {
            fwrite (image->frame1[0], ncolumns * nrows * (sizeof (guint16)), 1,
                    fp);

            if (image->header->x_corr) {
                fwrite (image->frame2[0], ncolumns * nrows * (sizeof (guint16)), 1,
                        fp);
            }
        }
    }
}

/*
 * From gpiv_img_utils.h
 */
void
gpiv_alloc_imgframe(GpivImage *image)
/*-----------------------------------------------------------------------------
 * Allocates GpivImage frame data
 */
{    image->frame1 = gpiv_matrix_guint16 (image->header->nrows, 
                                          image->header->ncolumns);
    if (image->header->x_corr) {
        image->frame2 = gpiv_matrix_guint16 (image->header->nrows, 
                                             image->header->ncolumns);
    } else {
        image->frame2 = image->frame1;
    }
}


GpivImage *
gpiv_alloc_img (const GpivImagePar *image_par
                )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for GpivImage frames
 *
 * INPUTS:
 *      image_par:      image parameters
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      img:            2-dim image data arry
 *---------------------------------------------------------------------------*/
{
    GpivImage *image = g_new0 (GpivImage, 1);;
    GpivImagePar *image_par_dest = NULL;


    image_par_dest = gpiv_img_cp_parameters (image_par);
    image->header = image_par_dest;

    gpiv_alloc_imgframe (image);


    return image;
}



gchar *
gpiv_check_alloc_img (const GpivImage *image
                      )
/*-----------------------------------------------------------------------------
 * Check if gpiv_image frames have been allocated
 */
{
    gchar *err_msg = NULL;
    guint x;

    g_return_val_if_fail (image->frame1 != NULL, 
                          "gpiv_check_alloc_image: frame1 != NULL");
    g_return_val_if_fail (image->frame2 != NULL, 
                          "gpiv_check_alloc_image: frame2 != NULL");

/*     g_message ("gpiv_check_alloc_image: sizeof frame = %d",  */
/*                sizeof (image->frame1)); */
/*     g_message ("gpiv_check_alloc_image: sizeof guint16 x nrows x ncolumns = %d x %d x %d = %d",  */
/*                sizeof (guint16),  */
/*                image->header->nrows,  */
/*                image->header->ncolumns, */
/*                (sizeof (guint16)) * image->header->nrows * image->header->ncolumns */
/*                ); */

    return err_msg;
}


GpivImage *
gpiv_cp_img (const GpivImage *image
             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Duplicates img. The returned image will have to be freed with 
 *      gpiv_free_img when no longer needed.
 *
 * INPUTS:
 *      image_par:      image parameters
 *      img_src:            2-dim image data arry
 *
 * OUTPUTS:
 *      img_dest:            2-dim image data arry
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gint i, j;
    GpivImage *image_dest = NULL;


    if (image->frame1[0] == NULL) {
        gpiv_warning ("gpiv_cp_img: image->frame1 not allocated");
        return NULL;
    }

    if (image->header->x_corr__set == FALSE) {
        gpiv_warning ("gpiv_cp_img: x_corr not set");
        return NULL;
    }

    if (image->header->x_corr) {
        if (image->frame2[0] == NULL) {
            gpiv_warning ("gpiv_cp_img: x_corr && image->frame2 not allocated");
            return NULL;
        }
    }


    image_dest = gpiv_alloc_img (image->header);


    for (i = 0; i < image_dest->header->nrows; i++) {
        for (j = 0; j < image_dest->header->ncolumns; j++) {
            image_dest->frame1[i][j] = image->frame1[i][j];
        }
    }

    if (image->header->x_corr) {
        for (i = 0; i < image_dest->header->nrows; i++) {
            for (j = 0; j < image_dest->header->ncolumns; j++) {
                image_dest->frame2[i][j] = image->frame2[i][j];
            }
        }
    } else {
        image_dest->frame2[0] = image->frame1[0];
    }


    return image_dest;
}



gchar *
gpiv_cp_img_data (const GpivImage *image_src, 
                  GpivImage *image_dest
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Copies contents of image_src to image_dest. image_src and image_dest
 *      will have to be allocated with gpiv_alloc_img before and will have to
 *      be freed with gpiv_free_img when no longer needed.
 *
 * INPUTS:
 *      image_par:      image parameters
 *      image_src:            2-dim image data arry
 *
 * OUTPUTS:
 *      image_dest:            2-dim image data arry
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    if (image_src->frame1[0] == NULL) {
       err_msg = "gpiv_cp_img_data: image_src not allocated";
       gpiv_warning ("%s", err_msg);
       return err_msg;
    }

    if (image_dest->frame1[0] == NULL) {
        err_msg = "gpiv_cp_img_data: image_dest not allocated";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_src->header->x_corr__set == FALSE) {
        err_msg = "gpiv_cp_img_data: image_src: x_corr not set";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_dest->header->x_corr__set == FALSE) {
        err_msg = "gpiv_cp_img_data: image_dest: x_corr not set";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if ((image_src->header->x_corr == FALSE
         && image_dest->header->x_corr == TRUE)
        || (image_src->header->x_corr == TRUE
            && image_dest->header->x_corr == FALSE)) {
        err_msg = "gpiv_cp_img_data: image_src and image_dest: x_corr unequal";
        gpiv_warning ("%s", err_msg);
        return err_msg;
   }

    if (image_src->header->nrows != image_dest->header->nrows) {
        gpiv_warning ("gpiv_cp_img_data: unequal dimensions: image_src->header->nrows = %d image_dest->header->nrows = %d",
                      image_src->header->nrows,
                      image_dest->header->nrows);
        err_msg = "gpiv_cp_img_data: unequal dimensions of image_src and image_dest";
        return err_msg;
    }

    if (image_src->header->ncolumns != image_dest->header->ncolumns) {
        gpiv_warning ("gpiv_cp_img_data: unequal dimensions: image_src->header->ncolumns = %d image_dest->header->ncolumns = %d",
                      image_src->header->ncolumns,
                      image_dest->header->ncolumns);
        err_msg = "gpiv_cp_img_data: unequal dimensions of image_src and image_dest";
        return err_msg;
    }


    if (image_src->header->x_corr) {
        if (image_src->frame2[0] == NULL) {
            err_msg = "gpiv_cp_img_data: x_corr && image_src->frame2 not allocated";
            gpiv_warning ("%s", err_msg);
            return err_msg;
        }

        if (image_dest->frame2[0] == NULL) {
            err_msg = "gpiv_cp_img_data: x_corr && image_dest->frame2 not allocated";
            gpiv_warning ("%s", err_msg);
            return err_msg;
        }
    }

    

    for (i = 0; i < image_src->header->nrows; i++) {
        for (j = 0; j < image_src->header->ncolumns; j++) {
#ifdef DEBUG2
	  g_message ("gpiv_cp_img_data:: frame1 i = %d j = %d", i, j);
#endif
            image_dest->frame1[i][j] = image_src->frame1[i][j];
        }
    }

    if (image_src->header->x_corr) {
        for (i = 0; i < image_src->header->nrows; i++) {
            for (j = 0; j < image_src->header->ncolumns; j++) {
#ifdef DEBUG2
	  g_message ("gpiv_cp_img_data:: frame2 i = %d j = %d", i, j);
#endif
                image_dest->frame2[i][j] = image_src->frame2[i][j];
            }
        }
    }


    return NULL;
}



void 
gpiv_free_img (GpivImage *image
               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for image
 *
 * INPUTS:
 *      image_par:      image parameters
 *      img:            2-dim image data arry
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (image->frame1[0] != NULL); /* gpiv_error ("img not allocated"); */

    gpiv_free_matrix_guint16 (image->frame1);
    image->frame1 = NULL;
 
   if (image->header->x_corr == TRUE) {
        g_return_if_fail (image->frame2[0] != NULL); /* gpiv_error ("img not allocated"); */
        gpiv_free_matrix_guint16 (image->frame2);
        image->frame2 = NULL;
    }
}


#ifdef ENABLE_MPI
void
gpiv_img_mpi_bcast_image (GpivImage *image,
                          const gboolean alloc_frame
                      )
/*-----------------------------------------------------------------------------
 * Broadcasts image 
 */
{
    int rank = 0;


    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    gpiv_img_mpi_bcast_imgpar (image->header);
    if (alloc_frame && rank != 0) gpiv_alloc_imgframe (image);
    gpiv_img_mpi_bcast_imgframe (image);

#ifdef DEBUG
    my_utils_write_tmp_image (image, 
                              g_strdup_printf ("%s%d",  "bcast_rank", rank),
                              "Writing mpi-bcasted image to: ")
#endif
}


void
gpiv_img_mpi_bcast_imgframe (GpivImage *image
                             )
/*-----------------------------------------------------------------------------
 * Broadcasts image frame data
 */
{
    guint size  = image->header->ncolumns * image->header->nrows;

    
    if (MPI_Bcast(*image->frame1, size, MPI_SHORT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, mpi_bcast_img_frame: An error ocurred in MPI_Bcast");
    }

    if (image->header->x_corr) {
        if (MPI_Bcast(*image->frame2, size, MPI_SHORT, 0, MPI_COMM_WORLD) 
            != MPI_SUCCESS) {
            gpiv_error("@&%#$!, mpi_bcast_img_frame: An error ocurred in MPI_Bcast");
        }
    }
}


#endif /* ENABLE_MPI */
