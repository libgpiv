
/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2004, 2005 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>
   Julio Soria <julio.soria@eng.monash.edu.au>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------
FILENAME:                cam_par.c
LIBRARY:                 libgpiv
EXTERNAL FUNCTIONS:      
                         gpiv_cam_default_parameters
                         gpiv_cam_parameters_set
                         gpiv_cam_read_parameters
                         gpiv_cam_check_parameters_read
		         gpiv_cam_test_parameter
                         gpiv_cam_fprint_parameters

LAST MODIFICATION DATE:  $Id: cam_par.c,v 1.6 2008-05-05 14:38:41 gerber Exp $
--------------------------------------------------------------------------- */


#ifdef ENABLE_CAM
#include <gpiv.h>



void
gpiv_cam_default_parameters (GpivCamPar *cam_par_default,
                             const gboolean force
                             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Sets default GpivCamPar parameter values
 *
 * INPUTS:
 *
 * OUTPUTS:
 *     piv_cam_par_default:   structure of camera parameters
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    if (!cam_par_default->mode__set || force)
        cam_par_default->mode = GPIV_CAMPAR_DEFAULT__MODE;
    if (!cam_par_default->cycles__set || force)
        cam_par_default->cycles = GPIV_CAMPAR_DEFAULT__CYCLES;
    if (!cam_par_default->fname__set || force)
        snprintf(cam_par_default->fname, GPIV_MAX_CHARS, "%s", 
                 GPIV_CAMPAR_DEFAULT__FNAME);

    gpiv_cam_parameters_set(cam_par_default, TRUE);
}



void 
gpiv_cam_read_parameters (FILE *fp_par, 
                         GpivCamPar *cam_par, 
                         const gboolean verbose
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Read all GpivCamPar parameters
 *
 * INPUTS:
 *      fp_par:         file pointer to parameter file
 *      verbose:      flag to print parametrs to stdout
 *
 * OUTPUTS:
 *     piv_cam_par:     camera parameters
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    char line[GPIV_MAX_CHARS], par_name[GPIV_MAX_CHARS];
    gfloat val;

    while (fgets(line, GPIV_MAX_CHARS, fp_par) != NULL) {
        if (line[0] != '#' && line[0] != '\n' && line[0] != ' ' 
            && line[0] != '\t') {
            sscanf(line,"%s", par_name);

/*
 * Parameters used to acquire images
 */
            if (cam_par->mode__set == FALSE) {
                cam_par->mode__set =
                    gpiv_scan_iph (GPIV_CAMPAR_KEY, 
                                   GPIV_CAMPAR_KEY__MODE, TRUE,
                                   line, par_name, (int *) &cam_par->mode, 
                                   verbose, NULL);
            }
            
            if (cam_par->cycles__set == FALSE) {
                cam_par->cycles__set =
                    gpiv_scan_iph (GPIV_CAMPAR_KEY, 
                                   GPIV_CAMPAR_KEY__CYCLES, TRUE,
                                   line, par_name, &cam_par->cycles, 
                                   verbose, NULL);
            }
            
            if (cam_par->fname__set == FALSE) {
                cam_par->fname__set =
                    gpiv_scan_sph (GPIV_CAMPAR_KEY, 
                                   GPIV_CAMPAR_KEY__FNAME, TRUE,
                                   line, par_name, cam_par->fname, 
                                   verbose, NULL);
	    }
	    
	    
	}
    }
    
}



gchar *
gpiv_cam_check_parameters_read (GpivCamPar *cam_par,
                                const GpivCamPar *cam_par_default
                                )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Check out if all GpivCamPar parameters have been read
 *
 * INPUTS:
 *     piv_cam_par_default:
 *
 * OUTPUTS:
 *     piv_cam_par:    camera parameters
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;

    if (cam_par->mode__set == FALSE) {
        cam_par->mode__set = TRUE;
        if (cam_par_default == NULL) {
            err_msg = "Using default from libgpiv:";
            cam_par->mode = GPIV_CAMPAR_DEFAULT__MODE;
        } else {
            err_msg = "Using default:";
            cam_par->mode = cam_par_default->mode;
        }
        gpiv_warning("%s\nCAM.Mode %d", err_msg, 
                     cam_par_default->mode);
    }

    if (cam_par->cycles__set == FALSE) {
        cam_par->cycles__set = TRUE;
        if (cam_par_default == NULL) {
            err_msg = "Using default from libgpiv:";
            cam_par->cycles = GPIV_CAMPAR_DEFAULT__CYCLES;
        } else {
            err_msg = "Using default:";
            cam_par->cycles = cam_par_default->cycles;
        }
        gpiv_warning("%s\nCAM.Cycles %d", err_msg, 
                     cam_par_default->cycles);
    }

    if (cam_par->fname__set == FALSE) {
        cam_par->fname__set = TRUE;
        if (cam_par_default == NULL) {
            err_msg = "Using default from libgpiv:";
            snprintf(cam_par->fname, GPIV_MAX_CHARS, "%s", 
                     GPIV_CAMPAR_DEFAULT__FNAME);
        } else {
            err_msg = "Using default:";
            snprintf(cam_par->fname, GPIV_MAX_CHARS, "%s", 
                     cam_par_default->fname);
        }
        gpiv_warning("%s\nCAM.Fname %s", err_msg, 
                     cam_par_default->fname);
    }


    return (err_msg);
}



void
gpiv_cam_parameters_set(GpivCamPar *cam_par,
                        const gboolean flag
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Sets flags for __set variables of GpivCamPar 
 *
 * INPUTS:
 *     cam_par:        camera parameters
 *
 * OUTPUTS:
 *     cam_par:        camera parameters
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    cam_par->mode__set = flag;
    cam_par->cycles__set = flag;
    cam_par->fname__set = flag;

}



gchar *
gpiv_cam_test_parameter(const GpivCamPar *cam_par
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Testing GpivCamPar parameters on valid values and initializing derived 
 *     variables 
 *
 * INPUTS:
 *     cam_par:      
 *
 * OUTPUTS:
 *     cam_par:         camera parameters
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 * --------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;

    return (err_msg);
}



void 
gpiv_cam_print_parameters (FILE *fp_par_out, 
                           const GpivCamPar *cam_par
                           )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Prints GpivCamPar parameters to fp_par_out
 *
 * INPUTS:
 *      fp_par_out:     
 *      cam_par:       camera parameters
 *
 * OUTPUTS:
 *
 * RETURNS:
 *
 *---------------------------------------------------------------------------*/
{
    gfloat val;

    if (fp_par_out == NULL) fp_par_out = stdout;
    if (cam_par->mode__set) {
	fprintf(fp_par_out, "%s%s %d\n", GPIV_CAMPAR_KEY, GPIV_CAMPAR_KEY__MODE, 
		cam_par->mode);
    }

    if (cam_par->cycles__set) {
	fprintf(fp_par_out, "%s%s %d\n", GPIV_CAMPAR_KEY, GPIV_CAMPAR_KEY__CYCLES, 
		cam_par->cycles);
    }

    if (cam_par->fname__set) {
	fprintf(fp_par_out, "%s%s %s\n", GPIV_CAMPAR_KEY, GPIV_CAMPAR_KEY__FNAME, 
		cam_par->fname);
    }

}



#endif /* ENABLE_CAM */
