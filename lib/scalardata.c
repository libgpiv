/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                utils.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_null_scdata
			 gpiv_alloc_scdata
			 gpiv_free_scdata

                         gpiv_null_bindata
			 gpiv_alloc_bindata
			 gpiv_free_bindata

LAST MODIFICATION DATE:  $Id: post_utils.c,v 1.2 2007-12-19 08:46:46 gerber Exp $
 --------------------------------------------------------------------------- */

#include <gpiv.h>
#include "my_utils.h"

/*
 * Protoypes local functions
 */

static GpivScalarData *
read_scdata_from_file                   (FILE                   *fp
                                         );

static GpivScalarData *
read_scdata_from_stdin                  (void
                                         );

static void
obtain_scdata_fromline                  (gchar                  line[], 
                                         GpivScalarData         *sc_data,
                                         guint                  *i,
                                         guint                  *j
                                         );

static gchar *
fread_hdf5_sc_position                  (const gchar            *fname, 
                                         GpivScalarData         *scalar_data
                                         );

static gchar *
fwrite_hdf5_sc_position                 (const gchar            *fname, 
                                         GpivScalarData         *scalar_data
                                         );

static gchar *
fread_hdf5_scdata                       (const gchar            *fname, 
                                         GpivScalarData         *scalar_data,
                                         const gchar            *DATA_KEY
                                         );

static gchar *
fwrite_hdf5_scdata                      (const char             *fname, 
                                         GpivScalarData         *scalar_data,
                                         const gchar            *DATA_KEY
                                         );
/*
 * Public functions
 */

void
gpiv_null_scdata                        (GpivScalarData         *scal_data
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Sets all elements of scal_data structure to NULL
 *---------------------------------------------------------------------------*/
{
    scal_data->point_x = NULL;
    scal_data->point_y = NULL;
    scal_data->scalar = NULL;
    scal_data->flag = NULL;
}



GpivScalarData *
gpiv_alloc_scdata                       (const gint             nx,
                                         const gint             ny
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for GpivScalarData
 *
 * INPUTS:
 *      scal_data:      .nx and .ny members of GpivScalarData structure
 *
 * OUTPUTS:
 *      scal_data:       point_x, point_y, scalar, flag of GpivScalarData
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    GpivScalarData *scal_data = g_new0 (GpivScalarData, 1);
    gint i, j;


    if (ny <= 0 || nx <= 0) {
        gpiv_warning ("gpiv_alloc_scdata: nx = %d ny = %d",
                      nx, ny);
        return NULL;
    }
    scal_data->nx = nx;
    scal_data->ny = ny;

    scal_data->point_x = gpiv_matrix (scal_data->ny, scal_data->nx);
    scal_data->point_y = gpiv_matrix (scal_data->ny, scal_data->nx);
    scal_data->scalar = gpiv_matrix (scal_data->ny, scal_data->nx);
    scal_data->flag = gpiv_imatrix (scal_data->ny, scal_data->nx);
        
    for (i = 0; i < scal_data->ny; i++) {
        for (j = 0; j < scal_data->nx; j++) {
            scal_data->point_x[i][j] = 0.0;
            scal_data->point_y[i][j] = 0.0;
            scal_data->scalar[i][j] = 0.0;
            scal_data->flag[i][j] = 0;
        }
    }

    scal_data->scale = FALSE;
    scal_data->scale__set = FALSE;
    scal_data->comment = NULL;


    return scal_data;
}



void 
gpiv_free_scdata                        (GpivScalarData         *scal_data
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for GpivScalarData
 *
 * INPUTS:
 *      scal_data:      scalar data structure
 *
 * OUTPUTS:
 *      scal_data:       NULL pointer to point_x, point_y, scalar, flag
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{

    g_return_if_fail (scal_data->point_x != NULL); /* gpiv_error ("scal_data->point_x not allocated"); */
    g_return_if_fail (scal_data->point_y != NULL); /* gpiv_error ("scal_data->point_y not allocated"); */
    g_return_if_fail (scal_data->scalar != NULL); /* gpiv_error ("scal_data->scalar not allocated"); */
    g_return_if_fail (scal_data->flag != NULL); /* gpiv_error ("scal_data->flag not allocated"); */
    
    gpiv_free_matrix (scal_data->point_x);
    gpiv_free_matrix (scal_data->point_y);
    gpiv_free_matrix (scal_data->scalar);
    gpiv_free_imatrix (scal_data->flag);
    
    g_free (scal_data->comment);
    gpiv_null_scdata (scal_data);
}



gchar *
gpiv_scalar_gnuplot                     (const gchar            *fname_out, 
                                         const gchar            *title, 
                                         const gchar            *GNUPLOT_DISPLAY_COLOR, 
                                         const gint             GNUPLOT_DISPLAY_SIZE
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Graphical output with gnuplot for scalar data
 *
 * INPUTS:
 *      fname_out:     file name containing plot
 *      title:         title of plot
 *      GNUPLOT_DISPLAY_COLOR:  display color of window containing graph
 *      GNUPLOT_DISPLAY_SIZE:   display size of window containing graph
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    FILE *fp_cmd;
    const gchar *tmp_dir = g_get_tmp_dir ();
    gchar *fname_loc = "gpiv_gnuplot.cmd";
    gchar fname_cmd[GPIV_MAX_CHARS];
    gchar command[GPIV_MAX_CHARS];


    snprintf (fname_cmd, GPIV_MAX_CHARS, "%s/%s", tmp_dir, fname_loc);
    if ((fp_cmd = fopen (fname_cmd, "w")) == NULL) {
        err_msg = "gpiv_scalar_gnuplot: Failure opening %s for output";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    fprintf (fp_cmd, "set xlabel \"x (pixels)\"");
    fprintf (fp_cmd, "\nset ylabel \"y (pixels)\"");
    fprintf (fp_cmd, "\nset contour; set nosurface");
    fprintf (fp_cmd, "\nset view 0, 0");
    fprintf (fp_cmd,
	     "\nsplot \"%s\" title \"%s\" with lines",
	     fname_out, title);
    fprintf (fp_cmd, "\npause -1 \"Hit return to exit\"");
    fprintf (fp_cmd, "\nquit");
    fclose (fp_cmd);

    snprintf (command, GPIV_MAX_CHARS, "gnuplot -bg %s -geometry %dx%d %s",
	      GNUPLOT_DISPLAY_COLOR, GNUPLOT_DISPLAY_SIZE, 
	      GNUPLOT_DISPLAY_SIZE, fname_cmd);

    if (system (command) != 0) {
        err_msg = "gpiv_scalar_gnuplot: could not exec shell command";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


    return err_msg;
}



GpivScalarData *
gpiv_fread_scdata                       (const gchar            *fname
                                         )
/*-----------------------------------------------------------------------------
 *      Reads scalar data from file fname
 */
{
    FILE *fp = NULL;
    GpivScalarData *scalar_data = NULL;


    if ((fp = fopen (fname, "rb")) == NULL) {
        gpiv_warning ("gpiv_fread_scdata: failing fopen %s", fname);
        return NULL;
    }

    if ((scalar_data = gpiv_read_scdata (fp)) == NULL) {
        gpiv_warning ("gpiv_fread_scdata: failing gpiv_read_scdata");
        return NULL;
    }


    fclose (fp);
    return scalar_data;
}



gchar *
gpiv_fwrite_scdata                      (const gchar            *fname,
                                         GpivScalarData         *scalar_data,
                                         const gboolean         free
                                         )
/*-----------------------------------------------------------------------------
 *      Writes scalar data to file fname
 */
{
    FILE *fp = NULL;
    gchar *err_msg = NULL;


    if ((fp = fopen (fname, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_scdata: failing fopen %s", fname);
        return "gpiv_fwrite_scdata: failing fopen";
    }

    if ((err_msg = gpiv_write_scdata (fp, scalar_data, free)) != NULL) {
        return err_msg;
    }


    fclose (fp);
    return err_msg;
}



GpivScalarData *
gpiv_read_scdata                        (FILE                   *fp
                                         )
/*-----------------------------------------------------------------------------
 * Reads scalar data from ascii data file
 */
{
    GpivScalarData *sc_data = NULL;


/*
 * reads the data from stdin or file pointer
 */
    if (fp == stdin || fp == NULL) {
        sc_data =  read_scdata_from_stdin ();
    } else {
        sc_data =  read_scdata_from_file (fp);
    }


    return sc_data;
}



gchar *
gpiv_write_scdata                       (FILE                   *fp,
                                         GpivScalarData	        *scalar_data,
                                         const gboolean         free
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Writes scalar_data data to fp
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    g_return_val_if_fail (scalar_data->point_x != NULL,
                          "gpiv_write_scdata: point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, 
                          "gpiv_write_scdata: point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, 
                          "gpiv_write_scdata: scalar != NULL");
    g_return_val_if_fail (scalar_data->flag != NULL, 
                          "gpiv_write_scdata: flag != NULL");


    if (fp == stdout || fp == NULL) {
/*
 * Use stdout
 * Writing comment
 */
        if (scalar_data->comment != NULL) {
            printf ("%s", scalar_data->comment);
        }

/*
 * Writing data
 */
        if (scalar_data->scale == TRUE) {
            printf ("\n#       x(m)         y(m)        scalar      flag\n");
            for (i=0; i< scalar_data->ny; i++) {
                for (j=0; j< scalar_data->nx; j++) { 
                    printf (GPIV_SCALAR_S_FMT, 
                            scalar_data->point_x[i][j], 
                            scalar_data->point_y[i][j], 
                            scalar_data->scalar[i][j], 
                            scalar_data->flag[i][j]);      
                }
            }
        
        } else {
            printf ("\n# x(px) y(px) scalar      flag\n");
            for (i=0; i< scalar_data->ny; i++) {
                for (j=0; j< scalar_data->nx; j++) { 
                    printf (GPIV_SCALAR_FMT, 
                            scalar_data->point_x[i][j], 
                            scalar_data->point_y[i][j], 
                            scalar_data->scalar[i][j], 
                            scalar_data->flag[i][j]);      
                }
            }
        }


    } else {
/*
 * Use fp
 * Writing comment
 */
    if (scalar_data->comment != NULL) {
        fprintf (fp, "%s", scalar_data->comment);
    }

/*
 * Writing data
 */
        if (scalar_data->scale == TRUE) {
            fprintf (fp, "\n#       x(m)         y(m)        scalar      flag\n");
            for (i=0; i< scalar_data->ny; i++) {
                for (j=0; j< scalar_data->nx; j++) { 
                    fprintf (fp, GPIV_SCALAR_S_FMT, 
                             scalar_data->point_x[i][j], 
                             scalar_data->point_y[i][j], 
                             scalar_data->scalar[i][j], 
                             scalar_data->flag[i][j]);      
                }
            }
        
        } else {
            fprintf (fp, "\n# x(px) y(px) scalar      flag\n");
            for (i=0; i< scalar_data->ny; i++) {
                for (j=0; j< scalar_data->nx; j++) { 
                    fprintf (fp, GPIV_SCALAR_FMT, 
                             scalar_data->point_x[i][j], 
                             scalar_data->point_y[i][j], 
                             scalar_data->scalar[i][j], 
                             scalar_data->flag[i][j]);      
                }
            }
        }
        fflush (fp);
    }


    if (free) gpiv_free_scdata (scalar_data);
    return err_msg;  
}



gchar *
gpiv_write_sc_griddata                  (FILE                   *fp,
                                         GpivScalarData         *scalar_data,
                                         const gboolean         free
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes scalar data to file in grid format for gnuplot
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;
    

    g_return_val_if_fail (scalar_data->point_x != NULL, 
                          "gpiv_fwrite_sc_griddata: point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, 
                          "gpiv_fwrite_sc_griddata: point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, 
                          "gpiv_fwrite_sc_griddata: scalar != NULL");
    g_return_val_if_fail (scalar_data->flag != NULL, 
                          "gpiv_fwrite_sc_griddata: flag != NULL");
    

/*
 *  Writing comment
 */
    if (scalar_data->comment != NULL) {
        fprintf (fp, "%s", scalar_data->comment);
    }

/*
 * Writing data
 */
    if (scalar_data->scale == TRUE) {
        fprintf (fp,"\n#       x(m)         y(m)        scalar      flag\n");
        for (i = 0; i < scalar_data->ny; i++) {
            fprintf (fp, "\n");
            for (j = 0; j< scalar_data->nx; j++) { 
                fprintf (fp, GPIV_SCALAR_S_FMT, scalar_data->point_x[i][j], 
                         scalar_data->point_y[i][j], 
                         scalar_data->scalar[i][j], scalar_data->flag[i][j]);      
            }
        }

    } else {
        fprintf (fp, "\n# x(px) y(px) scalar      flag\n");
        for (i = 0; i< scalar_data->ny; i++) {
            fprintf (fp, "\n");
            for (j = 0; j < scalar_data->nx; j++) { 
                fprintf (fp, GPIV_SCALAR_FMT, scalar_data->point_x[i][j], 
                         scalar_data->point_y[i][j], 
                         scalar_data->scalar[i][j],scalar_data->flag[i][j]);      
            }
        }
    }
    

    fflush (fp);
    if (free) gpiv_free_scdata (scalar_data);
    return err_msg;
}



gchar *
gpiv_write_sc_mtvgriddata               (FILE                   *fp, 
                                         GpivScalarData         *scalar_data, 
                                         const gboolean         free
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes scalar data to file in grid format for plotmtv
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    g_return_val_if_fail (scalar_data->point_x != NULL, 
                          "gpiv_write_sc_mtvgriddata: point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, 
                          "gpiv_write_sc_mtvgriddata: point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, 
                          "gpiv_write_sc_mtvgriddata: scalar != NULL");
/*   g_return_val_if_fail (scalar_data->flag != NULL); */


/*
 * Opens output file
 * OBSOLETE
 */
/*   if((fp=fopen(fname,"w")) == NULL) {  */
/*       err_msg = "GPIV_FWRITE_SC_MTVGRIDDATA: Failure opening for output"; */
/*       return err_msg; */
/*   }  */


    if (fp == stdout || fp == NULL) {
/*
 *  Writing comment
 */
        if (scalar_data->comment != NULL) {
            printf ("%s", scalar_data->comment);
        }
/*
 *  Writing header for mtv format
 */
        printf ("$ DATA=CONTOUR\n");
        printf ("%% contfill=T\n");
        printf ("%% nx=%d xmin=%f xmax=%f\n", scalar_data->nx, 
                scalar_data->point_x[0][0], scalar_data->point_x[0][scalar_data->nx - 1]);
        printf ("%% ny=%d ymin=%f ymax=%f\n", scalar_data->ny, 
                scalar_data->point_y[0][0], scalar_data->point_y[scalar_data->ny - 1][0]);

/*
 * Writing data
 */
        for (i=0; i < scalar_data->ny; i++) {
            printf ("\n");
            for (j=0; j < scalar_data->nx; j++) { 
                printf ("%f ", scalar_data->scalar[i][j]);      
            }
        }


    } else {
/*
 *  Writing comment
 */
        if (scalar_data->comment != NULL) {
            fprintf (fp, "%s", scalar_data->comment);
        }

/*
 *  Writing header for mtv format
 */
        fprintf (fp, "$ DATA=CONTOUR\n");
        fprintf (fp, "%% contfill=T\n");
        fprintf (fp, "%% nx=%d xmin=%f xmax=%f\n", scalar_data->nx, 
                 scalar_data->point_x[0][0], scalar_data->point_x[0][scalar_data->nx - 1]);
        fprintf(fp, "%% ny=%d ymin=%f ymax=%f\n", scalar_data->ny, 
                scalar_data->point_y[0][0], scalar_data->point_y[scalar_data->ny - 1][0]);

/*
 * Writing data
 */
        for (i=0; i < scalar_data->ny; i++) {
            fprintf (fp, "\n");
            for (j=0; j < scalar_data->nx; j++) { 
                fprintf (fp, "%f ", scalar_data->scalar[i][j]);      
            }
        }
        fflush (fp);
    }


    if (free) gpiv_free_scdata (scalar_data);
    return err_msg;
}



GpivScalarData *
gpiv_fread_hdf5_scdata                  (const gchar            *fname,
                                         const gchar            *DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads scalar data from hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    GpivScalarData *scalar_data = NULL;
    gchar *err_msg = NULL;
    guint nx, ny;


    if ((err_msg = my_utils_fcount_hdf5_data (fname, &nx, &ny)) != NULL)  {
        g_message ("gpiv_fread_hdf5_scdata: %s", err_msg);
        return NULL;
    }

    if ((scalar_data = gpiv_alloc_scdata (nx, ny)) == NULL) {
        g_message ("gpiv_fread_hdf5_scdata: failing gpiv_alloc_pivdata");
        return NULL;
    }

    if ((err_msg = fread_hdf5_sc_position (fname, scalar_data)) != NULL)  {
        g_message ("gpiv_fread_hdf5_scdata: %s", err_msg);
        return NULL;
    }

    if ((err_msg = fread_hdf5_scdata (fname, scalar_data, DATA_KEY)) != NULL)  {
        g_message ("gpiv_fread_hdf5_scdata: %s", err_msg);
        return NULL;
    }



    return scalar_data;
}



gchar *
gpiv_fwrite_hdf5_scdata                 (const gchar            *fname, 
                                         GpivScalarData         *scalar_data, 
                                         const gchar            *DATA_KEY,
                                         const gboolean         free
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes scalar data to file in hdf version 5 format
 *
*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = fwrite_hdf5_scdata (fname, scalar_data, DATA_KEY)) != NULL) {
        gpiv_error ("gpiv_fwrite_hdf5_scdata: %s", err_msg);
    }

    if ((err_msg = fwrite_hdf5_sc_position (fname, scalar_data)) != NULL) {
        gpiv_error ("gpiv_fwrite_hdf5_sc_position: %s", err_msg);
    }

    if (free) gpiv_free_scdata (scalar_data);


    return err_msg;
}



/*
 * Local functions
 */

static GpivScalarData *
read_scdata_from_file (FILE *fp
                       )
/*-----------------------------------------------------------------------------
 * Reads scalar data from file pointer
 */
{
    GpivScalarData *sc_data = NULL;
    gint i = 0, j = 0;
    gchar line[GPIV_MAX_CHARS];

    guint nx = 0, ny = 0;
    gboolean scale;


    scale = my_utils_count_asciidata (fp, &nx, &ny);
    sc_data = gpiv_alloc_scdata (nx, ny);
    sc_data->scale = scale;

    fseek (fp, 0, SEEK_SET);
    while ((i < sc_data->ny - 1) || (j < sc_data->nx - 1)) {
        fgets (line, GPIV_MAX_CHARS, fp);
        obtain_scdata_fromline (line, sc_data, &i, &j);
    }
    fgets (line, GPIV_MAX_CHARS, fp);
    obtain_scdata_fromline (line, sc_data, &i, &j);
    


    return sc_data;
}



static GpivScalarData *
read_scdata_from_stdin (void
                        )
/*-----------------------------------------------------------------------------
 * Reads scalar data from stdin
 */
{
    GpivScalarData *sc_data = NULL;
    gint i = 0, j = 0, k = 0;
    gchar line[GPIV_MAX_LINES][GPIV_MAX_CHARS];

    guint nx = 0, ny = 0;
    gboolean scale = FALSE;

    gboolean increment = FALSE;
    gfloat x = 0, y = 0;
    gfloat dat_x = -10.0e5, dat_y = -10.0e5;
    gint line_nr = 0; 


/* 
 * Count nx, ny, similar to count_asciidata
 */

    k = 0;
    while (gets (line[k]) != NULL) {
        my_utils_obtain_nxny_fromline (line[k], &scale, &nx, &ny, 
                              &increment, &dat_x, &dat_y, &line_nr);
        k++;
    }
    nx = nx + 1;
    ny = ny + 1;
    sc_data = gpiv_alloc_scdata (nx, ny);
    sc_data->scale = scale;


    k = 0;
    i = 0;
    j = 0;
    while ((i < sc_data->ny - 1) || (j < sc_data->nx - 1)) {
       obtain_scdata_fromline (line[k], sc_data, &i, &j);
        k++;
    }
    obtain_scdata_fromline (line[k], sc_data, &i, &j);


    return sc_data;
}



static void
obtain_scdata_fromline (gchar line[], 
                        GpivScalarData *sc_data,
                        guint *i,
                        guint *j
                        )
/*-----------------------------------------------------------------------------
 * Scans a line to get GpivScalarData
 */
{
/*     *i = 0; */
/*     *j = 0; */

/*
 * Fill in comment, skip header
 */
    if (line[0] == '#' 
        && g_strstr_len(line, GPIV_MAX_CHARS, "x(m)") == NULL
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {

        if (sc_data->comment == NULL) {
            sc_data->comment = g_strdup (line);
        } else {
            sc_data->comment = g_strconcat (sc_data->comment, line, NULL);
        }


    } else if (line[0] != '\n' 
               && line[0] != '\t'
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL 
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
/*
 * Fill in SCALAR data, skip empty lines and header
 */
        sscanf (line, "%f %f %f %d", 
                &sc_data->point_x[*i][*j], 
                &sc_data->point_y[*i][*j], 
                &sc_data->scalar[*i][*j], 
                &sc_data->flag[*i][*j]);
        if (*j < sc_data->nx - 1) {
            *j = *j + 1; 
        } else if (*i < sc_data->ny - 1) { 
            *j = 0; 
            *i = *i + 1; 
        }
    }


    return;
}



static gchar *
fread_hdf5_sc_position                  (const gchar            *fname, 
                                         GpivScalarData         *scalar_data
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *    Reads position data from hdf5 data file into GpivScalarData struct
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL; 
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;


    g_return_val_if_fail (scalar_data->point_x != NULL, "scalar_data->point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, "scalar_data->point_y != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fread_hdf5_sc_position: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "POSITIONS", H5P_DEFAULT);

/*
 * point_x
 */
    dataset_id = H5Dopen(group_id, "point_x", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    rank_x = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_x != 1) {
        err_msg = "LIBGPIV: fread_hdf5_sc_position: rank_x != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    scalar_data->nx = dims[0];
    point_x_hdf = gpiv_vector(scalar_data->nx) ;
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, point_x_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
	

/*
 * point_y
 */
    dataset_id = H5Dopen(group_id, "point_y", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    rank_y = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_y != 1) {
        err_msg = "LIBGPIV: fread_hdf5_sc_position: rank_y != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    scalar_data->ny = dims[0];
    point_y_hdf = gpiv_vector(scalar_data->ny) ;
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, point_y_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * filling GpivPivData structure with point_x an point_y
 */
    for (i = 0; i < scalar_data->ny; i++) {
            for (j = 0; j < scalar_data->nx; j++) {
                scalar_data->point_x[i][j] = point_x_hdf[j];
                scalar_data->point_y[i][j] = point_y_hdf[i];
            }
    }
    gpiv_free_vector(point_x_hdf);
    gpiv_free_vector(point_y_hdf);


    H5Gclose (group_id);
    status = H5Fclose(file_id);
    return err_msg;
}



static gchar *
fwrite_hdf5_sc_position                 (const gchar            *fname, 
                                         GpivScalarData         *scalar_data
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *    Writes position data to hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, 
        dataset_id, dataspace_id, str_MC; 
    hsize_t     dims[2], dims_attrib[1];
    herr_t      status;


    g_return_val_if_fail (scalar_data->point_x != NULL, "scalar_data->point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, "scalar_data->point_y != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fwrite_hdf5_sc_position: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    group_id = H5Gcreate (file_id, "POSITIONS", 0, H5P_DEFAULT, H5P_DEFAULT);
    H5Gset_comment (group_id, ".", scalar_data->comment);
    dims_attrib[0] = 1;
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "scale", H5T_NATIVE_INT, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_INT, &scalar_data->scale); 
    H5Aclose(attribute_id); 


/*
 * data dimension
 */
    str_MC = H5Tcopy (H5T_C_S1);
    H5Tset_size (str_MC, GPIV_MAX_CHARS);
    dims_attrib[0] = 1;
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "dimension", str_MC, 
                             dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    if (scalar_data->scale) {
        H5Awrite(attribute_id, str_MC, "Positions in m");
    } else {
        H5Awrite(attribute_id, str_MC, "Positions in pixels");
    }

    H5Aclose(attribute_id); 

/*
 * point_x
 */
    point_x_hdf = gpiv_vector(scalar_data->nx);
    for (i = 0; i < scalar_data->nx; i++) point_x_hdf[i] = scalar_data->point_x[0][i];
    dims[0] = scalar_data->nx; 
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "point_x", H5T_NATIVE_FLOAT, dataspace_id, 
                           H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                      H5P_DEFAULT, point_x_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    gpiv_free_vector(point_x_hdf);

/*
 * point_y
 */
    point_y_hdf = gpiv_vector(scalar_data->ny);
    for (i = 0; i < scalar_data->ny; i++) point_y_hdf[i] = scalar_data->point_y[i][0];
    dims[0] = scalar_data->ny; 
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "point_y", H5T_NATIVE_FLOAT, 
                           dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                      H5P_DEFAULT, point_y_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    gpiv_free_vector(point_y_hdf);


    status = H5Gclose (group_id);
/*     group_id = H5Gcreate (file_id, "DATA", 0); */
/*     status = H5Gclose (group_id); */
/*     group_id = H5Gcreate (file_id, "PARAMETERS", 0); */
/*     status = H5Gclose (group_id); */
    status = H5Fclose(file_id); 
    return err_msg;
}



static gchar *
fread_hdf5_scdata                       (const gchar            *fname, 
                                         GpivScalarData         *scalar_data,
                                         const gchar            *DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads SCALAR data from a hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_data, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;


    g_return_val_if_fail (scalar_data->point_x != NULL, "scalar_data->point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, "scalar_data->point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, "scalar_data->scalar != NULL");
    g_return_val_if_fail (scalar_data->flag != NULL, "scalar_data->flag != NULL");

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fread_hdf5_scdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_data = H5Gopen (file_id, "SCALARS", H5P_DEFAULT);
    group_id = H5Gopen (group_data, DATA_KEY, H5P_DEFAULT);
    H5Gget_comment (group_id, ".", GPIV_MAX_CHARS, RCSID);
    scalar_data->comment = g_strdup (RCSID);
/*
 * scalar
 */
    dataset_id = H5Dopen(group_id, "scalar", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, scalar_data->scalar[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * flag
 */
    dataset_id = H5Dopen(group_id, "flag", H5P_DEFAULT);
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, scalar_data->flag[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);


    H5Gclose (group_id);
    H5Gclose (group_data);
    status = H5Fclose(file_id);
    return err_msg;
}



static gchar *
fwrite_hdf5_scdata                      (const char             *fname, 
                                         GpivScalarData         *scalar_data,
                                         const gchar            *DATA_KEY
                                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes _ONLY_ SCALAR data (no positions) to file in hdf version 5 format
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, k, line_nr=0;
    float *point_x_hdf = NULL;
/*
 * HDF file, dataset,-space identifier
 */
    hid_t       file_id, group_data, group_id, dataset_id, dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;
    
    g_return_val_if_fail (scalar_data->point_x != NULL, "scalar_data->point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, "scalar_data->point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, "scalar_data->scalar != NULL");
    g_return_val_if_fail (scalar_data->flag != NULL, "scalar_data->flag != NULL");
    
    
    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fwrite_hdf5_scdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_data = H5Gopen (file_id, "SCALARS", H5P_DEFAULT);
    group_id = H5Gcreate (group_data, DATA_KEY, 0, H5P_DEFAULT, H5P_DEFAULT);
    H5Gset_comment (group_id, ".", scalar_data->comment);

/*
 * Create the data space.
 */
   dims[0] = scalar_data->ny; 
   dims[1] = scalar_data->nx;


/* 
 * scalar
 */

   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "scalar", H5T_NATIVE_FLOAT, 
			  dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT, &scalar_data->scalar[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * flag
 */

   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "flag", H5T_NATIVE_INT, dataspace_id, 
			  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
		     &scalar_data->flag[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);


   status = H5Gclose (group_id);
   status = H5Gclose (group_data);
   status = H5Fclose(file_id);
   return err_msg;
}

