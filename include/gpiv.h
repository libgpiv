/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

/*!
\file                    gpiv.h
\brief                   main header of libgpiv

*/

/*! \mainpage Documentation of Libgpiv API
 *
 * \section intro_sec Introduction
 *
 * LIBGPIV is a software library for the so-called Digital Particle Image
 * Velocimetry (DPIV). It contains the core functions for image
 * recording, image processing, image interrogation (resulting into
 * estimators of particle image displacements, i.e. PIV data), validation
 * of PIV data (on peak-locking effects, erroneous vectors (outliers),
 * velocity gradient), post-processing (functions for
 * calculating (scalar) derivatives of the PIV data, like vorticity and
 * strain), input/output functions, memory allocation etc.
 *
 * The library has been implemented in several command-line driven
 * programs that can be found in the GPIVTOOLS distribution. A Graphic
 * User Interface (GPIV) is available as well, that controls all processes
 * and its parameters and displays its results graphically in a more
 * advanced way. GPIV has been implemented with the Gtk/GNOME libraries
 * and resides in GPIV package. Also a python module PYGPIV has been written 
 * that allows the use of this library functionalities within Python scripts.
 *
 * This library has been written in ANSI-C and has been developed on a
 * GNU/LINUX box and should work on any UNIX-like system.
 * The main web page can be found at:  http://gpiv.sourceforge.net/
 *
 *
 * \section licence_sec License
 *
 * LIBGPIV is Free Software licensed under the GNU
 * Public license terms. See the COPYING file for the license.
 *
 *
 * \section install_sec Installation
 *
 * Unpacking and compilation. The preferred way is to use CMake, although also
 * Autotools is (still) around:
 * \verbatim
gzip -d libgpiv-*.tar.gz
tar -xvf libgpiv-*.tar
mkdir build
cd build
ccmake ../libgpiv-*
c (from within ccmake)
g (from within ccmake)
make
make install
\endverbatim
 *
 * Some features will only work when they are enabled before the
 * building process. An IEEE1394 (Firewire) camera, using the IIDC
 * specifications protocol, is enabled with:
 * \verbatim
./configure --enable-cam
\endverbatim
 *
 * Triggering, using RealTime Linux (RTL) and RealTime Aplication
 * Interface (RTAI), is enabled with: 
 * \verbatim 
./configure --enable-trig
\endverbatim
 *
 * 
 * Libgpiv is included in the GNU/Debian system for most of the supported 
 * (currently 10) CPU architectures and is probably also included in Linux 
 * distro's that have been derived from GNU/Debian,
 * like Ubuntu and Knoppix. This may drasticly ease the 
 * installation, as one has not to be bothered about library dependencies etc.
 *
 *
 * \section policy_sec Interface naming and some policy rules
 *
 * It has been tried to write an intuitive and systematic API as possible
 * with mnemo-syntactic naming to the variables, functions and constants. 
 * But, as you probably know, its hard to be consistent.
 * Anyhow, here follow some general API policy rules.
 *
 * Macros start with: "GPIV_" and public functions with: "gpiv_".
 * Structures are named following: "GpivNameAnothername" convention,
 * while variables and parameters using that structure are mostly named as: 
 * "name_anothername", or have been named different for clarity. Each structure
 * has been defined in its own module, including functions to create, operate 
 * and delete them. Modules that have _proc in its name, are used for 
 * processing on data, in general following their module name: 

 * "img_" for image functions,

 * "imgproc_" for image processing,

 * "piv_" for image (PIV) interrogation,

 * "valid_" for (PIV) data validation, 

 * "post_" for data post-processing,

 * and / or following their functionality, like:

 * "(f)read_" and "(f)write_" for in and output,

 * "(free_)vector_" or "(free_)matrix_" for memory (de)allocation, 

 * (Ending on "_index" may be used for arbitrary starting and 
 * ending indices.)
 *  
 * In general, a function will return NULL on success or an error message 
 * will be returned if the function fails.
 *
 * To create a structure (and fill it with data) is done following:
 * \verbatim
struct = gpiv_* ()
\endverbatim 
 * or the function will return NULL if it fails.
 *
 * To modify the contents of an existing structure while calling a libgpiv function is done as:
 * \verbatim
gpiv_* (pointer to struct, ...)
\endverbatim
 *
 * To use the contents of an existing structure while calling a libgpiv function is done as:
 * \verbatim
gpiv_* (..., const pointer to struct, ...)
\endverbatim
 *
 *
 * To read data from a file into a structure is done following: 
 * \verbatim
struct = gpiv_read_* (FILE *fp, ...)
\endverbatim
 *
 * or if a file name is used:
 * \verbatim
struct = gpiv_fread_* (gchar *filename, ...)
\endverbatim
 *
 * Writing / storing the data of a structure to a file is done with:
 * \verbatim
gpiv_write_* (FILE *fp, struct, ...)
\endverbatim
 *
 * or with:
 * \verbatim
gpiv_fwrite_* (gchar *filename, struct, ...)
\endverbatim
 *
 *
 *
 * \section final_sec Final remarks
 *
 * We hope that the software will work fine and that you'll enjoy it
 * during using and developing. Remarks, suggestions and patches for
 * improvements/additions are welcome.
 */


#ifndef __LIBGPIV_H__
#define __LIBGPIV_H__

/*
 * Includes external library headers to be used in Libgpiv
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <math.h>
#include <gsl/gsl_fit.h>
#include <assert.h>

#include <time.h>
#include <fftw3.h>
#include <hdf5.h>
#include <glib-2.0/glib.h>
#include <png.h>

#ifdef ENABLE_CAM
#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>
#endif /* ENABLE_CAM */

#ifdef ENABLE_TRIG
#include <rtai.h>
#include <fcntl.h>
#endif /* ENABLE_TRIG */

#ifdef ENABLE_MPI
#include <mpi/mpi.h>
#endif /* ENABLE_MPI */

#ifdef _OPENMP
#include <omp.h>
#endif /* _OPENMP */

/*
 * General macro definitions
 */

#define USE_FFTW3

#define LIBNAME "LIBGPIV"	        /**< Library name */
#define GPIV_SYSTEM_RSC_FILE "gpiv.conf"/**< System wide parameter resources file */
#define GPIV_HOME_RSC_FILE ".gpivrc"	/**< home parameter resources file */
#define GPIV_NIMG_MAX 40                /**< maximum number of images to be retrieved */
#define GPIV_MAX_CHARS 80		/**< Maximum number of characters on a line */
#define GPIV_MAX_LINES 6400		/**< Maximum number of lines in a datafile */
#define GPIV_MAX_LINES_C 20		/**< Maximum number of comment lines in a datafile */
#define GPIV_MAX_IMG_SIZE 2000          /**< Maximum size of image */
#define GPIV_MAX_IMG_DEPTH 16           /**< Maximum image depth (in bits) */
#define GPIV_MIN_INTERR_SIZE 4	        /**< Minimum size of interrogation area */
#define GPIV_MAX_INTERR_SIZE 128	/**< Maximum size of interrogation area */
#define GPIV_NBINS_MAX 100		/**< maximum bins for histogram  */
#define GPIV_NBINS_DEFAULT 10           /**< Default number of nbins */
#define GPIV_SNR_NAN 99.0               /**< snr value for disabled estimator (tested on isnan) */

#define IA_GAUSS_WEIGHTING               /**< Gaus filtering of interrogation area */
#define SPOF_FILTERING                  /**< Symmetric Phase Only filtering */


#ifndef SYSTEM_RSC_DIR
#define SYSTEM_RSC_DIR "/etc"	/**< System wide resource directory */
#endif  /* SYSTEM_RSC_DIR */

/*
 * TODO in re-org branch:
__GpivBinData           .h .c
__GpivCamPar            .h
__GpivCamVar            .h
__GpivCovariance        .h .c
__GpivImage             .h .c
__GpivImagePar          .h .c
__GpivImageProcPar      .h .c
__GpivLinRegData        .h no .c
__GpivPivData           .h .c
__GpivPivPar            .h .c (git)
__GpivPostPar           .h .c mv post_par.c postpar.c (git!)
__GpivRoi               .h no .c
__GpivScalarData        .h .c mv post_utils.c scalardata.c (git!)
__GpivTrigPar           .h
__GpivTrigTime          .h
__GpivValidPar          .h .c mv valid_par.c validpar.c (git!)

mv lib/piv.c lib/piv_post.c (git!)
rm include/gpiv/gpiv_img-utils.h / .c (git!)
mv  lib/valid.c  lib/valid_proc.c (git)
mv lib/post.c lib/post_proc.c (git)
 *
 */

/*
 * Includes Libgpiv library headers
 */

#include <gpiv/gpiv-utils.h>
#include <gpiv/gpiv-utils_alloc.h>
#include <gpiv/gpiv-pivdata.h>
#include <gpiv/gpiv-scalardata.h>
#include <gpiv/gpiv-bindata.h>
/* #include <gpiv/gpiv-imagepar.h> in gpiv-image.h> */
#include <gpiv/gpiv-image.h>
#include <gpiv/gpiv-covariance.h>
#include <gpiv/gpiv-fastfourier.h>
#include <gpiv/gpiv-linregdata.h>

#include <gpiv/gpiv-genpar.h>
#include <gpiv/gpiv-imageprocpar.h>
#include <gpiv/gpiv-pivpar.h>
#include <gpiv/gpiv-validpar.h>
/* #include <gpiv/gpiv-roi.h> : in gpiv_post-par.h */
#include <gpiv/gpiv-postpar.h>

#ifdef ENABLE_CAM
#include <gpiv/gpiv-campar.h>
#include <gpiv/gpiv-camvar.h>
#endif /* ENABLE_CAM */

#ifdef ENABLE_TRIG
#include <gpiv/gpiv-trigtime.h>
#include <gpiv/gpiv-trigpar.h>
#endif /* ENABLE_TRIG */

#include <gpiv/gpiv-img_proc.h>
#include <gpiv/gpiv-piv_proc.h>
#include <gpiv/gpiv-valid_proc.h>
#include <gpiv/gpiv-post_proc.h>

#include <gpiv/gpiv-io.h>

#ifdef GPIV_ENABLE_DEPRECATED
#include <gpiv/back_intface.h>
#endif /* GPIV_ENABLE_DEPRECATED */


#endif /* __LIBGPIV_H__ */
