/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




 ------------------------------------------------------------------- */

/*!
\file                   gpiv-post_proc.h
\brief                  module for post-processing of PIV data

 */

#ifndef __LIBGPIV_POST_PROC_H__
#define __LIBGPIV_POST_PROC_H__


/**
 * Velocity component
 */
enum GpivVelComponent {
    GPIV_U,                    /**< Horizontal (x, column-wise */ 
    GPIV_V                     /**< Vertical (y, row-wise */
};



/**
 *     Piv post processing function to manipulate data; flipping rotating, etc.
 *
 *     @param[in] piv_data     PIV data to be manipulated in-place
 *     @param[in] post_par     post-processing parameters
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_post_manipiv			(GpivPivData		*piv_data,
					const GpivPostPar	*post_par
					);



/**
 *     Piv post processing function to calculate spatial mean, variances.
 *
 *     @param[in] piv_data     PIV data of which spatial average is calculated 
 *                             in-place
 *     @param[in] post_par     post-processing parameters
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_post_savg				(GpivPivData		*piv_data,
					const GpivPostPar	*post_par
					);



/**
 *     Calculating histogram of U (horizontal) or V (vertical) particle 
 *     displacements.
 *
 *     @param[in] piv_data      PIV data
 *     @param[in] nbins        number of bins that klass will contain
 *     @param[in] velcomp      velocity component from which histogram is calculated
 *     @return                 GpivBinData on success or NULL on failure
 *
 */
GpivBinData *
gpiv_post_uvhisto			(const GpivPivData	*piv_data, 
					const guint		nbins,
					const enum GpivVelComponent	velcomp
					);



/**
 *     Subtracts a specified quantity from the PIV displacements.
 *
 *     @param[in] piv_data     PIV data to be reduced in-place
 *     @param[in] z_off_dx     value in horizontal direction to be subtracted
 *     @param[in] z_off_dy     value in vertical direction to be subtracted
 *     @return                 NULL on success or error message on failure
 */
gchar * 
gpiv_post_subtract_dxdy			(GpivPivData		*piv_data,
					const gfloat		z_off_dx, 
					const gfloat		z_off_dy
					);



/**
 *     Piv post processing function to adjust for time and spatial scales.
 *
 *     @param[in] piv_data     PIV data to be scaled in-place
 *     @param[in] image_par    image parameter structure
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_post_scale				(GpivPivData		*piv_data,
					const GpivImagePar	*image_par
					);



/**
 *     Piv post processing function to adjust for inversed time and spatial scales
 *
 *     @param[in] piv_data      PIV data to be scaled in-place
 *     @param[in] image_par    image parameter structure
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_post_inverse_scale			(GpivPivData		*piv_data,
					const GpivImagePar	*image_par
					);



/**
 *     Piv post processing function to calculate vorticity and strain.
 *
 *     @param[in] piv_data     PIV data from which derivatives are calculated
 *     @param[in] post_par     post-processing parameters
 *     @return                 GpivScalarData containing vorticity or strain
 */
GpivScalarData *
gpiv_post_vorstra			(const GpivPivData	*piv_data,
					const GpivPostPar	*post_par
					);


#endif /* __LIBGPIV_POST_PROC_H__ */
