/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------


   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



			 
-----------------------------------------------------------------------------*/

/*!
\file                    gpiv-imagepar.h
\brief                   module for GpivImagePar: image header or parameters

 */

#ifndef __LIBGPIV_IMG_H__
#define __LIBGPIV_IMG_H__



#define GPIV_IMGPAR_DEFAULT__DEPTH     8	/**< Default parameter for depth of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__NCOLUMNS  256	/**< Default parameter for ncolumns of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__NROWS     256	/**< Default parameter for nrows of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__X_CORR    1	/**< Default parameter for x_corr of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__S_SCALE   1.0	/**< Default parameter for s_scale of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__T_SCALE   1.0	/**< Default parameter for t_scale of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__Z_OFF_X   0.0	/**< Default parameter for z_off_x of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__Z_OFF_Y   0.0	/**< Default parameter for z_off_y of __GpivImagePar */

#define GPIV_IMGPAR_DEFAULT__TITLE     ""	/**< Default parameter for title of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__CREATION_DATE ""	/**< Default parameter for creation_date of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__LOCATION  ""  	/**< Default parameter for location of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__AUTHOR    ""	/**< Default parameter for author of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__SOFTWARE  "gpiv / gpiv_rr"	     /**< Default parameter for software of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__SOURCE    ""	/**< Default parameter for source of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__USERTEXT  ""	/**< Default parameter for usertext of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__WARNING   ""	/**< Default parameter for warning of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__DISCLAIMER "See The GNU General Public License (GPL)"      /**< Default parameter for disclaimer of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__COMMENT   ""	/**< Default parameter for comment of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__COPYRIGHT ""	/**< Default parameter for copyright of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__EMAIL     ""	/**< Default parameter for email of __GpivImagePar */
#define GPIV_IMGPAR_DEFAULT__URL       ""	/**< Default parameter for url of __GpivImagePar */


#define GPIV_IMGPAR_KEY                "IMG"		/**< Key of image parameters */
#define GPIV_IMGPAR_KEY__DEPTH         "depth"          /**< Key for depth of __GpivImagePar */
#define GPIV_IMGPAR_KEY__NCOLUMNS      "ncolumns"       /**< Key for ncolumns of __GpivImagePar */
#define GPIV_IMGPAR_KEY__NROWS         "nrows"          /**< Key for nrows of __GpivImagePar */
#define GPIV_IMGPAR_KEY__X_CORR        "x_corr"         /**< Key for x_corr of __GpivImagePar */
#define GPIV_IMGPAR_KEY__S_SCALE       "s_scale"        /**< Key for s_scale of __GpivImagePar */
#define GPIV_IMGPAR_KEY__T_SCALE       "t_scale"        /**< Key for t_scale of __GpivImagePar */
#define GPIV_IMGPAR_KEY__Z_OFF_X       "zoff_x"         /**< Key for z_off_x of __GpivImagePar */
#define GPIV_IMGPAR_KEY__Z_OFF_Y       "zoff_y"         /**< Key for z_off_y of __GpivImagePar */

#define GPIV_IMGPAR_KEY__TITLE         "title"          /**< Key for title of __GpivImagePar */
#define GPIV_IMGPAR_KEY__CREATION_DATE "creation_date"  /**< Key for creation_date of __GpivImagePar */
#define GPIV_IMGPAR_KEY__LOCATION      "location"       /**< Key for location of __GpivImagePar */
#define GPIV_IMGPAR_KEY__AUTHOR        "author"         /**< Key for author of __GpivImagePar */
#define GPIV_IMGPAR_KEY__SOFTWARE      "software"       /**< Key for software of __GpivImagePar */
#define GPIV_IMGPAR_KEY__SOURCE        "source"         /**< Key for source of __GpivImagePar */
#define GPIV_IMGPAR_KEY__USERTEXT      "usertext"       /**< Key for usertext of __GpivImagePar */
#define GPIV_IMGPAR_KEY__WARNING       "warning"        /**< Key for warning of __GpivImagePar */
#define GPIV_IMGPAR_KEY__DISCLAIMER    "disclaimer"     /**< Key for disclaimer of __GpivImagePar */
#define GPIV_IMGPAR_KEY__COMMENT       "comment"        /**< Key for comment of __GpivImagePar */
#define GPIV_IMGPAR_KEY__COPYRIGHT     "copyright"      /**< Key for copyright of __GpivImagePar */
#define GPIV_IMGPAR_KEY__EMAIL         "email"          /**< Key for email of __GpivImagePar */
#define GPIV_IMGPAR_KEY__URL           "url"            /**< Key for url of __GpivImagePar */

typedef struct __GpivImagePar GpivImagePar;

/*!
 * \brief Image parameters or header info
 *
 * Some of these parameters are required to be resident in the image header. 
 * Else, they might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
 */
struct __GpivImagePar {
    guint depth;                        /**< Image color depth */
    gboolean depth__set;                /**< flag if depth has been defined */

    guint ncolumns;                     /**< Number of image columns */
    gboolean ncolumns__set;             /**< flag if ncolumns has been defined */

    guint nrows;                        /**< Number of image rows */
    gboolean nrows__set;                /**< flag if nrows has been defined */

    gboolean x_corr;                    /**< cross-correlation image pair */
    gboolean x_corr__set;               /**< flag if x_corr has been defined */

    gfloat s_scale;                     /**< spatial scale, used by gpiv_post_scale() */
    gboolean s_scale__set;              /**< flag if s_scale has been defined */

    gfloat t_scale;                     /**< time-scale, used by gpiv_post_scale() */
    gboolean t_scale__set;              /**< flag if t_scale has been defined */

    gfloat z_off_x;                     /**< zero offset in x (column) direction, used by gpiv_post_scale() */
    gboolean z_off_x__set;              /**< flag if z_off_x has been defined */

    gfloat z_off_y;                     /**< zero offset in y (row) direction, used by gpiv_post_scale() */
    gboolean z_off_y__set;              /**< flag if z_off_y has been defined */

    gchar title[GPIV_MAX_CHARS];        /**< Project name */
    gboolean title__set;                /**< flag if title has been defined */

    gchar creation_date[GPIV_MAX_CHARS];/**< Date of origin */
    gboolean creation_date__set;        /**< flag if creation_date has been defined */

    gchar location[GPIV_MAX_CHARS];     /**< Location of origin */
    gboolean location__set;             /**< flag if location has been defined */

    gchar author[GPIV_MAX_CHARS];       /**< Author name */
    gboolean author__set;               /**< flag if author has been defined */

    gchar software[GPIV_MAX_CHARS];     /**< Program that generated the image */
    gboolean software__set;             /**< flag if software has been defined */

    gchar source[GPIV_MAX_CHARS];       /**< Camera name and type */
    gboolean source__set;               /**< flag if source has been defined */

    gchar usertext[GPIV_MAX_CHARS];     /**< User comment */
    gboolean usertext__set;             /**< flag if usertext has been defined */

    gchar warning[GPIV_MAX_CHARS];      /**< Warning message */
    gboolean warning__set;              /**< flag if warning has been defined */

    gchar disclaimer[GPIV_MAX_CHARS];   /**< Disclaimer or licence text */
    gboolean disclaimer__set;           /**< flag if disclaimer has been defined */

    gchar comment[GPIV_MAX_CHARS];      /**< Comment from other image formats */
    gboolean comment__set;              /**< flag if comment has been defined */

    gchar copyright[GPIV_MAX_CHARS];    /**< Copyright text */
    gboolean copyright__set;            /**< flag if copyright has been defined */

    gchar email[GPIV_MAX_CHARS];        /**< E-mail author */
    gboolean email__set;                /**< flag if email has been defined */

    gchar url[GPIV_MAX_CHARS];          /**< URL of program, author, image and/or data */
    gboolean url__set;                  /**< flag if url has been defined */
};



/*
 * Function prototypes
 */

/**
 *      Sets flag for image_par__set
 *
 *      @param[in] flag         flag to enforce parameters set to defaults
 *      @param[out] image_par   image parameters
 *      @return                 void
 */
void
gpiv_img_parameters_set                 (GpivImagePar           *image_par,
                                         const gboolean         flag
                                         );



/**
 *      Reads image parameters from localrc, $HOME/.gpivrc and system-wide 
 *      gpiv.conf 
 *
 *      @param[in] localrc      resource filename containing parameter at 
 *                              current directory
 *      @param[in] verbose      prints parameter values when read
 *      @return                 GpivImagePar or NULL on failure
 */
GpivImagePar *
gpiv_img_get_parameters_from_resources  (const gchar		*localrc,
                                         const gboolean		verbose
                                         );


/**
 *      Sets default parameter values
 *
 *      @param[in] force        flag to enforce parameters set to defaults
 *      @param[out] image_par   image parameters
 *      @return                 void
 */
void
gpiv_img_default_parameters		(GpivImagePar		*image_par,
                                         const gboolean		force
                                         );



/**
 *      Reads each line of file fp and looks for image header parameters 
 *
 *      @param[in] fp                    file pointer to header or paremeter file.
 *                                       If NULL, stdin will be used.
 *      @param[in] fp_par_out            file pointer of which parameter will have to be printed
 *                                       or NULL for stdout
 *      @param[in] verbose               flag to print parameters to fp_par_out (TRUE) or not (FALSE)
 *      @param[in] include_key           flag if module key is included
 *      @param[out] image_par            image header parameters
 *      @return                          void
 */
void
gpiv_img_read_header			(FILE			*fp,
					GpivImagePar		*image_par,
					const gboolean		include_key,
					const gboolean		verbose,
					FILE			*fp_par_out
					);



/**
 *      Checks out if all image header info has been read
 *
 *      @param[in] image_par     image parameters
 *      @return                  NULL on success or error message on failure
 */
gchar *
gpiv_img_check_header_read		(const GpivImagePar     *image_par
					);



/**
 *      Checks out if the required image header info for reading/storing
 *      has been read
 *
 *      @param[in] image_par    image parameters
 *      @return                 NULL on success or error message on failure
 *
 */
gchar *
gpiv_img_check_header_required_read     (const GpivImagePar     *image_par
					);



/**
 *      Checks out if image header info for time/spatial scaling has been read
 *
 *      @param[in] image_par    image parameters
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_img_check_header_scale_read        (const GpivImagePar     *image_par
					);


/**
 *      Checks out if all header parameters have been read
 *
 *      @param[in] image_par    image parameters
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_img_test_header			(const GpivImagePar	*image_par
					);


/**
 *      Prints header information to file fp. Identic to 
 *      gpiv_img_print_parameters(), but without GPIV_IMGPAR_KEY
 *
 *      @param[in] fp           file pointer. If NULL, stdout will be used
 *      @param[in] image_par    image parameters
 *      @return                 void
 */
void 
gpiv_img_print_header			(FILE			*fp,
					const GpivImagePar	*image_par
					);


/**
 *      Prints image header parameters to file. Identic to 
 *      gpiv_img_fprint_parameters(), but without GPIV_IMGPAR_KEY
 *
 *      @param[in] fname        header filename
 *      @param[in] image_par    image parameters
 *      @return                 void
 */
void 
gpiv_img_fprint_header			(const gchar		*fname,
					const GpivImagePar	*image_par
					);


/**
 *      Prints image header or parameters to fp. Idententic to
 *      gpiv_img_print_header(), but includes IMAGE_PAR_KEY
 *
 *      @param[in] fp           file pointer. If NULL, stdout will be used
 *      @param[in] image_par    image parameters
 *      @return                 void
 */
void
gpiv_img_print_parameters		(FILE                   *fp, 
					const GpivImagePar	*image_par
					);


/**
 *      Prints image header or parameters to file. Idententic to
 *      gpiv_img_fprint_header(), but includes IMAGE_PAR_KEY
 *
 *      @param[in] fname        header filename
 *      @param[in] image_par    image parameters
 *      @return                 void
 */
void 
gpiv_img_fprint_parameters		(const gchar            *fname,
					const GpivImagePar	*image_par
					);


/**
 *      Generate a new copy of image parameters from image_par if 
 *      image_par_dest have not been set
 *
 *      @param[in] image_par    source image parameters to be copied
 *      @return                 GpivImagePar
 */
GpivImagePar *
gpiv_img_cp_parameters			(const GpivImagePar	*image_par
					);


/**
 *      Overwrites image parameters from image_par_src to image_par_dest
 *
 *      @param[in] image_par_src        source image parameters to be copied
 *      @param[in] image_par_dest       the copied image parameters
 *      @return                         void
 */
void
gpiv_img_ovwrt_parameters		(const GpivImagePar	*image_par_src,
					GpivImagePar		*image_par_dest
					);


/**
 *      Reads image parameters from hdf5 data file without IMAGE_PAR_KEY
 *
 *      @param[in] fname        filename
 *      @return                 GpivImagePar or NULL on failure
 */
GpivImagePar *
gpiv_img_fread_hdf5_parameters		(const gchar		*fname
					);


/**
 *      Writes image parameters to an existing hdf5 data file
 *
 *      @param[in] fname        filename
 *      @param[in] image_par    image parameters
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_img_fwrite_hdf5_parameters		(const gchar            *fname,
					const GpivImagePar	*image_par
					);



/**
 *      Reads image specifications from Davis formatted image, with ext .IMG, 
 *      from file
 *
 *      @param[in] fname        filename
 *      @param[out] image_par   image parameters
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_img_fread_davis_parameters		(const gchar		*fname,
					GpivImagePar		*image_par
					);

#ifdef ENABLE_MPI
/**
 *      Broadcasts only required parameters of GpivImagePar for MPI
 *
 *      @param[in] image_par   image parameters
 */
void
gpiv_img_mpi_bcast_imgpar		(GpivImagePar		*image_par
					);

#endif /* ENABLE_MPI */
#endif /* __LIBGPIV_IMG_H__ */

