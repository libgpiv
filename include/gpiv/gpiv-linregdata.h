/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-----------------------------------------------------------------------------*/

/*!
\file                   gpiv-linregdata.h
\brief                  module for GpivLinRegData: used for validating PIV data

*/

#ifndef __LIBGPIV_LINREGDATA_H__
#define __LIBGPIV_LINREGDATA_H__


typedef struct __GpivLinRegData GpivLinRegData;
/*!
 * \brief Data structure of linear regression data
 */
struct __GpivLinRegData {       
    double c0;                  /**< zero order constant */
    double c1;                  /**< first order constant */
    double cov00;               /**< variance of zero order constant */
    double cov01;               /**< correlation bewteen first and 2nd const. */
    double cov11;               /**< variance of zero order constant */
    double sumsq;               /**< sum of squared residuals */
};




#endif /* __LIBGPIV_LINREGDATA_H__ */
