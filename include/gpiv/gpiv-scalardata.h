/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

  libgpiv - library for Particle Image Velocimetry

  Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2011
  Gerber van der Graaf

  This file is part of libgpiv.

  Libgpiv is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

 -----------------------------------------------------------------------------*/


/*!
\file                   gpiv-scalardata.h
\brief                  module for GpivScalarData: scalars of image evaluation

 */


#ifndef __LIBGPIV_SCALARDATA_H__
#define __LIBGPIV_SCALARDATA_H__


#define GPIV_SCALAR_FMT "%4.0f %4.0f %12.3f     %2d\n"	        /**< Scalar data Format with PIV data positions */
#define GPIV_SCALAR_S_FMT "%-12.3f %-12.3f %-12.3f     %-2d\n"	/**< Scalar data Format with PIV scaled data positions */


typedef struct __GpivScalarData GpivScalarData;
/*!
 * \brief Holds scalar data, mostly derived from  
 * __GpivPivData 
 */
struct  __GpivScalarData {
    guint nx;			/**< number of data in x-direction */
    guint ny;			/**< number of data in y-direction */


    gfloat **point_x;		/**< position of data point in x-direction */
    gfloat **point_y;		/**< position of data point in y-direction */
    gfloat **scalar;		/**< scalar value representing vorticity in z-direction, shear strain or 
                                   normal strain */
    gint **flag;		/**< a flag; always nice, used for whatever it may be (disable data) */


    gboolean scale;             /**< flag for scaled estimators */
    gboolean scale__set;        /**< flag if scale has been defined */

    gchar *comment;             /**< comment on the data */
};



/**
 *      Sets all elements of sc_data structure to NULL.
 *
 *      @param[in] scal_data    Input Scalar data structure
 *      @param[out] scal_data   Output Scalar data structure
 *      @return                 void
 */
void
gpiv_null_scdata                        (GpivScalarData         *scal_data
                                         );


/**
 *      Allocates memory for GpivScalarData.
 *
 *      @param[in] nx           columns array length of GpivScalarData
 *      @param[in] ny           row array length of GpivScalarData structure
 *      @return                 GpivScalarData or NULL on failure
 */
GpivScalarData *
gpiv_alloc_scdata                       (const gint             nx,
                                         const gint             ny
                                         );


/**
 *      Frees memory for GpivScalarData.
 *
 *      @param[in] scal_data    scalar data structure
 *      @param[out] scal_data   NULL pointer to point_x, point_y, scalar, flag
 *      @return                 void
 */
void 
gpiv_free_scdata                        (GpivScalarData         *scal_data
                                         );



/**
 *      Graphical output with gnuplot for scalar data.
 *
 *      @param[in] fname_out               file name containing plot
 *      @param[in] title                   title of plot
 *      @param[in] GNUPLOT_DISPLAY_COLOR   display color of window containing graph
 *      @param[in] GNUPLOT_DISPLAY_SIZE    display size of window containing graph
 *      @return                            NULL on success or error message on failure
 */
gchar *
gpiv_scalar_gnuplot                     (const gchar            *fname_out, 
                                         const gchar            *title, 
                                         const gchar            *GNUPLOT_DISPLAY_COLOR, 
                                         const gint             GNUPLOT_DISPLAY_SIZE
                                         );


/**
 *      Reads scalar data from file fname.
 *
 *      @param[in] fname        complete input filename.
 *      @return                 GpivScalarData or NULL on failure
 */
GpivScalarData *
gpiv_fread_scdata                       (const gchar            *fname
                                         );



/**
 *      Writes scalar data to file fname in ASCII format.
 *
 *      @param[in] scalar_data  scalar data
 *      @param[in] fname        complete output filename.
 *      @param[in] free         flag if allocated memory if piv_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_scdata                      (const gchar            *fname,
                                         GpivScalarData         *scalar_data,
                                         const gboolean         free
                                         );



/**
 *      Reads scalar data form ascii file.
 *
 *      @param[in] fp           input file. If NULL, stin is used.
 *      @return                 GpivScalarData or NULL on failure
 */
GpivScalarData *
gpiv_read_scdata                        (FILE                   *fp
                                         );



/**
 *      Writes scalar data to file.
 *
 *      @param[in] fp           output file. If NULL, stout is used.
 *      @param[in] scalar_data  scalar data containing all variables in a datafile
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_scdata                       (FILE                   *fp,
                                         GpivScalarData	        *scalar_data,
                                         const gboolean         free
                                         );



/**
 *      Writes scalar data to file in grid format for gnuplot.
 *
 *      @param[in] fp           output file. If NULL, stdout is used.
 *      @param[in] scalar_data  scalar data containing all variables in a datafile
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_sc_griddata                  (FILE                   *fp,
                                         GpivScalarData         *scalar_data,
                                         const gboolean         free
                                         );



/**
 *      Writes scalar data to file in grid format for plotmtv.
 *
 *      @param[in] fp           output file. If NULL, stdout is used.
 *      @param[in] scalar_data  scalar data containing all variables in a datafile
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_sc_mtvgriddata               (FILE                   *fp, 
                                         GpivScalarData         *scalar_data, 
                                         const gboolean         free
                                         );


/**
 *      Reads SCALAR data from a hdf5 data file.
 *
 *      @param[in] fname         pointer to complete filename
 *      @param[in] DATA_KEY      key to specify data type/origin (NORMAL_STRAIN, ...)
 *      @return                  GpivScalarData or NULL on failure
 */
GpivScalarData *
gpiv_fread_hdf5_scdata                  (const gchar            *fname,
                                         const gchar            *DATA_KEY
                                         );


/**
 *      Writes SCALAR data to a file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] scalar_data  struct GpivScalarData containing all variables in a datafile
 *      @param[in] DATA_KEY     identifier of data type
 *      @param[in] free         boolean whether to free memory of SCALAR data
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_scdata                 (const gchar            *fname, 
                                         GpivScalarData         *scalar_data, 
                                         const gchar            *DATA_KEY,
                                         const gboolean         free
                                         );



#endif /* __LIBGPIV_SCALARDATA_H__ */
