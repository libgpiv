/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/


/*!
\file                   gpiv-bindata.h
\brief                  module for GpivBinData: data for histograms

 */


#ifndef __LIBGPIV_BINDATA_H__
#define __LIBGPIV_BINDATA_H__


typedef struct __GpivBinData GpivBinData;
/*!
 * \brief Used for making up histograms in bins or klasses
 *
 * This structure provides elements to construct histograms for statistics, 
 * validation (peak-locking effects, for example) etc.
 */
struct __GpivBinData {
    guint nbins;		/**< number of bins in histogram, to be read as a parameter */


    guint *count;		/**< value containing number of occurences at bin# */
    gfloat *bound;		/**< lower boundary of bin# */
    gfloat *centre;		/**< centre point of bin# */
    gfloat min;		        /**< minimum value of input data */
    gfloat max;		        /**< maximum value of input data */


    gchar *comment;             /**< comment on the data */
};



/**
 *      Sets all elements of bin_data structure to NULL.
 *
 *      @param[in] bin_data     Input Bin data structure
 *      @param[out] bin_data    Output Bin data structure
 *      @return                 void
 */
void
gpiv_null_bindata			(GpivBinData		*bin_data
					);


/**
 *      Allocates memory for GpivBinData.
 *
 *      @param[in] nbins        number of bins or klasses in GpivBinData
 *      @return                 GpivBinData on success or NULL on failure
 */
GpivBinData *
gpiv_alloc_bindata			(const guint		nbins
					);


/**
 *      Frees memory for GpivBinData.
 *
 *      @param[in] bin_data     data of bins
 *      @param[out] bin_data    NULL pointer to count, bound, centre
 *      @return                 void
 */
void
gpiv_free_bindata			(GpivBinData		*bin_data
					);


/**
 *       Wrapper for gpiv_read_bindata.
 *      DEPRECATED
 *
 *      @param[in] fp           input file.
 *      @return    bin_data     data containing the values for each bin 
 */
GpivBinData *
gpiv_read_histo                         (FILE *fp
                                         );


/**
 *      Writes bins data to file in histogram format.
 *      DEPRECATED
 *
 *      @param[in] fp           output file. If NULL, stdout is used.
 *      @param[in] bin_data     data containing the values for each bin
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_print_histo			(FILE			*fp, 
                                         GpivBinData		*bin_data, 
                                         const gboolean		free
                                         );

/**
 *      Reads bins data (bins of a histogram) from file in histogram format.
 *
 *      @param[in] fp           input file. If NULL, stdout is used.
 be freed
 *      @return    bin_data     data containing the values for each bin 
 */
GpivBinData *
gpiv_read_bindata                       (FILE                   *fp
                                         );


/**
 *      Reads bins data (bins of a histogram) from file in histogram format.
 *
 *      @param[in] fname        file name
 be freed
 *      @return    bin_data     data containing the values for each bin 
 */
GpivBinData *
gpiv_fread_bindata                      (const gchar            *fname
                                         );


/**
 *      Writes bins data to file in histogram format.
 *
 *      @param[in] fp           output file. If NULL, stdout is used.
 *      @param[in] bin_data     data containing the values for each bin
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_bindata			(FILE			*fp, 
                                         GpivBinData		*bin_data, 
                                         const gboolean		free
                                         );


/**
 *      Writes bins data to file in histogram format.
 *
 *      @param[in] fname        complete output filename.
 *      @param[in] bin_data     data containing the values for each bin
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_bindata                    (const gchar             *fname, 
                                        GpivBinData             *bin_data, 
                                        const gboolean          free
                                       );

/**
 *      Writing cumulative histogram data with an equal number of date per bin 
 *      or klass to fp. Special output for validation; work around to print
 *      float data as y-values.
 *
 *      @param[in] fp           output file
 *      @param[in] klass        struct GpivBinData containing the values for each bin
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_print_cumhisto_eqdatbin		(FILE 			*fp,
                                         GpivBinData 		*bin_data,
                                         const gboolean		free
                                         );

/**
 *      Reads histogram data to ouput file in hdf version 5 format.
 *      Discouraged. Better to use gpiv_fread_hdf5_bindata instead
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] klass        struct bin containing the values for each bin
 *      @param[in] DATA_KEY     identifier of data type (PEAK_LOCK, RESIDUS, ...)
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fread_hdf5_histo			(const gchar		*fname, 
                                         GpivBinData		*bin_data, 
                                         const gchar		*DATA_KEY
                                         );


/**
 *      Writes histogram data to ouput file in hdf version 5 format.
 *      Deprecated. Use gpiv_fwrite_hdf5_bindata instead
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] klass        struct bin containing the values for each bin
 *      @param[in] DATA_KEY     identifier of data type (PEAK_LOCK, RESIDUS, ...)
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_histo			(const gchar            *fname, 
                                         const GpivBinData      *bin_data, 
                                         const gchar            *DATA_KEY
                                         );


/**
 *      Reads bins data to ouput file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] klass        struct bin containing the values for each bin
 *      @param[in] DATA_KEY     identifier of data type (PEAK_LOCK, RESIDUS, ...)
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fread_hdf5_bindata			(const gchar		*fname, 
                                         GpivBinData		*bin_data, 
                                         const gchar		*DATA_KEY
                                         );


/**
 *      Writes histogram data to ouput file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] klass        struct bin containing the values for each bin
 *      @param[in] DATA_KEY     identifier of data type (PEAK_LOCK, RESIDUS, ...)
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_bindata		(const gchar            *fname, 
                                         const GpivBinData      *bin_data, 
                                         const gchar            *DATA_KEY
                                         );


#endif /* __LIBGPIV_BINDATA_H__ */
