/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */
/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




 --------------------------------------------------------------------------- */
/**
\file                   gpiv-utils.h
\brief                  miscellaneous utilities

 */

#ifndef __LIBGPIV_UTILS_H__
#define __LIBGPIV_UTILS_H__




#define GPIV_FAIL_INT -914

/**
 *      Reads parameters from local parameter file PARFILE
 *
 *     @param[in] PAR_KEY      Parameter key, specific for each process
 *     @param[in] parfile      Parameter file, specific for each process
 *     @param[in] verbose      flag for printing parameters
 *     @param[out] pstruct     pointer to parameter structure
 *     @return                 void
 */ 
void
gpiv_scan_parameter			(const gchar		*PAR_KEY, 
					const gchar		*parfile,
					void			*pstruct,
					gboolean		verbose
					);



/**
 *     Opens resource files GPIV_HOME_RSC_FILE (hidden) and 
 *     SYSTEM_RSC_DIR, GPIV_SYSTEM_RSC_FILE
 *     Reads parameters from it
 *
 *     @param[in] PAR_KEY      Parameter key, specific for each process
 *     @param[in] verbose      parameter to print to stdout
 *     @param[out] pstruct     pointer to parameter structure
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_scan_resourcefiles			(const gchar		*PAR_KEY, 
					void			*pstruct,
					gint			verbose
					);


/**
 *     Adds date and timesatmp to the comment for ASCII-formatted data
 *
 *     @param[in] comment      character string
 *     @return                 comment including date and time on success 
 *                             or NULL on failure
 */
gchar *
gpiv_add_datetime_to_comment		(gchar			*comment
					);



/**
 *      Orders array arr AND its belonging arrays arr_2 and arr_3
 *      Also swaps accompanying arrays
 *
 *      @param[in] n 	        length of array
 *      @param[in] arr 	        array to be sorted
 *      @param[in] arr_2 	second belonging array to arr
 *      @param[in] arr_3 	third belonging array to arr
 *      @return NULL on success or error message on failure
 */
gchar *
gpiv_sort_3				(const unsigned long	n,
					gfloat			arr[],
					gfloat			arr_2[],
					gfloat			arr_3[]
					);


/**
 *     Calculates maximum of longs a and b
 *
 *     @param[in] a            first variable to be tested
 *     @param[in] b            second variable to be tested
 *     @return                 result of maximum
 */
long 
gpiv_lmax				(long			a,
					long			b
					);



/**
 *     Calculates minimum of longs a and b
 *
 *     @param[in] a            first variable to be tested
 *     @param[in] b            second variable to be tested
 *     @return                 result of minimum
 */
long 
gpiv_lmin				(long			a,
					long			b
					);



/**
 *     Calculates maximum of integers a and b
 *
 *     @param[in] a            first variable to be tested
 *     @param[in] b            second variable to be tested
 *     @return                 result of maximum
 */
gint 
gpiv_max				(gint			a,
					gint			b
					);



/**
 *     Calculates minimum of integers a and b
 *
 *     @param[in] a            first variable to be tested
 *     @param[in] b            second variable to be tested
 *     @return                 result of minimum
 */
gint 
gpiv_min				(gint			a,
					gint			b
					);



/**
 *     Prints warning message with variable argument list
 *
 *     @param[in] message      warning message
 *     @return                 void
 */
void 
gpiv_warning				(gchar			*message, ...
					);



/**
 *     Prints error handling with variable argument list to stdout 
 *     and exits program
 *
 *     @param[in] message      warning message
 *     @return                 void
 */
void 
gpiv_error				(gchar			*message, ...
					);



/**
 *	Scans line string on **integer** parameter key
 *      and value or image header value (without program key). The
 *      value to be read is on the next line (_nl) of the key.
 *
 *      @param[in] fp_h            file pointer of header to read the data from
 *      @param[in] MOD_KEY          Module key as part of parameter key
 *      @param[in] PAR_KEY             parameter key
 *      @param[in] use_mod_key        flag to use program key
 *      @param[in] line            line to be scanned
 *      @param[in] par_name        parameter name which is scanned in line
 *      @param[out] parameter       parameter value to be returned
 *      @param[in] verbose       flag to print prarameter to stdout
 *      @param[in] fp           file pointer to print parameter to
 *
 *      @return flag representing parameter__set.
 *      Set to TRUE if parameter has been read, else FALSE
 */
gboolean
gpiv_fscan_iph_nl			(FILE			*fp_h, 
					const gchar		*MOD_KEY, 
					const gchar		*PAR_KEY, 
					const gboolean		use_mod_key,
					gchar			*line, 
					const gchar		*par_name, 
					gint			*parameter, 
					const gboolean		verbose,
					FILE			*fp
					);

/**
 *      Scans line string on **int** parameter key
 *      and value or image header value (without program key). Prints result
 *      to file.
 *
 *      @param[in] MOD_KEY      module key as part of parameter key
 *      @param[in] PAR_KEY      parameter key
 *      @param[in] use_mod_key  flag to use module key
 *      @param[in] line         line to be scanned
 *      @param[in] par_name     parameter name which is scanned in line
 *      @param[out] parameter   parameter value to be returned
 *      @param[in] verbose      flag to print prarameter to stdout
 *      @param[in] fp           file pointer to print parameter to
 *      @return                 flag representing parameter__set.
 *      Set to TRUE if parameter has been read, else FALSE
 */
gboolean
gpiv_scan_iph				(const gchar		*MOD_KEY,
					const gchar		*PAR_KEY,
					const gboolean		use_mod_key,
					const gchar		*line,
					const gchar		*par_name,
					gint			*parameter,
					const gboolean		verbose,
					FILE			*fp
					);



/**
 *      Scans line string on **char** parameter key
 *      and value or image header value (without program key). Prints result
 *      to file.
 *
 *      @param[in] MOD_KEY      module key as part of parameter key
 *      @param[in] PAR_KEY      parameter key
 *      @param[in] use_mod_key  flag to use module key
 *      @param[in] line         line to be scanned
 *      @param[in] par_name     parameter name which is scanned in line
 *      @param[out] parameter   parameter value to be returned
 *      @param[in] verbose      flag to print prarameter to stdout
 *      @param[in] fp           file pointer to print parameter to
 *      @return                 flag representing parameter__set.
 *      Set to TRUE if parameter has been read, else FALSE
 */
gboolean
gpiv_scan_cph				(const gchar		*MOD_KEY,
					const gchar		*PAR_KEY,
					const gboolean		use_mod_key,
					const gchar		*line,
					const gchar		*par_name,
					gchar			*parameter,
					const gboolean		verbose,
					FILE			*fp
					);



/**
 *     Scans line string on **gfloat** parameter/header
 *     key and value or image header value (without program key). Prints result
 *      to file.
 *
 *      @param[in] MOD_KEY      module key as part of parameter key
 *      @param[in] PAR_KEY      parameter key
 *      @param[in] use_mod_key  flag to use module key
 *      @param[in] line         line to be scanned
 *      @param[in] par_name     parameter name which is scanned in line
 *      @param[out] parameter   parameter value to be returned
 *      @param[in] verbose      flag to print prarameter to stdout
 *      @param[in] fp           file pointer to print parameter to
 *      @return                 flag representing parameter__set.
 *      Set to TRUE if parameter has been read, else FALSE
 */
gboolean
gpiv_scan_fph				(const gchar		*MOD_KEY,
					const gchar		*PAR_KEY,
					const gboolean		use_mod_key,
					const gchar		*line,
					const gchar		*par_name,
					gfloat			*parameter,
					const gboolean		verbose,
					FILE			*fp
					);



/**
 *      Scans line string on **string** parameter key
 *      and value or image header value (without program key). Prints result
 *      to file.
 *
 *      @param[in] MOD_KEY      module key as part of parameter key
 *      @param[in] PAR_KEY      parameter key
 *      @param[in] use_mod_key  flag to use module key
 *      @param[in] line         line to be scanned
 *      @param[in] par_name     parameter name which is scanned in line
 *      @param[out] parameter   parameter value to be returned
 *      @param[in] verbose      flag to print prarameter to stdout
 *      @param[in] fp           file pointer to print parameter to
 *      @return                 flag representing parameter__set.
 *      Set to TRUE if parameter has been read, else FALSE
 */
gboolean
gpiv_scan_sph				(const gchar		*MOD_KEY,
					const gchar		*PAR_KEY,
					const gboolean		use_mod_key,
					const gchar		*line,
					const gchar		*par_name,
					gchar			*parameter,
					const gboolean		verbose,
					FILE			*fp
					);



#endif /* __LIBGPIV_UTILS_H__ */
