/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

/*!
\file                  gpiv-imageprocpar.h
\brief                 module for GpivImageProcPar: parameters for image processing

*/


#ifndef __LIBGPIV_IMAGEPROCPAR_H__
#define __LIBGPIV_IMAGEPROCPAR_H__


#define GPIV_IMGPROCPAR_MAX__THRESHOLD 255
#define GPIV_IMGPROCPAR_MAX__WINDOW 50

#define GPIV_IMGPROCPAR_KEY "IMGPROC"       /**< Key of image processing parameters */

/**
 * Image filter type
 */
enum  GpivImgFilter {
    GPIV_IMGFI_MKTESTIMG,       /**< Generate test image */
    GPIV_IMGFI_SUBTRACT,        /**< Subtract image */
    GPIV_IMGFI_SMOOTH,          /**< Smooth filter */
    GPIV_IMGFI_HILO,            /**< High-low filter */
    GPIV_IMGFI_CLIP,            /**< Clipping */

    GPIV_IMGFI_FFT,             /**< FFT image */
    GPIV_IMGFI_INVFFT,          /**< Inverse FFT image */
    GPIV_IMGFI_CORR,            /**< Correlate */
    GPIV_IMGFI_CONV,            /**< Convolve */
    GPIV_IMGFI_LOWPASS,         /**< Low pass filter **/
    GPIV_IMGFI_HIGHPASS,        /**< High pass filter */

    GPIV_IMGFI_GETBIT  /**< Pointer operation to get the N least significant bits */
};

/**
 * Operator type when smoothing is performed
 */
enum GpivImgSmoothOperator {
    GPIV_IMGOP_EQUAL,    /**< No action */
    GPIV_IMGOP_SUBTRACT, /**< Subtract average */
    GPIV_IMGOP_ADD,      /**< Add avarage */
    GPIV_IMGOP_MULTIPLY, /**< Multiply with avarage */
    GPIV_IMGOP_DIVIDE    /**< Divide by avarage */
};


/* typedef struct __GpivImageProcPar GpivImageProcPar; */

/*!
 * \brief Image processing parameters
 *
 * Though there are many and excellent (free) image processing programs available, 
 * some image processes have been included in this library that are more 
 * specific for PIV purposes and cannot be found in general image processing
 * programs. 
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
 */
typedef struct __GpivImageProcPar {
    guint bit;                  /**< bit number */
    gboolean bit__set;          /**< flag if has bit been defined */

    enum GpivImgFilter filter;  /**< Filter type */
    gboolean filter__set;       /**< flag if filter has been defined */

    enum GpivImgSmoothOperator smooth_operator; /**< Type of sooting operaor */
    gboolean smooth_operator__set;              /**< flag smooth_operator if has been defined */

    guint window;               /**< geographic window in pixels */
    gboolean window__set;       /**< flag if window has been defined */

    guint threshold;            /**< minimum or maximum (intensity) value */
    gboolean threshold__set;    /**< flag if threshold has been defined */

    gchar fname[GPIV_MAX_CHARS]; /**< (image) filename for processing (like subtracting) */
    gboolean fname__set;         /**< flag if fname has been defined */
} GpivImageProcPar;




/**
 *      Sets flag for image_proc_par__set
 *
 *      @param[in] flag			flag to enforce parameters set to defaults
 *      @param[out] image_proc_par	image processing parameters
 *      @return				void
 */
void
gpiv_imgproc_parameters_set            (GpivImageProcPar       *image_proc_par,
                                        const gboolean         flag
                                        );



/**
 *     Sets default parameter values
 *
 *     @param[in] force                 flag to enforce parameters set to defaults
 *     @param[out] imgproc_par_default  structure of image processing parameters
 *     @return                          void
 */
void
gpiv_imgproc_default_parameters         (GpivImageProcPar       *image_proc_par,
                                        const gboolean         force
                                        );



/**
 *     Reads image processing parameters
 *
 *     @param[in] fp_h                 file to be read
 *     @param[in] image_proc_par       pointer to structure of image processing parameters
 *     @param[in] print_par            prints parameters to stdout during reading
 *     @param[out] image_proc_par      pointer to structure of image processing parameters
 *     @return void
 */
void
gpiv_imgproc_read_parameters            (FILE                   *fp_h, 
                                        GpivImageProcPar	*image_proc_par, 
                                        const gboolean		print_par
                                        );



/**
 *      Checks if all necessary image processing parameters have been read.
 *      If a parameter has not been read, it will be set to image_proc_par_default or to 
 *	its hard-coded default value in case image_proc_par_default is NULL.
 *
 *     @param[in] image_proc_par_default      image processing parameters containing default values
 *     @param[out] image_proc_par             structure of image processing parameters
 *     @return                                NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_check_parameters_read      (GpivImageProcPar	*image_proc_par,
                                        const GpivImageProcPar	*image_proc_par_default
                                        );



/**
 *     Tests image processing parameters on validity.
 *
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_test_parameters            (const GpivImageProcPar *image_proc_par
                                        );



/**
 *     Copies image processing parameters.
 *
 *     @param[in] imgproc_par  image processing parameters to be copied
 *     @return                 GpivImageProcPar or NULL on failure
 */
GpivImageProcPar *
gpiv_imgproc_cp_parameters		(const GpivImageProcPar *imgproc_par
					);



/**
 *     Prints image processing parameters to file.
 *
 *     @param[in] fp                   file pointer to which paraeters will 
 *                                     be printed. If NULL, stdout will be used
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         void
 */
void
gpiv_imgproc_print_parameters           (FILE                   *fp, 
                                        const GpivImageProcPar	*image_proc_par
                                        );


#endif /* __LIBGPIV_IMAGEPROCPAR_H__ */
