/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/


#ifndef __LIBGPIV_COV_H__
#define __LIBGPIV_COV_H__


typedef struct __GpivCovariance GpivCov;
/*!
 * \brief Covariance data
 *
 * The structure holds the 2-dimensional covariance data, some variables and 
 * other related parameters in order to 'fold' the data in an array.
 */
struct __GpivCovariance {	/**< data structure of covariance data */
    float **z;			/**< 2-dim array containing covariance data */
    float min;			/**< minimum covariance value */
    float max;			/**< maximum covariance value */

    long top_x;                 /**< x-position of maximum at pixel level */
    long top_y;                 /**< y-position of maximum at pixel level */
    float subtop_x;             /**< x-position of maximum at sub-pixel level */
    float subtop_y;             /**< y-position of maximum at sub-pixel level */
    float snr;                  /**< Signal to Noice defined by quotient of first 
                                   and second highest peaks */

    int z_rnl;			/**< lowest negative row index */
    int z_rnh;			/**< highest negative row index */
    int z_rpl;			/**< lowest positive row index */
    int z_rph;			/**< highest positive row index */

    int z_cnl;			/**< lowest negative column index */
    int z_cnh;			/**< highest negative column index */
    int z_cpl;			/**< lowest positive column index */
    int z_cph;			/**< highest positive index */

    int z_rl;			/**< general lowest row index */
    int z_rh;			/**< general highest row index */
    int z_cl;			/**< general lowest column index */
    int z_ch;			/**< general highest column index */
};

/**
 *      Allocates memory for GpivCov.
 *
 *      @param[in] int_size0     size of zere-padded interrogation area
 *      @param[in] x_corr        two frame image / cross correlation
 *      @return                  covariance on success or NULL on failure
 */
GpivCov *
gpiv_alloc_cov				(const guint            int_size0,
					const gboolean		x_corr
					);

/**
 *      Frees memory for GpivCov.
 *
 *      @param[in] cov          Covariance structure
 *      @return                 void
 */
void 
gpiv_free_cov				(GpivCov		*cov
					);
#endif /* __LIBGPIV_COV_H__ */
