/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf
   <gerber_graaf@users.sourceforge.net>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  





---------------------------------------------------------------------------- */

/*!
\file                   gpiv-camvar.h
\brief                  module for GpivCamVar: IEEE1394 camera control

 */
#ifndef __LIBGPIV_CAMVAR_H__
#define __LIBGPIV_CAMVAR_H__


typedef struct __GpivCamVar GpivCamVar;
/*!
 * \brief Camera variables
 *
 *  These variables are determined by the camera when connecting to the appliacation. 
 */
struct __GpivCamVar {
    guint numNodes;             /**< number of nodes available */
    guint numCameras;           /**< number of cameras available */
    guint maxspeed;             /**< maximum frame rate speed */
    guint port;                 /**< port number */
    raw1394handle_t handle;    	/**< handle */
    nodeid_t *camera_nodes;    	/**< camera node */

    dc1394_cameracapture *capture;
    dc1394_camerainfo *camera;
    dc1394_feature_info *feature_info;
    dc1394_feature_set *feature_set;
    dc1394_miscinfo *misc_info;
};




/**
 *     Get varaiables of connected cameras using firewire
 *
 *     @param[in] verbose      prints camera info to stdout
 *     @return                 GpivCamVar or NULL on failure
 */
GpivCamVar *
gpiv_cam_get_camvar 			(const gboolean 	verbose
                     			);



/**
 *     Free memory variables of connected cameras using firewire
 *
 *     @param[out] cam_var     structure of camera variables
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_cam_free_camvar			(GpivCamVar 		*cam_var
                      			);





#endif /*__LIBGPIV_CAMVAR_H__ */
