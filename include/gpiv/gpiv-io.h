/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf
   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




 --------------------------------------------------------------- */

/*!
\file                   gpiv-io.h
\brief                  module for input/output.


 */

#ifndef __LIBGPIV_IO_H__
#define __LIBGPIV_IO_H__


#define GPIV_EXT_PNG_IMAGE_PROC "_proc.png"    /**< Extension of processed png image file name */
#define GPIV_EXT_RAW_IMAGE_PROC "_proc.r"      /**< Extension of processed raw binary image file name */
#define GPIV_EXT_PAR           ".par"	       /**< Extension of file with used parameters */

#define GPIV_EXT_TA            ".ta"           /**< Extension of time averaged file name */
#define GPIV_EXT_SUBSTR        ".substr"       /**< Extension of file name with substracted data */

#define GPIV_EXT_COV           ".cov"	       /**< Extension of covariance data  */
#define GPIV_EXT_INT           ".int"	       /**< Extension of interrogation area */
#define GPIV_EXT_OLD           ".old.piv"      /**< Extension of old displacements */

#define GPIV_EXT_ERR_PIV       ".err.piv"      /**< Extension of erroneous-corrected piv file name */
#define GPIV_EXT_ERR_STAT      ".stat"         /**< Extension of residu statistics */
#define GPIV_EXT_PLK           ".plk"	       /**< Extension of peaklocking output */
#define GPIV_EXT_UVHISTO       ".pdf"          /**< Extension of displacement histogram */

#define GPIV_EXT_VOR           ".vor"	       /**< Extension of vorticity strain file name (ASCII format) */
#define GPIV_EXT_NSTR          ".nstr"	       /**< Extension of normal strain file name (ASCII format) */
#define GPIV_EXT_SSTR          ".sstr"	       /**< Extension of shear strain file name (ASCII format) */
#define GPIV_EXT_MANI          ".ma.piv"	       /**< Extension of manipulated file name (ASCII format) */
#define GPIV_EXT_SA            ".sa.piv"       /**< Extension of spatial averaged file name (ASCII format)*/
#define GPIV_EXT_SC            ".sc.piv"       /**< Extension of scaled data file name (ASCII format)*/


/* #define HD5_IMAGE_INT */                          /* Use integer for image data in HDF5 format */
#define GPIV_IMG_PARAM_RESOURCES /**<  As x_corr and depth are required 
to read properly the gpiv_image but may not
 * necesarraly be present in the header for some image formats under specific
 * situations, it is obtained from the parameter resources
 */




/**
 *      Constructs (output) filename from base name and extension. 
 *
 *      @param[in] fname_base   file base name
 *      @param[in] EXT          file extension name
 *      @param[out] fname_out   completed filename
 *      @return                 void
 */
void 
gpiv_io_make_fname			(const gchar		*fname_base,
					const gchar		*EXT,
					gchar			*fname_out
					);



/**
 *      Creates an hdf5 data file with POSITION, DATA and PARAMETERS groups.
 *
 *      @param[in] fname        pointer to complete filename
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fcreate_hdf5			(const gchar		*fname
					);


/**
 *      Reads parameters from hdf5 data file.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] par_key      key for apropiate parameter
 *      @param[out] pstruct     parameter structure
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fread_hdf5_parameters		(const gchar		*fname,
					const gchar		*par_key,
					void			*pstruct
					);


/**
 *      Writes parameters to hdf5 data file.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] par_key      key for apropiate parameter
 *      @param[in] pstruct      parameter structure
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_parameters		(const gchar		*fname,
					const gchar		*par_key,
					void			*pstruct
					);




/**
 *      Reads Davis formatted image, with ext .IMG from file.
 *
 *      @param[in] fp           pointer to input file
 *      @return                 GpivImage on success or NULL on failure
 */
GpivImage *
gpiv_read_davis_image			(FILE			*fp
					);


#endif /* __LIBGPIV_IO_H__ */
