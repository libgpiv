/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


 ------------------------------------------------------------------- */

/*!
\file                   gpiv-valid_proc.h
\brief                  module for validating PIV data

 */


#ifndef __LIBGPIV_VALID_PROC_H__
#define __LIBGPIV_VALID_PROC_H__


#define GPIV_GRADIENT_THRESHOLD        2.0     /**< Default threshold of displacement gradient */
#define GPIV_THRESHOLD_DEFAULT         1.2     /**< Default residu threshold */
#define GPIV_VALID_MAX_SWEEP           9       /**< maximum number of validation sweeps, starting from zero */





/**
 *     Calculates residu values (at the inner points) of a PIV data set 
 *     and applies to snr member of returned GpivPivData
 *
 *     @param[in] piv_data     piv dataset
 *     @param[in] valid_par    validation parameters
 *     @param[in] incl_point   flag to include data point under investigation
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_valid_residu			(GpivPivData            *piv_data, 
					const GpivValidPar	*valid_par,
					const gboolean		incl_point
					);



/**
 *     Calculates cumulative histogram of residus and linear regression fit
 *     parameters
 *
 *     @param[out] linreg      linear regression data structure
 *     @param[in] piv_data     piv dataset containing residu values in snr
 *     @param[in] nbins        number of points to be used for regression
 *     @return                 GpivBinData containing cumulative histogram or 
 *                             NULL on failure
 */
GpivBinData *
gpiv_valid_residu_stats			(const GpivPivData      *piv_data,
					const guint		nbins,
					GpivLinRegData		*linreg
					);



/**
 *     Piv data validation on peak-locking effects
 *
 *     @param[in] piv_data     input piv data
 *     @param[in] nbins        number of bins that klass will contain
 *     @return                 GpivBinData on success or NULL on failure
 *
 */
GpivBinData *
gpiv_valid_peaklck			(const GpivPivData      *piv_data,
					const guint		nbins
					);


/**
 *      Piv data validation function on outliers
 *
 *      @param[in] image       struct of image containing parameters and frames
 *      @param[in] piv_par     struct of piv image evaluation parameters
 *      @param[in] valid_par   struct of validation parameters
 *      @param[in] piv_data    piv data, will be corrected for outliers
 *      @param[in] ft          struct of arrays to perform fft
 *      @param[in] interrogate_valid   validation during (iterative) interrogation 
 *                                     process
 *     @return                 NULL or error message on failure
 */
gchar *
gpiv_valid_errvec			(GpivPivData		*piv_data,
                                         const GpivImage	*image,
                                         const GpivPivPar	*piv_par,
                                         const GpivValidPar	*valid_par,
                                         GpivFt                 *ft,
                                         const gboolean		interrogate_valid
                                         );


/**
 *     Searches vectors in a PIV data set that exceed the maximum gradient 
 *     (dUxdt/int_size > GPIV_GRADIENT_THRESHOLD)
 *
 *     @param[in] piv_par      struct of piv parameters
 *     @param[in] piv_data     PIV data set
 *
 *     @param[out] piv_data    PIV data set containing peak_no = -1 for exceeded maxima
 *     @return                 void
 */
void 
gpiv_valid_gradient			(const GpivPivPar       *piv_par, 
					GpivPivData		*piv_data
					);


/**
 *     Plots cumulative histogram with an equal number of data. 
 *     per bin of klass on screen with gnuplot
 *
 *     @param[in] fname_out    output file name
 *     @param[in] title        title of plot
 *     @param[in] GNUPLOT_DISPLAY_COLOR        color of gnuplot display
 *     @param[in] GNUPLOT_DISPLAY_SIZE         size of gnuplot display
 *     @param[in] linreg       linear regression data
 *     @return                 void
 */
void
gpiv_cumhisto_eqdatbin_gnuplot		(const gchar            *fname_out, 
					const gchar		*title, 
					const gchar		*GNUPLOT_DISPLAY_COLOR,
					const gint		GNUPLOT_DISPLAY_SIZE,
					const GpivLinRegData	*linreg
					);


/**
 *     Calculates threshold value (residu_max) from residus. 
 *     Will need int_size_f from the GpivPivPar struct
 *
 *     @param[in] piv_par      piv image evaluation parameters
 *     @param[in] valid_par    validation parameters
 *     @param[in] linreg       linear regression data
 *     @return                 threshold value
 */
gfloat 
gpiv_valid_threshold			(const GpivPivPar       *piv_par,
					const GpivValidPar	*valid_par,
					const GpivLinRegData	*linreg
					);





#endif /* __LIBGPIV_VALID_PROC_H__ */
