/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/


/*!
\file                   gpiv-pivdata.h
\brief                  module for GpivPivData: data for Particle Image Velocimetry

 */


#ifndef __LIBGPIV_PIVDATA_H__
#define __LIBGPIV_PIVDATA_H__


#define GPIV_PIV_FMT "%4.0f %4.0f %12.3f %12.3f %12.3f %2d\n"	/**< Piv data Format */
#define GPIV_PIV_S_FMT "%-12.5f %-12.5f %-12.5f  %-12.5f  %-6.2f     %-2d\n"	/**< Piv scaled data Format */


enum GpivDataFormat { 
    GPIV_RR = 1, 
    GPIV_DAV = 2 
};


typedef struct __GpivPivData GpivPivData;
/*!
 *  \brief Holds the variables of PIV data
 *
 * This structure is primarly intended for locations, (PIV) estimators 
 * (i.e. particle image displacements) and some stastistics, like mean, minimum, maximum and standard 
 * deviation or rms values. But might be used for any vectorial quantity. 
 * Quantities in z-direction have not yet been implemented by the library functions, though.
 */
struct __GpivPivData {
    guint nx;			/**< number of data in x-direction */
    guint ny;			/**< number of data in y-direction */
    guint nz;			/**< number of data in z-direction (for future use) */


    gfloat **point_x;		/**< position of data point in x-direction */
    gfloat **point_y;		/**< position of data point in y-direction */
    gfloat **point_z;		/**< position of data point in z-direction (for future use) */
    
    gfloat **dx;	        /**< particle image displacement in x-direction */
    gfloat **dy;		/**< particle image displacement in y-direction */
    gfloat **dz;		/**< particle image displacement in z-direction (for future use) */
    
    gfloat **snr;		/**< signal to noise ratio. The value has been defined as the ratio between
                                the first and second highest covariance peak, In case validation takes place
                                (during iterative interrogation or during a validation step after image 
                                interrogation), the snr value is used as the residual value between actual 
                                and surrounding estimators */
    gint **peak_no;		/**< Nth-highest covariance peak number. Will mostly be 1 (first highest peak
                                of covariance function. Set to 0 in case the estimator has been 
                                substituted during validation. If < 0 the data has been disabled. */
    gfloat **scalar;		/**< scalar value that might represent vorticity 
                                     , shear strain or normal strain, for example */
    guint count;                /**< total number of valid estimators */

    gfloat mean_dx;             /**< mean displacement in x-direction */
    gfloat sdev_dx;             /**< rms displacement in x-direction */
    gfloat min_dx;              /**< minimum displacement in x-direction */
    gfloat max_dx;              /**< maximum displacement in x-direction */

    gfloat mean_dy;             /**< mean displacement in y-direction */
    gfloat sdev_dy;             /**< rms displacement in y-direction */
    gfloat min_dy;              /**< minimum displacement in y-direction */
    gfloat max_dy;              /**< maximum displacement in y-direction */

    gfloat mean_dz;             /**< mean displacement in z-direction (for future use) */
    gfloat sdev_dz;             /**< rms displacement in z-direction (for future use) */
    gfloat min_dz;              /**< minimum displacement in z-direction (for future use) */
    gfloat max_dz;              /**< maximum displacement in z-direction (for future use) */


    gboolean scale;             /**< flag for scaled estimators */
    gboolean scale__set;        /**< flag if scale has been defined */
 
    gchar *comment;             /**< comment on the data */
};

/*
 * From piv-utils.h
 */

/**
 *      Sets all elements of piv_data structure to NULL.
 *
 *      @param[in] piv_data     Input PIV data structure
 *      @param[out] piv_data    Output PIV data structure
 *      @return                 void
 */
void
gpiv_null_pivdata                      (GpivPivData            *piv_data
                                        );


/**
 *      Allocates memory for GpivPivData.
 *
 *      @param[in] nx           number of nx  (column-wise data dimension)
 *      @param[in] ny           number of ny  (row-wise data dimension)
 *      @return                 piv_data on success or NULL on failure
 */
GpivPivData *
gpiv_alloc_pivdata                     (const guint            nx,
					const guint            ny
                                        );

/**
 *     Checks if piv_data have been allocated.
 *
 *     @param[in] piv_data     PIV data set to be checked if allocated
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_check_alloc_pivdata               (const GpivPivData      *piv_data
                                        );

/**
 *      Frees memory for GpivPivData.
 *
 *      @param[in] piv_data     PIV data structure
 *      @param[out] piv_data    NULL pointer to point_x, point_y, dx, dy, snr 
 *                              and peak_no
 *      @return                 void
 */
void 
gpiv_free_pivdata                      (GpivPivData            *piv_data
                                        );



/**
 *      Returns a copy of piv_data.  
 *      Piv_data_in will have to be allocated with gpiv_alloc_pivdata and the 
 *      returned datawill have to be freed with gpiv_free_pivdata.
 *
 *      @param[in] piv_data     Input PIV data
 *      @return                 GpivPivData on success or NULL on failure
 */
GpivPivData *
gpiv_cp_pivdata                        (const GpivPivData      *piv_data
                                        );


/**
 *      Overwrites piv_data_out with piv_data_in.
 *      Both PIV data sets will have to be allocated with gpiv_alloc_pivdata
 *      and must be of equal sizes.
 *
 *      @param[in] piv_data_in  Input PIV data
 *      @param[in] piv_data_out Output PIV data
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_ovwrt_pivdata                     (const GpivPivData      *piv_data_in,
                                        const GpivPivData      *piv_data_out
                                        );



/**
 *      Sets estimators, snr and peak_nr of piv_data to 0 or 0.0. 
 *      The structure will have to be allocated before (with 
 *      gpiv_alloc_pivdata).
 *
 *      @param[in] piv_data     PIV data
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_0_pivdata                         (const GpivPivData      *piv_data
                                        );


/**
 *      Adds displacements (dx, dy), snr and peak_nr from piv_data_in to 
 *      piv_data_out. Both structures will have to be allocated before 
 *      (with gpiv_alloc_pivdata).
 *
 *      @param[in] piv_data_in          Input PIV data structure
 *      @param[out] piv_data_out        Output PIV data structure
 *      @return                         NULL on success or error message on failure
 */
gchar *
gpiv_add_dxdy_pivdata                  (const GpivPivData      *piv_data_in,
                                        GpivPivData            *piv_data_out
                                        );



/**
 *      Adds all displacements in order to calculate residuals
 *      The structure will have to be allocated before (with 
 *      gpiv_alloc_pivdata).
 *
 *      @param[in] piv_data     PIV data structure
 *      @param[out] sum         resulting sum
 *      @return                 NULL on success or error message on failure
 */
gchar * 
gpiv_sum_dxdy_pivdata                  (const GpivPivData      *piv_data,
					gfloat                 *sum
                                        );


/*
 * Some MPI routines
 */
#ifdef ENABLE_MPI

/**
 *      Scatters PivData structure for parallel processing with MPI
 *      using MPI_Scatter().
 *      Except of pd->nx and pd->ny, which should broadcasted in forward
 *      in order to allocate memory.)
 *
 *      @param[in] pd PIV       data to be scattered
 *      @param[in] nprocs       number of processes
 *      @param[out] pd_scat     scattered piv data
 *      @return                 void
 */
void
gpiv_piv_mpi_scatter_pivdata           (GpivPivData            *pd, 
                                        GpivPivData             *pd_scat, 
                                        guint                   nprocs
                              		);

/**
 *     Gathers PivData structure for parallel processing with MPI
 *     using MPI_Gather().
 *     Counterpart of gpiv_piv_mpi_scatter_pivdata
 *
 *      @param[in] pd_scat      scattered piv data
 *      @param[out] pd          PIV data to be gathered
 *      @return                 void
 */
void 
gpiv_piv_mpi_gather_pivdata            (GpivPivData            *pd_scat, 
                                        GpivPivData            *pd, 
                                        guint                   nprocs
                                        );

/**
 *      Calculates number of data counts[] for scatterv rows of data
 *
 *      @param[in] nx           number of x-data (columns)
 *      @param[in] ny           number of y-data (rows)
 *      @return                 pointer to gint array representing counts[]
 */
gint * 
gpiv_piv_mpi_compute_counts            (const guint            nx, 
                                        const guint            ny
                                        );

/**
 *      Calculates displacements array displs[] for scatterv rows of data
 *
 *      @param[in] counts       array containing number of data to be scattered to each node
 *      @param[in] nx           number of x-data (columns)
 *      @param[in] ny           number of x-data (rows)
 *      @return                 pointer to gint array representing displs[]
 */
gint * 
gpiv_piv_mpi_compute_displs            (gint                   *counts, 
                                        const guint             nx, 
                                        const guint             ny
					);

/**
 *      Scatters PivData structure to variable arrays for parallel processing 
 *      with MPI using MPI_Scatterv().
 *      Except of pd->nx and pd->ny, which should broadcasted in forward
 *      in order to allocate memory.)
 *
 *      @param[in] pd           PIV data to be scattered
 *      @param[out] pd_scat     scattered piv data
 *      @param[in] counts       array containing number of data to be scattered 
 *                              to each node, obtained with gpiv_piv_mpi_compute_counts()
 *      @param[in] displs       array containing displacement to each node, , obtained 
 *                              with gpiv_piv_mpi_compute_displs()
 *      @return                 void
 */
void
gpiv_piv_mpi_scatterv_pivdata          (GpivPivData            *pd, 
                                        GpivPivData             *pd_scat, 
                                        gint                    *counts,
                                        gint                    *displs
					);

/**
 *      Gathers PivData structure to variable arrays for parallel processing 
 *      with MPI using MPI_Gatherv().
 *      Except of pd->nx and pd->ny, which should broadcasted in forward
 *      in order to allocate memory.)
 *
 *      @param[in] pd_scat      scattered PIV data
 *      @param[out] pd          PIV data to be gathered
 *      @param[in] counts       array containing number of data to be scattered 
 *                              to each node, obtained with gpiv_piv_mpi_compute_counts()
 *      @param[in] displs       array containing displacement to each node, , obtained 
 *                              with gpiv_piv_mpi_compute_displs()
 *      @return                 void
 */
void 
gpiv_piv_mpi_gatherv_pivdata           (GpivPivData            *pd_scat, 
                                        GpivPivData             *pd, 
                                        gint                    *counts,
                                        gint                    *displs
					);

/**
 *      Broadcasts PivData structure to variable arrays for parallel processing 
 *      with MPI using MPI_Bcast().
 *      Except of pd->nx and pd->ny, which should broadcasted in forward
 *      in order to allocate memory.)
 *
 *      @param[out] pd          PIV data to be broadcasted
 *      @return                 void
 */
void 
gpiv_piv_mpi_bcast_pivdata             (GpivPivData            *pd
                                        );
#endif /* ENABLE_MPI */

/**
 *      Determines the name of the program that generated the data.
 *
 *      @param[in] line         character line containing program that generated data
 *      @return pdf             enumerator of data origin
 */
enum GpivDataFormat
gpiv_find_pivdata_origin               (const gchar            line[GPIV_MAX_CHARS]
                                        );

/**
 *      Reads PIV data from file fname.
 *
 *      @param[in] fname        complete input filename.
 *      @return                 GpivPivData or NULL on failure
 */
GpivPivData *
gpiv_fread_pivdata                     (const gchar            *fname
                                        );

/**
 *      Writes PIV data to file fname in ASCII format.
 *
 *      @param[in] piv_data     PIV data
 *      @param[in] fname        complete output filename.
 *      @param[in] free         flag if allocated memory if piv_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_pivdata                    (const gchar             *fname,
                                        GpivPivData             *piv_data,
                                        const gboolean          free
                                        );

/**
 *      Reads PIV data from fp.
 *
 *      @param[in] fp           input file. If NULL, stdin is used.
 *      @return                 GpivPivData
 */
GpivPivData *
gpiv_read_pivdata                      (FILE                   *fp
                                        );

/**
 *      Reads PIV data from fp with fast running x-position variables.
 *      (1st column in data stream)
 *
 *      @param[in] fp           input file. If NULL, stdin is used.
 *      @return                 GpivPivData
 */
GpivPivData *
gpiv_read_pivdata_fastx                (FILE                   *fp
                                        );

/**
 *      Writes PIV data to fp in ASCII format.
 *
 *      @param[in] piv_data     PIV data
 *      @param[in] fp           output file. If NULL, stout is used.
 *      @param[in] free         flag if allocated memory if piv_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_pivdata                     (FILE                    *fp,
                                        GpivPivData             *piv_data,
                                        const gboolean          free
                                        );

/**
 *      Reads piv data from hdf5 data file.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] DATA_KEY     location of the data in the DATA group in the hdf file
 *      @return                 GpivPivData or NULL on failure
 */
GpivPivData *
gpiv_fread_hdf5_pivdata                (const gchar             *fname,
                                        const gchar             *DATA_KEY
                                        );


/**
 *      Writes PIV data to file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] piv_data     piv data containing all variables in a datafile
 *      @param[in] DATA_KEY     location of the data in the DATA group in the hdf file
 *      @param[in] free         boolean whether to free memory of PIV data
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_pivdata               (const gchar             *fname, 
                                        GpivPivData             *piv_data, 
                                        const gchar             *DATA_KEY,
                                        const gboolean          free
                                        );

#endif /* __LIBGPIV_PIVDATA_H__ */
