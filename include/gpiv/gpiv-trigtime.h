/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

/*!
\file                   gpiv-trigtime.h
\brief                  module for GpivTrigTime: time settings for triggering lightsource(s) and camera(s) using RTAI

 */

#ifndef __LIBGPIV_TRIGTIME_H__
#define __LIBGPIV_TRIGTIME_H__

typedef struct __GpivTrigTime GpivTrigTime;

/*!
 * \brief Trigger parameters
 *
 * Used for triggering camera and light source (mostly a double-cavity Nd/YAGG laser)
 * and defining separation time between the frames of a PIV image pair etc. 
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
 */
struct __GpivTrigTime {
    RTIME cam_acq_period;       /**< camera acquisition period time in ns (period) */
    RTIME laser_trig_pw;        /**< laser trigger pulse width in ns (tp) */
    RTIME time2laser;           /**< time from laser trigger->Hi (tl) until laser pulse: 0.19 ms in ns */
    RTIME dt;                   /**< time between 2 laser exposures in ns */
    enum GpivTimingMode mode;   /**< operating mode (indefinite/interrupt/definite) */
    gint cycles;                /**< number of cycles, equal to the number of image pairs to be recorded */
    RTIME increment;            /**< increment in dt (ms) */
};
  

#endif /* __LIBGPIV_TRIGTIME_H__ */

