/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


------------------------------------------------------------------------------*/

/*!
  \file                   gpiv-piv_proc.h
  \brief                  module for PIV image evaluation routines

*/



#ifndef __LIBGPIV_PIV_PROC_H__
#define __LIBGPIV_PIV_PROC_H__


#define GPIV_ZEROPAD_FACT 2     /**< magnification factor of zero-padded int.\ area */
#define GPIV_DIFF_ISI  0        /**< difference between interrogation sizes 
                                   of first and second image if zero 
                                   offset has been used */
#define GPIV_CUM_RESIDU_MIN 0.25 /**< minimum cumulative residu for convergence */
#define GPIV_MAX_PIV_SWEEP 10   /**< maximum number of PIV evaluation sweeps,
                                   starting from zero */
#define GPIV_SNR_DISABLE 88.0    /**< snr value for manually disabled estimator */
#define GPIV_DEFORMED_IMG_NAME "gpiv_defimg" /**< Deformed image to be stored in TMP_DIR */ 
#define GPIV_LOGFILE "gpiv.log"  /**< Log file to be stored in TMP_DIR */


#define CMPR_FACT 2            /**< Image compression factor for speeding up the
                                  evaluation */
#define GPIV_SHIFT_FACTOR 3    /**< For initial grid, apply 
                                  int_shift = int_size_i / GPIV_SHIFT_FACTOR */



/**
 * Type of sub-pixel estimation
 */
enum GpivIFit {
    GPIV_NONE = 0,     /**< No fitting */
    GPIV_GAUSS = 1,    /**< Gauss fitting */
    GPIV_POWER = 2,    /**< Power fitting */
    GPIV_GRAVITY = 3   /**< Plain gravity fitting */
};





/*-----------------------------------------------------------------------------
 * Function prototypes
 */

/**
 *     Calculates the number of interrogation areas from the image sizes, 
 *     pre-shift and area of interest.
 *
 *     @param[in] image_par    structure of image parameters
 *     @param[in] piv_par      structure of piv evaluation parameters
 *     @param[out] nx          number of columns (second array index)
 *     @param[out] ny          number of rows (first array index)
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_count_pivdata_fromimage        (const GpivImagePar     *image_par,
                                         const GpivPivPar       *piv_par,
                                         guint                  *nx,
                                         guint                  *ny
                                         );


/**
 *     PIV interrogation of an image pair at an entire grid.
 *
 *     @param[in] image        image containing data and header info
 *     @param[in] piv_par      image interrogation parameters
 *     @param[in] valid_par    PIV data validation parameters
 *     @param[out] verbose     prints progress of interrogation to stdout
 *     @return                 GpivPivData containing PIV estimators on success
 *                             or NULL on failure
 */
GpivPivData *
gpiv_piv_interrogate_img     		(const GpivImage        *image,
                                         const GpivPivPar       *piv_par,
                                         const GpivValidPar     *valid_par,
                                         const gboolean         verbose
                                         );
 

/**
 *      Interrogates a single Interrogation Area.
 *
 *      @param[in] index_y      y-index of interrogation area position  
 *      @param[in] index_x      x-index of interrogation area position
 *      @param[in] image        structure of image
 *      @param[in] piv_par      structure of piv evaluation parameters
 *      @param[in] sweep        sweep number of iterative process 
 *      @param[in] last_sweep   flag for last sweep
 *      @param[in] int_area_1   first interrogation area
 *      @param[in] int_area_2   second interrogation area
 *      @param[out] cov          structure containing covariance data
 *      @param[out] piv_data    modified piv data at [index_y][index_x] from 
 *                              interrogation
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_interrogate_ia      		(const guint           index_y,
                                         const guint           index_x,
                                         const GpivImage       *image,
                                         const GpivPivPar      *piv_par,
                                         const guint           sweep,
                                         const guint           last_sweep,
                                         GpivPivData           *piv_data,
                                         GpivFt                *ft
                                         );



/**
 *     Adjusts interrogation area sizes. For each interrogation sweep,
 *     (dest) int_size_i is halved, until it reaches (src)
 *     int_size_f. Then, isiz_last is set TRUE, which will avoid
 *     changing the interrogation sizes in next calls.
 *
 *     @param[in] piv_par_src          original parameters
 *     @param[out] piv_par_dest        actual parameters, to be modified 
 *                                     during sweeps
 *     @param[out] isiz_last           flag for last interrogation sweep
 *     @return                         void
 */
void
gpiv_piv_isizadapt      		(const GpivPivPar       *piv_par_src,
                                         GpivPivPar             	*piv_par_dest,
                                         gboolean               	*isiz_last
                                         );



/**
 *     Stores deformed image to file system with pre defined name to TMPDIR
 *     and prints message to stdout.
 *
 *     @param[in] image        image containing header and image data frames
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_write_deformed_image  		(GpivImage 		*image
                                         );




/**
 *     Reads fftw wisdoms from file and stores into a (public) string.
 *
 *     @param[in] dir          direction of fft; forward (+1) or inverse (-1)
 *     @return                 void
 */
void
gpiv_fread_fftw_wisdom 			(const gint   		dir
                                         );



/**
 *     Writes fftw wisdoms to a file.
 *
 *     @param[in] dir          direction of fft; forward (+1) or inverse (-1)
 *     @return                 void
 *
 */
void 
gpiv_fwrite_fftw_wisdom        		(const gint     	dir
                                         );


/**
 *      Calculates dx, dy of piv_data_dest from piv_data_src
 *      by bi-linear interpolation of inner points with shifted knots
 *      or extrapolation of outer lying points.
 *
 *      @param[in] piv_data_src         input piv data
 *      @param[out] piv_data_dest       output piv data
 *      @return NULL on success or *err_msg on failure
 */
gchar *
gpiv_piv_dxdy_at_new_grid       	(const GpivPivData      *piv_data_src,
                                         GpivPivData            	*piv_data_dest
                                         );



/**
 *     Shifts the knots of a 2-dimensional grid containing PIV data for improved 
 *     (bi-linear) interpolation.
 *
 *     See: T. Blu, P. Thevenaz, "Linear Interpolation Revitalized",
 *     IEEE Trans. in Image Processing, vol13, no 5, May 2004
 *
 *     @param[in] piv_data     piv data that will be shifted in-place
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_shift_grid     		(GpivPivData    	*piv_data
                                         );



/**
 *      Generates grid by Calculating the positions of interrogation areas.
 *
 *      @param[in] nx           number of horizontal grid points
 *      @param[in] ny           number of vertical grid points
 *      @param[in] image_par    structure of image parameters
 *      @param[in] piv_par      structure of piv evaluation parameters
 *      @return                 piv_data or NULL on failure
 */
GpivPivData *
gpiv_piv_gridgen        		(const guint            nx,
                                         const guint            	ny,
                                         const GpivImagePar     	*image_par,
                                         const GpivPivPar       	*piv_par
                                         );



/**
 *     Adjust grid nodes if zero_off or adaptive interrogation 
 *     area has been used. This is performed by modifying int_shift equal 
 *     to int_shift / GPIV_SHIFT_FACTOR, until it reaches (src)
 *     int_shift. Then, grid_last is set TRUE, which will avoid
 *     changing the interrogation shift in next calls and signal the
 *     (while loop in) the calling function.
 *     
 *     @param[in] image_par               image parameters
 *     @param[in] piv_par_src        piv evaluation parameters
 *     @param[in] piv_data                input PIV data
 *     @param[in] sweep                   interrogation sweep step
 *     @param[out] piv_par_dest       modified piv evaluation parameters
 *     @param[out] grid_last               flag if final grid refinement has been 
 *     @return                         piv_data or NULL on failure
 */
GpivPivData *
gpiv_piv_gridadapt      		(const GpivImagePar     *image_par, 
                                         const GpivPivPar       	*piv_par_src,
                                         GpivPivPar             	*piv_par_dest,
                                         const GpivPivData      	*piv_data,
                                         const guint            	sweep, 
                                         gboolean               	*grid_last
                                         );


/*
 * BUGFIX: Moved out from gpiv-pivdata.h
 */
 
/**
 *      Plots piv data as vectors on screen with gnuplot.
 *
 *      @param[in] title                        title of plot
 *      @param[in] gnuplot_scale                vector scale
 *      @param[in] GNUPLOT_DISPLAY_COLOR        display color of window containing graph
 *      @param[in] GNUPLOT_DISPLAY_SIZE         display size of window containing graph
 *      @param[in] image_par                    image parameters
 *      @param[in] piv_par                      piv evaluation parameters
 *      @param[in] piv_data                     piv data
 *      @return                                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_gnuplot                       (const gchar             *title, 
                                        const gfloat            gnuplot_scale,
                                        const gchar             *GNUPLOT_DISPLAY_COLOR, 
                                        const guint             GNUPLOT_DISPLAY_SIZE,
                                        const GpivImagePar      *image_par, 
                                        const GpivPivPar        *piv_par,
                                        const GpivPivData       *piv_data
                                        );

/**
 *      Plots histogram on screen with gnuplot.
 *
 *      @param[in] fname_out 	               output filename
 *      @param[in] title 	               plot title
 *      @param[in] GNUPLOT_DISPLAY_COLOR        display color of window containing graph
 *      @param[in] GNUPLOT_DISPLAY_SIZE         display size of window containing graph
 *      @return                                 NULL on success or error message on failure
 */
gchar *
gpiv_histo_gnuplot                     (const gchar            *fname_out,
                                        const gchar             *title,
                                        const gchar             *GNUPLOT_DISPLAY_COLOR,
					const gint              GNUPLOT_DISPLAY_SIZE
                                        );

/**
 *      Calculates histogram from GpivPivData
 *      (NOT from GpivScalarData!!).
 *
 *      @param[in] data         Input data
 *      @param[out] klass       Output data
 *      @return                 void
 */
void 
gpiv_histo                             (const GpivPivData      *data,
                                        GpivBinData             *klass 
                                        );

/**
 *      Calculates cumulative histogram from GpivPivData 
 *      (NOT from GpivScalarData!!).
 *
 *      @param[in] data         Input data
 *      @param[out] klass       Output data
 *      @return                 void
 */
void 
gpiv_cumhisto                          (const GpivPivData      *data, 
                                        GpivBinData             *klass
                                        );

#endif /* __LIBGPIV_PIV_PROC_H__ */
