/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2012 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/


#ifndef __LIBGPIV_FT_H__
#define __LIBGPIV_FT_H__


typedef struct __GpivFastfourierW GpivFt;
/*!
 * \brief Ft data
 *
 * Holds 2-dimensional input arrays (interrogation regions), 
 * plans for FT and some real and complex arrays to perform Fast Fourier 
 * Transformation using libfftw
 */
struct __GpivFastfourierW {	/**< arrays for Fast Fourier Transform using libfftw */
    guint size;                 /**< size of data arrays (use zero-padded length) */
    gfloat **intreg1;		/**< first input array (interrogation area) */
    gfloat **intreg2;		/**< second input array (interrogation area) */
    GpivCov *cov;               /**< covariance structure */
    fftw_plan plan_forwardFFT;  /**< plan of fftw library to perform FT */
    fftw_plan plan_reverseFFT;  /**< plan of fftw library to perform inverse FT */
    double **re;                /**< input (real) array for FT */
    fftw_complex **co;          /**< output (complex) array for FT */
    fftw_complex **A;           /**< Fourier Transform of first interrogation area */
    fftw_complex **B;           /**< Fourier Transform of second interrogation area */
};


/**
 *      Allocates memory for GpivFt.
 *
 *      @param[in] size          interrogation area size
 *      @return                  ft on success or NULL on failure
 */
GpivFt *
gpiv_alloc_ft				(const guint            size
                                         );

/**
 *      Checks for GpivFt if all arrays have been allocated.
 *
 *      @param[in] ft           Ft structure
 *      @return                 NULL if allocated, else a message
 */
gchar *
gpiv_check_alloc_ft                   (const GpivFt		*ft
                                       );

/**
 *      Frees memory for GpivFt.
 *
 *      @param[in] ft           Ft structure
 *      @return                 void
 */
void 
gpiv_free_ft				(GpivFt		*ft
                                         );
#endif /* __LIBGPIV_FT_H__ */
