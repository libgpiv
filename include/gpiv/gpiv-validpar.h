/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




 --------------------------------------------------------------------------- */

/*!
\file                   gpiv-validpar.h
\brief                  module for parameters for validating PIV data

*/


#ifndef __LIBGPIV_VALIDPAR_H__
#define __LIBGPIV_VALIDPAR_H__




#define GPIV_VALIDPAR_KEY      "VALID" /**< Key of  validation processing parameters */
#define GPIV_VALIDPAR_MAX__NEIGHBORS 9 /**< Maximum numbers of adjacent neighbours for validation testing and and calculation of substitution */
#define GPIV_RESIDU_MAX_NORMMEDIAN     2.0     /**< residu threshold if normalized median is used */



/**
 * Type of residu to detect outliers
 */
enum ResiduType {
    GPIV_VALID_RESIDUTYPE__SNR,         /**< Signal to noise ratio */
    GPIV_VALID_RESIDUTYPE__MEDIAN,      /**< median value of NxN surrounding estimators */
    GPIV_VALID_RESIDUTYPE__NORMMEDIAN   /**< normalized median, obtained from the ratio
                                             between datapoint under investigation and 
                                             median velocity (from 3x3 array, excluding
                                             current point) and median residu. 
                                             Ref. Westerweel, Scarano, Exp. in Fluids,
                                             39 (2005), pp1096-1100. */
};



/**
 * Erroneous vector substituted by type
 */
enum SubstitutionType {
    GPIV_VALID_SUBSTYPE__NONE,          /**< no value, point will be disabled by setting __GpivPivData peak_nr to -1 */
    GPIV_VALID_SUBSTYPE__L_MEAN,        /**< substitute by local mean value */
    GPIV_VALID_SUBSTYPE__MEDIAN,        /**< substitute by median value from surroundings */
    GPIV_VALID_SUBSTYPE__COR_PEAK       /**< use location of next highest correlation peak as estimator */
};



typedef struct __GpivValidPar GpivValidPar;

/*!
 * \brief Piv validation parameters
 *
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
 */
 struct __GpivValidPar {
    gfloat data_yield;                 /**< data yield ( from particle image concentration, 
                                            out-of plane flow, velocity gradient within interrogation area.
                                            ref: Keane and Adrian 1992) */
    gfloat residu_max;                 /**< maximum residu value */
    guint neighbors;                    /**< number of neighbouring estimators */
    enum ResiduType residu_type;       /**< use residu type */
    enum SubstitutionType subst_type;  /**< use substitution type */
    gint histo_type;                   /**< use histogram type */
    
    gboolean data_yield__set;          /**< flag if data_yield has been defined */
    gboolean residu_max__set;          /**< flag if residu_max has been defined */
    gboolean neighbors__set;           /**< flag if neighbors has been defined */
    gboolean residu_type__set;         /**< flag if residu_type has been defined */
    gboolean subst_type__set;          /**< flag if subst_type has been defined */
    gboolean histo_type__set;          /**< flag if histo_type has been defined */
};


/**
 *      Defines value of __set members of PivValidPar.
 *
 *      @param[in] valid_par   validation parameters
 *      @param[in] flag        boolean to define __set value
 *      @param[out] valid_par  struct of validation parameters
 *      @return                void
 */
void
gpiv_valid_parameters_set               (GpivValidPar           *valid_par,
                                         const gboolean         flag
                                         );



/**
 *      Sets parameters to default values.
 *
 *      @param[in] force               flag to enforce parameters set to defaults
 *      @param[out] valid_par_default  struct of validation parameters
 *      @return                        void
 */
void
gpiv_valid_default_parameters           (GpivValidPar           *valid_par_default,
                                         const gboolean         force
                                         );



/**
 *     Reads validation parameters from system-wide gpiv.conf and $HOME/.gpivrc.
 *
 *     @param[in] localrc      resource filename containing parameter at 
                               current directory
 *     @param[in] verbose      prints parameter values when read
 *     @return                 GpivValidPar or NULL on failure
 */
GpivValidPar *
gpiv_valid_get_parameters_from_resources(const gchar            *localrc,
                                         const gboolean         verbose
                                         );



/**
 *      Reads validation parameters from file.
 *
 *      @param[in] fp_par      file pointer to file to be read.
 *                             If NULL, stdin will be used.    
 *      @param[in] print_par   boolean to print parameters to stdout
 *      @param[out] valid_par  validation parameters
 *      @return                void
 */
void 
gpiv_valid_read_parameters              (FILE                   *fp,
                                         GpivValidPar           *valid_par,
                                         const gboolean         verbose
                                         );



/**
 *     Checks out if all parameters have been read. 
 *     If a parameter has not been read, it will be set to valid_par_default or to
 *     its hard-coded default value in case valid_par_default is NULL.
 *
 *     @param[in] valid_par_default    default validation parameters. If NULL, 
 *				       library default values are used.
 *     @param[in] valid_par            validation parameters
 *     @param[out] valid_par           validation parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_valid_check_parameters_read        (GpivValidPar           *valid_par,
                                         const GpivValidPar     *valid_par_default
                                         );





/**
 *     Tests if all validation parameters have been read and have been defined 
 *     to valid values.
 *
 *     @param[in]  valid_par   validation parameters
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_valid_testonly_parameters          (const GpivValidPar     *valid_par
                                         );


/**
 *     Tests if all validation parameters have been read and have been defined 
 *     to valid values. Aplies missing parameters to defaults, as hard-coded
 *     in the library and adjusts parameters if necessary.
 *
 *     @param[in]  valid_par   validation parameters
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_valid_testadjust_parameters        (GpivValidPar           *valid_par
                                         );


/**
 *      Prints all validation parameters in ASCII format to fp.
 *
 *      @param[in] fp          file pointer to printed file. 
 *                             If NULL, fp points to stdout.
 *      @param[in] valid_par   validation parameters
 *      @return                void
 */
void 
gpiv_valid_print_parameters             (FILE                   *fp,
                                         const GpivValidPar     *valid_par
                                         );


/**
 *     Copies validation parameters.
 *
 *     @param[in] valid_par    validation parameters to be copied
 *     @return                 GpivValidPar * or NULL on failure
 */
GpivValidPar *
gpiv_valid_cp_parameters                (const GpivValidPar     *valid_par
                                         );


/**
 *     Duplicates validation parameters from valid_par_src to
 *     valid_par_dest if valid_par_dest have not been set.
 *
 *     @param[in] valid_par_src        source validation parameters to be copied
 *     @param[out] valid_par_dest      the copied validation parameters
 *     @return                         void
 */
void
gpiv_valid_dupl_parameters              (const GpivValidPar     *valid_par_src, 
                                         GpivValidPar           *valid_par_dest
                                         );


/**
 *     Reads validation parameters from hdf5 data file.
 *
 *     @param[in] fname        file name of data file
 *     @return                 GpivValidPar or NULL on failure
 */
GpivValidPar *
gpiv_valid_fread_hdf5_parameters        (const gchar            *fname
                                         );



/**
 *     Writes validation parameters to an existing hdf5 data file.
 *
 *     @param[in] fname        file name of data file
 *     @param[in] valid_par    struct of validation parameters
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_valid_fwrite_hdf5_parameters       (const gchar        *fname, 
                                         const GpivValidPar *valid_par
                                         );

#ifdef ENABLE_MPI
/**
 *     Broadcasts valid_par for MPI using MPI_Bcast()
 *
 *     @param[in] valid_par    struct of validation parameters  to be broadcasted
 *     @return                 void
 */
void
gpiv_valid_mpi_bcast_validpar           (GpivValidPar           *valid_par
                                         );

#endif /* ENABLE_MPI */
#endif /* __LIBGPIV_VALIDPAR_H__ */

