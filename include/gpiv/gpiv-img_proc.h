/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


-----------------------------------------------------------------------------*/
/*!
\file                  gpiv-img_proc.h
\brief                 module for image processing routines

 */


#ifndef _LIBGPIV_IMGPROC_H__
#define _LIBGPIV_IMGPROC_H__


/**
 *     Genarates test image for image processing. This is not an image for 
 *     testing on PIV interrogation!
 *
 *     @param[in] image_par            structure of image parameters
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         GpivImage or NULL on failure
 */
GpivImage *
gpiv_imgproc_mktestimg		(const GpivImagePar		*image_par,
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Subtracts image intensities of img_in from img_out. To avoid swapping of 
 *     intensities, output values are set to zero if img_in is larger than img_out.
 *     Images will have to be of identic dimensions.
 *
 *     @param[in] image_in      image containing the intensities to be subtracted
 *     @param[out] image_out    modified image. will have to exist and allocated
 *     @return                  NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_subtractimg        (const GpivImage		*image_in,
                                 GpivImage			*image_out
                                 );


/**
 *     Smoothing filter by taking mean value of surrounding 
 *     window x window pixels
 *
 *     @param[in] image                image to be smoothed in-place
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_smooth		(GpivImage			*image, 
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Highpass filter on an image
 *     passing data from M - window,..,M/2, N - window,..,N/2
 *
 *     @param[in] image                image to be be high-passed in-place
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_highpass		(GpivImage			*image, 
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     High-low filter to maximize contrast by stretching pixel values
 *     to local max and min within window x window area
 *
 *     @param[in] image                image to be filtered in-place
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_highlow		(GpivImage			*image,
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Sets all pixel values lower than threshold to zero
 *
 *     @param[in] image                image to be clipped in-place.
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_clip		(GpivImage			*image,
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Pointer operation to get the N least significant bits and moves them to 
 *     most the significant bits
 *
 *     @param[in] image                image to be filtered in-place
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_getbit		(GpivImage			*image,
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Fast Fourier Transformation of image
 *
 *     @param[in] image                input image to be modified in-place. 
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_fft			(GpivImage              *image,
					const GpivImageProcPar	*image_proc_par
					);


#ifndef USE_FFTW3
/**
 *     Lowpass filter on an image
 *
 *     @param[in] image_par            structure of image parameters
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @param[in] img                  input image to be filtered in-place.
 *                                     Needs to be allocated
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_lowpass			(GpivImagePar		image_par, 
					GpivImageProcPar	image_proc_par,
					guint16			**img
					);



/**
 *     Correlates two images
 *
 *     @param[in] image_par            structure of image parameters
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @param[in] img1_in              first input image. Needs to be allocated
 *     @param[in] img2_in              second input image. Needs to be allocated
 *     @param[out] img_out             pointer to output image. Not necessarily allocated
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_correlate			(GpivImagePar           image_par, 
					GpivImageProcPar	image_proc_par,
					guint16			**img1_in, 
					guint16			**img2_in, 
					guint16*		**img_out
					);



/**
 *     Convolves two images
 *
 *     @param[in] image_par            structure of image parameters
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @param[in] img1_in              first input image. Needs to be allocated
 *     @param[in] img2_in              second input image. Needs to be allocated
 *     @param[out] img_out             pointer to output image. Not necessarily allocated
 *     @return                         NULL on success or error message on failure
 *
 */
gchar *
gpiv_imgproc_convolve		(GpivImagePar			image_par, 
				GpivImageProcPar		image_proc_par,
				 guint16			**img1_in, 
				 guint16			**img2_in, 
				 guint16*			**img_out
				 );

#endif /* USE_FFTW3 */


/**
 *     Image shifting and deformation routine for a single exposed, double 
 *     frame PIV image pair with magnitude of PIV estimations at each pixel.
 *     Deforms first frame halfway forward and second frame halfway backward.
 *
 *     @param[in] piv_data     PIV data
 *     @param[in] image        input image to be deformed in-place. Needs to be allocated
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_deform		(GpivImage			*image,
				const GpivPivData		*piv_data
				);


#endif /* __LIBGPIV_IMGPROC_H__ */

