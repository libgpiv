/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  





 ------------------------------------------------------------------- */

/*!
\file                   gpiv-postpar.h
\brief                  module for GpivPostPar: parameters for post-processing of PIV data

 */

#ifndef __LIBGPIV_POSTPAR_H__
#define __LIBGPIV_POSTPAR_H__


#include "gpiv-roi.h"


#define GPIV_POSTPAR_KEY                       "POST"        /**< Key of post processing parameters */


/**
 * Operator to transform (manipulate) PIV data
 */
enum GpivOperationManipiv {
    GPIV_FAST_Y = 0,	       /**< returns fast running y-positions of data */
    GPIV_FLIP_X = 1,	       /**< flip in x-direction */
    GPIV_FLIP_Y = 2,	       /**< flip in y-direction */
    GPIV_REVERT = 3,	       /**< reverts array indexes  */
    GPIV_ROT90 = 4,	       /**< rotates over 90 degrees */
    GPIV_ROT180 = 5,	       /**< rotates over 180 degrees */
    GPIV_FILTER_BLOCK = 6,     /**< filters out */
    GPIV_PASS_BLOCK = 7,       /**< passes through */
    GPIV_ADD_XY = 8            /**< adds specified displacement/velocity in x and y direction */
};



/**
 * Differential type
 */
enum GpivDifferentiation {
    GPIV_CENTRAL = 0,          /**< Central differential */
    GPIV_LEAST_SQUARES = 1,    /**< Least square */
    GPIV_RICHARDSON = 2,       /**< Richarchon differential */
    GPIV_CIRCULATION = 3       /**< Circulation method */
};



/**
 * Type of derivative operation
 */
enum GpivOperation {
    GPIV_VORTICITY = 0,        /**< Vorticity */
    GPIV_S_STRAIN = 1,         /**< Shear strain */
    GPIV_N_STRAIN = 2          /**< Normal strain */
};



/**
 * Variables for scaling PIV data
 */
enum GpivScaleType {
    GPIV_SCALE = 0,            /**< Scaling PIV data */
    GPIV_SCALE_INV = 1         /**< Inverse scaling PIV data */
};



typedef struct __GpivPostPar GpivPostPar;

/*!
 * \brief Parameters for post-processing PIV data
 *
 * These parameters are needed by the functions for post-processing of 
 * PIV data, like: data manipulation (fliping, rotating), calculation 
 * of derived (scalar) quantities, statistics etc. 
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
 * Though there are some functions in this module concerning offsetting and 
 * scaling (gpiv_post_scale() for example), the related parameters have been
 * included in __GpivImagePar
 */
struct __GpivPostPar {
    enum GpivOperationManipiv operator_manipiv; /**< operator on PIV data */
    gboolean operator_manipiv__set;	           /**< flag if 
                                   operator_manipiv has been defined */

    gboolean set;              /**< setting of PIV data: enable / disable */
    gboolean set__set;         /**< flag if set has been defined */

    float set_dx;              /**< value of disabled PIV data in horizontal 
                                    (column-wise) direction */
    gboolean set_dx__set;      /**< flag if set_dx has been defined */

    float set_dy;              /**< value of disabled PIV data in vertical 
                                    (row-wise) direction */
    gboolean set_dy__set;      /**< flag if set_dy has been defined */

    GpivRoi *block;            /**< Define a rectangular block for enabling / 
                                    disabling PIV data */
    gboolean block__set;

/*
 * Parameters for spatial average (s-avg)
 */
    int subtract;              /**< data type to subtract from estimators NONE (0), mean (1) Zoff_dx and Zoff_dy (2) */
    gboolean subtract__set;    /**< flag if subtract has been defined */

    float z_off_dx;            /**< offset value for horizontal PIV estimator */
    gboolean z_off_dx__set;    /**< flag if z_off_dx has been defined */

    float z_off_dy;            /**< offset value for vertical PIV estimator */
    gboolean z_off_dy__set;    /**< flag if z_off_dy has been defined */

/*
 * Parameters for vorticity and strain
 */
    enum GpivDifferentiation diff_type;  /**< differential type */
    enum GpivOperation operator_vorstra; /**< specify which derivative type from PIV data to calculate */

    int diff_type__set;             /**< flag if diff_type has been defined */
    gboolean operator_vorstra__set; /**< flag if operator_vorstra has been defined */

/*
 * Parameters for scaling PIV data. Other parameters for scaling are in 
 * __GpivImagePar.
 */
    enum GpivScaleType scale_type;   /**< normal or inverse scaling */
    gboolean scale_type__set; /**< flag if scale_type has been defined */
};




/**
 *      Sets flag for post_par __set.
 *
 *      @param[in] post_par    post-processing parameters
 *      @param[in] flag        boolean to set
 *      @return                void
 */
void
gpiv_post_parameters_set                (GpivPostPar            *post_par,
                                         const gboolean         flag
                                         );



/**
 *      Sets post-processing parameters to default values.
 *
 *      @param[in] force                flag to enforce parameters set to defaults
 *      @param[out] post_par_default    default post-processing parameters
 *      @return                         void
 */
void
gpiv_post_default_parameters            (GpivPostPar            *post_par_default,
                                         const gboolean         force
                                         );



/**
 *      Reads post-processing parameters from localrc, $HOME/.gpivrc and system-wide 
 *      gpiv.conf 
 *
 *      @param[in] localrc      resource filename containing parameter at 
 *                              current directory
 *      @param[in] verbose      prints parameter values when read
 *      @return                 GpivPostPar or NULL on failure
 */
GpivPostPar *
gpiv_post_get_parameters_from_resources (const gchar            *localrc,
                                         const gboolean         verbose
                                         );


/**
 *      Reads all parameters for PIV data validation.
 *
 *      @param[in] fp           pointer to input file
 *      @param[in] print_par    flag to print parameters to stdout
 *      @param[out] post_par    post-processing parameters
 *      @return                 void
 */
void 
gpiv_post_read_parameters               (FILE                   *fp, 
                                         GpivPostPar            *post_par,
                                         const gboolean         verbose
                                         );



/**
 *      Checks if all post-processing parameters have been read. 
 *      If a parameter has not been read, it will be set to post_par_default or 
 *      its hard-coded default value in case post_par_default is NULL.
 *
 *      @param[in] post_par             post-processing parameters
 *      @param[in] post_par_default     default post-processing parameters If NULL, 
 *					library default values are used.
 *      @param[out] post_par            post-processing parameters
 *      @return                         NULL on success or error message on failure
 */
gchar *
gpiv_post_check_parameters_read         (GpivPostPar            *post_par,
                                         const GpivPostPar      *post_par_default
                                         );



/**
 *      Prints parameters to fp.
 *
 *      @param[in] post_par     post-processing parameters
 *      @param[in] fp           pointer to output file
 *      @return                 void
 */
void 
gpiv_post_print_parameters              (FILE                   *fp,
                                         const GpivPostPar      *post_par
                                         );



/**
 *      Reads post parameters from hdf5 data file.
 *
 *      @param[in] fname       file name of data file
 *      @return                GpivPostPar or NULL on failure
 */
GpivPostPar *
gpiv_post_fread_hdf5_parameters         (const gchar        *fname
                                         );



/**
 *      Writes post parameters to an existing hdf5 data file.
 *
 *      @param[in] fname       file name of data file
 *      @param[in] post_par    post-processing parameters
 *      @return                NULL on success or error message on failure
 */
gchar *
gpiv_post_fwrite_hdf5_parameters        (const gchar            *fname, 
                                         const GpivPostPar      *post_par 
                                         );



#endif /* __LIBGPIV_POSTPAR_H_ */
