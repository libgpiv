/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-----------------------------------------------------------------------------/*

typedef struct __GpivRoi GpivRoi;

/*!
 * \brief Region Of Interest
 */

#ifndef __LIBGPIV_ROI_H__
#define __LIBGPIV_ROI_H__


typedef struct __GpivRoi GpivRoi;

/*!
 * \brief Defines Region of Interest
 *
 * Region of interest is defined by two coordinates of a rectangular square
 */
struct __GpivRoi {
    float x_1;			/**< lowest x value */
    float y_1;			/**< lowest y value */
    float x_2;			/**< highest x value */
    float y_2;			/**< highest x value */
};


#endif /* __LIBGPIV_ROI_H__ */
