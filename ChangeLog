2014-9-29 B. Ouharrou, G. Terre
	* Improved Gauss weighting for Interrogation Areas.

2013-02-13 Ryan Keedy
        * bugrepair SNR

2012-12-13 Matthias Fricke
	* Improved optimizing using OMP: fftw is not thread save
	
2011-2-4 Gerber Van der Graaf
	* added gpiv_(f)read_bindata and gpiv_(f)write_bindata

2009-03-16  Thomas Huxhorn
	* lib/io.c: added support for ppm image format.

2008-01-12 Thomas Huxhorn
	* lib/cam.c, lib/cam_par.c: bug repairs.
	
2008-22-11 Gerber Van der Graaf
	* build libgpiv_mpi libso name
	* Cleanup public variables
	* Changed guint to gint for variables used in for () loops in upstream
	* Control doxygen with configure options
	
2008-4-6 Gerber Van der Graaf
	* lib/piv.c: parallelised for distributed memory systems using MPI
	* lib/piv.c, lib/imgproc_deform.c: parallelised for shared systems memory 
	using OMP
	
2008-23-9 Gerber Van der Graaf
	* lib/io.c: Repaired reading from stdin.
	
2008-5-5 Gerber Van der Graaf
	* Removed semicolon (:) from gpiv.conf as this seems to be more convenient
	* include/gpiv/gpiv-io.h .h5 and .H5 extension for hdf files 
	(previously .gpi/.GPI)
	
2008-7-4 Gerber Van der Graaf
	* Changed __GpivPivPar: int_size_2 to int_size_i (initital) and 
	int_size_1 to int_size_f (final) in include/gpiv/gpiv-piv_par.h
	* Moved some GpivImageProcPar members to a new structure GpivGenPar
	containing general parameters, using key GEN in configuration.
	* Repaired gpiv_*_check_parameters_read; added *__set = TRUE to
	function properly without configuration

2008-27-3 Gerber Van der Graaf
	* Removed NONREQUIRED macro in lib/io_png.c
	
2007-10-12 Gerber Van der Graaf
	* Debugged

2007-20-11 Gerber Van der Graaf
	* Kafka: reduction and simplyfication of the API
	* New: Struct Image
	* Improved: all parameter and data structures. Extended with comment and
	third dimension components (for future use).
	* PyGpiv: Python module to use library in scripts, wrapped by SWIG, 
	in separate packet.
	* 'eval': files and API replaced by 'piv'
	* _speed.c/.h: included in piv.c/.h
	
2007-7-6 Gerber Van der Graaf
	* Parameter keys of identic format (process key entirely capital letters,
	each parameter starts with a capital letter, word separation with '_', 
	key names are centralized by macro's.

2007-10-5 Gerber Van der Graaf
	* renamed all parameters *_logic to *__set and functions *parameter_logic
	to *parameter_set

2007-20-4 Gerber Van der Graaf
	* Moved to fftw3 library
	* Disabled some memory leaks

2007-30-3 Gerber Van der Graaf
	* include: headers adapted for documentation with Doxygen

2007-22-3 Gerber Van der Graaf
	* libgpiv_0.4.0_patch1: lib/imgproc debugged

2007-21-2 Gerber Van der Graaf
	* libgpiv_0.4.0_patch1: new: spof filter, gauss weighting on interr. area.

2006-7-11 Gerber Van der Graaf
	* assert substituted by g_return_(val_)if_fail in public functions and
	by g_assert in private ones

2006-7-11 Gerber Van der Graaf
	* Support for Portable Network Graphics image format (native) and for
	tif, gif, pgm, and bmp by converting to png with netbpm on the fly.
	* lib/io_raw.c, lib/io_png.c, lib/io_hdf.c added

2006-23-10 Gerber Van der Graaf
	* lib/io_png.c: PNG (Portable Network Graphics) as native image format

2006-11-6 Gerber Van der Graaf
	* include/gpiv/dac.h substituted by include/gpiv/cam.h, include/trig.h
	* lib/dac.c substituted by lib/cam.c, lib/trig.c
	* lib/dac_par.c substituted by lib/cam_par.c, lib/trig_par.c
	* DISABLE_DAC substituted by ENABLE_CAM, ENABLE_TRIG
	* GPIV_GPIVPOSTPAR_DEFAULT__* substituted by GPIV_POSTPAR_DEFAULT__*

